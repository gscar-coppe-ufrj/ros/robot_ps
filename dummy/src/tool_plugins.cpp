// tool_plugins.cpp



#include <pluginlib/class_list_macros.h>
#include <dummy/A/tool_a.h>
#include <dummy/A/tool_a1.h>
#include <dummy/A/tool_a2.h>
#include <dummy/B/tool_b.h>
#include <dummy/B/tool_b1.h>
#include <dummy/B/tool_b2.h>


PLUGINLIB_EXPORT_CLASS(dummy::ToolA, robot_ps::Tool)
PLUGINLIB_EXPORT_CLASS(dummy::ToolA1, robot_ps::Tool)
PLUGINLIB_EXPORT_CLASS(dummy::ToolA2, robot_ps::Tool)
PLUGINLIB_EXPORT_CLASS(dummy::ToolB, robot_ps::Tool)
PLUGINLIB_EXPORT_CLASS(dummy::ToolB1, robot_ps::Tool)
PLUGINLIB_EXPORT_CLASS(dummy::ToolB2, robot_ps::Tool)
