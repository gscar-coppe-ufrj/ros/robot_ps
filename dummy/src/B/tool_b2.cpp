// tool_a2.cpp



#include <iostream>
#include <dummy/B/tool_b2.h>
#include <dummy/B/component_b.h>
#include <dummy/B/component_b1.h>
#include <dummy/B/component_b2.h>



void dummy::ToolB2::TreatAddComponentB(ComponentB* c)
{
    std::cout << this << " " << c << " I'm ToolB2 adding ComponentB" << std::endl;
}


void dummy::ToolB2::TreatRemoveComponentB(ComponentB *c)
{
    std::cout << this << " " << c << " I'm ToolB2 removing ComponentB" << std::endl;
    std::cout << c->trash << " " <<  c->newTrash << std::endl;
}


void dummy::ToolB2::TreatAddComponentB1(ComponentB1* c)
{
    std::cout << this << " " << c << " I'm ToolB2 adding ComponentB1" << std::endl;
}


void dummy::ToolB2::TreatRemoveComponentB1(ComponentB1 *c)
{
    std::cout << this << " " << c << " I'm ToolB2 removing ComponentB1" << std::endl;
    std::cout << c->trash << " " <<  c->newTrash << std::endl;
}


void dummy::ToolB2::TreatAddComponentB2(ComponentB2* c)
{
    std::cout << this << " " << c << " I'm ToolB2 adding ComponentB2" << std::endl;
}


void dummy::ToolB2::TreatRemoveComponentB2(ComponentB2 *c)
{
    std::cout << this << " " << c << " I'm ToolB2 removing ComponentB2" << std::endl;
    std::cout << c->trash << " " <<  c->newTrash << std::endl;
}
