// component_b.cpp



#include <iostream>
#include <dummy/B/component_b.h>



void dummy::ComponentB::TreatAddToolB(ToolB *t)
{
    std::cout << this << " " << t << " I'm ComponentB adding ToolB" << std::endl;
}


void dummy::ComponentB::TreatRemoveToolB(ToolB *t)
{
    std::cout << this << " " << t << " I'm ComponentB removing ToolB" << std::endl;
}


void dummy::ComponentB::onInit()
{
    std::cout << "initializing ComponentB" << std::endl;
}


void dummy::ComponentB::FindTools()
{
    AddMeToTools<ComponentB, ToolB>(&ComponentB::TreatAddToolB, &ComponentB::TreatRemoveToolB);
}
