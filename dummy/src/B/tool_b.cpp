// tool_b.cpp



#include <iostream>
#include <dummy/B/tool_b.h>
#include <dummy/B/component_b.h>
#include <dummy/B/component_b1.h>
#include <dummy/B/component_b2.h>



void dummy::ToolB::TreatAddComponentB(ComponentB* c)
{
    std::cout << this << " " << c << " I'm ToolB adding ComponentB" << std::endl;
}


void dummy::ToolB::TreatRemoveComponentB(ComponentB *c)
{
    std::cout << this << " " << c << " I'm ToolB removing ComponentB" << std::endl;
    std::cout << c->trash << " " <<  c->newTrash << std::endl;
}


void dummy::ToolB::TreatAddComponentB1(ComponentB1* c)
{
    std::cout << this << " " << c << " I'm ToolB adding ComponentB1" << std::endl;
}


void dummy::ToolB::TreatRemoveComponentB1(ComponentB1 *c)
{
    std::cout << this << " " << c << " I'm ToolB removing ComponentB1" << std::endl;
    std::cout << c->trash << " " <<  c->newTrash << std::endl;
}


void dummy::ToolB::TreatAddComponentB2(ComponentB2* c)
{
    std::cout << this << " " << c << " I'm ToolB adding ComponentB2" << std::endl;
}


void dummy::ToolB::TreatRemoveComponentB2(ComponentB2 *c)
{
    std::cout << this << " " << c << " I'm ToolB removing ComponentB2" << std::endl;
    std::cout << c->trash << " " <<  c->newTrash << std::endl;
}


void dummy::ToolB::FindComponents()
{
    AddMeToComponents<ToolB, ComponentB>(&ToolB::TreatAddComponentB, &ToolB::TreatRemoveComponentB);
    AddMeToComponents<ToolB, ComponentB1>(&ToolB::TreatAddComponentB1, &ToolB::TreatRemoveComponentB1);
    AddMeToComponents<ToolB, ComponentB2>(&ToolB::TreatAddComponentB2, &ToolB::TreatRemoveComponentB2);
}
