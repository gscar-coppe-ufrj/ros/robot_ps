// component_b2.cpp



#include <iostream>
#include <dummy/B/component_b2.h>



void dummy::ComponentB2::TreatAddAnotherListToolB(ToolB *t)
{
    std::cout << this << " " << t << " I'm ComponentB2 adding AnotherListToolB" << std::endl;
}


void dummy::ComponentB2::TreatRemoveAnotherListToolB(ToolB *t)
{
    std::cout << this << " " << t << " I'm ComponentB2 removing AnotherListToolB" << std::endl;
}


void dummy::ComponentB2::onInit()
{
    std::cout << "initializing ComponentB2" << std::endl;
}


void dummy::ComponentB2::FindTools()
{
    ComponentB::FindTools();
    AddMeToTools<ComponentB2, ToolB>(&ComponentB2::TreatAddAnotherListToolB, &ComponentB2::TreatRemoveAnotherListToolB);
}
