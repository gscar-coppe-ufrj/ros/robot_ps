// component_b1.cpp



#include <iostream>
#include <dummy/B/component_b1.h>



void dummy::ComponentB1::TreatAddAnotherListToolB(ToolB *t)
{
    std::cout << this << " " << t << " I'm ComponentB1 adding AnotherListToolB" << std::endl;
}


void dummy::ComponentB1::TreatRemoveAnotherListToolB(ToolB *t)
{
    std::cout << this << " " << t << " I'm ComponentB1 removing AnotherListToolB" << std::endl;
}


void dummy::ComponentB1::onInit()
{
    std::cout << "initializing ComponentB1" << std::endl;
}


void dummy::ComponentB1::FindTools()
{
    ComponentB::FindTools();
    AddMeToTools<ComponentB1, ToolB>(&ComponentB1::TreatAddAnotherListToolB, &ComponentB1::TreatRemoveAnotherListToolB);
}
