// tool_a1.cpp



#include <iostream>
#include <dummy/B/tool_b1.h>
#include <dummy/B/component_b.h>
#include <dummy/B/component_b1.h>
#include <dummy/B/component_b2.h>



void dummy::ToolB1::TreatAddComponentB(ComponentB* c)
{
    std::cout << this << " " << c << " I'm ToolB1 adding ComponentB" << std::endl;
}


void dummy::ToolB1::TreatRemoveComponentB(ComponentB *c)
{
    std::cout << this << " " << c << " I'm ToolB1 removing ComponentB" << std::endl;
    std::cout << c->trash << " " <<  c->newTrash << std::endl;
}


void dummy::ToolB1::TreatAddComponentB1(ComponentB1* c)
{
    std::cout << this << " " << c << " I'm ToolB1 adding ComponentB1" << std::endl;
}


void dummy::ToolB1::TreatRemoveComponentB1(ComponentB1 *c)
{
    std::cout << this << " " << c << " I'm ToolB1 removing ComponentB1" << std::endl;
    std::cout << c->trash << " " <<  c->newTrash << std::endl;
}


void dummy::ToolB1::TreatAddComponentB2(ComponentB2* c)
{
    std::cout << this << " " << c << " I'm ToolB1 adding ComponentB2" << std::endl;
}


void dummy::ToolB1::TreatRemoveComponentB2(ComponentB2 *c)
{
    std::cout << this << " " << c << " I'm ToolB1 removing ComponentB2" << std::endl;
    std::cout << c->trash << " " <<  c->newTrash << std::endl;
}
