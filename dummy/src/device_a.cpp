// device_a.cpp



#include <iostream>
#include <dummy/device_a.h>



dummy::DeviceA::DeviceA()
{
    current = new device::VariableTemplate<double>("Current", "aaa", true, device::Variable::typeDouble);
    current->SetValue(12.6);
    variables.push_back(current);

    auto group1 = new device::VariableGroup("group1", "", true);
    variables.push_back(group1);

    tension = new device::VariableTemplate<double>("Tension", "bbb", true, device::Variable::typeDouble);
    tension->SetValue(15.98);
    variables.push_back(tension);

    onOff2 = new device::VariableBool("On/Off2", "", true);
    onOff2->SetTrueString1("On");
    onOff2->SetTrueString2("Turn On");
    onOff2->SetFalseString1("Off");
    onOff2->SetFalseString2("Turn Off");
    onOff2->SetState(false);
    variables.push_back(onOff2);
    onOffVariables.push_back(onOff2);

    auto group2 = new device::VariableGroup("group2", "", true);
    variables.push_back(group2);

    options = new device::VariableCombo("Options", "bg", true);
    options->AddElement("Option 1");
    options->AddElement("Option 2");
    options->AddElement("Option 3");
    variables.push_back(options);
}



void dummy::DeviceA::UpdateVariable(device::Variable* variable)
{
    bool found = true;

    if (variable == deviceOnOff)
        deviceOnOff->SetState(deviceOnOff->GetNewState());
    else if (variable == current)
        current->SetValue(std::get<0>(current->GetNewValue()));
    else if (variable == tension)
        tension->SetValue(std::get<0>(tension->GetNewValue()));
    else if (variable == onOff2)
        onOff2->SetState(onOff2->GetNewState());
    else if (variable == options)
        options->SelectElement(options->GetNewSelection());
    else
        found = false;

    if (found)
        variable->Update();
    else
        DeviceOnOff::UpdateVariable(variable);
}


void dummy::DeviceA::UpdateAllVariables()
{
    deviceOnOff->SetState(deviceOnOff->GetNewState());
    deviceOnOff->Update();
    current->SetValue(std::get<0>(current->GetNewValue()));
    current->Update();
    tension->SetValue(std::get<0>(tension->GetNewValue()));
    tension->Update();
    onOff2->SetState(onOff2->GetNewState());
    onOff2->Update();
    options->SelectElement(options->GetNewSelection());
    options->Update();

    DeviceOnOff::UpdateAllVariables();
}

void dummy::DeviceA::onInit()
{
    std::cout << "I'm dummy::DeviceA" << std::endl;
}
