// tool_a2.cpp



#include <iostream>
#include <dummy/A/tool_a2.h>



void dummy::ToolA2::TreatAddComponentA(ComponentA *c)
{
    std::cout << this << " " << c << " I'm ToolA2 adding ComponentA" << std::endl;
}


void dummy::ToolA2::TreatAddComponentA2(ComponentA2 *c)
{
    std::cout << this << " " << c << " I'm ToolA2 adding ComponentA2" << std::endl;
}


void dummy::ToolA2::TreatRemoveComponentA2(ComponentA2 *c)
{
    std::cout << this << " " << c << " I'm ToolA2 removing ComponentA2" << std::endl;
}


void dummy::ToolA2::FindComponents()
{
    ToolA::FindComponents();
    AddMeToComponents<ToolA2, ComponentA2>(&ToolA2::TreatAddComponentA2, &ToolA2::TreatRemoveComponentA2);
}

