// tool_a.cpp



#include <iostream>
#include <dummy/A/tool_a.h>



void dummy::ToolA::TreatAddComponentA(ComponentA *c)
{
    std::cout << this << " " << c << " I'm ToolA adding ComponentA" << std::endl;
}


void dummy::ToolA::TreatRemoveComponentA(ComponentA *c)
{
    std::cout << this << " " << c << " I'm ToolA removing ComponentA" << std::endl;
}


void dummy::ToolA::FindComponents()
{
    AddMeToComponents<ToolA, ComponentA>(&ToolA::TreatAddComponentA, &ToolA::TreatRemoveComponentA);
}

