// tool_a1.cpp



#include <iostream>
#include <dummy/A/tool_a1.h>



void dummy::ToolA1::TreatAddComponentA(ComponentA *c)
{
    std::cout << this << " " << c << " I'm ToolA1 adding ComponentA" << std::endl;
}


void dummy::ToolA1::TreatAddComponentA1(ComponentA1 *c)
{
    std::cout << this << " " << c << " I'm ToolA1 adding ComponentA1" << std::endl;
}


void dummy::ToolA1::TreatRemoveComponentA1(ComponentA1 *c)
{
    std::cout << this << " " << c << " I'm ToolA1 removing ComponentA1" << std::endl;
}


void dummy::ToolA1::FindComponents()
{
    ToolA::FindComponents();
    AddMeToComponents<ToolA1, ComponentA1>(&ToolA1::TreatAddComponentA1, &ToolA1::TreatRemoveComponentA1);
}
