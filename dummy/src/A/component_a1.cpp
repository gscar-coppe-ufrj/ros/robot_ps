// component_a1.cpp



#include <iostream>
#include <dummy/A/component_a1.h>



void dummy::ComponentA1::TreatAddToolA(ToolA *t)
{
    std::cout << this << " " << t << " I'm ComponentA1 adding ToolA" << std::endl;
}


void dummy::ComponentA1::TreatRemoveToolA(ToolA *t)
{
    std::cout << this << " " << t << " I'm ComponentA1 removing ToolA" << std::endl;
}


void dummy::ComponentA1::TreatAddToolA1(ToolA1 *t)
{
    std::cout << this << " " << t << " I'm ComponentA1 adding ToolA1" << std::endl;
}


void dummy::ComponentA1::TreatRemoveToolA1(ToolA1 *t)
{
    std::cout << this << " " << t << " I'm ComponentA1 removing ToolA1" << std::endl;
}


void dummy::ComponentA1::onInit()
{
    std::cout << "initializing ComponentA1" << std::endl;
}


void dummy::ComponentA1::FindTools()
{
    ComponentA::FindTools();
    AddMeToTools<ComponentA1, ToolA1>(&ComponentA1::TreatAddToolA1, &ComponentA1::TreatRemoveToolA1);
}
