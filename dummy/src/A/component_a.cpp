// component_a.cpp



#include <iostream>
#include <dummy/A/component_a.h>



void dummy::ComponentA::TreatAddToolA(ToolA *t)
{
    std::cout << this << " " << t << " I'm ComponentA adding ToolA" << std::endl;
}


void dummy::ComponentA::TreatRemoveToolA(ToolA *t)
{
    std::cout << this << " " << t << " I'm ComponentA removing ToolA" << std::endl;
}


void dummy::ComponentA::onInit()
{
    std::cout << "initializing ComponentA" << std::endl;
}


void dummy::ComponentA::FindTools()
{
    AddMeToTools<ComponentA, ToolA>(&ComponentA::TreatAddToolA, &ComponentA::TreatRemoveToolA);
}
