// component_a2.cpp



#include <iostream>
#include <dummy/A/component_a2.h>



void dummy::ComponentA2::TreatAddToolA2(ToolA2 *t)
{
    std::cout << this << " " << t << " I'm ComponentA2 adding ToolA2" << std::endl;
}


void dummy::ComponentA2::TreatRemoveToolA2(ToolA2 *t)
{
    std::cout << this << " " << t << " I'm ComponentA2 removing ToolA2" << std::endl;
}


void dummy::ComponentA2::onInit()
{
    std::cout << "initializing ComponentA2" << std::endl;
}


void dummy::ComponentA2::FindTools()
{
    ComponentA::FindTools();
    AddMeToTools<ComponentA2, ToolA2>(&ComponentA2::TreatAddToolA2, &ComponentA2::TreatRemoveToolA2);
}
