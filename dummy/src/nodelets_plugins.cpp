// nodelets_plugins.cpp



#include <pluginlib/class_list_macros.h>
#include <dummy/A/component_a.h>
#include <dummy/A/component_a1.h>
#include <dummy/A/component_a2.h>
#include <dummy/B/component_b.h>
#include <dummy/B/component_b1.h>
#include <dummy/B/component_b2.h>
#include <dummy/device_a.h>



PLUGINLIB_EXPORT_CLASS(dummy::ComponentA, nodelet::Nodelet)
PLUGINLIB_EXPORT_CLASS(dummy::ComponentA1, nodelet::Nodelet)
PLUGINLIB_EXPORT_CLASS(dummy::ComponentA2, nodelet::Nodelet)
PLUGINLIB_EXPORT_CLASS(dummy::ComponentB, nodelet::Nodelet)
PLUGINLIB_EXPORT_CLASS(dummy::ComponentB1, nodelet::Nodelet)
PLUGINLIB_EXPORT_CLASS(dummy::ComponentB2, nodelet::Nodelet)
PLUGINLIB_EXPORT_CLASS(dummy::DeviceA, nodelet::Nodelet)
