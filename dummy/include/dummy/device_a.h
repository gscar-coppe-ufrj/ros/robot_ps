// device_a.h



#ifndef DUMMY_DEVICE_A_H
#define DUMMY_DEVICE_A_H



#include <custom_loader/custom_nodelet.h>
#include <device/device_on_off.h>
#include <device/variables/variable_template.h>
#include <device/variables/variable_bool.h>
#include <device/variables/variable_combo.h>
#include <device/variables/variable_group.h>



namespace dummy {



class DeviceA : public device::DeviceOnOff, public custom_loader::CustomNodelet
{
    device::VariableTemplate<double>* current;
    device::VariableTemplate<double>* tension;
    device::VariableBool* onOff2;
    device::VariableCombo* options;

public:
    DeviceA();

    void onInit();

    virtual void UpdateVariable(device::Variable* variable);
    virtual void UpdateAllVariables();
};



}



#endif // DEVICE_A_H
