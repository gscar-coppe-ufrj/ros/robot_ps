// tool_a.h



#ifndef DUMMY_TOOL_A
#define DUMMY_TOOL_A



#include <QWidget>
#include <robot_ps_core/tool.h>
#include "component_a.h"



namespace dummy {



class ComponentA;



class ToolA : public QWidget, public robot_ps::Tool
{
protected:
    virtual void TreatAddComponentA(ComponentA* c);
    virtual void TreatRemoveComponentA(ComponentA* c);

public:
    virtual void FindComponents();
};



}



#endif // TOOL_A
