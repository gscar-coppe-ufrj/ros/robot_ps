// component_a2.h



#ifndef DUMMY_COMPONENT_A2
#define DUMMY_COMPONENT_A2



#include "component_a.h"
#include "tool_a2.h"



namespace dummy {



class ToolA2;



class ComponentA2 : public ComponentA
{
protected:
    void TreatAddToolA2(ToolA2* t);
    void TreatRemoveToolA2(ToolA2* t);

public:
    virtual void onInit();
    void FindTools();
};



}



#endif // COMPONENT_A2
