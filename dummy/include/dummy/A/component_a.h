// component_a.h



#ifndef DUMMY_COMPONENT_A
#define DUMMY_COMPONENT_A



#include <custom_loader/custom_nodelet.h>
#include <robot_ps_core/component.h>
#include "tool_a.h"



namespace dummy {



class ToolA;



class ComponentA : public robot_ps::Component, public custom_loader::CustomNodelet
{
protected:
    virtual void TreatAddToolA(ToolA* t);
    virtual void TreatRemoveToolA(ToolA* t);

public:
    virtual void onInit();
    void FindTools();
};



}



#endif // COMPONENT_A
