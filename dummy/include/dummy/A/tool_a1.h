// tool_a1.h



#ifndef DUMMY_TOOL_A1
#define DUMMY_TOOL_A1



#include "tool_a.h"
#include "component_a1.h"



namespace dummy {



class ComponentA1;



class ToolA1 : public ToolA
{
protected:
    void TreatAddComponentA(ComponentA* c);
    void TreatAddComponentA1(ComponentA1* c);
    void TreatRemoveComponentA1(ComponentA1* c);

public:
    virtual void FindComponents();
};



}



#endif // TOOL_A1
