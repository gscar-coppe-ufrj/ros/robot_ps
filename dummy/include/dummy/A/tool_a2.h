// tool_a2.h



#ifndef DUMMY_TOOL_A2
#define DUMMY_TOOL_A2



#include "tool_a.h"
#include "component_a2.h"



namespace dummy {



class ComponentA2;



class ToolA2 : public ToolA
{
protected:
    void TreatAddComponentA(ComponentA* c);
    void TreatAddComponentA2(ComponentA2* c);
    void TreatRemoveComponentA2(ComponentA2* c);

public:
    virtual void FindComponents();
};



}



#endif // TOOL_A1
