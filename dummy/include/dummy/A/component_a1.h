// component_a1.h



#ifndef DUMMY_COMPONENT_A1
#define DUMMY_COMPONENT_A1



#include "component_a.h"
#include "tool_a1.h"



namespace dummy {



class ToolA1;



class ComponentA1 : public ComponentA
{
protected:
    void TreatAddToolA(ToolA* t);
    void TreatRemoveToolA(ToolA* t);
    void TreatAddToolA1(ToolA1* t);
    void TreatRemoveToolA1(ToolA1* t);

public:
    virtual void onInit();
    void FindTools();
};



}



#endif // COMPONENT_A1
