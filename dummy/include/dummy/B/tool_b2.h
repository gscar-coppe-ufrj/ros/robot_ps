// tool_b2.h



#ifndef DUMMY_TOOL_B2
#define DUMMY_TOOL_B2



#include "tool_b.h"



namespace dummy {



class ToolB2 : public ToolB
{
protected:
    virtual void TreatAddComponentB(ComponentB* c);
    virtual void TreatRemoveComponentB(ComponentB* c);
    virtual void TreatAddComponentB1(ComponentB1* c);
    virtual void TreatRemoveComponentB1(ComponentB1* c);
    virtual void TreatAddComponentB2(ComponentB2* c);
    virtual void TreatRemoveComponentB2(ComponentB2* c);
};



}



#endif // TOOL_B2
