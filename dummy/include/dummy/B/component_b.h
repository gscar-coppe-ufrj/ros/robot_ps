// component_b.h



#ifndef DUMMY_COMPONENT_B
#define DUMMY_COMPONENT_B



#include <custom_loader/custom_nodelet.h>
#include <robot_ps_core/component.h>
#include "tool_b.h"



namespace dummy {



class ToolB;



class ComponentBBase
{
public:
    int trash;
    ComponentBBase() : trash(100) {}
};



class ComponentB : public ComponentBBase, public robot_ps::Component, public custom_loader::CustomNodelet
{
protected:
    virtual void TreatAddToolB(ToolB* t);
    virtual void TreatRemoveToolB(ToolB* t);

public:
    int newTrash;

    ComponentB() : newTrash(90) {}

    virtual void onInit();
    void FindTools();
};



}



#endif // COMPONENT_B
