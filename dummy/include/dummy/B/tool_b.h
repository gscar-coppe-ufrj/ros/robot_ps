// tool_b.h



#ifndef DUMMY_TOOL_B
#define DUMMY_TOOL_B



#include <QWidget>
#include <robot_ps_core/tool.h>



namespace dummy {



class ComponentB;
class ComponentB1;
class ComponentB2;



class ToolB : public QWidget, public robot_ps::Tool
{
protected:
    virtual void TreatAddComponentB(ComponentB* c);
    virtual void TreatRemoveComponentB(ComponentB* c);
    virtual void TreatAddComponentB1(ComponentB1* c);
    virtual void TreatRemoveComponentB1(ComponentB1* c);
    virtual void TreatAddComponentB2(ComponentB2* c);
    virtual void TreatRemoveComponentB2(ComponentB2* c);

public:
    virtual void FindComponents();
};



}



#endif // TOOL_A
