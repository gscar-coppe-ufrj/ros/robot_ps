// component_b1.h



#ifndef DUMMY_COMPONENT_B1
#define DUMMY_COMPONENT_B1



#include "component_b.h"
#include "tool_b.h"



namespace dummy {



class ToolB;



class ComponentB1 : public ComponentB
{
protected:
    void TreatAddAnotherListToolB(ToolB* t);
    void TreatRemoveAnotherListToolB(ToolB* t);

public:
    virtual void onInit();
    void FindTools();
};



}



#endif // COMPONENT_B1
