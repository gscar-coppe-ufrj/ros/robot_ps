// component_b2.h



#ifndef DUMMY_COMPONENT_B2
#define DUMMY_COMPONENT_B2



#include "component_b.h"



namespace dummy {



class ToolB;



class ComponentB2 : public ComponentB
{
protected:
    void TreatAddAnotherListToolB(ToolB* t);
    void TreatRemoveAnotherListToolB(ToolB* t);

public:
    virtual void onInit();
    void FindTools();
};



}



#endif // COMPONENT_B2
