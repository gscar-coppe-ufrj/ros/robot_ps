// linux_dev_device.h



#ifndef LINUX_DEV_DEVICE_H
#define LINUX_DEV_DEVICE_H



#include <custom_loader/custom_nodelet.h>
#include <device/device.h>
#include <device/variables/variable_multi_select_list.h>
#include <device/variables/variable_info_list.h>
#include <linux_devs/linux_dev_user.h>



namespace linux_devs_device {



class LinuxDevDevice : public linux_devs::LinuxDevUser, public device::Device, public custom_loader::CustomNodelet
{
    device::VariableMultiSelectList* devSelection;
    device::VariableInfoList* devInfo;

protected:
    virtual void DevicesList(const std::vector<std::string> & list);
    virtual void SelectedDevicesList(const std::vector<std::string> & list);

public:
    LinuxDevDevice();

    void onInit();

    virtual void UpdateVariable(device::Variable* variable);
    virtual void UpdateAllVariables();
};



}



#endif // LINUX_DEV_DEVICE_H
