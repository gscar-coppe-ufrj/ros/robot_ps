// linux_dev_device.cpp



#include <iostream>
#include <linux_devs_device/linux_dev_device.h>

using namespace std;



linux_devs_device::LinuxDevDevice::LinuxDevDevice()
{
    devSelection = new device::VariableMultiSelectList("dev", "", true);
    variables.push_back(devSelection);

    devInfo = new device::VariableInfoList("info", "", true);
    variables.push_back(devInfo);
}


void linux_devs_device::LinuxDevDevice::UpdateVariable(device::Variable* variable)
{
    bool found = true;

    if (variable == devSelection)
        SelectDevices(devSelection->newElementSelection);
    else if (variable == devInfo)
    {
        map<string, string> temp;
        if (GetDeviceInfo(devInfo->elementSelected, temp))
            devInfo->info = temp;
    }
    else
        found = false;

    if (found)
        variable->Update();
    else
        Device::UpdateVariable(variable);
}


void linux_devs_device::LinuxDevDevice::UpdateAllVariables()
{
    SelectDevices(devSelection->newElementSelection);
    devSelection->Update();

    Device::UpdateAllVariables();
}


void linux_devs_device::LinuxDevDevice::onInit()
{
    ros::NodeHandle & privateNH = getPrivateNodeHandle();
    std::string senderName;
    privateNH.param("sender_name", senderName, getName());

    ConfigLinuxDev(this->getMTNodeHandle(), senderName);
}


void linux_devs_device::LinuxDevDevice::DevicesList(const std::vector<std::string> & list)
{
    devSelection->elements = list;
    devSelection->Update();

    devInfo->elements = list;
    devInfo->Update();
}


void linux_devs_device::LinuxDevDevice::SelectedDevicesList(const std::vector<std::string> & list)
{
    devSelection->selectedElements = list;
    devSelection->Update();
}
