// client.h



#ifndef MISSION_CONTROL_CLIENT_CLIENT_H
#define MISSION_CONTROL_CLIENT_CLIENT_H


#include <robot_ps_core/component.h>
#include "mission_classes.h"
#include "editor.h"
#include "scheduler.h"
#include "viewer.h"



namespace mission_control_client {



namespace ShiftOrders
{
    enum ShiftOrder
    {
        Up = true,
        Down = false
    };
}
typedef ShiftOrders::ShiftOrder ShiftOrder;


namespace BufferFlags
{
    enum BufferFlag
    {
        Play,
        Pause,
        Stop
    };
}
typedef BufferFlags::BufferFlag BufferFlag;


namespace BufferMessageTypes
{
    enum BufferMessageType
    {
        Add,
        Remove,
        Execute,
        Move
    };
}
typedef BufferMessageTypes::BufferMessageType BufferMessageType;



class Editor;
class Scheduler;
class Viewer;



class Client : public robot_ps::Component
{
protected:
    std::vector<MissionDefinition> missionDefinitions;
    std::vector<MissionPlan> missionPlans;
    std::vector<MissionPlanOnBuffer> missionBuffer;
    std::string currentMissionPlan;
    std::vector<ScheduleInfo> schedules;

    boost::mutex mut;

    virtual void TreatAddEditor(Editor* e){}
    virtual void TreatRemoveEditor(Editor* e){}
    virtual void TreatAddScheduler(Scheduler* s){}
    virtual void TreatRemoveScheduler(Scheduler* s){}
    virtual void TreatAddViewer(Viewer* v){}
    virtual void TreatRemoveViewer(Viewer* v){}

    void LoadDefinitions(std::string file);

public:
    Client();

    virtual void FindTools()
    {
        AddMeToTools<Client, Editor>(&Client::TreatAddEditor, &Client::TreatRemoveEditor);
        AddMeToTools<Client, Scheduler>(&Client::TreatAddScheduler, &Client::TreatRemoveScheduler);
        AddMeToTools<Client, Viewer>(&Client::TreatAddViewer, &Client::TreatRemoveViewer);
    }

    std::vector<MissionDefinition> GetMissionDefinitions();
    virtual bool SaveMissionPlan(MissionPlan & missionPlan) = 0;
    virtual bool DeleteMissionPlan(std::string name) = 0;
    virtual std::vector<std::string> GetMissionPlanList();
    virtual MissionPlan GetMissionPlan(std::string name);

    std::string GetCurrentMissionPlan();
    virtual std::vector<MissionPlanOnBuffer> GetBuffer();
    virtual void CancelCurrentMissionOnBuffer() = 0;
    virtual void MissionBufferPlay(bool play) = 0;
    virtual void PlayMissionOnBuffer(unsigned int position) = 0;
    virtual void MissionMovementOnBuffer(unsigned int position, int movement) = 0;
    virtual void MissionRemoveOnBuffer(unsigned int position) = 0;
    virtual void MissionAddOnBuffer(unsigned int position, std::string missionName) = 0;

    std::vector<ScheduleInfo> GetScheduleList();
    virtual void AddSchedule(ScheduleInfo schedule) = 0;
    virtual void RemoveSchedule(unsigned int id) = 0;

    virtual ~Client();
};



}



#endif
