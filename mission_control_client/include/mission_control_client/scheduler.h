// scheduler.h



#ifndef MISSION_CONTROL_CLIENT_SCHEDULER_H
#define MISSION_CONTROL_CLIENT_SCHEDULER_H



#include <robot_ps_core/tool.h>
#include "mission_classes.h"
#include "client.h"



namespace mission_control_client {



class Client;



class Scheduler : public robot_ps::Tool
{
protected:
    Client* currentClient;

    virtual void TreatAddClient(Client* c){}
    virtual void TreatRemoveClient(Client* c){}

public:
    Scheduler();

    virtual void FindComponents()
    {
        AddMeToComponents<Scheduler, Client>(&Scheduler::TreatAddClient, &Scheduler::TreatRemoveClient);
    }

    virtual void UpdateMissionPlanList(Client* c) = 0;
    virtual void UpdateScheduleList(Client* c) = 0;
};



}



#endif
