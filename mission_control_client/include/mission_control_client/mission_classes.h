// mission_classes.h



#ifndef MISSION_CONTROL_CLIENT_MISSION_CLASSES_H
#define MISSION_CONTROL_CLIENT_MISSION_CLASSES_H



#include <string>
#include <vector>



namespace mission_control_client {



class Parameter
{
public:
    std::string key;

    virtual ~Parameter(){};
};


class ParameterBool : public Parameter
{
public:
};


class ParameterInt : public Parameter
{
public:
};


class ParameterDouble : public Parameter
{
public:
};


class ParameterString : public Parameter
{
public:
};


class ParameterComboInt : public Parameter
{
public:
    std::vector<std::string> optionsValues;
    std::vector<std::string> optionsTip;
};


class ParameterComboString : public Parameter
{
public:
    std::vector<std::string> options;
};


class MissionDefinition
{
public:
    std::string name;
    std::vector<Parameter*> parameters;
};


class Mission
{
public:
    std::string name;
    std::vector<std::string> key;
    std::vector<std::string> parameterValue;

    Mission(){}
    Mission(MissionDefinition mD)
    {
        name = mD.name;
        for (auto p : mD.parameters)
        {
            key.push_back((*p).key);
            parameterValue.push_back("");
        }
    }
};


class MissionPlan
{
public:
    std::string name;
    std::vector<Mission> missions;
};


class MissionPlanOnBuffer
{
public:
    int bufferId;
    std::string missionPlanName;
};


class Date
{
public:
    unsigned char day;
    unsigned char month;
    unsigned int year;
};



namespace ScheduleTypes
{
    enum ScheduleType
    {
        Single,
        Timed,
        Recurring
    };
}
typedef ScheduleTypes::ScheduleType ScheduleType;



class ScheduleInfo
{
public:
    int id;
    std::string missionPlanName;
    Date startDate;
    Date endDate;
    unsigned int timeOfDay; // in seconds
    ScheduleType scheduleType;
    bool days[7];
    unsigned int timeDelay; // in seconds
};

}



#endif
