// scheduler_qwidget.h



#ifndef MISSION_CONTROL_CLIENT_SCHEDULER_QWIDGET_H
#define MISSION_CONTROL_CLIENT_SCHEDULER_QWIDGET_H



#include <QWidget>
#include <QComboBox>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QGroupBox>
#include <QRadioButton>
#include <QCheckBox>
#include <QSpacerItem>
#include <QLabel>
#include <QToolBar>
#include <QToolButton>
#include <QDateTimeEdit>
#include <QTableWidget>
#include "scheduler.h"



namespace mission_control_client {



class SchedulerQWidget : public QWidget, public Scheduler
{
    Q_OBJECT

    QComboBox* clientCombo;
    QComboBox* missionPlansCombo;
    QComboBox* typeCombo;
    QTableWidget* scheduleTable;

    QTimeEdit* startTimeEdit;
    QDateEdit* startDateEdit;
    QDateEdit* endDateEdit;
    QTimeEdit* periodTimeEdit;

    QCheckBox* weekRadio[7];

protected:
    virtual void TreatAddClient(Client* c);
    virtual void TreatRemoveClient(Client* c);

public:
    SchedulerQWidget();
    void Init();
    virtual void UpdateMissionPlanList(Client* c);
    virtual void UpdateScheduleList(Client* c);

signals:
    void SignalAddClient(Client* c);
    void SignalRemoveClient(Client* c);
    void SignalUpdateMissionPlanList();
    void SignalUpdateScheduleList();

public slots:
    void SlotAddClient(Client* c);
    void SlotRemoveClient(Client* c);
    void SlotClientChanged(int index);
    void SlotUpdateMissionPlanList();
    void SlotUpdateScheduleList();
    void SlotTypeChanged(const QString & text);
    void SlotScheduleAdd();
    void SlotScheduleRemove();
    void SlotScheduleLoad();
};



}



#endif
