// viewer.h



#ifndef MISSION_CONTROL_CLIENT_VIEWER_H
#define MISSION_CONTROL_CLIENT_VIEWER_H



#include <robot_ps_core/tool.h>
#include "mission_classes.h"
#include "client.h"



namespace mission_control_client {



class Client;



class Viewer : public robot_ps::Tool
{
protected:
    Client* currentClient;

    virtual void TreatAddClient(Client* c){}
    virtual void TreatRemoveClient(Client* c){}

public:
    Viewer();

    virtual void FindComponents()
    {
        AddMeToComponents<Viewer, Client>(&Viewer::TreatAddClient, &Viewer::TreatRemoveClient);
    }

    virtual void UpdateMissionPlanList(Client* c) = 0;
    virtual void UpdateCurrentMissionPlan(Client* c) = 0;
    virtual void UpdateBuffer(Client* c) = 0;
    virtual void UpdateBufferPlaying(Client* c, bool play) = 0;
};



}



#endif
