// editor.h



#ifndef MISSION_CONTROL_CLIENT_EDITOR_H
#define MISSION_CONTROL_CLIENT_EDITOR_H



#include <robot_ps_core/tool.h>
#include "mission_classes.h"
#include "client.h"



namespace mission_control_client {



class Client;



class Editor : public robot_ps::Tool
{
protected:
    MissionPlan missionPlan;
    Client* currentClient;

    virtual void TreatAddClient(Client* c){}
    virtual void TreatRemoveClient(Client* c){}

public:
    Editor();

    virtual void FindComponents()
    {
        AddMeToComponents<Editor, Client>(&Editor::TreatAddClient, &Editor::TreatRemoveClient);
    }

    virtual void UpdateMissionPlanList(Client* c) = 0;
};



}



#endif
