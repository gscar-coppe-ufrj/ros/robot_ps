// viewer_qwidget.h



#ifndef MISSION_CONTROL_CLIENT_VIEWER_QWIDGET_H
#define MISSION_CONTROL_CLIENT_VIEWER_QWIDGET_H



#include <QWidget>
#include <QComboBox>
#include <QToolButton>
#include <QToolBar>
#include <QListWidget>
#include <QVBoxLayout>
#include <QLabel>
#include "viewer.h"



namespace mission_control_client {



class ViewerQWidget : public QWidget, public Viewer
{
    Q_OBJECT

    bool playPauseBuffer;

    QComboBox* clientCombo;

    QLabel* currentMissionPlantLabel;
    QToolButton* playBufferPlayingButton;
    QComboBox* missionPlansCombo;
    QListWidget* missionBufferList;

protected:
    virtual void TreatAddClient(Client* c);
    virtual void TreatRemoveClient(Client* c);

public:
    ViewerQWidget();
    virtual void Init();
    virtual void UpdateMissionPlanList(Client* c);
    virtual void UpdateCurrentMissionPlan(Client* c);
    virtual void UpdateBuffer(Client* c);
    virtual void UpdateBufferPlaying(Client* c, bool play);

signals:
    void SignalAddClient(Client* c);
    void SignalRemoveClient(Client* c);
    void SignalUpdateMissionPlanList();
    void SignalUpdateCurrentMissionPlan();
    void SignalUpdateMissionBuffer();
    void SignalBufferPlaying(bool play);

public slots:
    void SlotAddClient(Client* c);
    void SlotRemoveClient(Client* c);
    void SlotUpdateMissionPlanList();
    void SlotUpdateCurrentMissionPlan();
    void SlotUpdateMissionBuffer();
    void SlotBufferPlaying(bool play);
    void SlotClientChanged(int index);
    void SlotCancelCurrentMissionPlan();
    void SlotBufferPlaying();
    void SlotPlayFromBuffer();
    void SlotTop();
    void SlotUp();
    void SlotDown();
    void SlotBottom();
    void SlotRemove();
    void SlotAdd();
};



}



#endif
