// client_nodelet.h



#ifndef MISSION_CONTROL_CLIENT_CLIENT_NODELET_H
#define MISSION_CONTROL_CLIENT_CLIENT_NODELET_H



#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <custom_loader/custom_nodelet.h>
#include <mission_control/services.h>
#include <mission_control/MissionListUpdate.h>
#include <mission_control/ScheduleListUpdate.h>
#include <mission_control/BufferMissionUpdate.h>
#include "client.h"



namespace mission_control_client {



class ClientNodelet : public Client, public custom_loader::CustomNodelet
{
    ros::Subscriber msgMissionPlanUpdateSub;
    ros::Subscriber msgMissionSchedUpdateSub;
    ros::Subscriber msgCurrentMissionUpdatePub;
    ros::Subscriber msgMissionBufferUpdateSub;
    ros::Subscriber msgMissionBufferPlayingSub;

    ros::ServiceClient srvAddMissionPlanCli;
    ros::ServiceClient srvRemoveMissionPlanCli;
    ros::ServiceClient srvMissionPlanListCli;

    ros::ServiceClient srvAddMissionPlanScheduleCli;
    ros::ServiceClient srvRemoveMissionPlanScheduleCli;
    ros::ServiceClient srvScheduleListCli;

    ros::ServiceClient srvSetCurrentMissionCli;
    ros::ServiceClient srvCancelCurrentMissionCli;

    ros::ServiceClient srvAddMissionToBufferCli;
    ros::ServiceClient srvMoveMissionOnBufferCli;
    ros::ServiceClient srvRemoveMissionFromBufferCli;
    ros::ServiceClient srvBufferControlCli;

    void TreatMissionPlanUpdate(const mission_control::MissionListUpdate::ConstPtr & msg);
    void TreatMissionSchedUpdate(const mission_control::ScheduleListUpdate::ConstPtr & msg);
    void TreatCurrentMissionUpdate(const std_msgs::String::ConstPtr & msg);
    void TreatMissionBufferUpdate(const mission_control::BufferMissionUpdate::ConstPtr & msg);
    void TreatMissionBufferPlaying(const std_msgs::Bool::ConstPtr & msg);

public:
    ClientNodelet();

    virtual void onInit();

    virtual bool SaveMissionPlan(MissionPlan & missionPlan);
    virtual bool DeleteMissionPlan(std::string name);

    virtual void CancelCurrentMissionOnBuffer();
    virtual void MissionBufferPlay(bool play);
    virtual void PlayMissionOnBuffer(unsigned int position);
    virtual void MissionMovementOnBuffer(unsigned int position, int movement);
    virtual void MissionRemoveOnBuffer(unsigned int position);
    virtual void MissionAddOnBuffer(unsigned int position, std::string missionName);

    virtual void AddSchedule(ScheduleInfo schedule);
    virtual void RemoveSchedule(unsigned int id);
};



}



#endif
