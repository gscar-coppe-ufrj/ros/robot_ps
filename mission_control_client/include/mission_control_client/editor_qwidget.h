// editor_qwidget.h



#ifndef MISSION_CONTROL_CLIENT_EDITOR_QWIDGET_H
#define MISSION_CONTROL_CLIENT_EDITOR_QWIDGET_H



#include <QWidget>
#include <QComboBox>
#include <QToolButton>
#include <QToolBar>
#include <QListWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSplitter>
#include <QLineEdit>
#include <QLabel>
#include <QMenu>
#include <QTableWidget>
#include <QHeaderView>
#include <QCheckBox>
#include <QSignalMapper>
#include "editor.h"



#define MISSION_CONTROL_CLIENT_EDITOR_QWIDGET_NEW_NAME_STRING "untitled"



namespace mission_control_client {



class EditorQWidget : public QWidget, public Editor
{
    Q_OBJECT

    QComboBox* clientCombo;

    QToolBar* mainToolBar;
    QToolButton* newButton;
    QToolButton* saveButton;
    QToolButton* loadButton;
    QToolButton* deleteButton;
    QMenu* loadMenu;
    QMenu* deleteMenu;

    QLineEdit* name;

    QToolBar* listToolBar;
    QComboBox* missionCombo;
    QToolButton* upButton;
    QToolButton* downButton;
    QToolButton* removeButton;
    QToolButton* addButton;

    QListWidget* missionList;
    QTableWidget* parametersTable;

    QSignalMapper* signalMapperParameterBool;
    QSignalMapper* signalMapperParameterInt;
    QSignalMapper* signalMapperParameterDouble;
    QSignalMapper* signalMapperParameterString;
    QSignalMapper* signalMapperParameterComboInt;
    QSignalMapper* signalMapperParameterComboString;

    virtual void TreatAddClient(Client* c);
    virtual void TreatRemoveClient(Client* c);

public:
    EditorQWidget();
    virtual void Init();
    virtual void UpdateMissionPlanList(Client* c);
    ~EditorQWidget();

signals:
    void SignalAddClient(Client* c);
    void SignalRemoveClient(Client* c);
    void SignalUpdateMissionPlanList();

private slots:
    void SlotAddClient(Client* c);
    void SlotRemoveClient(Client* c);
    void SlotUpdateMissionPlanList();
    void SlotClientChanged(int index);
    void SlotNew();
    void SlotSave();
    void SlotLoad(QAction* action);
    void SlotDelete(QAction* action);
    void SlotUp();
    void SlotDown();
    void SlotRemove();
    void SlotAdd();
    void SlotMissionChanged();
    void SlotParameterChangedBool(int paramNumber);
    void SlotParameterChangedInt(int paramNumber);
    void SlotParameterChangedDouble(int paramNumber);
    void SlotParameterChangedString(int paramNumber);
    void SlotParameterChangedComboInt(int paramNumber);
    void SlotParameterChangedComboString(int paramNumber);
};



}



#endif
