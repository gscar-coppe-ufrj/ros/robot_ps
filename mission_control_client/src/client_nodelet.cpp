// client_nodelet.cpp



#include <mission_control_client/client_nodelet.h>



mission_control_client::ClientNodelet::ClientNodelet()
{
}


void mission_control_client::ClientNodelet::onInit()
{
    ros::NodeHandle& privateNH = getPrivateNodeHandle();

    std::string definitions;
    privateNH.param("name", name, std::string(""));
    privateNH.param("definitions", definitions, std::string(""));
    LoadDefinitions(definitions);

    ros::NodeHandle nodeHandle = getNodeHandle();

    msgMissionPlanUpdateSub = nodeHandle.subscribe("plan/list_update", 5, &ClientNodelet::TreatMissionPlanUpdate, this);
    msgMissionSchedUpdateSub = nodeHandle.subscribe("schedule/list_update", 5, &ClientNodelet::TreatMissionSchedUpdate, this);
    msgCurrentMissionUpdatePub = nodeHandle.subscribe("current_mission/update", 5, &ClientNodelet::TreatCurrentMissionUpdate, this);
    msgMissionBufferUpdateSub = nodeHandle.subscribe("buffer/list_update", 5, &ClientNodelet::TreatMissionBufferUpdate, this);
    msgMissionBufferPlayingSub = nodeHandle.subscribe("buffer/playing", 5, &ClientNodelet::TreatMissionBufferPlaying, this);

    srvAddMissionPlanCli = nodeHandle.serviceClient<mission_control::AddMissionPlan>("plan/add");
    srvRemoveMissionPlanCli = nodeHandle.serviceClient<mission_control::RemoveMissionPlan>("plan/remove");
    srvMissionPlanListCli = nodeHandle.serviceClient<mission_control::MissionPlanList>("plan/list");

    srvAddMissionPlanScheduleCli = nodeHandle.serviceClient<mission_control::AddMissionPlanSchedule>("schedule/add");
    srvRemoveMissionPlanScheduleCli = nodeHandle.serviceClient<mission_control::RemoveMissionPlanSchedule>("schedule/remove");
    srvScheduleListCli = nodeHandle.serviceClient<mission_control::ScheduleList>("schedule/list");

    srvSetCurrentMissionCli = nodeHandle.serviceClient<mission_control::SetCurrentMission>("current_mission/set");
    srvCancelCurrentMissionCli = nodeHandle.serviceClient<mission_control::CancelCurrentMission>("current_mission/cancel");

    srvAddMissionToBufferCli = nodeHandle.serviceClient<mission_control::AddMissionToBuffer>("buffer/add");
    srvMoveMissionOnBufferCli = nodeHandle.serviceClient<mission_control::MoveMissionOnBuffer>("buffer/move");
    srvRemoveMissionFromBufferCli = nodeHandle.serviceClient<mission_control::RemoveMissionFromBuffer>("buffer/remove");
    srvBufferControlCli = nodeHandle.serviceClient<mission_control::BufferControl>("buffer/control");

    // acquiring mission plans
    mission_control::MissionPlanList srvMissionPlanList;
    srvMissionPlanListCli.call(srvMissionPlanList);
    for (unsigned int i = 0; i < srvMissionPlanList.response.missionPlanList.size(); i++)
    {
        MissionPlan mP;
        mP.name = srvMissionPlanList.response.missionPlanList[i].name;
        for (unsigned int j = 0; j < srvMissionPlanList.response.missionPlanList[i].missions.size(); j++)
        {
            Mission m;
            m.name = srvMissionPlanList.response.missionPlanList[i].missions[j].name;
            m.key = srvMissionPlanList.response.missionPlanList[i].missions[j].parameterKey;
            m.parameterValue = srvMissionPlanList.response.missionPlanList[i].missions[j].parameterValue;
            mP.missions.push_back(m);
        }
        missionPlans.push_back(mP);
    }

    // acquiring schedules
    mission_control::ScheduleList srvScheduleList;
    srvScheduleListCli.call(srvScheduleList);
    for (unsigned int i = 0; i < srvScheduleList.response.scheduleList.size(); i++)
    {
        ScheduleInfo sched;
        sched.id = srvScheduleList.response.scheduleList[i].id;
        sched.missionPlanName = srvScheduleList.response.scheduleList[i].missionPlanName;
        sched.scheduleType = ScheduleType(srvScheduleList.response.scheduleList[i].scheduleType);
        for (unsigned char i = 0; i < 7; i++)
            sched.days[i] = srvScheduleList.response.scheduleList[i].days[i];
        sched.startDate.day = srvScheduleList.response.scheduleList[i].startDate.day;
        sched.startDate.month = srvScheduleList.response.scheduleList[i].startDate.month;
        sched.startDate.year = srvScheduleList.response.scheduleList[i].startDate.year;
        sched.endDate.day = srvScheduleList.response.scheduleList[i].endDate.day;
        sched.endDate.month = srvScheduleList.response.scheduleList[i].endDate.month;
        sched.endDate.year = srvScheduleList.response.scheduleList[i].endDate.year;
        sched.timeOfDay = srvScheduleList.response.scheduleList[i].timeOfDay.toSec();
        sched.timeDelay = srvScheduleList.response.scheduleList[i].timeDelay.toSec();
        schedules.push_back(sched);
    }
}


void mission_control_client::ClientNodelet::TreatMissionPlanUpdate(const mission_control::MissionListUpdate::ConstPtr & msg)
{
    MissionPlan mP;
    mP.name = msg->mission.name;
    for (unsigned int i = 0; i < msg->mission.missions.size(); i++)
    {
        Mission m;
        m.name = msg->mission.missions[i].name;
        m.key = msg->mission.missions[i].parameterKey;
        m.parameterValue = msg->mission.missions[i].parameterValue;
        mP.missions.push_back(m);
    }

    mut.lock();
    bool found = false;
    unsigned int i = 0;
    for (; i < missionPlans.size(); i++)
    {
        if (missionPlans[i].name == mP.name)
        {
            found = true;
            break;
        }
    }
    if (found)
    {
        if (msg->action == BufferMessageType::Add) // (write over)
            missionPlans[i] = mP;
        else if (msg->action == BufferMessageType::Remove)
            missionPlans.erase(missionPlans.begin() + i);
    }
    else if (msg->action == BufferMessageType::Add)
    {
        unsigned int j = 0;
        for (; j < missionPlans.size(); j++)
        {
            if (missionPlans[j].name > msg->mission.name)
                break;
        }
        missionPlans.insert(missionPlans.begin() + j, mP);
    }
    mut.unlock();

    std::deque<Editor*>* editors = GetListOfTools<Client, Editor>();
    for (unsigned int i = 0; i < editors->size(); i++)
        (*editors)[i]->UpdateMissionPlanList(this);

    std::deque<Scheduler*>* schedulers = GetListOfTools<Client, Scheduler>();
    for (unsigned int i = 0; i < schedulers->size(); i++)
        (*schedulers)[i]->UpdateMissionPlanList(this);

    std::deque<Viewer*>* viewers = GetListOfTools<Client, Viewer>();
    for (unsigned int i = 0; i < viewers->size(); i++)
        (*viewers)[i]->UpdateMissionPlanList(this);
}


void mission_control_client::ClientNodelet::TreatMissionSchedUpdate(const mission_control::ScheduleListUpdate::ConstPtr & msg)
{
    bool update = false;

    mut.lock();
    switch (msg->action)
    {
    case BufferMessageType::Add:
    {
        ScheduleInfo sched;
        sched.id = msg->schedule.id;
        sched.missionPlanName = msg->schedule.missionPlanName;
        sched.scheduleType = ScheduleType(msg->schedule.scheduleType);
        for (unsigned char i = 0; i < 7; i++)
            sched.days[i] = msg->schedule.days[i];
        sched.startDate.day = msg->schedule.startDate.day;
        sched.startDate.month = msg->schedule.startDate.month;
        sched.startDate.year = msg->schedule.startDate.year;
        sched.endDate.day = msg->schedule.endDate.day;
        sched.endDate.month = msg->schedule.endDate.month;
        sched.endDate.year = msg->schedule.endDate.year;
        sched.timeOfDay = msg->schedule.timeOfDay.toSec();
        sched.timeDelay = msg->schedule.timeDelay.toSec();

        schedules.push_back(sched);
        update = true;
        break;
    }
    case BufferMessageType::Remove:
        for (auto s = schedules.begin(); s != schedules.end(); s++)
        {
            if ((*s).id == msg->schedule.id)
            {
                schedules.erase(s);
                update = true;
                break;
            }
        }
        break;
    default:
        break;
    }
    mut.unlock();

    if (update)
    {
        std::deque<Scheduler*>* schedulers = GetListOfTools<Client, Scheduler>();
        for (unsigned int i = 0; i < schedulers->size(); i++)
            (*schedulers)[i]->UpdateScheduleList(this);
    }
}


void mission_control_client::ClientNodelet::TreatCurrentMissionUpdate(const std_msgs::String::ConstPtr & msg)
{
    mut.lock();
    if (currentMissionPlan != msg->data)
    {
        currentMissionPlan = msg->data;
        mut.unlock();

        std::deque<Viewer*>* viewers = GetListOfTools<Client, Viewer>();
        for (unsigned int i = 0; i < viewers->size(); i++)
            (*viewers)[i]->UpdateCurrentMissionPlan(this);
    }
    else
        mut.unlock();
}


void mission_control_client::ClientNodelet::TreatMissionBufferUpdate(const mission_control::BufferMissionUpdate::ConstPtr & msg)
{
    mut.lock();
    missionBuffer.clear();
    for (unsigned int i = 0; i < msg->buffer.size(); i++)
    {
        MissionPlanOnBuffer mpob;
        mpob.bufferId = msg->buffer[i].bufferId;
        mpob.missionPlanName = msg->buffer[i].missionPlan.name;
        missionBuffer.push_back(mpob);
    }
    mut.unlock();

    std::deque<Viewer*>* tools = GetListOfTools<Client, Viewer>();
    for (unsigned int i = 0; i < tools->size(); i++)
        (*tools)[i]->UpdateBuffer(this);
}


void mission_control_client::ClientNodelet::TreatMissionBufferPlaying(const std_msgs::Bool::ConstPtr & msg)
{
    std::deque<Viewer*>* tools = GetListOfTools<Client, Viewer>();
    for (unsigned int i = 0; i < tools->size(); i++)
        (*tools)[i]->UpdateBufferPlaying(this, msg->data);
}


bool mission_control_client::ClientNodelet::SaveMissionPlan(mission_control_client::MissionPlan & missionPlan)
{
    // If the Mission Plan should be deleted before Adding, uncomment below
    /*// First I delete it (just to be sure it doesn't exist), then I add it again
    mission_control::RemoveMissionPlan srvRemove;
    // SET ID OR NAME OF THE MISSION PLAN TO BE DELETED
    srvRemoveMissionPlanCli.call(srvRemove);*/

    mission_control::AddMissionPlan srvAdd;
    srvAdd.request.missionPlan.name = missionPlan.name;
    for (unsigned int i = 0; i < missionPlan.missions.size(); i++)
    {
        mission_control::Mission m;
        m.name = missionPlan.missions[i].name;
        m.parameterKey = missionPlan.missions[i].key;
        m.parameterValue = missionPlan.missions[i].parameterValue;
        srvAdd.request.missionPlan.missions.push_back(m);
    }
    srvAddMissionPlanCli.call(srvAdd);
}


bool mission_control_client::ClientNodelet::DeleteMissionPlan(std::string name)
{
    mission_control::RemoveMissionPlan srv;
    srv.request.name = name;
    srvRemoveMissionPlanCli.call(srv);
}


void mission_control_client::ClientNodelet::CancelCurrentMissionOnBuffer()
{
    mission_control::CancelCurrentMission srv;
    srvCancelCurrentMissionCli.call(srv);
}


void mission_control_client::ClientNodelet::MissionBufferPlay(bool play)
{
    mission_control::BufferControl srv;
    if (play)
        srv.request.Flag = BufferFlag::Play;
    else
        srv.request.Flag = BufferFlag::Pause;
    srvBufferControlCli.call(srv);
}


void mission_control_client::ClientNodelet::PlayMissionOnBuffer(unsigned int position)
{
    mission_control::SetCurrentMission srv;
    srv.request.position = position;
    srvSetCurrentMissionCli.call(srv);
}


void mission_control_client::ClientNodelet::MissionMovementOnBuffer(unsigned int position, int movement)
{
    mission_control::MoveMissionOnBuffer srv;
    srv.request.position = position;
    if (movement > 0)
    {
        srv.request.times = movement;
        srv.request.shift = ShiftOrder::Down;
    }
    else
    {
        srv.request.times = -movement;
        srv.request.shift = ShiftOrder::Up;
    }
    srvMoveMissionOnBufferCli.call(srv);
}


void mission_control_client::ClientNodelet::MissionRemoveOnBuffer(unsigned int position)
{
    mission_control::RemoveMissionFromBuffer srv;
    srv.request.pos = position;
    srv.request.id = missionBuffer.at(position).bufferId;
    srvRemoveMissionFromBufferCli.call(srv);
}


void mission_control_client::ClientNodelet::MissionAddOnBuffer(unsigned int position, std::string missionName)
{
    mission_control::AddMissionToBuffer srv;
    srv.request.name = missionName;
    srv.request.position = position;
    srvAddMissionToBufferCli.call(srv);
}


void mission_control_client::ClientNodelet::AddSchedule(mission_control_client::ScheduleInfo schedule)
{
    mission_control::AddMissionPlanSchedule srv;
    srv.request.schedule.id = 0; // the server will generate the actual ID
    srv.request.schedule.missionPlanName = schedule.missionPlanName;
    srv.request.schedule.scheduleType = schedule.scheduleType;
    srv.request.schedule.startDate.day = schedule.startDate.day;
    srv.request.schedule.startDate.month = schedule.startDate.month;
    srv.request.schedule.startDate.year = schedule.startDate.year;
    srv.request.schedule.endDate.day = schedule.endDate.day;
    srv.request.schedule.endDate.month = schedule.endDate.month;
    srv.request.schedule.endDate.year = schedule.endDate.year;
    for (unsigned char i = 0; i < 7; i++)
        srv.request.schedule.days[i] = schedule.days[i];
    srv.request.schedule.timeOfDay.fromSec(schedule.timeOfDay);
    srv.request.schedule.timeDelay.fromSec(schedule.timeDelay);
    srvAddMissionPlanScheduleCli.call(srv);
}


void mission_control_client::ClientNodelet::RemoveSchedule(unsigned int id)
{
    if (id < schedules.size())
    {
        mission_control::RemoveMissionPlanSchedule srv;
        srv.request.Id = schedules[id].id;
        srvRemoveMissionPlanScheduleCli.call(srv);
    }
}

