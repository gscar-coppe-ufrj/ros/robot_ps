// tool_plugins.cpp



#include <pluginlib/class_list_macros.h>
#include <mission_control_client/scheduler_qwidget.h>
#include <mission_control_client/viewer_qwidget.h>
#include <mission_control_client/editor_qwidget.h>



PLUGINLIB_EXPORT_CLASS(mission_control_client::EditorQWidget, robot_ps::Tool)
PLUGINLIB_EXPORT_CLASS(mission_control_client::SchedulerQWidget, robot_ps::Tool)
PLUGINLIB_EXPORT_CLASS(mission_control_client::ViewerQWidget, robot_ps::Tool)
