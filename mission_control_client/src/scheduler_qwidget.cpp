// scheduler_qwidget.cpp



#include <QHeaderView>
#include <mission_control_client/scheduler_qwidget.h>



mission_control_client::SchedulerQWidget::SchedulerQWidget()
{
    QHBoxLayout* l1 = new QHBoxLayout;
    setLayout(l1);
    QVBoxLayout* l2 = new QVBoxLayout;
    l1->addLayout(l2);

    clientCombo = new QComboBox;
    l2->addWidget(clientCombo);
    missionPlansCombo = new QComboBox;
    missionPlansCombo->addItem("Mission Plans");
    auto newFont = QFont("FontFamily",-1, -1, true);
    missionPlansCombo->setItemData(0, newFont, Qt::FontRole);
    l2->addWidget(missionPlansCombo);
    typeCombo = new QComboBox;
    typeCombo->addItem("Type");
    typeCombo->setItemData(0, newFont, Qt::FontRole);
    typeCombo->addItem("Single");
    typeCombo->addItem("Timed");
    typeCombo->addItem("Recurring");
    l2->addWidget(typeCombo);

    QGroupBox* weekDateGroupBox = new QGroupBox("Week Day");
    l2->addWidget(weekDateGroupBox);
    QGridLayout* weekDateGrid = new QGridLayout;
    weekDateGroupBox->setLayout(weekDateGrid);
    weekRadio[0] = new QCheckBox("Sunday");
    weekDateGrid->addWidget(weekRadio[0], 0, 0);
    weekRadio[1] = new QCheckBox("Monday");
    weekDateGrid->addWidget(weekRadio[1], 1, 0);
    weekRadio[2] = new QCheckBox("Tuesday");
    weekDateGrid->addWidget(weekRadio[2], 2, 0);
    weekRadio[3] = new QCheckBox("Wednesday");
    weekDateGrid->addWidget(weekRadio[3], 3, 0);
    weekRadio[4] = new QCheckBox("Thursday");
    weekDateGrid->addWidget(weekRadio[4], 0, 1);
    weekRadio[5] = new QCheckBox("Friday");
    weekDateGrid->addWidget(weekRadio[5], 1, 1);
    weekRadio[6] = new QCheckBox("Saturday");
    weekDateGrid->addWidget(weekRadio[6], 2, 1);

    QGroupBox* dateGroupBox = new QGroupBox("Date");
    l2->addWidget(dateGroupBox);
    QGridLayout* dateGrid = new QGridLayout;
    dateGroupBox->setLayout(dateGrid);
    startTimeEdit = new QTimeEdit(QTime::currentTime());
    startTimeEdit->setDisplayFormat(startTimeEdit->displayFormat() + ":ss");
    startDateEdit = new QDateEdit(QDate::currentDate());
    endDateEdit = new QDateEdit(QDate::currentDate());
    startDateEdit->setCalendarPopup(true);
    endDateEdit->setCalendarPopup(true);
    startDateEdit->setDisplayFormat(startDateEdit->displayFormat() + "  ");
    endDateEdit->setDisplayFormat(endDateEdit->displayFormat() + "  ");
    periodTimeEdit = new QTimeEdit;
    periodTimeEdit->setDisplayFormat(periodTimeEdit->displayFormat() + ":ss");
    dateGrid->addWidget(new QLabel("Start"), 0, 0);
    dateGrid->addWidget(new QLabel("End"), 1, 0);
    dateGrid->addWidget(new QLabel("Period"), 2, 0);
    dateGrid->addWidget(startTimeEdit, 0, 1);
    dateGrid->addWidget(periodTimeEdit, 2, 1);
    dateGrid->addWidget(startDateEdit, 0, 2);
    dateGrid->addWidget(endDateEdit, 1, 2);

    l2->addStretch(1);

    QToolBar* tb = new QToolBar;
    tb->setOrientation(Qt::Vertical);
    l1->addWidget(tb);
    QToolButton* addButton = new QToolButton;
    addButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/add.svg"));
    tb->addWidget(addButton);
    QToolButton* removeButton = new QToolButton;
    removeButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/remove.svg"));
    tb->addWidget(removeButton);
    QToolButton* loadButton = new QToolButton;
    loadButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/back.svg"));
    tb->addWidget(loadButton);

    scheduleTable = new QTableWidget;
    scheduleTable->setColumnCount(7);
    QStringList scheduleLabels;
    scheduleLabels.append("Mission Plan");
    scheduleLabels.append("Type");
    scheduleLabels.append("Weekdays");
    scheduleLabels.append("Start Date");
    scheduleLabels.append("End Date");
    scheduleLabels.append("Start Time");
    scheduleLabels.append("Period");
    scheduleTable->setHorizontalHeaderLabels(scheduleLabels);
    scheduleTable->verticalHeader()->hide();
    scheduleTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    scheduleTable->setSelectionMode(QAbstractItemView::SingleSelection);
    l1->addWidget(scheduleTable, 1);

    connect(this, SIGNAL(SignalAddClient(Client*)), this, SLOT(SlotAddClient(Client*)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(SignalRemoveClient(Client*)), this, SLOT(SlotRemoveClient(Client*)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(SignalUpdateMissionPlanList()), this, SLOT(SlotUpdateMissionPlanList()), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(SignalUpdateScheduleList()), this, SLOT(SlotUpdateScheduleList()), Qt::BlockingQueuedConnection);
    connect(clientCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(SlotClientChanged(int)));
    connect(typeCombo, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(SlotTypeChanged(const QString &)));
    connect(addButton, SIGNAL(clicked()), this, SLOT(SlotScheduleAdd()));
    connect(removeButton, SIGNAL(clicked()), this, SLOT(SlotScheduleRemove()));
    connect(loadButton, SIGNAL(clicked()), this, SLOT(SlotScheduleLoad()));

    SlotTypeChanged(""); // disable some control options
}


void mission_control_client::SchedulerQWidget::Init()
{
    clientCombo->addItem("Client");
    auto newFont = QFont("FontFamily",-1, -1, true);
    clientCombo->setItemData(0, newFont, Qt::FontRole);

    auto list = GetListOfComponents<Scheduler, Client>();
    for (auto c : (*list))
        SlotAddClient(c);
}


void mission_control_client::SchedulerQWidget::TreatAddClient(mission_control_client::Client* c)
{
    emit SignalAddClient(c);
}


void mission_control_client::SchedulerQWidget::TreatRemoveClient(mission_control_client::Client* c)
{
    emit SignalRemoveClient(c);
}


void mission_control_client::SchedulerQWidget::UpdateMissionPlanList(mission_control_client::Client* c)
{
    if (c == dynamic_cast<Client*>(currentClient))
        emit SignalUpdateMissionPlanList();
}


void mission_control_client::SchedulerQWidget::UpdateScheduleList(mission_control_client::Client* c)
{
    if (c == dynamic_cast<Client*>(currentClient))
        emit SignalUpdateScheduleList();
}


void mission_control_client::SchedulerQWidget::SlotAddClient(mission_control_client::Client* c)
{
    int index = 1;
    for (int i = 1; i <= clientCombo->count(); i++)
    {
        if (!(clientCombo->itemText(i).toStdString() < c->name))
            break;
        index++;
    }
    clientCombo->insertItem(index, QString(c->name.c_str()));
}


void mission_control_client::SchedulerQWidget::SlotRemoveClient(mission_control_client::Client* c)
{
    int index = clientCombo->findText(QString(c->name.c_str()));
    if (index != -1)
        clientCombo->removeItem(index);
}


void mission_control_client::SchedulerQWidget::SlotClientChanged(int index)
{
    Client* oldCurrentClient = currentClient;
    std::string client = clientCombo->currentText().toStdString();
    if (index > 0)
    {
        auto list = GetListOfComponents<Scheduler, Client>();
        for (auto c : (*list))
        {
            if (c->name == client)
                currentClient = c;
        }
    }
    else
        currentClient = NULL;

    if (currentClient != oldCurrentClient)
    {
        if (currentClient == NULL)
        {
            missionPlansCombo->clear();
            missionPlansCombo->addItem("Mission Plans");
            auto newFont = QFont("FontFamily",-1, -1, true);
            missionPlansCombo->setItemData(0, newFont, Qt::FontRole);
            scheduleTable->setRowCount(0);
        }
        else
        {
            SlotUpdateMissionPlanList();
            SlotUpdateScheduleList();
        }
    }
}


void mission_control_client::SchedulerQWidget::SlotUpdateMissionPlanList()
{
    QString temp = missionPlansCombo->currentText();
    missionPlansCombo->clear();
    missionPlansCombo->addItem("Mission Plans");
    auto newFont = QFont("FontFamily",-1, -1, true);
    missionPlansCombo->setItemData(0, newFont, Qt::FontRole);
    auto list = currentClient->GetMissionPlanList();
    for (auto mP : list)
        missionPlansCombo->addItem(mP.c_str());
    int found = missionPlansCombo->findText(temp);
    if (found != -1)
        missionPlansCombo->setCurrentIndex(found);
}


void mission_control_client::SchedulerQWidget::SlotUpdateScheduleList()
{
    auto list = currentClient->GetScheduleList();
    scheduleTable->setRowCount(list.size());
    int i = 0;
    for (ScheduleInfo sched : list)
    {
        scheduleTable->setItem(i, 0, new QTableWidgetItem(sched.missionPlanName.c_str()));
        switch (sched.scheduleType)
        {
        case ScheduleType::Timed:
            scheduleTable->setItem(i, 1, new QTableWidgetItem("Timed"));
            break;
        case ScheduleType::Recurring:
            scheduleTable->setItem(i, 1, new QTableWidgetItem("Recurring"));
            break;
        default: // ScheduleType::Single
            scheduleTable->setItem(i, 1, new QTableWidgetItem("Single"));
            break;
        }

        QString weekDays;
        if (sched.days[0])
            weekDays += "Su ";
        else
            weekDays += "__ ";
        if (sched.days[1])
            weekDays += "Mo ";
        else
            weekDays += "__ ";
        if (sched.days[2])
            weekDays += "Tu ";
        else
            weekDays += "__ ";
        if (sched.days[3])
            weekDays += "We ";
        else
            weekDays += "__ ";
        if (sched.days[4])
            weekDays += "Th ";
        else
            weekDays += "__ ";
        if (sched.days[5])
            weekDays += "Fr ";
        else
            weekDays += "__ ";
        if (sched.days[6])
            weekDays += "Sa";
        else
            weekDays += "__";

        scheduleTable->setItem(i, 2, new QTableWidgetItem(weekDays));
        scheduleTable->setItem(i, 3, new QTableWidgetItem(QDate(sched.startDate.year, sched.startDate.month, sched.startDate.day).toString("dd/MM/yyyy")));
        scheduleTable->setItem(i, 4, new QTableWidgetItem(QDate(sched.endDate.year, sched.endDate.month, sched.endDate.day).toString("dd/MM/yyyy")));
        scheduleTable->setItem(i, 5, new QTableWidgetItem(QTime::fromMSecsSinceStartOfDay(sched.timeOfDay*1000).toString()));
        scheduleTable->setItem(i, 6, new QTableWidgetItem(QTime::fromMSecsSinceStartOfDay(sched.timeDelay*1000).toString()));

        for (unsigned char j = 0; j < 7; j++)
            scheduleTable->item(i, j)->setTextAlignment(Qt::AlignCenter);
        i++;
    }
    scheduleTable->resizeColumnsToContents();
}


void mission_control_client::SchedulerQWidget::SlotTypeChanged(const QString & text)
{
    bool showWeekDays = false;
    bool showPeriod = false;
    bool showEndDate = true;

    if (text == "Single")
        showEndDate = false;
    if (text == "Recurring")
        showWeekDays = true;
    else if (text == "Timed")
        showPeriod = true;

    for (unsigned char i = 0; i < 7; i++)
        weekRadio[i]->setEnabled(showWeekDays);

    periodTimeEdit->setEnabled(showPeriod);
    endDateEdit->setEnabled(showEndDate);
}


void mission_control_client::SchedulerQWidget::SlotScheduleAdd()
{
    if (missionPlansCombo->currentIndex() <= 0 || typeCombo->currentIndex() <= 0 || currentClient == NULL)
        return;

    ScheduleInfo schedule;
    schedule.missionPlanName = missionPlansCombo->currentText().toStdString();
    switch (typeCombo->currentIndex())
    {
    case 1: // Single
        schedule.scheduleType = ScheduleType::Single;
        break;
    case 2: // Timed
        schedule.scheduleType = ScheduleType::Timed;
        break;
    case 3: // Recurring
        schedule.scheduleType = ScheduleType::Recurring;
        break;
    default:
        break;
    }

    auto date = startDateEdit->date();
    schedule.startDate.day = date.day();
    schedule.startDate.month = date.month();
    schedule.startDate.year = date.year();
    schedule.endDate.day = 0;
    schedule.endDate.month = 0;
    schedule.endDate.year = 0;
    schedule.timeDelay = 0;

    if (schedule.scheduleType != ScheduleType::Single)
    {
        date = endDateEdit->date();
        schedule.endDate.day = date.day();
        schedule.endDate.month = date.month();
        schedule.endDate.year = date.year();
    }

    if (schedule.scheduleType == ScheduleType::Timed)
        schedule.timeDelay = periodTimeEdit->time().msecsSinceStartOfDay()/1000;

    if (schedule.scheduleType == ScheduleType::Recurring)
    {
        for (unsigned char i = 0; i < 7; i++)
            schedule.days[i] = weekRadio[i]->isChecked();
    }
    else
    {
        for (unsigned char i = 0; i < 7; i++)
            schedule.days[i] = false;
    }

    schedule.timeOfDay = startTimeEdit->time().msecsSinceStartOfDay()/1000;

    currentClient->AddSchedule(schedule);
}


void mission_control_client::SchedulerQWidget::SlotScheduleRemove()
{
    if (currentClient != NULL && scheduleTable->selectedRanges().count() > 0)
        currentClient->RemoveSchedule(scheduleTable->currentRow());
}


void mission_control_client::SchedulerQWidget::SlotScheduleLoad()
{
    if (currentClient != NULL && scheduleTable->selectedRanges().count() > 0)
    {
        ScheduleInfo sched = currentClient->GetScheduleList()[scheduleTable->currentRow()];

        /*std::vector<std::string> missionPlans = currentClient->GetMissionPlanList();
        if (missionPlans.find(missionPlans.begin(), missionPlans.end(), sched.missionPlanName) == missionPlans.end())
            return;*/
        int index = missionPlansCombo->findText(sched.missionPlanName.c_str());
        if (index <= 0)
            return;
        missionPlansCombo->setCurrentText(sched.missionPlanName.c_str());

        switch (sched.scheduleType)
        {
        case ScheduleType::Timed:
            typeCombo->setCurrentText("Timed");
            break;
        case ScheduleType::Recurring:
            typeCombo->setCurrentText("Recurring");
            break;
        default: // ScheduleType::Single
            typeCombo->setCurrentText("Single");
            break;
        }

        startDateEdit->setDate(QDate(sched.startDate.year, sched.startDate.month, sched.startDate.day));
        endDateEdit->setDate(QDate(sched.endDate.year, sched.endDate.month, sched.endDate.day));
        startTimeEdit->setTime(QTime::fromMSecsSinceStartOfDay(sched.timeOfDay*1000));
        periodTimeEdit->setTime(QTime::fromMSecsSinceStartOfDay(sched.timeDelay*1000));

        for (unsigned char i = 0; i < 7; i++)
            weekRadio[i]->setChecked(sched.days[i]);
    }
}
