// editor_qwidget.cpp



#include <QMenu>
#include <mission_control_client/editor_qwidget.h>
#include <iostream>


mission_control_client::EditorQWidget::EditorQWidget()
{
    clientCombo = new QComboBox;

    mainToolBar = new QToolBar;
    name = new QLineEdit(MISSION_CONTROL_CLIENT_EDITOR_QWIDGET_NEW_NAME_STRING);
    newButton = new QToolButton;
    newButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/document-new.svg"));
    saveButton = new QToolButton;
    saveButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/document-save.svg"));
    loadButton = new QToolButton;
    loadButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/document-open.svg"));
    loadMenu = new QMenu;
    loadButton->setMenu(loadMenu);
    loadButton->setPopupMode(QToolButton::InstantPopup);
    deleteButton = new QToolButton;
    deleteButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/edit-delete.svg"));
    deleteMenu = new QMenu;
    deleteButton->setMenu(deleteMenu);
    deleteButton->setPopupMode(QToolButton::InstantPopup);
    mainToolBar->addWidget(name);
    mainToolBar->addWidget(newButton);
    mainToolBar->addWidget(saveButton);
    mainToolBar->addWidget(loadButton);
    mainToolBar->addWidget(deleteButton);

    listToolBar = new QToolBar;
    upButton = new QToolButton;
    upButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/up.svg"));
    downButton = new QToolButton;
    downButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/down.svg"));
    removeButton = new QToolButton;
    removeButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/remove.svg"));
    missionCombo = new QComboBox;
    missionCombo->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
    addButton = new QToolButton;
    addButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/add.svg"));
    listToolBar->addWidget(upButton);
    listToolBar->addWidget(downButton);
    listToolBar->addWidget(removeButton);
    listToolBar->addWidget(missionCombo);
    listToolBar->addWidget(addButton);

    missionList = new QListWidget;
    missionList->setMinimumSize(100,100);
    parametersTable = new QTableWidget;
    parametersTable->setColumnCount(1);
    QStringList parameterLabels;
    parameterLabels.append("Value");
    parametersTable->setHorizontalHeaderLabels(parameterLabels);
    parametersTable->horizontalHeader()->setStretchLastSection(true);
    parametersTable->hide();

    QSplitter* splitter = new QSplitter;
    splitter->addWidget(missionList);
    splitter->addWidget(parametersTable);

    QVBoxLayout* l = new QVBoxLayout;
    l->addWidget(clientCombo);
    l->addWidget(mainToolBar);
    l->addWidget(listToolBar);
    l->addWidget(splitter);
    l->setContentsMargins(0, 0, 0, 0);
    l->setSpacing(0);
    l->setMargin(0);
    setLayout(l);

    connect(this, SIGNAL(SignalAddClient(Client*)), this, SLOT(SlotAddClient(Client*)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(SignalRemoveClient(Client*)), this, SLOT(SlotRemoveClient(Client*)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(SignalUpdateMissionPlanList()), this, SLOT(SlotUpdateMissionPlanList()), Qt::BlockingQueuedConnection);
    connect(clientCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(SlotClientChanged(int)));
    connect(newButton, SIGNAL(clicked()), this, SLOT(SlotNew()));
    connect(saveButton, SIGNAL(clicked()), this, SLOT(SlotSave()));
    connect(loadMenu, SIGNAL(triggered(QAction*)), this, SLOT(SlotLoad(QAction*)));
    connect(deleteMenu, SIGNAL(triggered(QAction*)), this, SLOT(SlotDelete(QAction*)));
    connect(upButton, SIGNAL(clicked()), this, SLOT(SlotUp()));
    connect(downButton, SIGNAL(clicked()), this, SLOT(SlotDown()));
    connect(removeButton, SIGNAL(clicked()), this, SLOT(SlotRemove()));
    connect(addButton, SIGNAL(clicked()), this, SLOT(SlotAdd()));
    connect(missionList, SIGNAL(itemSelectionChanged()), this, SLOT(SlotMissionChanged()));

    signalMapperParameterBool = NULL;
    signalMapperParameterInt = NULL;
    signalMapperParameterDouble = NULL;
    signalMapperParameterString = NULL;
    signalMapperParameterComboInt= NULL;
    signalMapperParameterComboString= NULL;
    signalMapperParameterBool = new QSignalMapper(this);
    connect(signalMapperParameterBool, SIGNAL(mapped(int)), this, SLOT(SlotParameterChangedBool(int)));
    signalMapperParameterInt = new QSignalMapper(this);
    connect(signalMapperParameterInt, SIGNAL(mapped(int)), this, SLOT(SlotParameterChangedInt(int)));
    signalMapperParameterDouble = new QSignalMapper(this);
    connect(signalMapperParameterDouble, SIGNAL(mapped(int)), this, SLOT(SlotParameterChangedDouble(int)));
    signalMapperParameterString = new QSignalMapper(this);
    connect(signalMapperParameterString, SIGNAL(mapped(int)), this, SLOT(SlotParameterChangedString(int)));
    signalMapperParameterComboInt = new QSignalMapper(this);
    connect(signalMapperParameterComboInt, SIGNAL(mapped(int)), this, SLOT(SlotParameterChangedComboInt(int)));
    signalMapperParameterComboString = new QSignalMapper(this);
    connect(signalMapperParameterComboString, SIGNAL(mapped(int)), this, SLOT(SlotParameterChangedComboString(int)));
}


void mission_control_client::EditorQWidget::Init()
{
    clientCombo->addItem("Client");
    auto newFont = QFont("FontFamily",-1, -1, true);
    clientCombo->setItemData(0, newFont, Qt::FontRole);

    auto list = GetListOfComponents<Editor, Client>();
    for (auto c : (*list))
        SlotAddClient(c);
}


void mission_control_client::EditorQWidget::TreatAddClient(mission_control_client::Client* c)
{
    emit SignalAddClient(c);
}


void mission_control_client::EditorQWidget::TreatRemoveClient(mission_control_client::Client* c)
{
    emit SignalRemoveClient(c);
}


void mission_control_client::EditorQWidget::UpdateMissionPlanList(Client* c)
{
    if (c == dynamic_cast<Client*>(currentClient))
        emit SignalUpdateMissionPlanList();
}


void mission_control_client::EditorQWidget::SlotAddClient(mission_control_client::Client* c)
{
    int index = 1;
    for (int i = 1; i <= clientCombo->count(); i++)
    {
        if (!(clientCombo->itemText(i).toStdString() < c->name))
            break;
        index++;
    }
    clientCombo->insertItem(index, QString(c->name.c_str()));
}


void mission_control_client::EditorQWidget::SlotRemoveClient(mission_control_client::Client* c)
{
    int index = clientCombo->findText(QString(c->name.c_str()));
    if (index != -1)
    {
        std::cout << "removing" << std::endl;
        clientCombo->removeItem(index);
    }
}


void mission_control_client::EditorQWidget::SlotUpdateMissionPlanList()
{
    loadMenu->clear();
    deleteMenu->clear();
    auto missionPlanList = currentClient->GetMissionPlanList();
    for (auto mP : missionPlanList)
    {
        loadMenu->addAction(QString(mP.c_str()));
        deleteMenu->addAction(QString(mP.c_str()));
    }
}


void mission_control_client::EditorQWidget::SlotClientChanged(int index)
{
    Client* oldCurrentClient = currentClient;
    std::string client = clientCombo->currentText().toStdString();
    if (index > 0)
    {
        auto list = GetListOfComponents<Editor, Client>();
        for (auto c : (*list))
        {
            if (c->name == client)
                currentClient = c;
        }
    }
    else
        currentClient = NULL;

    if (currentClient != oldCurrentClient)
    {
        missionPlan.name = "";
        missionPlan.missions.clear();
        missionCombo->clear();
        loadMenu->clear();
        deleteMenu->clear();
        missionList->clear();

        if (currentClient != NULL)
        {
            auto missionDefinitionsList = currentClient->GetMissionDefinitions();
            for (auto mD : missionDefinitionsList)
                missionCombo->addItem(QString(mD.name.c_str()));
            auto missionPlanList = currentClient->GetMissionPlanList();
            for (auto mP : missionPlanList)
            {
                loadMenu->addAction(QString(mP.c_str()));
                deleteMenu->addAction(QString(mP.c_str()));
            }
        }
    }
}


void mission_control_client::EditorQWidget::SlotNew()
{
    name->setText(MISSION_CONTROL_CLIENT_EDITOR_QWIDGET_NEW_NAME_STRING);
    missionList->clear();
    missionPlan.name = "";
    missionPlan.missions.clear();
}


void mission_control_client::EditorQWidget::SlotSave()
{
    missionPlan.name = name->text().toStdString();
    if (currentClient != NULL)
        currentClient->SaveMissionPlan(missionPlan);
}


void mission_control_client::EditorQWidget::SlotLoad(QAction* action)
{
    missionPlan = currentClient->GetMissionPlan(action->text().toStdString());
    if (missionPlan.name == action->text().toStdString())
    {
        name->setText(action->text());
        missionList->clear();
        for (auto m : missionPlan.missions)
            missionList->addItem(QString(m.name.c_str()));
    }
}


void mission_control_client::EditorQWidget::SlotDelete(QAction* action)
{
    if (currentClient != NULL)
        currentClient->DeleteMissionPlan(action->text().toStdString());
}


void mission_control_client::EditorQWidget::SlotUp()
{
    int i = missionList->currentRow();
    if (i > 0)
    {
        Mission tempMission = missionPlan.missions[i];
        missionPlan.missions[i] = missionPlan.missions[i - 1];
        missionPlan.missions[i - 1] = tempMission;

        QString tempString = missionList->item(i)->text();
        missionList->item(i)->setText(missionList->item(i - 1)->text());
        missionList->item(i - 1)->setText(tempString);
        missionList->setCurrentRow(i - 1);
    }
}


void mission_control_client::EditorQWidget::SlotDown()
{
    int i = missionList->currentRow();
    if (i < missionList->count() - 1)
    {
        Mission tempMission = missionPlan.missions[i];
        missionPlan.missions[i] = missionPlan.missions[i + 1];
        missionPlan.missions[i + 1] = tempMission;

        QString tempString = missionList->item(i)->text();
        missionList->item(i)->setText(missionList->item(i + 1)->text());
        missionList->item(i + 1)->setText(tempString);
        missionList->setCurrentRow(i + 1);
    }
}


void mission_control_client::EditorQWidget::SlotRemove()
{
    int row = missionList->currentRow();
    if (row >= 0)
    {
        missionPlan.missions.erase(missionPlan.missions.begin() + row);
        delete missionList->takeItem(row);
    }
}


void mission_control_client::EditorQWidget::SlotAdd()
{
    if (currentClient == NULL)
        return;

    std::vector<MissionDefinition> mDs = currentClient->GetMissionDefinitions();
    std::string missionDefinitionName = missionCombo->currentText().toStdString();
    for (unsigned int i = 0; i < mDs.size(); i++)
    {
        if (mDs[i].name == missionDefinitionName)
        {
            int row = missionList->currentRow();
            if (row < 0)
                row = missionList->count() - 1;
            missionList->insertItem(row + 1, QString(missionDefinitionName.c_str()));
            Mission m;
            m.name = mDs[i].name;
            for (unsigned int j = 0; j < mDs[i].parameters.size(); j++)
            {
                m.key.push_back(mDs[i].parameters[j]->key);
                
                // Set default values.
                if (dynamic_cast<ParameterBool*>(mDs[i].parameters[j]))
                {
                    m.parameterValue.push_back("True");
                }
                else if (dynamic_cast<ParameterInt*>(mDs[i].parameters[j]))
                {
                    m.parameterValue.push_back("0");
                }
                else if (dynamic_cast<ParameterDouble*>(mDs[i].parameters[j]))
                {
                    m.parameterValue.push_back("0.0");
                }
                else if (dynamic_cast<ParameterString*>(mDs[i].parameters[j]))
                {
                    m.parameterValue.push_back("");
                }
                else if (dynamic_cast<ParameterComboInt*>(mDs[i].parameters[j]))
                {
                    ParameterComboInt* param = dynamic_cast<ParameterComboInt*>(mDs[i].parameters[j]);
                    m.parameterValue.push_back(param->optionsValues[0]);
                }
                else if (dynamic_cast<ParameterComboString*>(mDs[i].parameters[j]))
                {
                    ParameterComboString* param = dynamic_cast<ParameterComboString*>(mDs[i].parameters[j]);
                    m.parameterValue.push_back(param->options[0]);
                }
            }
            if (missionPlan.missions.empty())
                missionPlan.missions.insert(missionPlan.missions.begin(), m);
            else
                missionPlan.missions.insert(missionPlan.missions.begin() + row + 1, m);
            break;
        }
    }
}


void mission_control_client::EditorQWidget::SlotMissionChanged()
{
    auto temp = missionList->selectedItems();
    if (temp.size() > 0)
    {
        parametersTable->show();
        int row = missionList->row(temp[0]);
        Mission & m = missionPlan.missions[row];

        for (unsigned int i = 0; i < parametersTable->rowCount(); i++)
            parametersTable->setCellWidget(i, 0, NULL);

        parametersTable->setRowCount(m.key.size());
        QStringList parameterLabels;
        auto mDs = currentClient->GetMissionDefinitions();

        for (unsigned int i = 0; i < m.key.size(); i++)
        {
            for (unsigned int j = 0; j < mDs.size(); j++)
            {
                if (m.name == mDs[j].name)
                {
                    parameterLabels.append(QString(m.key[i].c_str()));
                    if (dynamic_cast<ParameterBool*>(mDs[j].parameters[i]))
                    {
                        QCheckBox* checkbox = new QCheckBox;
                        if (m.parameterValue[i] == "False")
                            checkbox->setCheckState(Qt::Unchecked);
                        else
                            checkbox->setCheckState(Qt::Checked);
                        parametersTable->setCellWidget(i, 0, checkbox);

                        connect(checkbox, SIGNAL(stateChanged(int)), signalMapperParameterBool, SLOT(map()));
                        signalMapperParameterBool->setMapping(checkbox, i);
                    }
                    else if (dynamic_cast<ParameterInt*>(mDs[j].parameters[i]))
                    {
                        QLineEdit* edit = new QLineEdit((m.parameterValue[i].c_str()));
                        edit->setValidator(new QIntValidator);
                        parametersTable->setCellWidget(i, 0, edit);

                        connect(edit, SIGNAL(editingFinished()), signalMapperParameterInt, SLOT(map()));
                        signalMapperParameterInt->setMapping(edit, i);
                    }
                    else if (dynamic_cast<ParameterDouble*>(mDs[j].parameters[i]))
                    {
                        QLineEdit* edit = new QLineEdit((m.parameterValue[i].c_str()));
                        edit->setValidator(new QDoubleValidator);
                        parametersTable->setCellWidget(i, 0, edit);

                        connect(edit, SIGNAL(editingFinished()), signalMapperParameterDouble, SLOT(map()));
                        signalMapperParameterDouble->setMapping(edit, i);
                    }
                    else if (dynamic_cast<ParameterString*>(mDs[j].parameters[i]))
                    {
                        QLineEdit* edit = new QLineEdit((m.parameterValue[i].c_str()));
                        parametersTable->setCellWidget(i, 0, edit);

                        connect(edit, SIGNAL(editingFinished()), signalMapperParameterString, SLOT(map()));
                        signalMapperParameterString->setMapping(edit, i);
                    }
                    else if (dynamic_cast<ParameterComboInt*>(mDs[j].parameters[i]))
                    {
                        QComboBox* combo = new QComboBox;
                        for (auto tip : (((ParameterComboInt*)mDs[j].parameters[i])->optionsTip))
                            combo->addItem(QString(tip.c_str()));
                        int temp;
                        try
                        {
                            temp = std::stoi(m.parameterValue[i]);
                        }
                        catch (const std::invalid_argument & ia)
                        {
                            std::cout << "invalid argument in " << m.key[i] << "; value = " << m.parameterValue[i] << std::endl;
                            temp = 0;
                        }
                        combo->setCurrentIndex(temp);
                        parametersTable->setCellWidget(i, 0, combo);

                        connect(combo, SIGNAL(currentIndexChanged(int)), signalMapperParameterComboInt, SLOT(map()));
                        signalMapperParameterComboInt->setMapping(combo, i);
                    }
                    else if (dynamic_cast<ParameterComboString*>(mDs[j].parameters[i]))
                    {
                        QComboBox* combo = new QComboBox;
                        auto options = ((ParameterComboString*)mDs[j].parameters[i])->options;
                        for (auto option : options)
                            combo->addItem(QString(option.c_str()));
                        auto found = std::find(options.begin(), options.end(), m.parameterValue[i]);
                        if (found == options.end())
                            combo->setCurrentText(QString(options.front().c_str()));
                        else
                            combo->setCurrentText(QString(m.parameterValue[i].c_str()));
                        parametersTable->setCellWidget(i, 0, combo);

                        connect(combo, SIGNAL(currentIndexChanged(int)), signalMapperParameterComboString, SLOT(map()));
                        signalMapperParameterComboString->setMapping(combo, i);
                    }
                    break;
                }
            }
        }
        parametersTable->setVerticalHeaderLabels(parameterLabels);
        parametersTable->resizeColumnsToContents();
    }
    else
    {
        parametersTable->hide();
        parametersTable->setRowCount(0);
    }
}


void mission_control_client::EditorQWidget::SlotParameterChangedBool(int paramNumber)
{
    auto temp = missionList->selectedItems();
    if (temp.size() > 0)
    {
        int row = missionList->row(temp[0]);
        Mission & m = missionPlan.missions[row];
        if (((QCheckBox*)parametersTable->cellWidget(paramNumber,0))->checkState() == Qt::Checked)
            m.parameterValue[paramNumber] = "True";
        else
            m.parameterValue[paramNumber] = "False";
    }
}


void mission_control_client::EditorQWidget::SlotParameterChangedInt(int paramNumber)
{
    auto temp = missionList->selectedItems();
    if (temp.size() > 0)
    {
        int row = missionList->row(temp[0]);
        Mission & m = missionPlan.missions[row];
        m.parameterValue[paramNumber] = ((QLineEdit*)parametersTable->cellWidget(paramNumber,0))->text().toStdString();
    }
}


void mission_control_client::EditorQWidget::SlotParameterChangedDouble(int paramNumber)
{
    auto temp = missionList->selectedItems();
    if (temp.size() > 0)
    {
        int row = missionList->row(temp[0]);
        Mission & m = missionPlan.missions[row];
        m.parameterValue[paramNumber] = ((QLineEdit*)parametersTable->cellWidget(paramNumber,0))->text().toStdString();
    }
}


void mission_control_client::EditorQWidget::SlotParameterChangedString(int paramNumber)
{
    auto temp = missionList->selectedItems();
    if (temp.size() > 0)
    {
        int row = missionList->row(temp[0]);
        Mission & m = missionPlan.missions[row];
        m.parameterValue[paramNumber] = ((QLineEdit*)parametersTable->cellWidget(paramNumber,0))->text().toStdString();
    }
}


void mission_control_client::EditorQWidget::SlotParameterChangedComboInt(int paramNumber)
{
    auto temp = missionList->selectedItems();
    if (temp.size() > 0)
    {
        int row = missionList->row(temp[0]);
        Mission & m = missionPlan.missions[row];
        QComboBox* combo = ((QComboBox*)parametersTable->cellWidget(paramNumber, 0));
        m.parameterValue[paramNumber] = std::to_string(combo->currentIndex());
    }
}


void mission_control_client::EditorQWidget::SlotParameterChangedComboString(int paramNumber)
{
    auto temp = missionList->selectedItems();
    if (temp.size() > 0)
    {
        int row = missionList->row(temp[0]);
        Mission & m = missionPlan.missions[row];
        QComboBox* combo = ((QComboBox*)parametersTable->cellWidget(paramNumber, 0));
        m.parameterValue[paramNumber] = combo->currentText().toStdString();
    }
}


mission_control_client::EditorQWidget::~EditorQWidget()
{
    if (signalMapperParameterBool != NULL)
        delete signalMapperParameterBool;
    if (signalMapperParameterInt != NULL)
        delete signalMapperParameterInt;
    if (signalMapperParameterDouble != NULL)
        delete signalMapperParameterDouble;
    if (signalMapperParameterString != NULL)
        delete signalMapperParameterString;
    if (signalMapperParameterComboInt != NULL)
        delete signalMapperParameterComboInt;
    if (signalMapperParameterComboString != NULL)
        delete signalMapperParameterComboString;
}
