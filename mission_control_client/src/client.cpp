// client.cpp



#include <mission_control_client/client.h>
#include <simple_xml_settings/simple_xml_settings.h>



mission_control_client::Client::Client()
{
}


void mission_control_client::Client::LoadDefinitions(std::string file)
{
    if (file == "")
        return;

    SimpleXMLSettings settings(file, "MissionDefinitions");
    if (settings.HasLock())
    {
        settings.BeginLoad();

        for (int index = 0; settings.BeginGroup("Mission", index); index++)
        {
            MissionDefinition mD;
            mD.name = settings.LoadText("name", "");

            for (int index2 = 0; settings.BeginGroup("Parameter", index2); index2++)
            {
                Parameter* p;
                std::string type = settings.LoadText("type", "");
                if (type == "bool")
                    p = new ParameterBool;
                else if (type == "int")
                    p = new ParameterInt;
                else if (type == "double")
                    p = new ParameterDouble;
                else if (type == "combo_int")
                {
                    p = new ParameterComboInt;
                    int i = 0;
                    while (settings.BeginGroup("Element", i))
                    {
                        ((ParameterComboInt*)p)->optionsValues.push_back(settings.LoadText("value", ""));
                        ((ParameterComboInt*)p)->optionsTip.push_back(settings.LoadText("tip", ""));
                        settings.EndGroup();
                        i++;
                    }
                }
                else if (type == "combo_string")
                {
                    p = new ParameterComboString;
                    int i = 0;
                    while (settings.BeginGroup("Element", i))
                    {
                        ((ParameterComboString*)p)->options.push_back(settings.LoadText("value", ""));
                        settings.EndGroup();
                        i++;
                    }
                }
                else
                    p = new ParameterString;
                p->key = settings.LoadText("key", "");
                mD.parameters.push_back(p);
                settings.EndGroup();
            }

            missionDefinitions.push_back(mD);

            settings.EndGroup();
        }

        settings.EndLoad();
    }
}


std::vector<mission_control_client::MissionDefinition> mission_control_client::Client::GetMissionDefinitions()
{
    return missionDefinitions;
}


std::vector<std::string> mission_control_client::Client::GetMissionPlanList()
{
    mut.lock();
    std::vector<std::string> ret;
    for (auto i = missionPlans.begin(); i != missionPlans.end(); i++)
        ret.push_back((*i).name);
    mut.unlock();
    return ret;
}


mission_control_client::MissionPlan mission_control_client::Client::GetMissionPlan(std::string name)
{
    mut.lock();
    for (auto i = missionPlans.begin(); i != missionPlans.end(); i++)
        if ((*i).name == name)
        {
            mut.unlock();
            return *i;
        }
    MissionPlan m;
    mut.unlock();
    return m;
}


std::string mission_control_client::Client::GetCurrentMissionPlan()
{
    return currentMissionPlan;
}


std::vector<mission_control_client::MissionPlanOnBuffer> mission_control_client::Client::GetBuffer()
{
    return missionBuffer;
}


std::vector<mission_control_client::ScheduleInfo> mission_control_client::Client::GetScheduleList()
{
    return schedules;
}


mission_control_client::Client::~Client()
{
    for (auto i = 0; i < missionDefinitions.size(); i++)
    {
        for (auto j = 0; j < missionDefinitions[i].parameters.size(); j++)
        {
            if (missionDefinitions[i].parameters[j] != NULL)
                delete missionDefinitions[i].parameters[j];
        }
    }
}
