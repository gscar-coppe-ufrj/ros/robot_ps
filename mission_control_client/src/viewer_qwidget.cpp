// viewer_qwidget.cpp



#include <mission_control_client/viewer_qwidget.h>



mission_control_client::ViewerQWidget::ViewerQWidget()
{
    playPauseBuffer = false;

    QVBoxLayout* l = new QVBoxLayout;
    setLayout(l);

    clientCombo = new QComboBox;
    l->addWidget(clientCombo);

    QToolBar* currentMissionPlanToolBar = new QToolBar;
    l->addWidget(currentMissionPlanToolBar);

    playBufferPlayingButton = new QToolButton;
    playBufferPlayingButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/player_play.svg"));
    currentMissionPlanToolBar->addWidget(playBufferPlayingButton);
    currentMissionPlanToolBar->addSeparator();
    currentMissionPlantLabel = new QLabel;
    currentMissionPlantLabel->setAlignment(Qt::AlignCenter);
    currentMissionPlantLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    currentMissionPlanToolBar->addWidget(currentMissionPlantLabel);
    QToolButton* cancelCurrentMissionPlanButton = new QToolButton;
    cancelCurrentMissionPlanButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/cancel.svg"));
    currentMissionPlanToolBar->addWidget(cancelCurrentMissionPlanButton);

    missionBufferList = new QListWidget;
    l->addWidget(missionBufferList);

    QToolBar* bufferToolBar = new QToolBar;
    l->addWidget(bufferToolBar);

    QToolButton* playFromBufferButton = new QToolButton;
    playFromBufferButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/player_play.svg"));
    bufferToolBar->addWidget(playFromBufferButton);
    QToolButton* topButton = new QToolButton;
    topButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/top.svg"));
    bufferToolBar->addWidget(topButton);
    QToolButton* upButton = new QToolButton;
    upButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/up.svg"));
    bufferToolBar->addWidget(upButton);
    QToolButton* downButton = new QToolButton;
    downButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/down.svg"));
    bufferToolBar->addWidget(downButton);
    QToolButton* bottomButton = new QToolButton;
    bottomButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/bottom.svg"));
    bufferToolBar->addWidget(bottomButton);
    QToolButton* removeButton = new QToolButton;
    removeButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/remove.svg"));
    bufferToolBar->addWidget(removeButton);
    missionPlansCombo = new QComboBox;
    missionPlansCombo->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    bufferToolBar->addWidget(missionPlansCombo);
    QToolButton* addButton = new QToolButton;
    addButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/add.svg"));
    bufferToolBar->addWidget(addButton);

    connect(this, SIGNAL(SignalAddClient(Client*)), this, SLOT(SlotAddClient(Client*)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(SignalRemoveClient(Client*)), this, SLOT(SlotRemoveClient(Client*)), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(SignalUpdateMissionPlanList()), this, SLOT(SlotUpdateMissionPlanList()), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(SignalUpdateCurrentMissionPlan()), this, SLOT(SlotUpdateCurrentMissionPlan()), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(SignalUpdateMissionBuffer()), this, SLOT(SlotUpdateMissionBuffer()), Qt::BlockingQueuedConnection);
    connect(this, SIGNAL(SignalBufferPlaying(bool)), this, SLOT(SlotBufferPlaying(bool)), Qt::BlockingQueuedConnection);
    connect(clientCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(SlotClientChanged(int)));
    connect(cancelCurrentMissionPlanButton, SIGNAL(clicked()), this, SLOT(SlotCancelCurrentMissionPlan()));
    connect(playBufferPlayingButton, SIGNAL(clicked()), this, SLOT(SlotBufferPlaying()));
    connect(playFromBufferButton, SIGNAL(clicked()), this, SLOT(SlotPlayFromBuffer()));
    connect(topButton, SIGNAL(clicked()), this, SLOT(SlotTop()));
    connect(upButton, SIGNAL(clicked()), this, SLOT(SlotUp()));
    connect(downButton, SIGNAL(clicked()), this, SLOT(SlotDown()));
    connect(bottomButton, SIGNAL(clicked()), this, SLOT(SlotBottom()));
    connect(removeButton, SIGNAL(clicked()), this, SLOT(SlotRemove()));
    connect(addButton, SIGNAL(clicked()), this, SLOT(SlotAdd()));
}


void mission_control_client::ViewerQWidget::Init()
{
    clientCombo->addItem("Client");
    auto newFont = QFont("FontFamily",-1, -1, true);
    clientCombo->setItemData(0, newFont, Qt::FontRole);

    auto list = GetListOfComponents<Viewer, Client>();
    for (auto c : (*list))
        SlotAddClient(c);
}


void mission_control_client::ViewerQWidget::TreatAddClient(mission_control_client::Client* c)
{
    emit SignalAddClient(c);
}


void mission_control_client::ViewerQWidget::TreatRemoveClient(mission_control_client::Client* c)
{
    emit SignalRemoveClient(c);
}


void mission_control_client::ViewerQWidget::UpdateMissionPlanList(Client* c)
{
    if (c == dynamic_cast<Client*>(currentClient))
        emit SignalUpdateMissionPlanList();
}


void mission_control_client::ViewerQWidget::UpdateCurrentMissionPlan(Client* c)
{
    if (c == dynamic_cast<Client*>(currentClient))
        emit SignalUpdateCurrentMissionPlan();
}


void mission_control_client::ViewerQWidget::UpdateBuffer(Client* c)
{
    if (c == dynamic_cast<Client*>(currentClient))
        emit SignalUpdateMissionBuffer();
}


void mission_control_client::ViewerQWidget::UpdateBufferPlaying(Client* c, bool play)
{
    if (c == dynamic_cast<Client*>(currentClient))
        emit SignalBufferPlaying(play);
}


void mission_control_client::ViewerQWidget::SlotAddClient(mission_control_client::Client* c)
{
    int index = 1;
    for (int i = 1; i <= clientCombo->count(); i++)
    {
        if (!(clientCombo->itemText(i).toStdString() < c->name))
            break;
        index++;
    }
    clientCombo->insertItem(index, QString(c->name.c_str()));
}


void mission_control_client::ViewerQWidget::SlotRemoveClient(mission_control_client::Client* c)
{
    int index = clientCombo->findText(QString(c->name.c_str()));
    if (index != -1)
        clientCombo->removeItem(index);
}


void mission_control_client::ViewerQWidget::SlotUpdateMissionPlanList()
{
    missionPlansCombo->clear();
    auto missionPlanList = currentClient->GetMissionPlanList();
    for (auto mP : missionPlanList)
        missionPlansCombo->addItem(QString(mP.c_str()));
}


void mission_control_client::ViewerQWidget::SlotUpdateCurrentMissionPlan()
{
    currentMissionPlantLabel->setText(QString(currentClient->GetCurrentMissionPlan().c_str()));
}


void mission_control_client::ViewerQWidget::SlotUpdateMissionBuffer()
{
    missionBufferList->clear();
    std::vector<MissionPlanOnBuffer> temp = currentClient->GetBuffer();
    for (unsigned int i = 0; i < temp.size(); i++)
        missionBufferList->addItem(QString(temp[i].missionPlanName.c_str()));
}


void mission_control_client::ViewerQWidget::SlotBufferPlaying(bool play)
{
    playPauseBuffer = play;
    if (playPauseBuffer)
        playBufferPlayingButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/player_pause.svg"));
    else
        playBufferPlayingButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/player_play.svg"));
}


void mission_control_client::ViewerQWidget::SlotClientChanged(int index)
{
    Client* oldCurrentClient = currentClient;
    std::string client = clientCombo->currentText().toStdString();
    if (index > 0)
    {
        auto list = GetListOfComponents<Viewer, Client>();
        for (auto c : (*list))
        {
            if (c->name == client)
                currentClient = c;
        }
    }
    else
        currentClient = NULL;

    if (currentClient != oldCurrentClient)
    {
        SlotBufferPlaying(false);
        if (currentClient == NULL)
        {
            currentMissionPlantLabel->clear();
            missionPlansCombo->clear();
            missionBufferList->clear();
        }
        else
        {
            SlotUpdateMissionPlanList();
            SlotUpdateCurrentMissionPlan();
            SlotUpdateMissionBuffer();
        }
    }
}


void mission_control_client::ViewerQWidget::SlotCancelCurrentMissionPlan()
{
    if (currentClient != NULL)
        currentClient->CancelCurrentMissionOnBuffer();
}


void mission_control_client::ViewerQWidget::SlotBufferPlaying()
{
    if (currentClient != NULL)
        currentClient->MissionBufferPlay(!playPauseBuffer);
}


void mission_control_client::ViewerQWidget::SlotPlayFromBuffer()
{
    if (currentClient != NULL && missionBufferList->currentRow() != -1)
        currentClient->PlayMissionOnBuffer(missionBufferList->currentRow());
}


void mission_control_client::ViewerQWidget::SlotTop()
{
    if (currentClient != NULL && missionBufferList->count() > 1 && missionBufferList->currentRow() > 0)
        currentClient->MissionMovementOnBuffer(missionBufferList->currentRow(), -missionBufferList->currentRow());
}


void mission_control_client::ViewerQWidget::SlotUp()
{
    if (currentClient != NULL && missionBufferList->count() > 1 && missionBufferList->currentRow() > 0)
        currentClient->MissionMovementOnBuffer(missionBufferList->currentRow(), -1);
}


void mission_control_client::ViewerQWidget::SlotDown()
{
    if (currentClient != NULL && missionBufferList->count() > 1 && missionBufferList->currentRow() < missionBufferList->count() - 1)
        currentClient->MissionMovementOnBuffer(missionBufferList->currentRow(), 1);
}


void mission_control_client::ViewerQWidget::SlotBottom()
{
    if (currentClient != NULL && missionBufferList->count() > 1 && missionBufferList->currentRow() < missionBufferList->count() - 1)
        currentClient->MissionMovementOnBuffer(missionBufferList->currentRow(), missionBufferList->count() - missionBufferList->currentRow() - 1);
}


void mission_control_client::ViewerQWidget::SlotRemove()
{
    if (currentClient != NULL && missionBufferList->count() >= 1 && missionBufferList->currentRow() != -1)
        currentClient->MissionRemoveOnBuffer(missionBufferList->currentRow());
}


void mission_control_client::ViewerQWidget::SlotAdd()
{
    if (currentClient != NULL && missionPlansCombo->currentIndex() != -1)
        currentClient->MissionAddOnBuffer(missionBufferList->currentRow() + 1, missionPlansCombo->currentText().toStdString());
}
