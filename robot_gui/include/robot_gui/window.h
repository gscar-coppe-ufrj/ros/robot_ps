// window.h



#ifndef ROBOT_GUI_WINDOW_H
#define ROBOT_GUI_WINDOW_H


#include <deque>
#include <QMainWindow>
#include <QLabel>
#include <QToolBar>
#include <QToolButton>
#include <QMenuBar>
#include <QTimer>
#include <QAction>
#include "robot_core_gui.h"
#include "dock_tool.h"



namespace robot_gui {



class RobotCoreGUI;



class Window : public QMainWindow
{
    Q_OBJECT

    QString name;

    QMenu* menuWindows;
    QMenu* menuLayout;
    QAction* actionSaveLayout;
    std::deque<QAction*> actionsOfMenuWindows;
    std::deque<QAction*> actionsOfMenuLayouts;
    std::deque<Window*> windowsOfMenuWindows;
    std::deque<QToolButton*> listButtons;
    QTimer timerShortcuts;
    QToolBar* toolsToolBar;
    QLabel* canvas;

    bool displayShortcuts, displayNames;
    int toolNumber;

    DockTool* NewDockTool(QString name, int id);

    void ConfigMenuBar();
    void HideShortcuts();

    void SaveSettings(QString organization, QString application, bool useApplicationName = false);
    void LoadSettings(QString organization, QString application, bool useApplicationName = false);

    // these are QMainWindow methods been redefined
    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *);
    void paintEvent(QPaintEvent *);
    void changeEvent(QEvent *);
    void resizeEvent(QResizeEvent *);
    void focusOutEvent(QFocusEvent *);
    void moveEvent(QMoveEvent *);
    bool eventFilter(QObject *, QEvent *);

public:
    RobotCoreGUI* robotCoreGUI;
    std::deque<DockTool*> dockTools;

    QString currentSelectedLayout;

    explicit Window(QWidget* parent = 0);

    void ResetToolBar();
    void AddActionToToolBar(std::string name, std::string icon, std::string description);

    /*! \brief Checks if any of this Window children has the given name.
     */
    bool HasName(const std::string & name);

    /*! \brief Returns a deque containing the names of all of this Window's children.
     */
    std::deque<std::string> FindNames();

    void SetName(std::string newName);

    void LoadSettings();
    void SaveSettings();

    void WindowsList(std::deque<std::string> list);
    void LayoutsList(std::deque<std::string> list);

    ~Window();

private slots:
    void ExitFunc();
    void NewToolClicked(const QString & name);
    void NewWindowClicked();
    void RenameWindowClicked();
    void ConfigureToolsClicked();
    void ConfigureLaunchersClicked();
    void SaveClicked();
    void SaveAsClicked();
    void DeleteClicked();
    void WindowClicked(const QString & name);
    void LayoutClicked(const QString & name);
    void ShowShortcuts();

public slots:
    void SelectWindow();
    void DockToolClose(QObject* dockTool);
};



}



#endif // WINDOW_H
