// docktool.h



#ifndef ROBOT_GUI_DOCK_TOOL_H
#define ROBOT_GUI_DOCK_TOOL_H


#include "savable.h"
#include <QDockWidget>
#include <robot_ps_core/tool.h>



namespace robot_gui {



class DockTool : public QDockWidget, public Savable
{
Q_OBJECT

    void closeEvent(QCloseEvent* event);

public:
    QString name;
    robot_ps::Tool* tool;

    DockTool(const QString & title, QWidget* parent = 0, Qt::WindowFlags flags = 0);
    DockTool(QWidget* parent = 0, Qt::WindowFlags flags = 0);

    void SaveCustom();
    void LoadCustom();

signals:
    void Close(QObject* dockTool);
};



}



#endif // DOCK_TOOL_H
