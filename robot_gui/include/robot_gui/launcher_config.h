// launcher_config.h



#ifndef ROBOT_GUI_LAUNCHER_CONFIG_H
#define ROBOT_GUI_LAUNCHER_CONFIG_H


#include <string>
#include <deque>
#include <tuple>
#include <QDialog>
#include <QListWidget>
#include <QToolButton>
#include <QLabel>



namespace robot_gui {



#define LAUNCHER_CONFIG_PLAY_ICON "/usr/share/icons/Humanity/actions/24/player_play.svg"
#define LAUNCHER_CONFIG_STOP_ICON "/usr/share/icons/Humanity/actions/24/player_stop.svg"



class LauncherConfig : public QDialog
{
Q_OBJECT
    std::deque<QString> launcherInstanceNames;
    std::deque<QString> launcherNames;
    std::deque<QString> launcherInstanceArgs;
    std::deque<bool> launcherInstancePlaying;
    QListWidget* launchers;
    QListWidget* launcherInstances;
    QToolButton* newLauncher;
    QToolButton* deleteLauncher;
    QToolButton* newLauncherInstance;
    QToolButton* deleteLauncherInstance;
    QToolButton* playLauncherInstance;
    QLineEdit* instanceName;
    QLabel* instanceLauncher;
    QLineEdit* instanceArguments;
    QString lastFolder;

public:
    LauncherConfig(QWidget *parent = 0, Qt::WindowFlags f = 0);
    void Config(std::deque<std::string> launcherList, std::deque<std::tuple<std::string, std::string, std::string, bool>> launcherInstanceList);

signals:
    void LaunchersList(std::deque<std::string> newList);
    void NewLauncherInstance(std::string launcherInstance, std::string launcher, std::string launcherInstanceArgs);
    void DeleteLauncherInstance(std::string launcherInstance);
    void PlayStopLauncherInstance(std::string launcherInstance);

private slots:
    void NewLauncher();
    void DeleteLauncher();
    void NewLauncherInstance();
    void DeleteLauncherInstance();
    void PlayStopLauncherInstance();
    void LauncherSelectionChanged(int currentRow);
    void LauncherInstanceSelectionChanged(int currentRow);

public slots:
    void LauncherInstancePlaying(std::string launcherInstance, bool playing);
};



}



#endif // LAUNCHER_CONFIG_H
