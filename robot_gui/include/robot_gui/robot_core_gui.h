// robot_core_gui.h



#ifndef ROBOT_GUI_ROBOT_CORE_GUI
#define ROBOT_GUI_ROBOT_CORE_GUI



#include <QObject>
#include <QDialog>
#include <robot_ps_core/robot_core.h>
#include "window.h"
#include "launcher_config.h"



namespace robot_gui {



class Window;


/*!
 * \brief The RobotCoreGUI class implements the graphical user interface that
 *  interacts with RobotCore methods.
 */

class RobotCoreGUI : public QObject, public robot_ps::RobotCore
{
    Q_OBJECT

    bool saved;

    std::deque<Window*> windows;
    std::deque<std::string> layoutsNames;

    void SetToolBarOnWindow(Window* w);

    bool ToolNameAvailable(std::string name);
    bool WindowNameAvailable(std::string name);
    std::string GetNextToolName();
    std::string GetNextNewWindowName();

    void UpdateWindowsNameList();
    void UpdateLayoutsList();

    /*!
     * \brief LoadSettings loads settings using QSettings from the file
     * ~/.config/robot_gui/robot_gui.conf .
     * Loads from previos section:
     * List of tools avaliable through pluginlib.
     * List of layouts
     * Windows opened
     * Launch files added
     * Launcher instances created
     */
    void LoadSettings();
    void SaveSettings();

protected:
    void Shutdown();

public:
    /*!
     * \brief RobotCoreGUI contructor calls LoadSettings.
     * \param argc
     * \param argv
     */
    RobotCoreGUI(int argc, char *argv[]);

    virtual void UpdateToolList(std::deque<std::string> newList);

    void NewWindow(std::string name = "", std::string layout = "");
    void DeleteWindow(Window * w);

    void RaiseWindow(std::string name = "");
    void RenameWindow(Window* w, std::string name = "");

    std::deque<std::string> LayoutList();
    void SaveLayoutAs(std::string name = "");
    void DeleteLayout(std::string name = "");

    /*!
     * \brief ConfigureTools constructs and shows a QDialog to configure tools.
     * It uses a QTreeWidgetItem and gets a list of available tools from
     * GetDeclaredToolsList(). Tools currently selected are checked. After
     * ok pressed calls UpdateToolList with the new list of selected tools.
     * \param dialog QDialog to be filled.
     */
    void ConfigureTools(QDialog* dialog);
    void ConfigureLaunchers(robot_gui::LauncherConfig* dialog);

    ~RobotCoreGUI();

signals:
    void LauncherInstancePlaying(std::string launcherInstance, bool playing);

private slots:
    void LaunchersList(std::deque<std::string> newList);
    void NewLauncherInstance(std::string launcherInstance, std::string launcher, std::string args);
    void DeleteLauncherInstance(std::string launcherInstance);
    void PlayStopLauncherInstance(std::string launcherInstance);
};



}



#endif // ROBOT_CORE_GUI
