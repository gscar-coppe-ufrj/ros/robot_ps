// savable.h



#ifndef ROBOT_GUI_SAVABLE_H
#define ROBOT_GUI_SAVABLE_H



#include <QWidget>
#include <QSettings>
#include <QString>



namespace robot_gui {



class Savable
{
private:
    static const char *NAME_OBJ_NAME, *NAME_SIZE, *NAME_GEOMETRY, *NAME_FONT_SIZE, *NAME_LAYOUT_DIREC, *NAME_STYLE_SHEET,
    *NAME_STATUS_TIP, *NAME_HIDDEN, *NAME_ENABLED, *NAME_FOCUS_POLICY, *NAME_TEXT, *NAME_ECHO_MODE, *NAME_TAB_INDEX,
    *NAME_CHECKED, *NAME_CHECKABLE, *NAME_DOWN, *NAME_MAXIMUM, *NAME_MINIMUM, *NAME_VALUE, *NAME_CONTENTS,
    *NAME_DOCKED, *NAME_FLOAT_POS, *NAME_COMBO_SELECTION;

    bool saveAll;

    std::vector<std::string> forbiddenNames;

    QSettings* settings;
    QWidget* root;

    void LoadSettings(QWidget* widget);
    void SaveSettings(QWidget* widget);

    void ForceGroup(const std::string & path);
    void ForceEndGroup();

    const std::string GetPath(const QWidget* widget);
    std::string GetName(const QObject* widget);

public:
    Savable(QWidget* rootWidget);
    Savable(const QString & orgName, const QString & appName, QWidget* rootWidget);

    void EnableAll(bool flag);

    void loadWidget(QWidget* widget, bool flagIterate = false);
    void SaveWidget(QWidget* widget, bool flagIterate = false);
    void LoadAll();
    void SaveAll();
    void Save();
    void Load();
    void SaveValue(const QString & key, const QVariant & value);
    QVariant LoadValue(const QString & key, const QVariant & defaultValue = QVariant());

    void Forbid(const QWidget * widget);

    void Clear();

    virtual void SaveCustom() = 0;
    virtual void LoadCustom() = 0;

    virtual ~Savable();
};



}



#endif // SAVABLE_H
