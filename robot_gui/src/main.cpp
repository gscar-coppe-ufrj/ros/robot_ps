// main.cpp



#include <signal.h>
#include <QApplication>
#include <robot_gui/robot_core_gui.h>



void mySigintHandler(int sig)
{
    QCoreApplication::quit();
}


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    robot_gui::RobotCoreGUI core(argc, argv);

    signal(SIGINT, mySigintHandler);

    app.exec();

	return 0;
}
