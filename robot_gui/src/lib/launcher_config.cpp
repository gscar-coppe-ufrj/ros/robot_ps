// launcher_config.cpp



#include <iostream>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QTreeWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QHeaderView>
#include <QSignalMapper>
#include <QFileDialog>
#include <QGroupBox>
#include <robot_gui/launcher_config.h>



robot_gui::LauncherConfig::LauncherConfig(QWidget* parent, Qt::WindowFlags f) : QDialog(parent, f)
{
    setWindowTitle("Configure Launchers");
    setWindowModality(Qt::ApplicationModal);
    launchers = new QListWidget;
    launchers->setWordWrap(true);
    launcherInstances = new QListWidget;
    launcherInstances->setIconSize(QSize(16,16));
    QHBoxLayout* layoutHer = new QHBoxLayout;
    QHBoxLayout* layoutHorAddRemoveLauncher = new QHBoxLayout;
    QVBoxLayout* layoutVerAddRemoveLauncher = new QVBoxLayout;
    QHBoxLayout* layoutHorAddRemoveLauncherInstance = new QHBoxLayout;
    QVBoxLayout* layoutVerAddRemoveLauncherInstance = new QVBoxLayout;
    QVBoxLayout* layoutVerAddRemoveLauncherInstance2 = new QVBoxLayout;
    QGridLayout* layoutGridAddRemoveLauncherInstance = new QGridLayout;
    QGroupBox* groupBoxLauncher = new QGroupBox("Launchers");
    QGroupBox* groupBoxLauncherInstance = new QGroupBox("Instances");
    newLauncher = new QToolButton();
    newLauncher->setIcon(QIcon("/usr/share/icons/Humanity/actions/32/edit-add.svg"));
    newLauncher->setFixedSize(35, 35);
    deleteLauncher = new QToolButton();
    deleteLauncher->setDisabled(true);
    deleteLauncher->setIcon(QIcon("/usr/share/icons/Humanity/actions/32/edit-delete.svg"));
    deleteLauncher->setFixedSize(35, 35);
    newLauncherInstance = new QToolButton();
    newLauncherInstance->setDisabled(true);
    newLauncherInstance->setIcon(QIcon("/usr/share/icons/Humanity/actions/32/edit-add.svg"));
    newLauncherInstance->setFixedSize(35, 35);
    deleteLauncherInstance = new QToolButton();
    deleteLauncherInstance->setDisabled(true);
    deleteLauncherInstance->setIcon(QIcon("/usr/share/icons/Humanity/actions/32/edit-delete.svg"));
    deleteLauncherInstance->setFixedSize(35, 35);
    playLauncherInstance = new QToolButton();
    playLauncherInstance->hide();
    playLauncherInstance->setFixedSize(35, 35);
    instanceName = new QLineEdit;
    instanceLauncher = new QLabel;
    instanceLauncher->setWordWrap(true);
    instanceArguments = new QLineEdit;
    lastFolder = "/home";

    layoutHorAddRemoveLauncher->addLayout(layoutVerAddRemoveLauncher);
    layoutVerAddRemoveLauncher->addWidget(newLauncher, Qt::AlignTop);
    layoutVerAddRemoveLauncher->addWidget(deleteLauncher, Qt::AlignTop);
    layoutVerAddRemoveLauncher->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding));
    layoutHorAddRemoveLauncher->addWidget(launchers, 1);
    layoutVerAddRemoveLauncherInstance->addLayout(layoutHorAddRemoveLauncherInstance);
    layoutHorAddRemoveLauncherInstance->addLayout(layoutVerAddRemoveLauncherInstance2);
    layoutVerAddRemoveLauncherInstance2->addWidget(newLauncherInstance, Qt::AlignTop);
    layoutVerAddRemoveLauncherInstance2->addWidget(deleteLauncherInstance, Qt::AlignTop);
    layoutVerAddRemoveLauncherInstance2->addWidget(playLauncherInstance, Qt::AlignTop);
    layoutVerAddRemoveLauncherInstance2->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding));
    layoutHorAddRemoveLauncherInstance->addWidget(launcherInstances, 1);
    layoutGridAddRemoveLauncherInstance->addWidget(new QLabel("Name"), 0, 0);
    layoutGridAddRemoveLauncherInstance->addWidget(instanceName, 0, 1);
    layoutGridAddRemoveLauncherInstance->addWidget(new QLabel("Launcher"), 1, 0);
    layoutGridAddRemoveLauncherInstance->addWidget(instanceLauncher, 1, 1);
    layoutGridAddRemoveLauncherInstance->addWidget(new QLabel("Arguments"), 2, 0);
    layoutGridAddRemoveLauncherInstance->addWidget(instanceArguments, 2, 1);
    layoutVerAddRemoveLauncherInstance->addLayout(layoutGridAddRemoveLauncherInstance);
    layoutHer->addWidget(groupBoxLauncher, 3);
    layoutHer->addWidget(groupBoxLauncherInstance, 2);
    groupBoxLauncher->setLayout(layoutHorAddRemoveLauncher);
    groupBoxLauncherInstance->setLayout(layoutVerAddRemoveLauncherInstance);
    setLayout(layoutHer);

    QObject::connect(newLauncher, SIGNAL(clicked()), this, SLOT(NewLauncher()));
    QObject::connect(deleteLauncher, SIGNAL(clicked()), this, SLOT(DeleteLauncher()));
    QObject::connect(newLauncherInstance, SIGNAL(clicked()), this, SLOT(NewLauncherInstance()));
    QObject::connect(deleteLauncherInstance, SIGNAL(clicked()), this, SLOT(DeleteLauncherInstance()));
    QObject::connect(playLauncherInstance, SIGNAL(clicked()), this, SLOT(PlayStopLauncherInstance()));
    QObject::connect(launchers, SIGNAL(currentRowChanged(int)), this, SLOT(LauncherSelectionChanged(int)));
    QObject::connect(launcherInstances, SIGNAL(currentRowChanged(int)), this, SLOT(LauncherInstanceSelectionChanged(int)));
    //QObject::connect(launcherInstances, SIGNAL(currentRowChanged(int)), this, SLOT(LauncherInstanceSelectionChanged(int)));
}


void robot_gui::LauncherConfig::Config(std::deque<std::string> launcherList, std::deque<std::tuple<std::string, std::string, std::string, bool>> launcherInstanceList)
{
    for (auto i = launcherList.begin(); i != launcherList.end(); i++)
        launchers->addItem(QString::fromStdString(*i));

    for (auto i = launcherInstanceList.begin(); i != launcherInstanceList.end(); i++)
    {
        launcherInstanceNames.push_back(QString::fromStdString(std::get<0>(*i)));
        launcherInstances->addItem(QString::fromStdString(std::get<0>(*i)));
        QListWidgetItem* item = launcherInstances->findItems(QString::fromStdString(std::get<0>(*i)),Qt::MatchExactly).front();
        if (std::get<3>(*i))
            item->setIcon(QIcon(LAUNCHER_CONFIG_PLAY_ICON));
        else
            item->setIcon(QIcon(LAUNCHER_CONFIG_STOP_ICON));
        launcherNames.push_back(QString::fromStdString(std::get<1>(*i)));
        launcherInstanceArgs.push_back(QString::fromStdString(std::get<2>(*i)));
        launcherInstancePlaying.push_back(std::get<3>(*i));
    }
}


void robot_gui::LauncherConfig::NewLauncher()
{
    std::cout << launchers->count() << std::endl;
    QString file = QFileDialog::getOpenFileName(this, tr("Open file"), lastFolder, "(*.launch)");
    lastFolder = file.left(file.lastIndexOf("/"));
    if (file != "")
    {
        auto found = launchers->findItems(file, Qt::MatchFlag::MatchExactly);
        if (found.size() == 0)
        {
            launchers->addItem(file);

            std::deque<std::string> newList;
            for (auto i = 0; i != launchers->count(); i++)
                newList.push_back(launchers->item(i)->text().toStdString());
            emit LaunchersList(newList);
        }
    }
}


void robot_gui::LauncherConfig::DeleteLauncher()
{
    launchers->takeItem(launchers->currentRow());

    std::deque<std::string> newList;
    for (auto i = 0; i != launchers->count(); i++)
        newList.push_back(launchers->item(i)->text().toStdString());
    emit LaunchersList(newList);
}


void robot_gui::LauncherConfig::NewLauncherInstance()
{
    auto found = std::find(launcherInstanceNames.begin(), launcherInstanceNames.end(), instanceName->text());
    bool differentName = found == launcherInstanceNames.end();

    if (differentName && instanceName->text() != "" && launchers->selectedItems().size() > 0)
    {
        launcherInstances->addItem(instanceName->text());
        launcherInstances->item(launcherInstances->count() - 1)->setIcon(QIcon(LAUNCHER_CONFIG_STOP_ICON));
        launcherInstanceNames.push_back(instanceName->text());
        launcherNames.push_back(launchers->selectedItems().front()->text());
        launcherInstanceArgs.push_back(instanceArguments->text());
        launcherInstancePlaying.push_back(false);

        emit NewLauncherInstance(launcherInstanceNames.back().toStdString(), launchers->currentItem()->text().toStdString(), launcherInstanceArgs.back().toStdString());
    }
}


void robot_gui::LauncherConfig::DeleteLauncherInstance()
{
    if (launcherInstances->selectedItems().size() > 0)
    {
        int i = launcherInstances->currentRow();

        emit DeleteLauncherInstance(launcherInstanceNames[i].toStdString());

        launcherInstances->takeItem(i);
        launcherInstanceNames.erase(i + launcherInstanceNames.begin());
        launcherNames.erase(i + launcherNames.begin());
        launcherInstanceArgs.erase(i + launcherInstanceArgs.begin());
        launcherInstancePlaying.erase(i + launcherInstancePlaying.begin());
    }
}


void robot_gui::LauncherConfig::PlayStopLauncherInstance()
{
    emit PlayStopLauncherInstance((*launcherInstances->selectedItems().begin())->text().toStdString());
}


void robot_gui::LauncherConfig::LauncherInstancePlaying(std::string launcherInstance, bool playing)
{
    auto found = std::find(launcherInstanceNames.begin(), launcherInstanceNames.end(), QString::fromStdString(launcherInstance));

    if (found != launcherInstanceNames.end())
    {
        int i = found - launcherInstanceNames.begin();
        launcherInstancePlaying[i] = playing;
        QListWidgetItem* item = launcherInstances->findItems(QString::fromStdString(launcherInstance),Qt::MatchExactly).front();
        if (playing)
            item->setIcon(QIcon(LAUNCHER_CONFIG_PLAY_ICON));
        else
            item->setIcon(QIcon(LAUNCHER_CONFIG_STOP_ICON));
        if (launcherInstances->selectedItems().front()->text().toStdString() == launcherInstance)
        {
            if (playing)
                playLauncherInstance->setIcon(QIcon(LAUNCHER_CONFIG_STOP_ICON));
            else
                playLauncherInstance->setIcon(QIcon(LAUNCHER_CONFIG_PLAY_ICON));
        }
    }
}


void robot_gui::LauncherConfig::LauncherSelectionChanged(int currentRow)
{
    if (currentRow < 0)
    {
        newLauncherInstance->setEnabled(false);
        deleteLauncher->setEnabled(false);
        instanceLauncher->setText("");
    }
    else
    {
        newLauncherInstance->setEnabled(true);
        deleteLauncher->setEnabled(true);
        instanceLauncher->setText(launchers->item(currentRow)->text());
    }
}


void robot_gui::LauncherConfig::LauncherInstanceSelectionChanged(int currentRow)
{
    if (currentRow < 0)
    {
        deleteLauncherInstance->setEnabled(false);
        playLauncherInstance->hide();
        instanceName->setText("");
        instanceLauncher->setText("");
        instanceArguments->setText("");
    }
    else
    {
        instanceName->setText(launcherInstanceNames[currentRow]);
        instanceArguments->setText(launcherInstanceArgs[currentRow]);
        deleteLauncherInstance->setEnabled(true);
        if (launcherInstancePlaying[currentRow])
        {
            playLauncherInstance->setIcon(QIcon(LAUNCHER_CONFIG_STOP_ICON));
        }
        else
        {
            playLauncherInstance->setIcon(QIcon(LAUNCHER_CONFIG_PLAY_ICON));
        }
        playLauncherInstance->show();

        auto match = launchers->findItems(launcherNames[currentRow], Qt::MatchExactly);
        if (match.size() > 0)
        {
            launchers->setCurrentItem(match[0]);
            instanceLauncher->setText(match[0]->text());
        }
        else
        {
            if (launchers->selectedItems().size() > 0)
                instanceLauncher->setText(launchers->selectedItems().front()->text());
            else
                instanceLauncher->setText("");
        }
    }
}
