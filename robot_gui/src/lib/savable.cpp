// savable.h



#include <robot_gui/savable.h>



#include <iostream>

#include <QLabel>
#include <QLineEdit>
#include <QAbstractButton>
#include <QAbstractSlider>
#include <QSpinBox>
#include <QTabWidget>
#include <QDockWidget>
#include <QComboBox>



const char* robot_gui::Savable::NAME_OBJ_NAME           = "objName";
const char* robot_gui::Savable::NAME_SIZE               = "size";
const char* robot_gui::Savable::NAME_GEOMETRY           = "geometry";
const char* robot_gui::Savable::NAME_FONT_SIZE          = "fontSize";
const char* robot_gui::Savable::NAME_LAYOUT_DIREC       = "layoutDirection";
const char* robot_gui::Savable::NAME_STYLE_SHEET        = "styleSheet";
const char* robot_gui::Savable::NAME_STATUS_TIP         = "statusTip";
const char* robot_gui::Savable::NAME_HIDDEN             = "hidden";
const char* robot_gui::Savable::NAME_ENABLED            = "enabled";
const char* robot_gui::Savable::NAME_FOCUS_POLICY       = "focusPolicy";
const char* robot_gui::Savable::NAME_TEXT               = "text";
const char* robot_gui::Savable::NAME_ECHO_MODE          = "echoMode";
const char* robot_gui::Savable::NAME_CHECKED            = "checked";
const char* robot_gui::Savable::NAME_CHECKABLE          = "checkable";
const char* robot_gui::Savable::NAME_DOWN               = "down";
const char* robot_gui::Savable::NAME_MAXIMUM            = "maximum";
const char* robot_gui::Savable::NAME_MINIMUM            = "minimum";
const char* robot_gui::Savable::NAME_VALUE              = "value";
const char* robot_gui::Savable::NAME_CONTENTS           = "contentsMargin";
const char* robot_gui::Savable::NAME_TAB_INDEX          = "tabIndex";
const char* robot_gui::Savable::NAME_DOCKED             = "dock";
const char* robot_gui::Savable::NAME_FLOAT_POS          = "floatPos";
const char* robot_gui::Savable::NAME_COMBO_SELECTION    = "comboSelection";



robot_gui::Savable::Savable(QWidget* rootWidget)
{
    settings = new QSettings();
    root = rootWidget;

    saveAll = false;
}


robot_gui::Savable::Savable(const QString & orgName, const QString & appName, QWidget* rootWidget)
{
    settings = new QSettings(orgName, appName);
    root = rootWidget;

    saveAll = false;
}


void robot_gui::Savable::EnableAll(bool flag)
{
    saveAll = flag;
}


void robot_gui::Savable::ForceGroup(const std::string & path)
{
    settings->beginGroup( QString::fromStdString(path)+'/'+root->objectName() );
}


void robot_gui::Savable::ForceEndGroup()
{
    settings->endGroup();
}


const std::string robot_gui::Savable::GetPath(const QWidget* widget)
{
    std::string group = this->GetName(widget);
    if (group.empty())
        return "";

    QObject* parent = widget->parent();
    QObject* parent_root = root;
    std::string parentName;
    while (parent != parent_root)
    {
        parentName = this->GetName(parent);
        if (parentName.empty())
            return NULL;

        group  = parentName + '/' + group;
        parent = parent->parent();
    }
    group = '/' + group;

    return group;
}


std::string robot_gui::Savable::GetName(const QObject* widget)
{
    QString name = widget->objectName();
    name.replace(" ", "_");
    return name.toStdString();
}


void robot_gui::Savable::LoadSettings(QWidget* widget)
{
    QVariant variant;

    // Default loads
    variant = settings->value(NAME_STYLE_SHEET);
    if (settings->contains(NAME_STYLE_SHEET))
    {
        /*
         * This should be the first setting to be loaded, because it may
         * mess with the other.
         */
        widget->setStyleSheet(variant.toString());
    }

    variant = settings->value(NAME_FONT_SIZE);
    if (settings->contains(NAME_FONT_SIZE))
    {
        /*
         * For some reason changing the font is also messing with the ui.
         */
        QFont font = widget->font();
        font.setPointSize( variant.toInt() );
        widget->setFont(font);
    }

    variant = settings->value(NAME_CONTENTS);
    if (settings->contains(NAME_CONTENTS))
    {
        QRect margins = variant.toRect();
        widget->setContentsMargins(margins.left(), margins.top(), margins.width(), margins.height());
    }

    variant = settings->value(NAME_SIZE);
    if (settings->contains(NAME_SIZE))
        widget->resize(variant.toSize());

    variant = settings->value(NAME_GEOMETRY);
    if (settings->contains(NAME_GEOMETRY))
    {
        widget->setGeometry(variant.toRect());
        widget->updateGeometry();
    }

    variant = settings->value(NAME_LAYOUT_DIREC);
    if (settings->contains(NAME_LAYOUT_DIREC))
        widget->setLayoutDirection( static_cast<Qt::LayoutDirection>(variant.toUInt()) );

    variant = settings->value(NAME_STATUS_TIP);
    if (settings->contains(NAME_STATUS_TIP))
        widget->setStatusTip(variant.toString());

    variant = settings->value(NAME_HIDDEN);
    if (settings->contains(NAME_HIDDEN))
        widget->setHidden(variant.toBool());

    variant = settings->value(NAME_ENABLED);
    if (settings->contains(NAME_ENABLED))
        widget->setEnabled(variant.toBool());

    variant = settings->value(NAME_FOCUS_POLICY);
    if (settings->contains(NAME_FOCUS_POLICY))
        widget->setFocusPolicy( static_cast<Qt::FocusPolicy>(variant.toUInt()) );

    // Specific loads
    if ( QLabel *label = dynamic_cast<QLabel*>(widget) )
    {
        variant = settings->value(NAME_TEXT);
        if (settings->contains(NAME_TEXT))
            label->setText(variant.toString());
    }
    else if ( QLineEdit *edit = dynamic_cast<QLineEdit*>(widget) )
    {
        variant = settings->value(NAME_TEXT);
        if (settings->contains(NAME_TEXT))
            edit->setText(variant.toString());

        variant = settings->value(NAME_ECHO_MODE);
        if (settings->contains(NAME_ECHO_MODE))
            edit->setEchoMode( static_cast<QLineEdit::EchoMode>(variant.toUInt()) );
    }
    else if ( QAbstractButton *button = dynamic_cast<QAbstractButton*>(widget) )
    {
        variant = settings->value(NAME_TEXT);
        if (settings->contains(NAME_TEXT))
            button->setText(variant.toString());

        variant = settings->value(NAME_CHECKED);
        if (settings->contains(NAME_CHECKED))
            button->setChecked(variant.toBool());

        variant = settings->value(NAME_CHECKABLE);
        if (settings->contains(NAME_CHECKABLE))
            button->setCheckable(variant.toBool());

        variant = settings->value(NAME_DOWN);
        if (settings->contains(NAME_DOWN))
            button->setDown(variant.toBool());
    }
    else if ( QAbstractSlider *slider = dynamic_cast<QAbstractSlider*>(widget) )
    {
        variant = settings->value(NAME_MAXIMUM);
        if (settings->contains(NAME_MAXIMUM))
            slider->setMaximum(variant.toInt());

        variant = settings->value(NAME_MINIMUM);
        if (settings->contains(NAME_MINIMUM))
            slider->setMinimum(variant.toInt());

        variant = settings->value(NAME_VALUE);
        if (settings->contains(NAME_VALUE))
            slider->setValue(variant.toInt());
    }
    else if ( QSpinBox *spin = dynamic_cast<QSpinBox*>(widget) )
    {
        variant = settings->value(NAME_MAXIMUM);
        if (settings->contains(NAME_MAXIMUM))
            spin->setMaximum(variant.toInt());

        variant = settings->value(NAME_MINIMUM);
        if (settings->contains(NAME_MINIMUM))
            spin->setMinimum(variant.toInt());

        variant = settings->value(NAME_VALUE);
        if (settings->contains(NAME_VALUE))
            spin->setValue(variant.toInt());
    }
    else if ( QTabWidget *tab = dynamic_cast<QTabWidget*>(widget) )
    {
        variant = settings->value(NAME_TAB_INDEX);
        if (settings->contains(NAME_TAB_INDEX))
            tab->setCurrentIndex(variant.toInt());
    }
    else if ( QDockWidget *dock = dynamic_cast<QDockWidget*>(widget) )
    {
        variant = settings->value(NAME_DOCKED);
        if (settings->contains(NAME_DOCKED))
            dock->setFloating(!variant.toBool());
        if (dock->isFloating())
        {
            variant = settings->value(NAME_FLOAT_POS);
            if (settings->contains(NAME_FLOAT_POS))
                dock->move(variant.toPoint());
        }
    }
    else if ( QComboBox * combo = dynamic_cast<QComboBox*>(widget) )
    {
        variant = settings->value(NAME_COMBO_SELECTION);
        if (settings->contains(NAME_COMBO_SELECTION))
            combo->setCurrentIndex(variant.toInt());
    }
}


void robot_gui::Savable::SaveSettings(QWidget* widget)
{
    // Default saves
    settings->setValue(NAME_OBJ_NAME,		widget->objectName());
    settings->setValue(NAME_SIZE,			widget->size());
    settings->setValue(NAME_GEOMETRY,		widget->geometry());
    settings->setValue(NAME_FONT_SIZE,		widget->font().pointSize());
    settings->setValue(NAME_LAYOUT_DIREC,	widget->layoutDirection());
    settings->setValue(NAME_STYLE_SHEET,	widget->styleSheet());
    settings->setValue(NAME_STATUS_TIP,		widget->statusTip());
    settings->setValue(NAME_HIDDEN,			widget->isHidden());
    settings->setValue(NAME_ENABLED,		widget->isEnabled());
    settings->setValue(NAME_FOCUS_POLICY,	widget->focusPolicy());
    int left, top, right, bottom;
    widget->getContentsMargins(&left, &top, &right, &bottom);
    QRect margin(left, top, right, bottom);
    settings->setValue(NAME_CONTENTS,		margin);

    // Specific saves
    if ( QLabel * label = dynamic_cast<QLabel*>(widget) )
    {
        settings->setValue(NAME_TEXT,       label->text());
    }
    else if ( QLineEdit * edit = dynamic_cast<QLineEdit*>(widget) )
    {
        settings->setValue(NAME_TEXT,		edit->text());
        settings->setValue(NAME_ECHO_MODE,	edit->echoMode());
    }
    else if ( QAbstractButton * button = dynamic_cast<QAbstractButton*>(widget) )
    {
        settings->setValue(NAME_TEXT,		button->text());
        settings->setValue(NAME_CHECKED,	button->isChecked());
        settings->setValue(NAME_CHECKABLE,	button->isCheckable());
        settings->setValue(NAME_DOWN,		button->isDown());
    }
    else if ( QAbstractSlider * slider = dynamic_cast<QAbstractSlider*>(widget) )
    {
        settings->setValue(NAME_MAXIMUM,	slider->maximum());
        settings->setValue(NAME_MINIMUM,	slider->minimum());
        settings->setValue(NAME_VALUE,		slider->value());
    }
    else if ( QSpinBox * spin = dynamic_cast<QSpinBox*>(widget) )
    {
        settings->setValue(NAME_MAXIMUM,	spin->maximum());
        settings->setValue(NAME_MINIMUM,	spin->minimum());
        settings->setValue(NAME_VALUE,		spin->value());
    }
    else if ( QTabWidget * tab = dynamic_cast<QTabWidget*>(widget) )
    {
        settings->setValue(NAME_TAB_INDEX,	tab->currentIndex());
    }
    else if ( QDockWidget * dock = dynamic_cast<QDockWidget*>(widget) )
    {
        settings->setValue(NAME_DOCKED,     !dock->isFloating());
        settings->setValue(NAME_FLOAT_POS,  dock->pos());
    }
    else if ( QComboBox * combo = dynamic_cast<QComboBox*>(widget) )
    {
        settings->setValue(NAME_COMBO_SELECTION, combo->currentIndex());
    }
}


void robot_gui::Savable::loadWidget(QWidget* widget, bool flagIterate)
{
    std::string path;
    /*
     * When the widget is not the root, only save it if:
     *		1. it has a name;
     *		2. it has root as ancestor.
     */
    if (widget != root)
    {
        if ( widget->objectName().isEmpty() || !root->isAncestorOf(widget) )
            return;
        else
        {
            path = this->GetPath(widget);
            if (std::find(forbiddenNames.begin(), forbiddenNames.end(), path) != forbiddenNames.end())
                return;
        }
    }
    else
        path = this->GetName(root);

    if (path.empty())
        return;

    settings->beginGroup(path.c_str());


    if (widget != root)
    {
        if (Savable * savable = dynamic_cast<Savable*>(widget))
        {
            savable->ForceGroup( settings->group().toStdString() + path );
            savable->Load();
            savable->ForceEndGroup();
        }
        else
            this->LoadSettings(widget);

        settings->endGroup();
    }
    else
        this->LoadSettings(widget);

    if (!flagIterate)
    {
        if (widget == root)
            settings->endGroup();
        return;
    }

    // Iterate through children
    Savable* savable;
    QList<QWidget*> children = widget->findChildren<QWidget*>();
    if (children.length() > 0)
    {
        foreach(QWidget* child, children)
        {
            if ( child->parentWidget() == widget )
            {
                savable = dynamic_cast<Savable*>(child);
                if (savable)
                {
                    savable->ForceGroup( settings->group().toStdString() + path );
                    savable->Load();
                    savable->ForceEndGroup();
                }
                else
                    this->loadWidget(child, true);
            }
        }
    }

    if (widget == root)
        settings->endGroup();
}


void robot_gui::Savable::SaveWidget(QWidget* widget, bool flagIterate)
{
    std::string path;
    /*
     * When the widget is not the root, only save it if:
     *		1. it has a name;
     *		2. it has root as ancestor.
     */
    if (widget != root)
    {
        if ( widget->objectName().isEmpty() || !root->isAncestorOf(widget) )
            return;
        else
            path = this->GetPath(widget);
        if (std::find(forbiddenNames.begin(), forbiddenNames.end(), path) != forbiddenNames.end())
            return;
    }
    else
        path = this->GetName(root);

    if (path.empty())
        return;

    settings->beginGroup(path.c_str());

    if (widget != root)
    {
        if (Savable * savable = dynamic_cast<Savable*>(widget))
        {
            savable->ForceGroup( settings->group().toStdString() + path );
            savable->Save();
            savable->ForceEndGroup();
        }
        else
            this->SaveSettings(widget);

        settings->endGroup();
    }
    else
        this->SaveSettings(widget);

    if (!flagIterate)
    {
        if (widget == root)
            settings->endGroup();
        return;
    }

    // Iterate through children
    Savable * savable;
    QList<QWidget*> children = widget->findChildren<QWidget*>();
    if (children.length() > 0)
    {
        foreach(QWidget* child, children)
        {
            if ( child->parentWidget() == widget )
            {
                savable = dynamic_cast<Savable*>(child);
                if (savable)
                {
                    savable->ForceGroup( settings->group().toStdString() + path );
                    savable->Save();
                    savable->ForceEndGroup();
                }
                else
                    this->SaveWidget(child, true);
            }
        }
    }

    if (widget == root)
        settings->endGroup();
}


void robot_gui::Savable::LoadAll()
{
    this->loadWidget(root, true);
}


void robot_gui::Savable::SaveAll()
{
    this->SaveWidget(root, true);
}


void robot_gui::Savable::Save()
{
    Clear();
    if (saveAll)
        SaveAll();
    else
        SaveWidget(root, false);
    SaveCustom();
}


void robot_gui::Savable::Load()
{
    LoadAll();
    LoadCustom();
}


void robot_gui::Savable::SaveValue(const QString &key, const QVariant &value)
{
    settings->setValue(key, value);
}


QVariant robot_gui::Savable::LoadValue(const QString &key, const QVariant &defaultValue)
{
    return settings->value(key, defaultValue);
}


void robot_gui::Savable::Forbid(const QWidget * widget)
{
    std::string path = GetPath(widget);
    forbiddenNames.push_back(path.c_str());
}


void robot_gui::Savable::Clear()
{
    std::string name = this->GetName(root);
    if (!name.empty())
        settings->remove( QString::fromStdString(name) );
}


robot_gui::Savable::~Savable()
{
    delete settings;
}
