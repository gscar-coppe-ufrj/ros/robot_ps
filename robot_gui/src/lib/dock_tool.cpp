// docktool.cpp



#include <robot_gui/dock_tool.h>



robot_gui::DockTool::DockTool(const QString& title, QWidget* parent, Qt::WindowFlags flags) :
    QDockWidget(title, parent, flags) ,
    Savable(this)
{
    tool = NULL;
    name = title;
    Savable::EnableAll(true);
}


robot_gui::DockTool::DockTool(QWidget* parent, Qt::WindowFlags flags) :
    QDockWidget(parent, flags) ,
    Savable(this)
{
    tool = NULL;
    Savable::EnableAll(true);
}


void robot_gui::DockTool::closeEvent(QCloseEvent* event)
{
    emit Close(this);
}


void robot_gui::DockTool::SaveCustom()
{
}


void robot_gui::DockTool::LoadCustom()
{
}
