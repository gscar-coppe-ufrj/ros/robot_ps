// robot_core_gui.cpp



#include <tuple>
#include <sstream>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTreeWidget>
#include <QPushButton>
#include <QHeaderView>
#include <QSignalMapper>
#include <QFileDialog>
#include <QListWidget>
#include <QCoreApplication>
#include <robot_gui/robot_core_gui.h>
#include <robot_gui/launcher_config.h>



robot_gui::RobotCoreGUI::RobotCoreGUI(int argc, char *argv[]) : RobotCore(argc, argv)
{
    saved = false;

    LoadSettings();
}


void robot_gui::RobotCoreGUI::UpdateToolList(std::deque<std::string> newList)
{
    auto currentList = GetCurrentToolsList();

    for (auto i = currentList.begin(); i != currentList.end(); i++)
    {
        auto found = std::find(newList.begin(), newList.end(), *i);
        if (found == newList.end())
        {
            for (auto w = windows.begin(); w != windows.end(); w++)
            {
                std::deque<DockTool*> dockToolsToDelete;
                for (auto dT = (*w)->dockTools.begin(); dT != (*w)->dockTools.end(); dT++)
                {
                    if ((*dT)->windowTitle().toStdString() == *i)
                        dockToolsToDelete.push_back(*dT);
                }
                for (auto dT = dockToolsToDelete.begin(); dT != dockToolsToDelete.end(); dT++)
                    (*dT)->close();
            }
        }
    }

    LoadTools(newList);

    for (auto w = windows.begin(); w != windows.end(); w++)
        SetToolBarOnWindow(*w);
}


void robot_gui::RobotCoreGUI::SetToolBarOnWindow(Window *w)
{
    w->ResetToolBar();
    for (unsigned char i = 0; i < toolList.size(); i++)
        w->AddActionToToolBar(toolList[i], toolsIconList[i], toolsDescriptionList[i]);
}


bool robot_gui::RobotCoreGUI::ToolNameAvailable(std::string name)
{
    for (auto iter = windows.begin(); iter != windows.end(); ++iter)
    {
        if ((*iter)->HasName(name))
            return false;
    }
    return true;
}


bool robot_gui::RobotCoreGUI::WindowNameAvailable(std::string name)
{
    for (std::deque<Window*>::iterator w = windows.begin(); w != windows.end(); w++)
    {
        std::string objectname = (*w)->objectName().toUtf8().constData();
        if (name == objectname)
            return false;
    }
    return true;
}


std::string robot_gui::RobotCoreGUI::GetNextToolName()
{
    std::deque<std::string> names;
    std::deque<std::string> others;
    for (auto iter_window = windows.begin(); iter_window != windows.end(); ++iter_window)
    {
        others = (*iter_window)->FindNames();
        names.insert(names.end(), others.begin(), others.end());
    }

    unsigned int i = 1;
    const std::string basename = "Tool";
    std::stringstream stream(basename);
    auto iter_begin = names.begin();
    auto iter_end   = names.end();
    while (true)
    {
        if (std::find(iter_begin, iter_end, stream.str()) == iter_end)
            break;
        stream.str("");
        stream << basename << "_" << i++;
    }
    return stream.str();
}


std::string robot_gui::RobotCoreGUI::GetNextNewWindowName()
{
    std::string wstr = "Robot GUI - ";
    unsigned int i = 1;
    bool stop = false;
    while (true)
    {
        stop = true;
        std::stringstream ss;
        ss << i;
        std::string istr = ss.str();
        for (std::deque<Window*>::iterator w = windows.begin(); w != windows.end(); w++)
        {
            std::string objectname = (*w)->objectName().toUtf8().constData();
            if (wstr + istr == objectname)
            {
                stop = false;
                break;
            }
        }
        if (stop)
            return wstr + istr;
        i++;
    }
}


void robot_gui::RobotCoreGUI::UpdateWindowsNameList()
{
    std::deque<std::string> windowsNameList;
    for (auto win = windows.begin(); win != windows.end(); win++)
        windowsNameList.push_back((*win)->objectName().toStdString());
    for (auto win = windows.begin(); win != windows.end(); win++)
        (*win)->WindowsList(windowsNameList);
}


void robot_gui::RobotCoreGUI::UpdateLayoutsList()
{
    for (auto win = windows.begin(); win != windows.end(); win++)
        (*win)->LayoutsList(layoutsNames);
}


void robot_gui::RobotCoreGUI::NewWindow(std::string name, std::string layout)
{
    std::string newName;
    if (name == "")
        newName = GetNextNewWindowName();
    else
    {
        if (WindowNameAvailable(name))
            newName = name;
        else
            newName = GetNextNewWindowName();
    }

    robot_gui::Window* w = new robot_gui::Window;
    w->SetName(newName);

    w->robotCoreGUI = this;
    windows.push_back(w);
    SetToolBarOnWindow(w);
    w->show();
    w->currentSelectedLayout = QString::fromStdString(layout);
    w->LayoutsList(layoutsNames);

    UpdateWindowsNameList();
}


void robot_gui::RobotCoreGUI::DeleteWindow(robot_gui::Window *w)
{
    auto found = std::find(windows.begin(), windows.end(), w);
    if (found != windows.end())
    {
        if (windows.size() == 1)
            SaveSettings();
        windows.erase(found);
    }

    UpdateWindowsNameList();
}


void robot_gui::RobotCoreGUI::RaiseWindow(std::string name)
{
    for (auto w = windows.begin(); w != windows.end(); w++)
    {
        if ((*w)->objectName() == name.c_str())
        {
            (*w)->SelectWindow();
            break;
        }
    }
}


void robot_gui::RobotCoreGUI::RenameWindow(robot_gui::Window* w, std::string name)
{
    if (WindowNameAvailable(name))
        w->SetName(name);

    UpdateWindowsNameList();
}


std::deque<std::string> robot_gui::RobotCoreGUI::LayoutList()
{
    return layoutsNames;
}


void robot_gui::RobotCoreGUI::SaveLayoutAs(std::string name)
{
    if (name == "")
        return;

    auto found = std::find(layoutsNames.begin(), layoutsNames.end(), name);
    if (found == layoutsNames.end())
        layoutsNames.push_back(name);
    UpdateLayoutsList();
}


void robot_gui::RobotCoreGUI::DeleteLayout(std::string name)
{
    if (name == "")
        return;

    auto found = std::find(layoutsNames.begin(), layoutsNames.end(), name);
    if (found != layoutsNames.end())
        layoutsNames.erase(found);
    UpdateLayoutsList();
}


void robot_gui::RobotCoreGUI::LoadSettings()
{
    QSettings settings("robot_gui", "robot_gui");
    int size;

    // Loading pluginlib list
    int numberOfToolsOnPluginlib = settings.beginReadArray("pluginlib");
    std::deque<std::string> newListForPluginlib;
    for (unsigned char i = 0; i < numberOfToolsOnPluginlib; i++)
    {
        settings.setArrayIndex(i);
        newListForPluginlib.push_back(settings.value("name").toString().toStdString());
    }
    settings.endArray();
    LoadTools(newListForPluginlib);

    // Updating layouts list
    size = settings.beginReadArray("layouts");
    for (int i = 0; i < size; i++)
    {
        settings.setArrayIndex(i);
        std::string name = settings.value("name").toString().toStdString();
        layoutsNames.push_back(name);
    }
    settings.endArray();

    // recreating windows
    size = settings.beginReadArray("windows");
    for (int i = 0; i < size; i++)
    {
        settings.setArrayIndex(i);
        std::string name = settings.value("name").toString().toStdString();
        std::string layout = settings.value("layout").toString().toStdString();
        NewWindow(name, layout);
    }
    settings.endArray();

    // recreating launchers
    size = settings.beginReadArray("launchers");
    for (int i = 0; i < size; i++)
    {
        settings.setArrayIndex(i);
        launchers.push_back(settings.value("name").toString().toStdString());
    }
    settings.endArray();
    size = settings.beginReadArray("launcherInstances");
    for (int i = 0; i < size; i++)
    {
        settings.setArrayIndex(i);
        std::string launcherInstanceName = settings.value("instanceName").toString().toStdString();
        std::string launcherName = settings.value("launcherName").toString().toStdString();
        std::string args = settings.value("args").toString().toStdString();
        launcherInstances.push_back(std::make_tuple(launcherInstanceName, launcherName, args, false));
        bool playing = settings.value("playing").toBool();
        if (playing)
            PlayStopLauncherInstance(launcherInstanceName);
    }
    settings.endArray();

    // Now I can load it internally
    for (auto w = windows.begin(); w != windows.end(); w++)
        (*w)->LoadSettings();

    if (windows.empty())
        NewWindow();
}


void robot_gui::RobotCoreGUI::SaveSettings()
{
    if (!saved)
    {
        QSettings settings("robot_gui", "robot_gui");
        settings.clear();

        // Record the list of windows
        settings.beginWriteArray("windows");
        for (int i = 0; i < windows.size(); i++)
        {
            settings.setArrayIndex(i);
            settings.setValue("name", windows[i]->objectName());
            settings.setValue("layout", windows[i]->currentSelectedLayout);
        }
        settings.endArray();

        // Here we have to save all windows
        for (auto w = windows.begin(); w != windows.end(); w++)
            (*w)->SaveSettings();

        // Now, the layouts that have been configured will be saved
        settings.beginWriteArray("layouts");
        for (int i = 0; i < layoutsNames.size(); i++)
        {
            settings.setArrayIndex(i);
            settings.setValue("name", QString::fromStdString(layoutsNames[i]));
        }
        settings.endArray();

        // Saving pluginlib list
        settings.beginWriteArray("pluginlib");
        std::vector<std::string> currentToolList = GetCurrentToolsList();
        for (unsigned char i = 0; i < currentToolList.size(); i++)
        {
            settings.setArrayIndex(i);
            settings.setValue("name", QString::fromStdString(currentToolList[i]));
        }
        settings.endArray();

        // Saving launchers list
        settings.beginWriteArray("launchers");
        for (unsigned char i = 0; i < launchers.size(); i++)
        {
            settings.setArrayIndex(i);
            settings.setValue("name", QString::fromStdString(launchers[i]));
        }
        settings.endArray();

        // Saving launcher instances list
        settings.beginWriteArray("launcherInstances");
        for (unsigned char i = 0; i < launcherInstances.size(); i++)
        {
            settings.setArrayIndex(i);
            settings.setValue("instanceName", QString::fromStdString(std::get<0>(launcherInstances[i])));
            settings.setValue("launcherName", QString::fromStdString(std::get<1>(launcherInstances[i])));
            settings.setValue("args", QString::fromStdString(std::get<2>(launcherInstances[i])));
            settings.setValue("playing", QString::number(std::get<3>(launcherInstances[i])));
        }
        settings.endArray();

        saved = true;
    }
}


void robot_gui::RobotCoreGUI::Shutdown()
{
    QCoreApplication::quit();
    robot_ps::RobotCore::Shutdown();
}


void robot_gui::RobotCoreGUI::ConfigureTools(QDialog* dialog)
{
    dialog->setWindowTitle("Configure Tools");
    dialog->setWindowModality(Qt::ApplicationModal);
    QTreeWidget* tree = new QTreeWidget;
    QVBoxLayout* layoutVer = new QVBoxLayout;
    QHBoxLayout* layoutHor = new QHBoxLayout;
    QPushButton* ok = new QPushButton("Ok");
    QPushButton* cancel = new QPushButton("Cancel");
    cancel->setAutoDefault(false);
    layoutVer->addWidget(tree, 1);
    layoutVer->addLayout(layoutHor, 0);
    layoutHor->addSpacerItem(new QSpacerItem(0,0,QSizePolicy::Expanding, QSizePolicy::Expanding));
    layoutHor->addWidget(cancel, 0);
    layoutHor->addWidget(ok, 0);
    dialog->setLayout(layoutVer);

    std::vector<std::string> currentTools = GetCurrentToolsList();
    std::vector<std::string> declaredTools = GetDeclaredToolsList();

    tree->setColumnCount(1);
    QList<QTreeWidgetItem*> items;
    for (auto i = declaredTools.begin(); i != declaredTools.end(); i++)
    {
        QTreeWidgetItem* item = new QTreeWidgetItem();
        if (std::find(currentTools.begin(), currentTools.end(), *i) == currentTools.end())
            item->setCheckState(0, Qt::Unchecked);
        else
            item->setCheckState(0, Qt::Checked);
        item->setText(0, QString::fromStdString(*i));
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
        items.append(item);
    }
    tree->insertTopLevelItems(0, items);
    tree->header()->hide();


    QObject::connect(ok, SIGNAL(clicked()), dialog, SLOT(accept()));
    QObject::connect(cancel, SIGNAL(clicked()), dialog, SLOT(reject()));

    dialog->show();
    if (dialog->exec())
    {
        std::deque<std::string> newList;
        for (auto i = items.begin(); i != items.end(); i++)
        {
            QTreeWidgetItem* item = *i;
            if (item->checkState(0) == Qt::Checked)
                newList.push_back(item->text(0).toStdString());
        }
        UpdateToolList(newList);
    }
}


void robot_gui::RobotCoreGUI::ConfigureLaunchers(LauncherConfig* dialog)
{
    dialog->Config(launchers, launcherInstances);

    QObject::connect(dialog, SIGNAL(LaunchersList(std::deque<std::string>)), this, SLOT(LaunchersList(std::deque<std::string>)));
    QObject::connect(dialog, SIGNAL(NewLauncherInstance(std::string, std::string, std::string)), this, SLOT(NewLauncherInstance(std::string, std::string, std::string)));
    QObject::connect(dialog, SIGNAL(DeleteLauncherInstance(std::string)), this, SLOT(DeleteLauncherInstance(std::string)));
    QObject::connect(dialog, SIGNAL(PlayStopLauncherInstance(std::string)), this, SLOT(PlayStopLauncherInstance(std::string)));
    QObject::connect(this, SIGNAL(LauncherInstancePlaying(std::string, bool)), dialog, SLOT(LauncherInstancePlaying(std::string, bool)));

    dialog->show();
    dialog->exec();
}


void robot_gui::RobotCoreGUI::LaunchersList(std::deque<std::string> newList)
{
    launchers = newList;
}


void robot_gui::RobotCoreGUI::NewLauncherInstance(std::string launcherInstance, std::string launcher, std::string args)
{
    bool found = false;
    for (auto i = launcherInstances.begin(); i != launcherInstances.end(); i++)
    {
        if (std::get<0>(*i) == launcherInstance)
        {
            found = true;
            break;
        }
    }

    if (!found)
    {
        launcherInstances.push_back(std::make_tuple(launcherInstance, launcher, args, false));
    }
}


void robot_gui::RobotCoreGUI::DeleteLauncherInstance(std::string launcherInstance)
{
    bool found = false;
    auto i = launcherInstances.begin();
    for (; i != launcherInstances.end(); i++)
    {
        if (std::get<0>(*i) == launcherInstance)
        {
            found = true;
            break;
        }
    }

    if (found)
    {
        int j = i - launcherInstances.begin();
        launcherInstances.erase(i);
    }
}


void robot_gui::RobotCoreGUI::PlayStopLauncherInstance(std::string launcherInstance)
{
    bool found = false;
    auto i = launcherInstances.begin();
    for (; i != launcherInstances.end(); i++)
    {
        if (std::get<0>(*i) == launcherInstance)
        {
            found = true;
            break;
        }
    }

    if (found)
    {
        if (ROSLaunch(i))
        {
            std::get<3>(*i) = !std::get<3>(*i);
            emit LauncherInstancePlaying(launcherInstance, std::get<3>(*i));
        }
    }
}


robot_gui::RobotCoreGUI::~RobotCoreGUI()
{
    if (!windows.empty())
        SaveSettings();

    // Now we can close the windows
    while (!windows.empty())
        delete windows.back();
    windows.clear();
}
