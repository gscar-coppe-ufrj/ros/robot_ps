// window.cpp



#include <QApplication>
#include <QSignalMapper>
#include <QPainter>
#include <QRect>
#include <QKeyEvent>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDialog>
#include <QLineEdit>
#include <QPushButton>
#include <QComboBox>
#include <robot_gui/window.h>
#include <robot_gui/launcher_config.h>



robot_gui::Window::Window(QWidget* parent) : QMainWindow(parent), displayShortcuts(false), displayNames(false), toolNumber(1)
{
    setDockOptions(QMainWindow::AllowNestedDocks | QMainWindow::AllowTabbedDocks);
    setAttribute(Qt::WA_DeleteOnClose);

    ConfigMenuBar();

    toolsToolBar = new QToolBar("Tools");
    toolsToolBar->setObjectName("Tools");
    toolsToolBar->setGeometry(0, 0, 200, 20);
    addToolBar(toolsToolBar);

    timerShortcuts.setInterval(500);
    timerShortcuts.setSingleShot(true);
    QObject::connect(&timerShortcuts, SIGNAL(timeout()), this, SLOT(ShowShortcuts()));

    canvas = new QLabel(this);
    canvas->setMargin(0);
    canvas->setContentsMargins(0, 0, 0, 0);
    canvas->move(0, 0);
    canvas->setFixedSize(0, 0);
    canvas->setAttribute(Qt::WA_TranslucentBackground);
    canvas->setStyleSheet("background: rgba(0, 0, 0, 255)");
    canvas->setFocusPolicy(Qt::NoFocus);

    installEventFilter(this);
}


void robot_gui::Window::ResetToolBar()
{
    toolsToolBar->clear();
}


void robot_gui::Window::ConfigMenuBar()
{
    QAction* configureTools = new QAction("Configure &Tools", this);
    QAction* configureLaunchers = new QAction("Configure &Launchers", this);
    QAction* quit = new QAction("&Quit", this);

    QObject::connect(configureTools, SIGNAL(triggered()), this, SLOT(ConfigureToolsClicked()));
    QObject::connect(configureLaunchers, SIGNAL(triggered()), this, SLOT(ConfigureLaunchersClicked()));
    QObject::connect(quit, SIGNAL(triggered()), this, SLOT(ExitFunc()));

    QMenu* menuFile;
    menuFile = menuBar()->addMenu("&File");
    menuFile->addAction(configureTools);
    menuFile->addAction(configureLaunchers);
    menuFile->addAction(quit);

    QAction* newWindow = new QAction("&New", this);
    QAction* renameWindow = new QAction("&Rename", this);
    QObject::connect(newWindow, SIGNAL(triggered()), this, SLOT(NewWindowClicked()));
    QObject::connect(renameWindow, SIGNAL(triggered()), this, SLOT(RenameWindowClicked()));

    menuWindows = menuBar()->addMenu("&Windows");
    menuWindows->addAction(newWindow);
    menuWindows->addAction(renameWindow);
    menuWindows->addSeparator();

    actionSaveLayout = new QAction("&Save", this);
    actionSaveLayout->setEnabled(false);
    QAction* actionSaveLayoutAs = new QAction("Save &As...", this);
    QAction* actionDeleteLayout = new QAction("&Delete", this);
    QObject::connect(actionSaveLayout, SIGNAL(triggered()), this, SLOT(SaveClicked()));
    QObject::connect(actionSaveLayoutAs, SIGNAL(triggered()), this, SLOT(SaveAsClicked()));
    QObject::connect(actionDeleteLayout, SIGNAL(triggered()), this, SLOT(DeleteClicked()));

    menuLayout = menuBar()->addMenu("&Layout");
    menuLayout->addAction(actionSaveLayout);
    menuLayout->addAction(actionSaveLayoutAs);
    menuLayout->addAction(actionDeleteLayout);
    menuLayout->addSeparator();

    menuBar()->setNativeMenuBar(false);
}


void robot_gui::Window::AddActionToToolBar(std::string name, std::string icon, std::string description)
{
    QAction* action = new QAction(QIcon(icon.c_str()), description.c_str(), NULL);
    toolsToolBar->addAction(action);
    action->setShortcut( QKeySequence( QString::fromStdString("Ctrl+"+std::to_string(toolNumber++)) ) );

    QSignalMapper* signalMapper = new QSignalMapper(this);
    QObject::connect(action, SIGNAL(triggered()), signalMapper, SLOT(map()));
    signalMapper->setMapping(action, name.c_str());
    QObject::connect(signalMapper, SIGNAL(mapped(const QString &)), this, SLOT(NewToolClicked(const QString &)));
}


bool robot_gui::Window::HasName(const std::string & name)
{
    // Now check if any of the dockable tools has the name
    for (auto iter = dockTools.begin(); iter != dockTools.end(); ++iter)
    {
        if ( (*iter)->objectName().toStdString() == name )
            return true;
    }

    // Name was not found
    return false;
}


std::deque<std::string> robot_gui::Window::FindNames()
{
    std::deque<std::string> names;
    for (auto iter = dockTools.begin(); iter != dockTools.end(); ++iter)
        names.push_back( (*iter)->objectName().toStdString() );
    return names;
}


void robot_gui::Window::SetName(std::string newName)
{
    name = QString::fromStdString(newName);
    QString temp;
    if (currentSelectedLayout != "")
        temp = " [" + currentSelectedLayout + "]";
    setWindowTitle(name + temp);
    setObjectName(name);
}


robot_gui::DockTool* robot_gui::Window::NewDockTool(QString name, int id)
{
    robot_ps::Tool* tool = robotCoreGUI->NewTool(name.toStdString());
    QWidget* toolQWidget = dynamic_cast<QWidget*>(tool);
    DockTool* dockTool = NULL;
    if (toolQWidget == NULL)
        robotCoreGUI->DeleteTool(tool);
    else
    {
        dockTool = new DockTool(name, this);
        name = QString::number(id) + "/" + name;
        dockTool->tool = tool;
        dockTool->setContentsMargins(0, 0, 0, 0);
        dockTool->setObjectName(name);
        dockTool->setFloating(true);
        toolQWidget->setParent(dockTool);
        toolQWidget->setObjectName(name);

        QWidget* wid = new QWidget;
        wid->setObjectName("holder");
        wid->setContentsMargins(0, 0, 0, 0);
        dockTool->setWidget(wid);
        QHBoxLayout* horDocklayout = new QHBoxLayout;
        horDocklayout->setSpacing(0);
        horDocklayout->setContentsMargins(0, 0, 0, 0);
        wid->setLayout(horDocklayout);
        horDocklayout->addWidget(toolQWidget);

        tool->Init();

        dockTool->show();
        dockTools.push_back(dockTool);

        QObject::connect(dockTool, SIGNAL(Close(QObject*)), this, SLOT(DockToolClose(QObject*)));
    }

    return dockTool;
}


void robot_gui::Window::LoadSettings(QString organization, QString application, bool useApplicationName)
{
    QSettings settings(organization, application);
    QString name;
    if (useApplicationName)
        name = application + "/";
    else
        name = objectName() + "/";

    int numberOfTools = settings.beginReadArray(name + "tools");
    for (unsigned char i = 0; i < numberOfTools; i++)
    {
        settings.setArrayIndex(i);
        QString name = settings.value("name").toString();
        auto list = robotCoreGUI->GetCurrentToolsList();
        if (std::find(list.begin(), list.end(), name.toStdString()) != list.end())
            NewDockTool(name, i);
    }
    settings.endArray();

    // Now we can restore the dockTool's widgets
    for (auto dockTool = dockTools.begin(); dockTool != dockTools.end(); dockTool++)
    {
        Savable* savable = dynamic_cast<Savable*>(*dockTool);
        if (savable)
            savable->Load();
    }

    // The state of the window is restored after his widgets (dockTools) have already been created and loaded
    if (settings.value(name + "maximized").toBool())
        showMaximized();
    else
        showNormal();
    setGeometry(settings.value(name + "geometry").value<QRect>());
    restoreState(settings.value(name + "windowState").toByteArray());
}


void robot_gui::Window::LoadSettings()
{
    LoadSettings("robot_gui", "robot_gui");
}


void robot_gui::Window::SaveSettings(QString organization, QString application, bool useApplicationName)
{
    QSettings settings(organization, application);
    QString name;
    if (useApplicationName)
    {
        settings.clear();
        name = application + "/";
    }
    else
        name = objectName() + "/";
    settings.setValue(name + "geometry", QVariant(geometry()));
    settings.setValue(name + "windowState", saveState());
    settings.setValue(name + "maximized", this->isMaximized());

    settings.beginWriteArray(name + "tools");
    for (unsigned char i = 0; i < dockTools.size(); i++)
    {
        settings.setArrayIndex(i);
        settings.setValue("name", dockTools[i]->name);
        Savable* savable = dynamic_cast<Savable*>(dockTools[i]);
        if (savable)
            savable->Save();
    }
    settings.endArray();
}


void robot_gui::Window::SaveSettings()
{
    SaveSettings("robot_gui", "robot_gui");
}


void robot_gui::Window::WindowsList(std::deque<std::string> list)
{
    for (auto i = actionsOfMenuWindows.begin(); i != actionsOfMenuWindows.end(); i++)
        delete *i;
    actionsOfMenuWindows.clear();
    for (auto i = list.begin(); i != list.end(); i++)
    {
        QAction* action = new QAction((*i).c_str(), this);
        actionsOfMenuWindows.push_back(action);
        menuWindows->addAction(action);

        QSignalMapper* signalMapper = new QSignalMapper(this);
        QObject::connect(action, SIGNAL(triggered()), signalMapper, SLOT(map()));
        signalMapper->setMapping(action, (*i).c_str());
        QObject::connect(signalMapper, SIGNAL(mapped(const QString &)), this, SLOT(WindowClicked(const QString &)));
    }
}


void robot_gui::Window::LayoutsList(std::deque<std::string> list)
{
    for (auto i = actionsOfMenuLayouts.begin(); i != actionsOfMenuLayouts.end(); i++)
        delete *i;
    actionsOfMenuLayouts.clear();
    for (auto i = list.begin(); i != list.end(); i++)
    {
        QAction* action = new QAction((*i).c_str(), this);
        actionsOfMenuLayouts.push_back(action);
        menuLayout->addAction(action);

        QSignalMapper* signalMapper = new QSignalMapper(this);
        QObject::connect(action, SIGNAL(triggered()), signalMapper, SLOT(map()));
        signalMapper->setMapping(action, (*i).c_str());
        QObject::connect(signalMapper, SIGNAL(mapped(const QString &)), this, SLOT(LayoutClicked(const QString &)));
    }

    auto found = std::find(list.begin(), list.end(), currentSelectedLayout.toStdString());
    if (found == list.end())
    {
        currentSelectedLayout = "";
        actionSaveLayout->setText("&Save");
        actionSaveLayout->setEnabled(false);
    }
    else
    {
        actionSaveLayout->setText("&Save [" + currentSelectedLayout + "]");
        actionSaveLayout->setEnabled(true);
    }
    SetName(name.toStdString());
}


void robot_gui::Window::ExitFunc()
{
    qApp->quit();
}


void robot_gui::Window::NewToolClicked(const QString & name)
{
    NewDockTool(name, dockTools.size());
}


void robot_gui::Window::NewWindowClicked()
{
    robotCoreGUI->NewWindow();
}


void robot_gui::Window::RenameWindowClicked()
{
    QDialog dialog(this);
    dialog.setWindowTitle("Rename Window");
    dialog.setWindowModality(Qt::ApplicationModal);
    QLineEdit* line = new QLineEdit;
    QVBoxLayout* layoutVer = new QVBoxLayout;
    QHBoxLayout* layoutHor = new QHBoxLayout;
    QPushButton* ok = new QPushButton("Ok");
    QPushButton* cancel = new QPushButton("Cancel");
    layoutVer->addWidget(line);
    layoutVer->addLayout(layoutHor);
    layoutHor->addWidget(ok);
    layoutHor->addWidget(cancel);
    dialog.setLayout(layoutVer);

    QObject::connect(ok, SIGNAL(clicked()), &dialog, SLOT(accept()));
    QObject::connect(cancel, SIGNAL(clicked()), &dialog, SLOT(reject()));

    if (dialog.exec())
    {
        if (line->text() != "")
        {
            robotCoreGUI->RenameWindow(this, line->text().toStdString());
        }
    }
}


void robot_gui::Window::ConfigureToolsClicked()
{
    QDialog dialog(this);
    robotCoreGUI->ConfigureTools(&dialog);
}


void robot_gui::Window::ConfigureLaunchersClicked()
{
    LauncherConfig dialog(this);
    robotCoreGUI->ConfigureLaunchers(&dialog);
}


void robot_gui::Window::SaveClicked()
{
    SaveSettings("robot_gui", currentSelectedLayout, true);
}


void robot_gui::Window::SaveAsClicked()
{
    QDialog dialog(this);
    dialog.setWindowTitle("Save Layout As");
    dialog.setWindowModality(Qt::ApplicationModal);
    QLineEdit* line = new QLineEdit;
    QVBoxLayout* layoutVer = new QVBoxLayout;
    QHBoxLayout* layoutHor = new QHBoxLayout;
    QPushButton* ok = new QPushButton("Ok");
    QPushButton* cancel = new QPushButton("Cancel");
    cancel->setAutoDefault(false);
    layoutVer->addWidget(line);
    layoutVer->addLayout(layoutHor);
    layoutHor->addSpacerItem(new QSpacerItem(0,0,QSizePolicy::Expanding, QSizePolicy::Expanding));
    layoutHor->addWidget(cancel, 0);
    layoutHor->addWidget(ok, 0);
    dialog.setLayout(layoutVer);

    QObject::connect(ok, SIGNAL(clicked()), &dialog, SLOT(accept()));
    QObject::connect(cancel, SIGNAL(clicked()), &dialog, SLOT(reject()));

    if (dialog.exec())
    {
        if (line->text() != "")
        {
            currentSelectedLayout = line->text();
            robotCoreGUI->SaveLayoutAs(line->text().toStdString());
            SaveClicked();
        }
    }
}


void robot_gui::Window::DeleteClicked()
{
    QDialog dialog(this);
    dialog.setWindowTitle("Delete Layout");
    dialog.setWindowModality(Qt::ApplicationModal);
    QComboBox* combo = new QComboBox;
    std::deque<std::string> layoutList = robotCoreGUI->LayoutList();
    for (auto l = layoutList.begin(); l != layoutList.end(); l++)
        combo->addItem(QString::fromStdString(*l));
    QVBoxLayout* layoutVer = new QVBoxLayout;
    QHBoxLayout* layoutHor = new QHBoxLayout;
    QPushButton* ok = new QPushButton("Ok");
    QPushButton* cancel = new QPushButton("Cancel");
    cancel->setAutoDefault(false);
    layoutVer->addWidget(combo);
    layoutVer->addLayout(layoutHor);
    layoutHor->addSpacerItem(new QSpacerItem(0,0,QSizePolicy::Expanding, QSizePolicy::Expanding));
    layoutHor->addWidget(cancel, 0);
    layoutHor->addWidget(ok, 0);
    dialog.setLayout(layoutVer);

    QObject::connect(ok, SIGNAL(clicked()), &dialog, SLOT(accept()));
    QObject::connect(cancel, SIGNAL(clicked()), &dialog, SLOT(reject()));

    if (dialog.exec())
    {
        if (currentSelectedLayout == combo->currentText())
            currentSelectedLayout = "";
        robotCoreGUI->DeleteLayout(combo->currentText().toStdString());
    }
}


void robot_gui::Window::WindowClicked(const QString & name)
{
    robotCoreGUI->RaiseWindow(name.toStdString());
}


void robot_gui::Window::LayoutClicked(const QString & name)
{
    while (!dockTools.empty())
        dockTools.back()->close();

    currentSelectedLayout = name;
    actionSaveLayout->setText("&Save [" + currentSelectedLayout + "]");
    actionSaveLayout->setEnabled(true);
    SetName(this->name.toStdString());
    LoadSettings("robot_gui", name, true);
}


void robot_gui::Window::SelectWindow()
{
    raise();
    activateWindow();
}


void robot_gui::Window::HideShortcuts()
{
    timerShortcuts.stop();
    displayShortcuts = displayNames = false;
    listButtons.clear();
    canvas->setFixedSize(0, 0);
    canvas->move(0, 0);
    canvas->lower();
    repaint();
    //
    foreach (QObject * obj, children())
        obj->removeEventFilter(this);
}


void robot_gui::Window::ShowShortcuts()
{
    listButtons.clear();
    //
    QList<QString> names;
    foreach (QAction * action, toolsToolBar->actions())
        names.push_back(action->text());
    //
    foreach (QObject * obj, toolsToolBar->children())
    {
        if (QToolButton * button = dynamic_cast<QToolButton*>(obj))
        {
            if ( names.indexOf(button->toolTip()) != -1 )
                listButtons.push_back(button);
        }
    }
    displayShortcuts = true;
    displayNames = false;
    canvas->setFixedSize(width(), height() - toolsToolBar->height());
    canvas->move(0, toolsToolBar->rect().bottom());
    canvas->raise();
    repaint();
    //
    foreach (QObject * obj, children())
        obj->installEventFilter(this);
}


void robot_gui::Window::keyPressEvent(QKeyEvent * evt)
{
    if (evt->key() == Qt::Key_Control)
    {
        timerShortcuts.start();
        setFocus(Qt::OtherFocusReason);
    }
    else if (evt->key() == Qt::Key_Alt && displayShortcuts)
    {
        displayNames = true;
        repaint();
    }
    else
    {
        evt->ignore();
        return;
    }
    evt->accept();
}


void robot_gui::Window::keyReleaseEvent(QKeyEvent * evt)
{
    if (evt->key() == Qt::Key_Control)
        HideShortcuts();
    else if (evt->key() == Qt::Key_Alt)
    {
        displayNames = false;
        if (displayShortcuts)
            repaint();
    }
    else
    {
        evt->ignore();
        return;
    }
    evt->accept();
}


void robot_gui::Window::paintEvent(QPaintEvent *)
{
    if (displayShortcuts)
    {
        QPen pen_black = QPen(QBrush(Qt::black), 1);
        QPen pen_white = QPen(QBrush(Qt::white), 1);
        //
        QPixmap pixmap(canvas->width(), canvas->height());
        pixmap.fill(QColor(Qt::transparent));
        //
        QFont font;
        font.setBold(true);
        font.setPointSize(10);
        //
        QPainter painter(&pixmap);
        painter.setRenderHint(QPainter::Antialiasing);
        painter.setFont(font);
        painter.setBrush( QBrush(QColor(0, 0, 0)) );
        QFontMetrics metrics = painter.fontMetrics();
        //
        painter.translate(0, menuBar()->height());
        //
        int height = metrics.height() + 2;
        int k = 1;
        foreach (QToolButton * button, listButtons)
        {
            if (button->isHidden())
                continue;

            QString number = std::to_string(k++).c_str();
            int width = metrics.width(number) + 4;
            QPoint pos = button->mapToParent(QPoint(0, 0));
            // Draw number
            QRect rect_number(pos.x() + (button->width() - width)/2, 0, width, height);
            painter.setPen(pen_black);
            painter.drawRect(rect_number);
            painter.setPen(pen_white);
            painter.drawText(rect_number, Qt::AlignCenter, number);
            // Draw name
            if (displayNames)
            {
                painter.save();
                painter.translate(rect_number.center().x(), rect_number.bottom() + 4);
                painter.rotate(45);
                QRect rect_name(0, 0, metrics.width(button->toolTip()) + 8, height);
                painter.setPen(pen_black);
                painter.drawRect(rect_name);
                painter.setPen(pen_white);
                painter.drawText(rect_name, Qt::AlignCenter, button->toolTip());
                painter.restore();
            }
        }
        //
        canvas->setPixmap(pixmap);
    }
}


void robot_gui::Window::changeEvent(QEvent * evt)
{
    if (evt->type() == QEvent::ActivationChange)
        HideShortcuts();
}


void robot_gui::Window::resizeEvent(QResizeEvent *)
{
    if (displayShortcuts)
        HideShortcuts();
}


void robot_gui::Window::focusOutEvent(QFocusEvent *)
{
    if (displayShortcuts)
        HideShortcuts();
}


void robot_gui::Window::moveEvent(QMoveEvent *)
{
    if (displayShortcuts)
        HideShortcuts();
}


bool robot_gui::Window::eventFilter(QObject * obj, QEvent * evt)
{
    if (displayShortcuts)
    {
        obj->event(evt);
        int type = evt->type();
        if (type == QEvent::KeyPress || type == QEvent::KeyRelease)
        {
            event(evt);
            return true;
        }
        return false;
    }
    return QMainWindow::eventFilter(obj, evt);
}


void robot_gui::Window::DockToolClose(QObject* dockTool)
{
    robot_ps::Tool* tool = ((DockTool*)dockTool)->tool;
    robotCoreGUI->DeleteTool(tool);

    auto found = std::find(dockTools.begin(), dockTools.end(), dockTool);
    if (found != dockTools.end())
    {
        // I guess it has to be deleted now because right after this a layout could be loaded, and if the dockTool remains with the same name (objectName), the positioning will bug
        delete *found;
        dockTools.erase(found);
    }
}


robot_gui::Window::~Window()
{
    for (auto d = dockTools.begin(); d != dockTools.end(); d++)
        robotCoreGUI->DeleteTool((*d)->tool);
    robotCoreGUI->DeleteWindow(this);
}
