// component.h



#ifndef ROBOT_PS_CORE_COMPONENT
#define ROBOT_PS_CORE_COMPONENT



#include <string>
#include <deque>
#include <algorithm>
#include "class_id.h"
#include "robot_core.h"
#include "tool.h"



namespace robot_ps {



class RobotCore;
class Tool;

/*!
 * \class Component
 * \brief The Component class provides the infrastructure to connect components
 *  and tools in such a way that the when a component is started, it looks for
 *  correspondent tools running and vice versa.
 */

class Component
{
    /// List of ClassIDs
    std::deque<ClassIDBase*> listOfClassIDs;
    /// List of Tools
    std::deque< std::deque<Tool*>* > listOfTools;
    /// List of callback functions TreatAddTool
    std::deque<void (Component::*)(Tool*)> listOfTreatAddCallbacks;
    /// List of callback functions TreatRemoveTool
    std::deque<void (Component::*)(Tool*)> listOfTreatRemoveCallbacks;
    /// List of callbacks to remove itself from components.
    /// Are obtained with AddMeToTools and called on Delete.
    std::deque<bool (Tool::*)(Tool*, Component*)> listOfRemoveComponentCallbacks;

    /*!
     * \brief GetListOfAllTools
     * \return A list of all tools from robotCore.
     */
    std::deque<Tool*> GetListOfAllTools();

    template<class C, class T>
    int GetNumberOfList()
    {
        unsigned int i = 0;
        for (auto id = listOfClassIDs.begin(); id != listOfClassIDs.end(); id++)
        {
            ClassIDBase* id2 = *id;
            if (dynamic_cast<ClassID<C, T>*>(id2) != NULL)
                return i;
            i++;
        }

        return -1;
    }

    template<class T>
    std::deque<T*> FindListOfThisTool()
    {
        std::deque<T*> ret;

        std::deque<Tool*> listTools = GetListOfAllTools();
        for (std::deque<Tool*>::iterator tool = listTools.begin(); tool != listTools.end(); tool++)
        {
            T* t = dynamic_cast<T*>(*tool);
            if (t != NULL)
                ret.push_back(t);
        }

        return ret;
    }

protected:
    /*!
     * \brief AddMeToTools creates an association between a component and a
     * tool by adding new ClassID, a new tool, and callbacks. Derived classes
     * should provide the template types as the own component type and the
     * tool type of the tool it should connect to. Calls AddAComponentToMe on
     * the tool that has
     * \param callbackAddPointer function pointer to the calback to add a tool,
     *  typically defined on the derived class.
     * \param callbackRemovePointer function pointer to the calback to remove a
     *  tool, typically defined on the derived class.
     */
    template<class C, class T>
    void AddMeToTools(void (C::* callbackAddPointer)(T* t), void (C::* callbackRemovePointer)(T* t))
    {
        listOfClassIDs.push_back(new ClassID<C, T>);
        std::deque<Tool*>* listTools = new std::deque<Tool*>;
        listOfTools.push_back(listTools);
        listOfTreatAddCallbacks.push_back(reinterpret_cast<void (Component::*)(Tool*)>(callbackAddPointer));
        listOfTreatRemoveCallbacks.push_back(reinterpret_cast<void (Component::*)(Tool*)>(callbackRemovePointer));
        listOfRemoveComponentCallbacks.push_back(reinterpret_cast<bool (Tool::*)(Tool*, Component*)>(&T::template RemoveAComponentFromMe<T, C>));

        std::deque<T*> listAllTools = FindListOfThisTool<T>();

        for (typename std::deque<T*>::iterator tool = listAllTools.begin(); tool != listAllTools.end(); tool++)
            if ((*tool)->AddAComponentToMe(*tool, (C*)this))
                listTools->push_back(*tool);
    }

public:
    std::string name;

    RobotCore* robotCore;

    template<class C, class T>
    std::deque<T*>* GetListOfTools()
    {
        unsigned int i = 0;
        for (auto id = listOfClassIDs.begin(); id != listOfClassIDs.end(); id++)
        {
            ClassIDBase* id2 = *id;
            if (dynamic_cast<ClassID<C, T>*>(id2) != NULL)
                return (std::deque<T*>*)(listOfTools[i]);
            i++;
        }

        return NULL;
    }

    /*!
     * \brief FindTools is typically used to call AddMeToTools on derived classes and
     * set appropriate tool and component types and callbacks to add and remove the tool.
     */
    virtual void FindTools() = 0;

    template<class C, class T>
    bool AddAToolToMe(C* c, T* t)
    {
        int i = GetNumberOfList<C, T>();
        if (i != -1)
        {
            auto found = std::find(listOfTools[i]->begin(), listOfTools[i]->end(), reinterpret_cast<Tool*>(t));
            if (found == listOfTools[i]->end())
            {
                listOfTools[i]->push_back(t);
                void (C::*treatFunction)(T* t) = reinterpret_cast<void (C::*)(T* t)>(listOfTreatAddCallbacks[i]);
                (((C*)(this))->*treatFunction)(t);
                return true;
            }
            else
                return false;
        }

        return false;
    }

    template<class C, class T>
    bool RemoveAToolFromMe(Component* c2, Tool* t2)
    {
        T* t = dynamic_cast<T*>(t2);
        int i = GetNumberOfList<C, T>();
        if (i != -1)
        {
            auto found = std::find(listOfTools[i]->begin(), listOfTools[i]->end(), t);
            if (found != listOfTools[i]->end())
            {
                void (C::*treatFunction)(T* t) = reinterpret_cast<void (C::*)(T* t)>(listOfTreatRemoveCallbacks[i]);
                (((C*)(this))->*treatFunction)(t);
                listOfTools[i]->erase(found);
                return true;
            }
            else
                return false;
        }

        return false;
    }

    /*!
     * \brief Delete is called before component nodelet is unloaded. Performs
     * removal procedure, i.e. calls remove callback from all tools it is
     * connected to, clears all lists and erases itself from component list on RobotCore
     */
    virtual void Delete();

    virtual ~Component(){}
};



}



#endif
