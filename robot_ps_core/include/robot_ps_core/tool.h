// tool.h



#ifndef ROBOT_PS_CORE_TOOL
#define ROBOT_PS_CORE_TOOL



#include <string>
#include <deque>
#include <algorithm>
#include "class_id.h"
#include "robot_core.h"
#include "component.h"



namespace robot_ps {



class RobotCore;
class Component;



class Tool
{
    std::deque<ClassIDBase*> listOfClassIDs;
    std::deque< std::deque<Component*>* > listOfComponents;
    std::deque<void (Tool::*)(Component*)> listOfTreatAddCallbacks;
    std::deque<void (Tool::*)(Component*)> listOfTreatRemoveCallbacks;
    std::deque<bool (Component::*)(Component*, Tool*)> listOfRemoveToolCallbacks;

    std::deque<Component*> GetListOfAllComponents();

    template<class T, class C>
    int GetNumberOfList()
    {
        unsigned int i = 0;
        for (auto id = listOfClassIDs.begin(); id != listOfClassIDs.end(); id++)
        {
            ClassIDBase* id2 = *id;
            if (dynamic_cast<ClassID<T, C>*>(id2) != NULL)
                return i;
            i++;
        }

        return -1;
    }

    template<class C>
    std::deque<C*> FindListOfThisComponent()
    {
        std::deque<C*> ret;

        std::deque<Component*> listComponents = GetListOfAllComponents();
        for (std::deque<Component*>::iterator component = listComponents.begin(); component != listComponents.end(); component++)
        {
            C* c = dynamic_cast<C*>(*component);
            if (c != NULL)
                ret.push_back(c);
        }

        return ret;
    }

protected:
    template<class T, class C>
    void AddMeToComponents(void (T::* callbackAddPointer)(C* t), void (T::* callbackRemovePointer)(C* t))
    {
        listOfClassIDs.push_back(new ClassID<T, C>);
        std::deque<Component*>* listComponents = new std::deque<Component*>;
        listOfComponents.push_back(listComponents);
        listOfTreatAddCallbacks.push_back(reinterpret_cast<void (Tool::*)(Component*)>(callbackAddPointer));
        listOfTreatRemoveCallbacks.push_back(reinterpret_cast<void (Tool::*)(Component*)>(callbackRemovePointer));
        listOfRemoveToolCallbacks.push_back(reinterpret_cast<bool (Component::*)(Component*, Tool*)>(&C::template RemoveAToolFromMe<C, T>));

        std::deque<C*> listAllComponents = FindListOfThisComponent<C>();

        for (typename std::deque<C*>::iterator component = listAllComponents.begin(); component != listAllComponents.end(); component++)
            if ((*component)->AddAToolToMe(*component, (T*)this))
                listComponents->push_back(*component);
    }

public:
    std::string name;

    RobotCore* robotCore;

    template<class T, class C>
    std::deque<C*>* GetListOfComponents()
    {
        unsigned int i = 0;
        for (auto id = listOfClassIDs.begin(); id != listOfClassIDs.end(); id++)
        {
            ClassIDBase* id2 = *id;
            if (dynamic_cast<ClassID<T, C>*>(id2) != NULL)
                return (std::deque<C*>*)(listOfComponents[i]);
            i++;
        }

        return NULL;
    }

    virtual void FindComponents() = 0;

    template<class T, class C>
    bool AddAComponentToMe(T* t, C* c)
    {
        int i = GetNumberOfList<T, C>();
        if (i != -1)
        {
            auto found = std::find(listOfComponents[i]->begin(), listOfComponents[i]->end(), reinterpret_cast<Component*>(c));
            if (found == listOfComponents[i]->end())
            {
                listOfComponents[i]->push_back(c);
                void (T::*treatFunction)(C* c) = reinterpret_cast<void (T::*)(C* c)>(listOfTreatAddCallbacks[i]);
                (((T*)(this))->*treatFunction)(c);
                return true;
            }
            else
                return false;
        }

        return false;
    }

    template<class T, class C>
    bool RemoveAComponentFromMe(Tool* t2, Component* c2)
    {
        C* c = dynamic_cast<C*>(c2);
        int i = GetNumberOfList<T, C>();
        if (i != -1)
        {
            auto found = std::find(listOfComponents[i]->begin(), listOfComponents[i]->end(), c);
            if (found != listOfComponents[i]->end())
            {
                void (T::*treatFunction)(C* c) = reinterpret_cast<void (T::*)(C* c)>(listOfTreatRemoveCallbacks[i]);
                (((T*)this)->*treatFunction)(c);
                listOfComponents[i]->erase(found);
                return true;
            }
            else
                return false;
        }

        return false;
    }

    virtual void Delete();

    virtual void Init(){}

    virtual ~Tool(){}
};



}



#endif
