// class_id.h



#ifndef ROBOT_PS_CORE_CLASS_ID
#define ROBOT_PS_CORE_CLASS_ID



namespace robot_ps {



class ClassIDBase
{
    /*!
     * \brief DummyFunction is created just to make the ClassIDBase a polymorphic class.
     */
    virtual void DummyFunction(){}
};


/*!
 * \class ClassID
 * \brief ClassID is used to create an association between component and tool.
 * Through dynamic_cast it is possible to check if that if that association is valid.
 */
template<typename... classes>
class ClassID : public ClassIDBase
{
};



}



#endif
