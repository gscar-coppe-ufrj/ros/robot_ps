// robot_core.h



#ifndef ROBOT_CORE
#define ROBOT_CORE



#include <deque>
#include <vector>
#include <tuple>
#include <boost/thread.hpp>
#include <boost/thread/barrier.hpp>
#include <boost/shared_ptr.hpp>
#include "tool.h"
#include "component.h"



namespace robot_ps {



class Tool;
class Component;


/*!
 * \class RobotCore
 * \brief The RobotCore class
 */
class RobotCore
{
    /// Thread to run robot_ps::RobotCore::ROSSpinThread
    boost::thread* rosSpinThread;
    boost::barrier threadBarrier;

    /// A pointer to custom_loader::Loader
    void* componentsManager;
    void* voidToolLoader;
    /// Variable created so that the Tools won't be deleted before the right time
    std::deque< boost::shared_ptr<robot_ps::Tool> > sharedPointersTrash;

    /*!
     * \brief Initiate initializes ROS, creates an instance of custom_loader::Loader
     *  and binds LoadComponents and UnloadComponents defined here to function
     *  wrappers from Boost library declared on custom_loader::Loader. Starts a
     *  Multi-threaded ROS spinner with 20 threads.
     * \param argc CLI arguments
     * \param argv CLI arguments
     */
    void Initiate(int argc, char *argv[]);

    /*!
     * \brief ROSSpinThread is executed by a thread initiated on this class'
     * constructor and calls the robot_core::Initiate method. The main purpose
     * of this thread is to run ROS Spin.
     * \param core a pointer to this object
     * \param argc CLI arguments
     * \param argv CLI arguments
     */
    static void ROSSpinThread(RobotCore* core, int & argc, char** argv);

    /*!
     * \brief IsComponent checks if a given plugin type is a compoment by checking the
     * "component" attribute on the plugin description XML.
     * \param typeToCheck Plugin type to be checked
     * \return Returns true if it is a component and false otherwise.
     */
    bool IsComponent(std::string typeToCheck);

    /*!
     * \brief LoadComponents is called by custom_loader::Loader everytime a
     * nodelet is loaded, in order to verify if its a component and connect to
     * Tools. FindTools() is called after the component is successfuly created.
     * \param c Pointer to a nodelet that can be a component
     * \param name Component name as declared on plugin XML
     * \param type Component type as declared on plugin XML
     */
    void LoadComponents(void* c, std::string name, std::string type);

    /*!
     * \brief UnloadComponents is called by custom_loader::Loader everytime a nodelet
     * is unlodaded.
     * \param c Pointer to a nodelet that can be a component
     * \param name Component name as declared on plugin XML
     */
    void UnloadComponents(void* c, std::string name);

protected:
    /// Vector with the tool names loaded from XML (only tools that are loaded).
    std::vector<std::string> toolList;
    /// Vector with the tool descriptions loaded from XML
    std::vector<std::string> toolsDescriptionList;
    /// Vector with the icon paths loaded from XML
    std::vector<std::string> toolsIconList;

    /// List of lauch file paths.
    std::deque<std::string> launchers;

    /// A deque of tuples where:
    /// The first element is the name given by the user in the interface
    /// The second element is the path to the .lauch file
    /// The third element is the ROS launch arguments
    /// The fourth element is a flag to indicate if the laucher instance is running.
    std::deque<std::tuple<std::string, std::string, std::string, bool>> launcherInstances;

    /*!
     * \brief LoadTools function loads tools using pluginlib.
     * Is called on two moments, when the tool list is updated (user checks
     * new tools) and when configuration is loaded (program initialization).
     * \param filterList
     */
    void LoadTools(std::deque<std::string> filterList = std::deque<std::string>());

    /*!
     * \brief ROSLaunch lauches or kills a launcher instance from launcherInstances by
     * formulating a rolaunch command and using system call to execute it.
     * \param launcherInstance
     * \return True if launch or kill was successful false otherwise.
     */
    bool ROSLaunch(std::deque<std::tuple<std::string, std::string, std::string, bool>>::iterator launcherInstance);

    /*!
     * \brief Shutdown Pure virtual function defined on RobotCoreGUI
     */
    virtual void Shutdown(){}

public:
    /// List of loaded components
    std::deque<Component*> components;
    /// List of tools
    std::deque<Tool*> tools;

    /*!
     * \brief RobotCore creates a thread for ROSSpinThread.
     * \param argc CLI arguments
     * \param argv CLI arguments
     */
    RobotCore(int argc, char *argv[]);

    /*!
     * \brief GetCurrentToolsList
     * \return toolList List of tools currently loaded.
     */
    std::vector<std::string> GetCurrentToolsList();

    /*!
     * \brief GetDeclaredToolsList
     * \return List of all classes derived from robot_ps::Tool declared on pluginlib.
     */
    std::vector<std::string> GetDeclaredToolsList();

    /*!
     * \brief NewTool loads a tool class using pluginlib ClassLoader
     * and creates an instance with boost::shared_ptr connect it to
     * associated components.
     * \param toolName Name of the tool to be created
     * \return Pointer to the tool created
     */
    Tool* NewTool(std::string toolName);

    /*!
     * \brief DeleteTool
     * \param tool
     */
    void DeleteTool(Tool* tool);

    /*!
      * Delete Components, stop ROSlaunchers, delete Tools
      */
    ~RobotCore();
};



}



#endif
