// tool_a.cpp



#include <robot_ps_core/tool.h>



std::deque<robot_ps::Component*> robot_ps::Tool::GetListOfAllComponents()
{
    return robotCore->components;
}


void robot_ps::Tool::Delete()
{
    for (auto i = 0; i != listOfComponents.size(); i++)
    {
        for (int j = 0; j != listOfComponents[i]->size(); j++)
        {
            Component* c = (*listOfComponents[i])[j];
            (c->*(listOfRemoveToolCallbacks[i]))(c, this);
        }
        delete listOfComponents[i];
    }
    listOfComponents.clear();
    for (auto i = listOfClassIDs.begin(); i != listOfClassIDs.end(); i++)
        delete *i;
    listOfClassIDs.clear();
    listOfTreatAddCallbacks.clear();
    listOfTreatRemoveCallbacks.clear();
    listOfRemoveToolCallbacks.clear();

    robotCore->tools.erase(std::find(robotCore->tools.begin(), robotCore->tools.end(), this));
}

