// component_a.cpp



#include <robot_ps_core/component.h>



std::deque<robot_ps::Tool*> robot_ps::Component::GetListOfAllTools()
{
    return robotCore->tools;
}


void robot_ps::Component::Delete()
{
    for (auto i = 0; i != listOfTools.size(); i++)
    {
        for (int j = 0; j != listOfTools[i]->size(); j++)
        {
            Tool* t = (*listOfTools[i])[j];
            (t->*(listOfRemoveComponentCallbacks[i]))(t, this);
        }
        delete listOfTools[i];
    }
    listOfTools.clear();
    for (auto i = listOfClassIDs.begin(); i != listOfClassIDs.end(); i++)
        delete *i;
    listOfClassIDs.clear();
    listOfTreatAddCallbacks.clear();
    listOfTreatRemoveCallbacks.clear();
    listOfRemoveComponentCallbacks.clear();

    robotCore->components.erase(std::find(robotCore->components.begin(), robotCore->components.end(), this));
}
