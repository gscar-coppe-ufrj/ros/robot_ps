// robot_core.cpp



#include <cstdlib>
#include <sstream>
#include <iostream>
#include <fstream>
#include <signal.h>
#include <pluginlib/class_loader.h>
#include <robot_ps_core/robot_core.h>
#include <ros/ros.h>
#include <custom_loader/custom_loader.h>
#include <custom_loader/custom_nodelet.h>
#include <tinyxml.h>



robot_ps::RobotCore::RobotCore(int argc, char *argv[]) : threadBarrier(2)
{
    rosSpinThread = new boost::thread(robot_ps::RobotCore::ROSSpinThread, this, argc, argv);
}


void robot_ps::RobotCore::Initiate(int argc, char *argv[])
{
    ros::init(argc, argv, "robot_gui", ros::init_options::AnonymousName);
    custom_loader::Loader* manager = new custom_loader::Loader;
    componentsManager = (void*)manager;
    manager->funcForLoad = boost::bind(&robot_ps::RobotCore::LoadComponents, this, _1, _2, _3);
    manager->funcForUnload = boost::bind(&robot_ps::RobotCore::UnloadComponents, this, _1, _2);
    ros::MultiThreadedSpinner spinner(20);
    spinner.spin();

    Shutdown();

    threadBarrier.wait();
}


void robot_ps::RobotCore::ROSSpinThread(RobotCore* core, int & argc, char** argv)
{
    core->Initiate(argc, argv);

    std::cout << "ros spin over" << std::endl;
}


bool robot_ps::RobotCore::IsComponent(std::string typeToCheck)
{
    pluginlib::ClassLoader<robot_ps::Tool>* toolLoader = new pluginlib::ClassLoader<robot_ps::Tool>("nodelet", "nodelet::Nodelet");
    auto pathList = toolLoader->getPluginXmlPaths();
    for (auto path = pathList.begin(); path != pathList.end(); path++)
    {
        TiXmlDocument doc(*path);
        if (!doc.LoadFile())
            continue;

        TiXmlHandle hDoc(&doc);

        TiXmlElement* elemLibrary = hDoc.FirstChildElement().Element();
        if (elemLibrary)
        {
            std::string value = elemLibrary->Value();
            if (value == "library")
            {
                TiXmlHandle handleLibrary(elemLibrary);
                TiXmlElement* elemClass = handleLibrary.FirstChild("class").Element();
                while (elemClass)
                {
                    std::string type;
                    elemClass->QueryStringAttribute("name", &type);
                    if (type == typeToCheck)
                    {
                        bool component;
                        elemClass->QueryBoolAttribute("component", &component);
                        return component;
                    }

                    elemClass = elemClass->NextSiblingElement();
                }
            }
        }
    }
}


void robot_ps::RobotCore::LoadComponents(void* c, std::string name, std::string type)
{
    if (IsComponent(type))
    {
        std::cout << "loading component " << c << " " << name << " " << type << std::endl;

        custom_loader::CustomNodelet* n = (custom_loader::CustomNodelet*)c;
        Component* comp = dynamic_cast<Component*>(n);
        if (comp != NULL)
        {
            components.push_back(comp);
            comp->robotCore = this;
            if (comp->name == "")
                comp->name = name;

            comp->FindTools();
        }
    }
}


void robot_ps::RobotCore::UnloadComponents(void* c, std::string name)
{
    custom_loader::CustomNodelet* n = (custom_loader::CustomNodelet*)c;
    Component* comp = dynamic_cast<Component*>(n);
    for (auto component = components.begin(); component != components.end(); component++)
    {
        if (*component == comp)
        {
            std::cout << "unloading component " << c << std::endl;

            comp->Delete();
            return;
        }
    }
}


std::vector<std::string> robot_ps::RobotCore::GetCurrentToolsList()
{
    return toolList;
}


std::vector<std::string> robot_ps::RobotCore::GetDeclaredToolsList()
{
    pluginlib::ClassLoader<robot_ps::Tool>* toolLoader = (pluginlib::ClassLoader<robot_ps::Tool>*)voidToolLoader;
    return toolLoader->getDeclaredClasses();
}


void robot_ps::RobotCore::LoadTools(std::deque<std::string> filterList)
{
    pluginlib::ClassLoader<robot_ps::Tool>* toolLoader = new pluginlib::ClassLoader<robot_ps::Tool>("robot_ps_core", "robot_ps::Tool");
    voidToolLoader = toolLoader;
    toolList.clear();
    auto tempToolList = toolLoader->getDeclaredClasses();
    for (auto i = tempToolList.begin(); i != tempToolList.end(); i++)
    {
        toolLoader->loadLibraryForClass(*i); // without this line the program bugs when trying to load Tools and libraries have been loaded somewhere elese
        auto j = std::find(filterList.begin(), filterList.end(), *i);
        if (j != filterList.end())
            toolList.push_back(*i);
    }
    toolsDescriptionList.resize(toolList.size());
    toolsIconList.resize(toolList.size());

    auto pathList = toolLoader->getPluginXmlPaths();
    for (auto path = pathList.begin(); path != pathList.end(); path++)
    {
        TiXmlDocument doc(*path);
        if (!doc.LoadFile())
            continue;

        TiXmlHandle hDoc(&doc);

        TiXmlElement* elemLibrary = hDoc.FirstChildElement().Element();
        if (elemLibrary)
        {
            std::string value = elemLibrary->Value();
            if (value == "library")
            {
                TiXmlHandle handleLibrary(elemLibrary);
                TiXmlElement* elemClass = handleLibrary.FirstChild("class").Element();
                while (elemClass)
                {
                    std::string type;
                    elemClass->QueryStringAttribute("type", &type);
                    if (type != "")
                    {
                        auto found = std::find(toolList.begin(), toolList.end(), type);
                        if (found != toolList.end())
                        {
                            auto pos = found - toolList.begin();

                            TiXmlHandle handleClass(elemClass);
                            TiXmlElement* elemDescription = handleClass.FirstChild("description").Element();
                            if (elemDescription)
                                toolsDescriptionList[pos] = elemDescription->GetText();
                            TiXmlElement* elemIcon = handleClass.FirstChild("icon").Element();
                            if (elemIcon)
                                toolsIconList[pos] = elemIcon->GetText();
                        }

                        elemClass = elemClass->NextSiblingElement();
                    }
                }
            }
        }
    }
}


bool robot_ps::RobotCore::ROSLaunch(std::deque<std::tuple<std::string, std::string, std::string, bool>>::iterator launcherInstance)
{
    if (std::get<3>(*launcherInstance)) // the launcher should stop
    {

        std::ifstream myfile (".process_id_" + std::get<0>(*launcherInstance));
        int pid = 0;
        if (myfile.is_open())
        {
            if (!myfile.eof())
            {
                std::string line;
                getline(myfile,line);

                pid = std::stoi(line);

                myfile.close();
            }

            if (kill(pid, 2) == 0)
                return true;
        }
    }
    else
    {
        std::string arg = std::get<1>(*launcherInstance) + " manager:=" + ros::this_node::getName();
        arg += " " + std::get<2>(*launcherInstance);
        arg += " __ns:=" + std::get<0>(*launcherInstance);
        std::string sysStr = "roslaunch " + arg + " --pid=.process_id_" + std::get<0>(*launcherInstance) +  " &";
        system(sysStr.c_str());
        return true;
    }

    return false;
}


robot_ps::Tool* robot_ps::RobotCore::NewTool(std::string toolName)
{
    pluginlib::ClassLoader<robot_ps::Tool>* toolLoader = (pluginlib::ClassLoader<robot_ps::Tool>*)voidToolLoader;
    boost::shared_ptr<robot_ps::Tool> toolPtr = toolLoader->createInstance(toolName);
    sharedPointersTrash.push_back(toolPtr);
    Tool* tool = &(*toolPtr);
    tools.push_back(tool);
    tool->robotCore = this;
    tool->FindComponents();

    return tool;
}


void robot_ps::RobotCore::DeleteTool(robot_ps::Tool* tool)
{
    for (auto ptr = sharedPointersTrash.begin(); ptr != sharedPointersTrash.end(); ptr++)
    {
        if (&(**ptr) == tool)
        {
            tool->Delete();
            sharedPointersTrash.erase(ptr);
            break;
        }
    }
    auto itList = std::find(tools.begin(), tools.end(), tool);
    if (itList != tools.end())
        tools.erase(itList);
}


robot_ps::RobotCore::~RobotCore()
{
    // The order of what is deleted (components or tools first) shouldn't matter

    // Deleting Components
    while (components.begin() != components.end())
        (*components.begin())->Delete();
    ros::shutdown();
    threadBarrier.wait();
    ((custom_loader::Loader*)componentsManager)->clear();
    rosSpinThread->join();
    delete ((custom_loader::Loader*)componentsManager);
    delete rosSpinThread;

    // Stopping ROSlaunchers
    for (auto i = launcherInstances.begin(); i != launcherInstances.end(); i++)
    {
        if (std::get<3>(*i))
            ROSLaunch(i);
    }


    // Deleting Tools
    while (tools.begin() != tools.end())
    {
        for (auto ptr = sharedPointersTrash.begin(); ptr != sharedPointersTrash.end(); ptr++)
        {
            if (&(**ptr) == *tools.begin())
            {
                sharedPointersTrash.erase(ptr);
                break;
            }
        }
    }
    tools.clear();
    sharedPointersTrash.clear();

    if (voidToolLoader != NULL)
        delete (pluginlib::ClassLoader<robot_ps::Tool>*)voidToolLoader;
}
