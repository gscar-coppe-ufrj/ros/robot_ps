/*!
 * \file gamepad_manager_nodelet.h
 */



#ifndef GAMEPAD_GAMEPAD_MANAGER_NODELET_H
#define GAMEPAD_GAMEPAD_MANAGER_NODELET_H



#include <gamepad/gamepad.h>
#include <gamepad/gamepad_nodelet.h>

#include <iostream>
#include <list>
#include <string>
#include <vector>

#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <std_msgs/Empty.h>

#include <linux_devs/linux_dev_manager.h>



namespace gamepad {



/*!
 * \class GamepadManager
 * \brief GamepadManager is a factory for gamepad::Gamepad.
 *
 * The class uses linux_devs::LinuxDevManager to locate gamepad devices, and 
 * provide an interface to select those devices. Each time a device is selected
 * it will create a new Gamepad object to capture its input.
 */
class GamepadManager : public nodelet::Nodelet, public linux_devs::LinuxDevManager
{
	/// The minimum axis movement that is treated as movement.
    double deadzone;
    /// The rate to resend events when no new event is present.
    double autorepeatRate;
    /// The minimum time between two consecutive axis events to send.
    double coalesceInterval;

	/// Internal list of gamepad created.
    std::list<Gamepad*> gamepadList;
    /// Internal list of devices used when SelectDevices is called to see if the
    /// call is for an inclusion or removal.
    std::list<std::string> deviceList;

	/*!
	 * \brief CreateGamepad will create a new Gamepad object.
	 *
	 * It will fill the required information and call Gamepad::StartCycle() to 
	 * let the object handle input events.
	 *
	 * \param devicePath The path of the device (i.e. /dev/input/jsX).
	 */
    void CreateGamepad(std::string devicePath);
    /*!
     * \brief CloseGamepad will atempt to shutdown a gamepad device.
     * 
     * The device will stop handling input events.
     *
     * \param devicePath The path of the device (i.e. /dev/input/jsX).
     */
    void CloseGamepad(std::string devicePath);

	/*!
     * \brief Filter devices with names containing 'js'.
     * \param nodePath The nodepath to filter.
     * \return Return true if the name contains 'js' and false otherwise.
     */
    virtual bool FilterDeviceByPath(std::string& nodePath) const;
    /*!
     * \brief SelectDevices is called everytime the list of devices changes.
     * \param list The new list of devices.
     */
    virtual void SelectDevices(const std::vector<std::string> & list);

public:
    /*!
     * \brief GamepadManager does nothing.
     */
    GamepadManager();
    
    /*!
     * \brief onInit is called when the nodelet is created.
     *
     * This function will get the rosparams used in roslaunch.
     */
    virtual void onInit();
    
    /*!
     * \brief ~GamepadManager shutdowns the loaded gamepads.
     */
    virtual ~GamepadManager();
};



}



#endif
