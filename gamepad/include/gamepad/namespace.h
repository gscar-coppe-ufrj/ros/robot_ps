/*!
 * \file namespace.h
 */



#ifndef NAMESPACE_H
#define NAMESPACE_H



/*!
 * \namespace gamepad
 * \brief The gamepad namespace provides a collection of classes to
 *      control inputs from a gamepad.
 */
namespace gamepad {
}



#endif
