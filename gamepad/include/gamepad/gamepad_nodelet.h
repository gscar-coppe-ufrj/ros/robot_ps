/*!
 * \file gamepad_nodelet.h
 */



#ifndef GAMEPAD_GAMEPAD_NODELET_H
#define GAMEPAD_GAMEPAD_NODELET_H



#include <gamepad/gamepad.h>

#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <sensor_msgs/Joy.h>



namespace gamepad {



/*!
 * \class GamepadNodelet
 * \brief
 */
class GamepadNodelet : public Gamepad
{
    /// ROS node handle.
    ros::NodeHandle nodeHandle;

    /// ROS publisher.
    ros::Publisher pub;

    /// Gamepad state message.
    sensor_msgs::Joy message; 

public:
    /*!
     * \brief GamepadNodelet does nothing.
     */
    GamepadNodelet(ros::NodeHandle& node);
    /*!
     * \brief ~GamepadNodelet does nothing.
     */
    virtual ~GamepadNodelet();

    /*!
     * \brief SetParameters use the ROS nodelet params to set the internal
     *      parameters of the joystick.
     */
    void SetParameters();

    /*!
     * \brief CheckParameters checks all the gamepad parameters values for
     *      errors.
     */
    void CheckParameters();

    /*!
     * \brief ButtonEvent handles a joystick button event, JS_EVENT_BUTTON.
     * \param buttonId The button that received the event.
     * \param value The value of the event, i.e. pressed or released.
     */
    virtual void ButtonEvent(unsigned char buttonId, int value);
    /*!
     * \brief ButtonEvent handles a joystick axis event, JS_EVENT_AXIS.
     * \param axisId The axis that received the event.
     * \param value The value of the event, i.e. axis position.
     */
    virtual void AxisEvent(unsigned char axisId, int value);

    /*!
     * \brief PublishGamepadState sends the gamepad state through a ROS topic.
     */
    virtual void PublishGamepadState();

    /*!
     * \brief StartCycle should setup the internal gamepad cycle.
     *
     * It is called after creating the gamepad object and setting its device
     * path. Afterwards the object will execute its function by itself.
     */
    virtual void StartCycle();
};



}



#endif
