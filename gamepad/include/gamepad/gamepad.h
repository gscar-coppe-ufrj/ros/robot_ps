/*!
 * \file gamepad.h
 */



#ifndef GAMEPAD_GAMEPAD_H
#define GAMEPAD_GAMEPAD_H



#include <iostream>
#include <fcntl.h>
#include <unistd.h>

#include <linux/joystick.h>

#include <boost/thread.hpp>



namespace gamepad {



typedef boost::posix_time::ptime Time;
typedef boost::posix_time::time_duration TimeDuration;



/*!
 * \class Gamepad
 * \brief The Gamepad class treats the input from a gamepad and sends a message
        with it.
 */
class Gamepad
{   
protected:
    /// Time of the last message sent.
    Time lastMessageSent;

    /// Path of the device.
    std::string gamepadPath;
    /// Name of the device.
    std::string gamepadName;

    /// The gamepad's event processing thread.
    boost::thread* cycleThread;

    /// Variable responsible for stopping the loop and closing the joystick.
    bool shutdown;

    /// Scale of the axis.
    double scale;

    /// Unscaled deadzone of the gamepad.
    double unscaledDeadzone;

    /// Interval of time between two inputs.
    double autorepeatInterval;

    /// The gamepad's file device.
    int gamepadFileDescriptor;

    /*!
     * \brief CycleThreadFunction calls the function
     *      Gamepad::EventCycle() to start the internal
     *      cycle.
     * \param gamepad Joystick object that will start treating
     *      inputs.
     */
    static void CycleThreadFunction(Gamepad* gamepad);

public:
    enum Priority {
        PUBLISH_SOON = 0,
        PUBLISH_NOW = 1,
        DONT_PUBLISH = 2
    };

    /// Deadzone of the gamepad.
    double deadzone;
    /// Autorepeat rate of the gamepad, in Hz. 0 for no repeat.
    double autorepeatRate;
    /// Time it takes to send an axis input.
    double coalesceInterval;

    /*!
     * \brief Gamepad does nothing.
     */
    Gamepad();

    /*!
     * \brief SetName sets the gamepad's readable name.
     * \param name The gamepad's name.
     */
    void SetName(std::string name);
    /*!
     * \brief GetName returns the gamepad's name.
     * \return Returns a string with the gamepad's name.
     */
    std::string GetName() const;

    /*!
     * \brief SetDevicePath sets the path used to open the device.
     * \param path The device's path.
     */
    void SetDevicePath(std::string path);
    /*!
     * \brief GetDevicePath returns the device's path.
     * \return Returns a string with the device's path.
     */
    std::string GetDevicePath() const;

    /*!
     * \brief StartCycle should setup the internal gamepad cycle.
     *
     * It is called after creating the gamepad object and setting its device
     * path. Afterwards the object will execute its function by itself.
     */
    virtual void StartCycle() = 0;

    /*!
     * \brief OpenGamepad opens the gamepad file.
     */
    void OpenGamepad();
    /*!
     * \brief CloseGamepad closes the gamepad file.
     */
    void CloseGamepad();

    /*!
     * \brief Shutdown should be called before the object is deleted.
     *
     * After the internal thread is created, the only way to properly stop the
     * execution of the internal loop is by calling this function.
     */
    void Shutdown();

    /*!
     * \brief ButtonEvent handles a joystick button event, JS_EVENT_BUTTON.
     * \param buttonId The button that received the event.
     * \param value The value of the event, i.e. pressed or released.
     */
    virtual void ButtonEvent(unsigned char buttonId, int value) = 0;
    /*!
     * \brief ButtonEvent handles a joystick axis event, JS_EVENT_AXIS.
     * \param axisId The axis that received the event.
     * \param value The value of the event, i.e. axis position.
     */
    virtual void AxisEvent(unsigned char axisId, int value) = 0;

    /*!
     * \brief TreatGamepadEvent treats an event key of the gamepad.
     * \param event Event from the gamepad, key pressed or an axis input.
     * \param msg Contains all the buttons and axis processed.
     * \return Returns true if the message with the inputs is published now or
     *      false if the message will be published soon.
     */
    int TreatGamepadEvent(js_event& event);

    /*!
     * \brief PublishGamepadState sends the gamepad state through a ROS topic.
     */
    virtual void PublishGamepadState() = 0;

    /*!
     * \brief EventCycle is the gamepad's event processing loop.
     */
    void EventCycle();

    /*!
     * \brief ~Gamepad does nothing.
     */
    virtual ~Gamepad();
};



}



#endif
