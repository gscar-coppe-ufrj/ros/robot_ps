// gamepad.cpp



#include <gamepad/gamepad.h>



using namespace std;
using namespace gamepad;



Gamepad::Gamepad()
{
}


void Gamepad::CycleThreadFunction(Gamepad* gamepad)
{
    gamepad->EventCycle();
}


void Gamepad::OpenGamepad()
{
    bool firstFault = false;

    while (true)
    {
        if (shutdown)
        {
            //ROS_INFO("joystick_nodelet shut down.");
            return;
        }
        gamepadFileDescriptor = open(gamepadPath.c_str(), O_RDONLY);
        if (gamepadFileDescriptor != -1)
        {
            firstFault = true;
            // There seems to be a bug in the driver or something where the
            // initial events that are to define the initial state of the
            // joystick are not the values of the joystick when it was opened
            // but rather the values of the joystick when it was last closed.
            // Opening then closing and opening again is a hack to get more
            // accurate initial state data.
            close(gamepadFileDescriptor);
            gamepadFileDescriptor = open(gamepadPath.c_str(), O_RDONLY);
        }
        if (gamepadFileDescriptor != -1)
            break;

        if (firstFault)
        {
            //ROS_ERROR("Couldn't open gamepad %s. Will retry every second.", gamepadPath.c_str());
            firstFault = false;
        }
        sleep(1.0f);
    }

    //ROS_INFO("Opened gamepad: %s.", gamepadPath.c_str());
}


void Gamepad::CloseGamepad()
{
    if (gamepadFileDescriptor != -1)
    {
        close(gamepadFileDescriptor);
    }
}


void Gamepad::Shutdown()
{
    shutdown = true;
    cycleThread->join();
}


void Gamepad::SetName(string name)
{
    gamepadName = name;
}


string Gamepad::GetName() const
{
    return gamepadName;
}


void Gamepad::SetDevicePath(string path)
{
    gamepadPath = path;
}


string Gamepad::GetDevicePath() const
{
    return gamepadPath;
}


int Gamepad::TreatGamepadEvent(js_event& event)
{
    int eventPriority = Priority::DONT_PUBLISH;
    
    switch(event.type)
    {
    case JS_EVENT_BUTTON:
    case JS_EVENT_BUTTON | JS_EVENT_INIT:
        ButtonEvent(event.number, event.value);
        // For initial events, wait a bit before sending to try to catch
        // all the initial events.
        if ((event.type & JS_EVENT_INIT) == false)
            eventPriority = Priority::PUBLISH_NOW;
        else
            eventPriority = Priority::PUBLISH_SOON;

        break;
    case JS_EVENT_AXIS:
    case JS_EVENT_AXIS | JS_EVENT_INIT:
        // Will wait a bit before sending to try to combine events.
        if ((event.type & JS_EVENT_INIT) == false) // Init event.value is wrong.
            AxisEvent(event.number, event.value);

        eventPriority = Priority::PUBLISH_SOON;
        break;
    default:
        //ROS_WARN("Gamepad: Unknown event type. Please file a ticket."
        //         "Time=%u, Value=%d, Type=%Xh, Number=%d", 
        //         event.time, event.value, event.type, event.number);
        break;
    }
    
    return eventPriority;
}


void Gamepad::EventCycle()
{
    js_event event;
    struct timeval tv;
    fd_set set;    

    OpenGamepad();

    tv.tv_sec = 1;
    tv.tv_usec = 0;

    int publishPriority = Priority::DONT_PUBLISH;

    lastMessageSent = boost::posix_time::microsec_clock::local_time();

    while (shutdown == false)
    {
        FD_ZERO(&set);
        FD_SET(gamepadFileDescriptor, &set);

        int select_out = select(gamepadFileDescriptor + 1, &set, NULL, NULL, &tv);

        if (select_out == -1)
        {
            tv.tv_sec = 0;
            tv.tv_usec = 0;
            continue;
        }

        if (FD_ISSET(gamepadFileDescriptor, &set) == true)
        {
            int retval = read(gamepadFileDescriptor, &event, sizeof(js_event));
            if (retval == -1 && errno != EAGAIN)
            {
                break; // Joystick is probably closed. Definitely occurs.
            }

            publishPriority = TreatGamepadEvent(event);
        }

        if (publishPriority == Priority::PUBLISH_NOW)
        {
            PublishGamepadState();
            publishPriority = Priority::DONT_PUBLISH;
        }

        Time currentTime = boost::posix_time::microsec_clock::local_time();
        TimeDuration delta = currentTime - lastMessageSent;

        // If an axis event occurred, start a timer to combine with other
        // events.
        if (publishPriority == Priority::PUBLISH_SOON)
        {
            if (delta.total_milliseconds() >= coalesceInterval * 1000)
            {
                PublishGamepadState();
                publishPriority = Priority::DONT_PUBLISH;
            }
        }

        /* If nothing is going on, start a timer to do autorepeat.
        if (delta.total_milliseconds() >= autorepeatInterval * 1000)
        {
            PublishGamepadState();
        }*/
    } // End of joystick open loop.

    if (!shutdown)
    {
        //ROS_ERROR("Connection to gamepad device lost unexpectedly. Will reopen.");
    }

    CloseGamepad();
}


Gamepad::~Gamepad()
{
}
