// gamepad_manager_nodelet.cpp



#include <gamepad/gamepad_manager.h>



using namespace std;
using namespace gamepad;



GamepadManager::GamepadManager()
{
}


void GamepadManager::SelectDevices(const vector<string> & list)
{
    // Search for new devices.
    for (auto it = list.begin(); it != list.end(); it++)
    {
        auto findIt = find(deviceList.begin(), deviceList.end(), *it);
        if (findIt == deviceList.end())
        {
            deviceList.push_back(*it);
            CreateGamepad(*it);
        }
    }

    // Search for removed devices.
    vector <string> removedDevices;

    for (auto it = deviceList.begin(); it != deviceList.end(); it++)
    {
        auto findIt = find(list.begin(), list.end(), *it);
        if (findIt == list.end())
        {
            removedDevices.push_back(*it);
        }
    }

    // Remove unselected devices.
    for (auto it = removedDevices.begin(); it != removedDevices.end(); it++)
    {
        auto findIt = find(deviceList.begin(), deviceList.end(), *it);
        CloseGamepad(*it);
        deviceList.erase(findIt);
    }
}


void GamepadManager::onInit()
{
    ros::NodeHandle & privateNH = getPrivateNodeHandle();

    privateNH.param("config", configPath, string(""));
    privateNH.param("field_name", fieldName, string(""));
    privateNH.param<double>("deadzone", deadzone, 0.05);
    privateNH.param<double>("autorepeat_rate", autorepeatRate, 1);
    privateNH.param<double>("coalesce_interval", coalesceInterval, 0.001);

    if (configPath == "")
        ROS_WARN("GamepadManager: No config path set, i'll be unable to save/load configuration. Please use _config:=<path> in your .launch file.");

    if (fieldName == "")
        ROS_WARN("GamepadManager: Please use _field_name:=<name> in your .launch file.");

    netLink = "udev";
    subSystem = "input";
    
    AddAttributeToSearch("name");

    vector<string> devList;
    ConfigLinuxDev(getNodeHandle(), devList, getName());
}


void GamepadManager::CreateGamepad(string devicePath)
{
    if (devicePath.find("unknown") != string::npos)
        return;
    
    Gamepad* gamepad = new GamepadNodelet(getNodeHandle());

    const linux_devs::DeviceInfo* device = GetDeviceByNodePath(devicePath);
    auto it = device->attrsMap.find("name");
    if (it != device->attrsMap.end())
        gamepad->SetName(it->second);
    else
        gamepad->SetName("");

    gamepad->SetDevicePath(devicePath);
    gamepad->deadzone = deadzone;
    gamepad->autorepeatRate = autorepeatRate;
    gamepad->coalesceInterval = coalesceInterval;
    gamepad->StartCycle();

    gamepadList.push_back(gamepad);
}


void GamepadManager::CloseGamepad(string devicePath)
{
    for (auto it = gamepadList.begin(); it != gamepadList.end(); it++)
    {
        Gamepad* gamepad = *it;
        if (gamepad->GetDevicePath() == devicePath)
        {
            gamepad->Shutdown();
            gamepadList.erase(it);

            delete gamepad;
            break;
        }
    }
}


bool GamepadManager::FilterDeviceByPath(std::string& nodePath) const
{
    size_t found = nodePath.find("js");

    if (found == string::npos)
        return Filter::DENY;
    else
        return Filter::ACCEPT;
}


GamepadManager::~GamepadManager()
{
    auto it = gamepadList.begin();
    while (it != gamepadList.end())
    {
        Gamepad* gamepad = *it;
        gamepad->Shutdown();

        delete gamepad;
        it++;
    }
}
