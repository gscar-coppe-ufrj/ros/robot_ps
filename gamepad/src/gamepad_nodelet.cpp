// gamepad_nodelet.h



#include <gamepad/gamepad_nodelet.h>



using namespace std;
using namespace gamepad;



GamepadNodelet::GamepadNodelet(ros::NodeHandle& node)
{
    nodeHandle = node;
    shutdown = false;
    gamepadFileDescriptor = -1;
}


void GamepadNodelet::SetParameters()
{
    scale = -1. / 32767.; // This is hard coded and should change to use InputButtons::GetAxisScale() (somehow...)
    unscaledDeadzone = deadzone / std::abs(scale);
    autorepeatInterval = 1 / autorepeatRate;

    CheckParameters();
}


void GamepadNodelet::CheckParameters()
{
    if (autorepeatRate > 1 / coalesceInterval)
        ROS_WARN("joy_node: autorepeat_rate (%f Hz) > 1/coalesce_interval (%f Hz) does not make sense. Timing behavior is not well defined.", autorepeatRate, 1/coalesceInterval);

    if (deadzone >= 1)
    {
        ROS_WARN("joy_node: deadzone greater than 1 was requested. The semantics of deadzone have changed. It is now related to the range [-1:1].");
        deadzone *= scale;
    }

    if (deadzone > 0.9)
    {
        ROS_WARN("joy_node: deadzone (%f) greater than 0.9, setting it to 0.9", deadzone);
        deadzone = 0.9;
    }

    if (deadzone < 0.01)
    {
        ROS_WARN("joy_node: deadzone (%f) less than 0.01, setting to 0.01. You should have some deadzone...", deadzone);
        deadzone = 0.01;
    }

    if (autorepeatRate < 0)
    {
        ROS_WARN("joy_node: autorepeat_rate (%f) less than 0, setting to 0.", autorepeatRate);
        autorepeatRate = 0;
    }

    if (coalesceInterval < 0)
    {
        ROS_WARN("joy_node: coalesce_interval (%f) less than 0, setting to 0.", coalesceInterval);
        coalesceInterval = 0;
    }
}


void GamepadNodelet::ButtonEvent(unsigned char buttonId, int value)
{
    // Reallocate if is out of boundaries.
    if(buttonId >= message.buttons.size())
    {
        int old_size = message.buttons.size();
        message.buttons.resize(buttonId + 1);
        for (unsigned int i = old_size; i < message.buttons.size(); i++)
            message.buttons[i] = 0;
    }

    message.buttons[buttonId] = (value ? 1 : 0);
}


void GamepadNodelet::AxisEvent(unsigned char axisId, int value)
{
    if(axisId >= message.axes.size())
    {
        int old_size = message.axes.size();
        message.axes.resize(axisId + 1);
        for(unsigned int i = old_size; i < message.axes.size(); i++)
            message.axes[i] = 0.0f;
    }
    double val = value;

    // Deadzone computation
    if (std::abs(val) < unscaledDeadzone)
    {
        message.axes[axisId] = 0;
    }
    else
    {
        val = scale * val;
        double s = std::signbit(val) ? -1 : 1;
        message.axes[axisId] = (val - s * deadzone) / (1 - deadzone);
    }
}


void GamepadNodelet::PublishGamepadState()
{
    if (nodeHandle.ok())
        pub.publish(message);
        lastMessageSent = boost::posix_time::microsec_clock::local_time();
}


void GamepadNodelet::StartCycle()
{
    char hostname[100];
    gethostname(hostname, 100);

    string topicName = hostname;
    replace(topicName.begin(), topicName.end(), '-', '/');

    topicName += string("/gamepads/");

    // Get only the last part of the device path.
    string devicePath = GetDevicePath();
    auto slashPosition = devicePath.find_last_of("/\\");

    topicName += devicePath.substr(slashPosition + 1);

    pub = nodeHandle.advertise<sensor_msgs::Joy>(topicName, 1);

    SetParameters();

    cycleThread = new boost::thread(Gamepad::CycleThreadFunction, this);
}


GamepadNodelet::~GamepadNodelet()
{
}
