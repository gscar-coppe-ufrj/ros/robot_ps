// custom_nodelet.h



#ifndef CUSTOM_LOADER_CUSTOM_NODELET_H
#define CUSTOM_LOADER_CUSTOM_NODELET_H



#include <nodelet/nodelet.h>



namespace custom_loader {



class CustomNodelet : public nodelet::Nodelet
{
public:
    virtual void onInit(){}
};



}



#endif // CUSTOM_NODELET_H
