// receiver.cpp



#include <QGLWidget>
#include <video/receiver.h>



video::Receiver::Receiver()
{
    recording = false;
}


bool video::Receiver::ImageConverter(cv::Mat & image)
{
    imageMutex.lock();

    image.copyTo(originalImage);

    originalImageCopied = originalImage;

    imageRatio = (float)image.cols/(float)image.rows;

    if (originalImageCopied.channels() == 3)
        convertedImage = QImage((const unsigned char*)(originalImageCopied.data), originalImageCopied.cols, originalImageCopied.rows,
                              originalImageCopied.step, QImage::Format_RGB888).rgbSwapped();
    else if (originalImage.channels() == 1)
        convertedImage = QImage((const unsigned char*)(originalImage.data), originalImage.cols, originalImage.rows,
                              originalImage.step, QImage::Format_Indexed8);
    else
        return false;

    convertedImage = QGLWidget::convertToGLFormat(convertedImage);

    imageMutex.unlock();

    for (unsigned int i = 0; i < videoWidgets.size(); i++)
        (videoWidgets[i])->ShowImage();

    return true;
}


video::Receiver::~Receiver()
{
}
