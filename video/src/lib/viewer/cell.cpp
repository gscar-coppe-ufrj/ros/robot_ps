// cell.cpp



#include <QMovie>
#include <video/viewer/cell.h>



video::Cell::Cell(ViewerConfigQWidget* originConfigInput, QWidget* parent) : QGroupBox(parent)
{
    setAlignment(Qt::AlignVCenter);

    sizeHintW = 300;
    sizeHintH = 130;

    setMaximumHeight(130);
    setMaximumWidth(300);

    originConfig = originConfigInput;

    widgetBody = new  QHBoxLayout;
    infoBox = new  QVBoxLayout;
    videoName = new QLabel;
    recordingGifLabel = new QLabel;
    recordingGif = new QMovie("recording.gif");
    rotateLayout = new QHBoxLayout;
    widthInput = new QLineEdit;
    heightInput = new QLineEdit;
    applySize = new QToolButton;
    xLabel = new QLabel("x");
    sizeLayout = new QHBoxLayout;

    widthInput->setFixedWidth(40);
    heightInput->setFixedWidth(40);
    applySize->setFixedSize(24,24);
    xLabel->setFixedWidth(10);
    applySize->setIcon(QIcon(":/video/ok-icon"));

    recordingGifLabel->setMovie(recordingGif);

    rotateButtonCCW = new QToolButton;
    rotateButtonCCW->setIcon(QIcon(ROTATE_BUTTON_COUNTERCLOCKWISE));
    connect(rotateButtonCCW, SIGNAL(clicked()), this, SLOT(RotateImageCCW()));
    rotateButtonCW = new QToolButton;
    rotateButtonCW->setIcon(QIcon(ROTATE_BUTTON_CLOCKWISE));
    connect(rotateButtonCW, SIGNAL(clicked()), this, SLOT(RotateImageCW()));
    recordButton = new QToolButton;
    connect(recordButton, SIGNAL(clicked()), this, SLOT(TreatRecordButton()));

    videoWidgetObject = new VideoWidget(1);
    videoWidgetObject->originVideoCell = this;
    videoWidgetObject->setFixedSize(140,80);

    QObject::connect(this, SIGNAL(mousePressEvent(QMouseEvent*)), this, SLOT(TreatDoubleClick()));
    QObject::connect(applySize, SIGNAL(clicked()), this, SLOT(TreatResolutionButton()));

    setLayout(widgetBody);
    widgetBody->setAlignment(Qt::AlignTop);
    widgetBody->addWidget(videoWidgetObject);
    widgetBody->addLayout(infoBox);
    infoBox->setAlignment(Qt::AlignTop);
    infoBox->addWidget(videoName);
    infoBox->addLayout(rotateLayout);
    infoBox->addLayout(sizeLayout);
    rotateLayout->setAlignment(Qt::AlignLeft);
    rotateLayout->addWidget(rotateButtonCCW);
    rotateLayout->addWidget(rotateButtonCW);
    sizeLayout->setAlignment(Qt::AlignLeft);
    sizeLayout->addWidget(widthInput);
    sizeLayout->addWidget(xLabel);
    sizeLayout->addWidget(heightInput);
    sizeLayout->addWidget(applySize);

    rotateCount = 0;
    hasRecordButton = false;
}


void video::Cell::ChangeRecordStatus()
{
    if (videoWidgetObject != nullptr)
    {
        if (!(videoWidgetObject->noVideo))
        {
            if (!hasRecordButton)
            {
                hasRecordButton = true;
                rotateLayout->addWidget(recordButton);
                rotateLayout->addWidget(recordingGifLabel);
            }
            bool status = videoWidgetObject->videoReceiver->recording;

            if (!status)
            {
                recordButton->setIcon(QIcon(RECORD_BUTTON_START));
                recordingGif->stop();
                recordingGif->jumpToFrame(0);
            }
            else
            {
                recordButton->setIcon(QIcon(RECORD_BUTTON_STOP));
                recordingGif->jumpToFrame(1);
                recordingGif->start();
            }
        }
    }
}

void video::Cell::SetResolution(int w, int h)
{
    widthInput->setText(QString::number(w));
    heightInput->setText(QString::number(h));
}


void video::Cell::RotateImageCCW()
{
    if (!videoWidgetObject->noVideo)
    {
        if (rotateCount == 3)
            rotateCount = 0;
        else
            rotateCount++;

        videoWidgetObject->rotateCount = rotateCount;

        originConfig->Rotate(this);

        TreatDoubleClick();

        //std::cout << rotatecount << std::endl;
    }
}


void video::Cell::RotateImageCW()
{
    if (!(videoWidgetObject->noVideo))
    {
        if (rotateCount == -3)
            rotateCount = 0;
        else
            rotateCount--;

        videoWidgetObject->rotateCount = rotateCount;

        originConfig->Rotate(this);

        TreatDoubleClick();

        //std::cout << rotatecount << std::endl;
    }
}


void video::Cell::TreatDoubleClick()
{
    originConfig->SetCellClicked(videoWidgetObject, this);
}


void video::Cell::SetDeviceName(bool noVideo)
{
    if (!noVideo)
        senderName = QString::fromStdString(videoWidgetObject->videoReceiver->SenderName());
    else
    {
        senderName = "No Video";
        widthInput->hide();
        heightInput->hide();
        applySize->hide();
        xLabel->hide();
    }

    videoName->setText(senderName);
}


void video::Cell::TreatRecordButton()
{
    if (videoWidgetObject->videoReceiver->recording)
        videoWidgetObject->videoReceiver->ChangeRecordStatus(false);
    else
        videoWidgetObject->videoReceiver->ChangeRecordStatus(true);


    ChangeRecordStatus();
}

void video::Cell::TreatResolutionButton()
{
    videoWidgetObject->videoReceiver->ChangeResolution(widthInput->text().toInt(), heightInput->text().toInt());
}


QSize video::Cell::sizeHint() const
{
    return QSize(sizeHintW, sizeHintH);
}
