// viewer_config_qwidget.cpp



#include <QMovie>
#include <video/viewer/viewer_config_qwidget.h>



video::ViewerConfigQWidget::ViewerConfigQWidget(ViewerQWidget* tool)
{
    sizeHintW = 1600;
    sizeHintH = 800;

    originTool = tool;

    hWindowLayout = new QHBoxLayout;
    vBodyLayout = new QVBoxLayout;
    hConfigLayout = new QHBoxLayout;
    gridLayout = new QGridLayout;
    hConfigApply = new QHBoxLayout;
    vCellListLayout = new QVBoxLayout;
    sACellList = new QScrollArea;
    sAWidget = new QWidget;
    sALayout = new QVBoxLayout;

    setLayout(hWindowLayout);

    gridLayout->setHorizontalSpacing(0);
    gridLayout->setVerticalSpacing(0);
    gridLayout->setGeometry(QRect(0, 0, 100, 100));

    numColumns = new QLineEdit;
    numColumns->setText(QString::number(originTool->numColumns));

    numRows = new QLineEdit;
    numRows->setText(QString::number(originTool->numRows));

    oldNumColumns = originTool->numColumns;
    oldNumRows = originTool->numRows;

    columnLabel = new QLabel;
    columnLabel->setText("Columns:");

    rowsLabel = new QLabel;
    rowsLabel->setText("Rows:");

    buttonGridApply = new QToolButton;
    buttonGridApply->setIcon(QIcon(CONFIG_NUM_COLUMNS_ROWS_APPLY));
    connect(buttonGridApply, SIGNAL(clicked()), this, SLOT(ConfigNumRowsApply()));

    buttonApplyAll = new QToolButton;
    buttonApplyAll->setText("Apply");
    connect(buttonApplyAll, SIGNAL(clicked()), this, SLOT(ApplyAll()));

    hConfigLayout->addWidget(columnLabel);
    hConfigLayout->addWidget(numColumns);
    hConfigLayout->addWidget(rowsLabel);
    hConfigLayout->addWidget(numRows);
    hConfigLayout->addWidget(buttonGridApply);


    vCellListLayout->addWidget(sACellList, 1);
    sACellList->setWidgetResizable(true);
    sACellList->setWidget(sAWidget);
    sACellList->setMinimumWidth(320);
    sACellList->setMaximumWidth(320);
    sAWidget->setLayout(sALayout);
    sALayout->setAlignment(Qt::AlignTop);
    //salayout->addWidget(videowidgetobject1, 1);
    //salayout->addWidget(videowidgetobject2, 1);

    vBodyLayout->addLayout(hConfigLayout);
    vBodyLayout->addLayout(gridLayout);
    vBodyLayout->addLayout(hConfigApply);

    //hconfigapply->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding));
    hConfigApply->addWidget(buttonApplyAll);

    hWindowLayout->addLayout(vBodyLayout);
    hWindowLayout->addLayout(vCellListLayout);

    videoObjectCreated = false;
    isCellClicked = false;
    isVideoClicked = false;

    AddCellsInScroll();
}


void video::ViewerConfigQWidget::AddCellsInScroll()
{
    Cell* videoCell = new Cell(this);
    videoCell->SetDeviceName(true);
    sALayout->addWidget(videoCell, 1);
    videoCellList.push_back(videoCell);

    auto list = originTool->GetListOfComponents<Viewer, Receiver>();
    for (std::deque<Receiver*>::iterator receiver = list->begin(); receiver != list->end(); receiver++)
    {
        Cell* videoCell = new Cell(this);
        videoCell->SetResolution((*receiver)->width, (*receiver)->height);
        videoCell->videoWidgetObject->ConnectReceiver((*receiver));
        videoCell->SetDeviceName(false);
        videoCell->ChangeRecordStatus();
        sALayout->addWidget(videoCell, 1);
        videoCellList.push_back(videoCell);
        //cout << "created" << endl;
    }
}


QSize video::ViewerConfigQWidget::sizeHint() const
{
    return QSize(sizeHintW, sizeHintH);
}


void video::ViewerConfigQWidget::closeEvent(QCloseEvent *event)
{
    originTool->DeleteConfigurator();
}


void video::ViewerConfigQWidget::CreateCells()
{
    DeleteCells();

    //int quant = (numrows->text().toInt() * numcolumns->text().toInt());

    CreateVideoWidget();

    std::deque<VideoWidget*>::iterator videoWidgetObjectIt = videoWidgetObjectList.begin();
    int limit = videoWidgetObjectList.size();
    int count = 0;

    for (int i = 0; i < numRows->text().toInt(); i++)
    {
        for (int j = 0; j < numColumns->text().toInt(); j++)
        {
            if (count < limit)
            {
                QWidget* blankQWidget = new QWidget;
                QVBoxLayout* blankLayout = new QVBoxLayout;
                blankQWidget->setLayout(blankLayout);
                blankLayout->addWidget(*videoWidgetObjectIt);
                blankLayout->setSpacing(0);
                blankLayout->setMargin(0);
                blankLayout->setContentsMargins(2, 2, 2, 2);
                blankQWidget->setStyleSheet("border: 40px solid #f2f1f0;");
                (*videoWidgetObjectIt)->SetBlankQwidget(blankQWidget);
                gridLayout->addWidget(blankQWidget, i, j);
                videoWidgetObjectIt++;
                blankQWidgetList.push_back(blankQWidget);
            }
            else
            {
                VideoWidget* videoWidgetObject = new VideoWidget(3);
                videoWidgetObject->originConfig = this;
                videoWidgetObjectList.push_back(videoWidgetObject);

                QWidget* blankQWidget = new QWidget;
                QVBoxLayout* blankLayout = new QVBoxLayout;
                blankQWidget->setLayout(blankLayout);
                blankLayout->addWidget(videoWidgetObject);
                blankLayout->setSpacing(0);
                blankLayout->setMargin(0);
                blankLayout->setContentsMargins(2, 2, 2, 2);
                blankQWidget->setStyleSheet("border: 40px solid #f2f1f0;");
                videoWidgetObject->SetBlankQwidget(blankQWidget);
                gridLayout->addWidget(blankQWidget, i, j);
                blankQWidgetList.push_back(blankQWidget);
            }
            count++;
        }
    }
    glFinish();
    videoObjectCreated = true;
}


void video::ViewerConfigQWidget::DeleteCells()
{
    for (std::deque<VideoWidget*>::iterator videoWidgetObject = videoWidgetObjectList.begin(); videoWidgetObject != videoWidgetObjectList.end(); videoWidgetObject++)
        delete (*videoWidgetObject);

    for (std::deque<QWidget*>::iterator blankQWidget = blankQWidgetList.begin(); blankQWidget != blankQWidgetList.end(); blankQWidget++)
        delete (*blankQWidget);

    videoWidgetObjectList.clear();
    blankQWidgetList.clear();
}


void video::ViewerConfigQWidget::CreateVideoWidget()
{
    for (std::deque<VideoWidget*>::iterator videoWidgetObjectIt = originTool->videoWidgetObjectList.begin(); videoWidgetObjectIt != originTool->videoWidgetObjectList.end(); videoWidgetObjectIt++)
    {
        VideoWidget* videoWidgetObject = new VideoWidget(3);
        videoWidgetObject->originConfig = this;
        if (!(*videoWidgetObjectIt)->noVideo)
        {
            videoWidgetObject->rotateCount = (*videoWidgetObjectIt)->rotateCount;
            videoWidgetObject->ConnectReceiver((*videoWidgetObjectIt)->videoReceiver);
        }
        videoWidgetObjectList.push_back(videoWidgetObject);

        //std::cout << "i = " << i << std::endl;
    }
}


void video::ViewerConfigQWidget::ChangeVideoWidget()
{
    Receiver* receiver;

    if (videoWidgetCell->videoReceiver != NULL || videoWidgetCell->noVideo)
    {
        if (isVideoClicked && videoWidget->videoReceiver != videoWidgetCell->videoReceiver)
        {
            int noVideoLocal = videoWidget->noVideo;
            if (!noVideoLocal)
            {
                receiver = videoWidget->videoReceiver;
                receiver->imageMutex.lock();
            }

            if (!(videoWidgetCell->noVideo) && (videoWidgetCell->videoReceiver != NULL))
                videoWidgetCell->videoReceiver->imageMutex.lock();

            videoWidget->DisconnectReceiver();
            if (!videoWidgetCell->noVideo)
                videoWidget->ConnectReceiver(videoWidgetCell->videoReceiver);
            videoWidget->rotateCount = videoWidgetCell->rotateCount;
            ClearBackgroundsCellList(videoCell);
            isCellClicked = false;

            if (videoWidget->noVideo)
                videoWidget->FillBlack();

            if (!noVideoLocal)
                receiver->imageMutex.unlock();

            if (!(videoWidgetCell->noVideo) && (videoWidgetCell->videoReceiver != NULL))
                videoWidgetCell->videoReceiver->imageMutex.unlock();
        }
        else
        {
            ClearBackgroundsCellList(videoCell);
            isCellClicked = false;
        }
    }
}


void video::ViewerConfigQWidget::Rotate(Cell *videoCellInput)
{
    videoWidgetCell = videoCellInput->videoWidgetObject;
    if (!(videoWidgetCell->noVideo) && videoWidget->videoReceiver == videoWidgetCell->videoReceiver)
    {
            //std::cout << "rotate" << std::endl;

            videoWidget->rotateCount = videoWidgetCell->rotateCount;
    }
}


void video::ViewerConfigQWidget::ClearBackgroundsCellList(Cell* videoCellInput)
{
    for (std::deque<Cell*>::iterator videoCellIt = videoCellList.begin(); videoCellIt != videoCellList.end(); videoCellIt++)
    {
        if ((*videoCellIt) != videoCellInput)
            (*videoCellIt)->setStyleSheet("");
    }
}


void video::ViewerConfigQWidget::ClearBordersBlankQWidgetList()
{
    for (std::deque<QWidget*>::iterator blankQWidgetIt = blankQWidgetList.begin(); blankQWidgetIt != blankQWidgetList.end(); blankQWidgetIt++)
            (*blankQWidgetIt)->setStyleSheet("border: 3px solid #f2f1f0;");
}


void video::ViewerConfigQWidget::SetCellClicked(VideoWidget* videoWidget, Cell* videoCellInput)
{
    videoCell = videoCellInput;
    videoCell->setStyleSheet("background-color: #FF9900;");
    ClearBackgroundsCellList(videoCell);
    videoWidgetCell = videoWidget;
    isCellClicked = true;
    ChangeVideoWidget();
}


void video::ViewerConfigQWidget::SetVideoClicked(VideoWidget* videoWidgetInput)
{
    isVideoClicked = true;
    videoWidget = videoWidgetInput;
    ClearBordersBlankQWidgetList();
    videoWidgetInput->blankQWidget->setStyleSheet("border: 2px solid #ff9900;");

    for (std::deque<Cell*>::iterator videoCellIt = videoCellList.begin(); videoCellIt != videoCellList.end(); videoCellIt++)
    {
        if ((*videoCellIt)->videoWidgetObject->videoReceiver == videoWidgetInput->videoReceiver)
        {
            (*videoCellIt)->setStyleSheet("background-color: #FF9900;");
            sACellList->ensureWidgetVisible(*videoCellIt);
            (*videoCellIt)->rotateCount = (*videoCellIt)->videoWidgetObject->rotateCount = videoWidgetInput->rotateCount;
            ClearBackgroundsCellList(*videoCellIt);
        }
    }
}


void video::ViewerConfigQWidget::ConfigNumRowsApply()
{
    oldNumColumns = numColumns->text().toInt();
    oldNumRows = numRows->text().toInt();

    CreateCells();
    SetVideoClicked(videoWidgetObjectList.at(0));
}


void video::ViewerConfigQWidget::ApplyAll()
{
    originTool->CreateVideoWidget(videoWidgetObjectList, oldNumRows, oldNumColumns);
    originTool->DeleteConfigurator();
}


video::ViewerConfigQWidget::~ViewerConfigQWidget()
{
    for (std::deque<VideoWidget*>::iterator videoWidgetObject = videoWidgetObjectList.begin(); videoWidgetObject != videoWidgetObjectList.end(); videoWidgetObject++)
        delete (*videoWidgetObject);
    videoWidgetObjectList.clear();
}
