// viewer_qwidget.cpp



#include <video/viewer/viewer_qwidget.h>



video::ViewerQWidget::ViewerQWidget()
{
    sizeHintW = 640;
    sizeHintH = 480;

    numRows = numColumns = 1;

    gridLayout = new QGridLayout;

    gridLayout->setHorizontalSpacing(1);
    gridLayout->setVerticalSpacing(1);

    QObject::connect(this, SIGNAL(mouseDoubleClickEvent(QMouseEvent*)), this, SLOT(TreatDoubleClick()));

    configuratorCreated = false;

    setLayout(gridLayout);

    VideoWidget* videoWidgetObject = new VideoWidget(2);
    videoWidgetObject->originVideoWidget=this;
    videoWidgetObjectList.push_back(videoWidgetObject);

    gridLayout->addWidget(videoWidgetObjectList[0], 0, 0);
}


void video::ViewerQWidget::DeleteVideoWidgets()
{
    for (std::deque<VideoWidget*>::iterator videoWidgetObject = videoWidgetObjectList.begin(); videoWidgetObject != videoWidgetObjectList.end(); videoWidgetObject++)
        delete (*videoWidgetObject);

    videoWidgetObjectList.clear();
}


void video::ViewerQWidget::CreateVideoWidget(std::deque<VideoWidget*> videoWidgetObjectListInput, int numRowsInput, int numColumnsInput)
{
    DeleteVideoWidgets();

    numRows = numRowsInput;
    numColumns = numColumnsInput;

    for (std::deque<VideoWidget*>::iterator videoWidgetObjectIt = videoWidgetObjectListInput.begin(); videoWidgetObjectIt != videoWidgetObjectListInput.end(); videoWidgetObjectIt++)
    {
        VideoWidget* videoWidgetObject = new VideoWidget(2);
        if(!(*videoWidgetObjectIt)->noVideo)
            videoWidgetObject->ConnectReceiver((*videoWidgetObjectIt)->videoReceiver);
        videoWidgetObject->originVideoWidget = this;
        videoWidgetObject->rotateCount = (*videoWidgetObjectIt)->rotateCount;
        videoWidgetObjectList.push_back(videoWidgetObject);
    }

    int count = 0;
    for (int i = 0; i < numRows; i++)
    {
        for (int j = 0; j < numColumns; j++)
        {
            gridLayout->addWidget(videoWidgetObjectList.at(count), i, j);
            count++;
        }
    }
}


QSize video::ViewerQWidget::sizeHint() const
{
    return QSize(sizeHintW, sizeHintH);
}


void video::ViewerQWidget::TreatDoubleClick()
{
    //std::cout << "double click" << std::endl;
    if (!configuratorCreated)
    {
        configurator = new ViewerConfigQWidget(this);
        configuratorCreated = true;
        configurator->show();
        configurator->CreateCells();
        configurator->SetVideoClicked(configurator->videoWidgetObjectList.at(0));
    }
}


void video::ViewerQWidget::DeleteConfigurator()
{
    if (configuratorCreated)
    {
        delete configurator;
        configuratorCreated = false;
        //std::cout << "delete configurator" << std::endl;
    }
}


video::ViewerQWidget::~ViewerQWidget()
{
    for (std::deque<VideoWidget*>::iterator videowidgetobject = videoWidgetObjectList.begin(); videowidgetobject != videoWidgetObjectList.end(); videowidgetobject++)
        delete (*videowidgetobject);
    videoWidgetObjectList.clear();
    DeleteConfigurator();
}
