// video_widget.cpp



#include <video/viewer/video_widget.h>



video::VideoWidget::VideoWidget(int type, QWidget* parent) : QGLWidget(parent)
{
    noVideo = true;

    sizeHintW = 160;
    sizeHintH = 120;

    imageStarted = false;

    sceneChanged = false;
    bgColor = QColor::fromRgb(0, 0, 0);

    outH = 0;
    outW = 0;
    imgRatio = 16.0f/9.0f;
    //mImgRatio = mRenderQtImg.width()/mRenderQtImg.height();

    posX = 0;
    posY = 0;

    QObject::connect(this, SIGNAL(mousePressEvent(QMouseEvent*)), this, SLOT(TreatClick()));
    QObject::connect(this, SIGNAL(mouseDoubleClickEvent(QMouseEvent*)), this, SLOT(TreatDoubleClick()));

    rotateCount = 0;

    widgetType = type;

    FillBlack();
}


void video::VideoWidget::SetBlankQwidget(QWidget* blankQWidgetInput)
{
    blankQWidget = blankQWidgetInput;
}

void video::VideoWidget::GotResolution(int w, int h)
{
    originVideoCell->SetResolution(w, h);
    internalH = h;
    internalW = w;
}


void video::VideoWidget::TreatClick()
{
    if (widgetType==1)
        originVideoCell->TreatDoubleClick();
    else if (widgetType==3)
        originConfig->SetVideoClicked(this);
}


void video::VideoWidget::ChangeRecordStatus()
{
    if (originVideoCell != nullptr)
        originVideoCell->ChangeRecordStatus();
}


void video::VideoWidget::ConnectReceiver(Receiver* videoReceiverInput)
{
    videoReceiver = videoReceiverInput;
    videoReceiver->videoWidgets.push_back(this);

    noVideo = false;
}



void video::VideoWidget::TreatDoubleClick()
{
    if (widgetType == 2)
        originVideoWidget->TreatDoubleClick();
}


QSize video::VideoWidget::sizeHint() const
{
    return QSize(sizeHintW, sizeHintH);
}

void video::VideoWidget::initializeGL()
{
    makeCurrent();
    qglClearColor(bgColor);
}

void video::VideoWidget::resizeGL(int width, int height)
{
    widgetHeight = height;
    widgetWidth = width;

    makeCurrent();
    glViewport(0, 0, (GLint)width, (GLint)height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0, width, 0, height, 0, 1);	// To Draw image in the center of the area

    glMatrixMode(GL_MODELVIEW);

    // ---> Scaled Image Sizes
    outH = width/imgRatio;
    outW = width;

    if (outH > height)
    {
        outW = height*imgRatio;
        outH = height;
    }

    emit ImageSizeChanged(outW, outH);
    // <--- Scaled Image Sizes

    posX = (width - outW)/2;
    posY = (height - outH)/2;


    sceneChanged = true;

    UpdateScene();
}

void video::VideoWidget::UpdateScene()
{
    if (sceneChanged && this->isVisible())
        update();
}

void video::VideoWidget::paintGL()
{
    makeCurrent();

    if (!sceneChanged)
        return;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    RenderImage();

    sceneChanged = false;
}

void video::VideoWidget::RenderImage()
{
    if (!imageStarted)
        return;

    if (!noVideo)
        videoReceiver->imageMutex.lock();

    makeCurrent();

    glClear(GL_COLOR_BUFFER_BIT);

    if (!renderQtImg.isNull())
    {
        glLoadIdentity();

        QImage image; // the image rendered

        glPushMatrix();
        {
            int tempOutH = outH;
            int tempOutW = outW;

            int imW = renderQtImg.width();
            int imH = renderQtImg.height();

            QTransform rot;
            rot.rotate(90.0*rotateCount, Qt::ZAxis);
            renderQtImg = renderQtImg.transformed(rot, Qt::SmoothTransformation);
            //image.trueMatrix(rot, imW, imH);
            if (rotateCount%2 != 0)
            {

                tempOutH = outW;
                tempOutW = outH;
            }

            imgRatio = ((float)renderQtImg.width())/((float)renderQtImg.height());


            // ---> Scaled Image Sizes
            tempOutH = widgetWidth/imgRatio;
            tempOutW = widgetWidth;

            if (tempOutH>widgetHeight)
            {
                tempOutW = widgetHeight*imgRatio;
                tempOutH = widgetHeight;
            }
            // <--- Scaled Image Sizes

            posX = (widgetWidth - tempOutW)/2;
            posY = (widgetHeight - tempOutH)/2;

            // The image is to be resized to fit the widget?
            if (imW != this->size().width() && imH != this->size().height() && renderQtImg.width() != 0 && renderQtImg.height() != 0)
            {
                //std::cout << "width = " << mRenderQtImg.width() << " height = " << mRenderQtImg.height() << std::endl ;
                image = renderQtImg.scaled( //this->size(),
                                             QSize(tempOutW,tempOutH),
                                             Qt::IgnoreAspectRatio,
                                             Qt::SmoothTransformation
                                            );

                //qDebug( QString( "Image size: (%1x%2)").arg(imW).arg(imH).toAscii() );
            }
            else
                image = renderQtImg;

            // ---> Centering image in draw area            

            glRasterPos2i(posX, posY);
            //std::cout << "mPosX = " << mPosX << "mPosY = " << mPosY << std::endl;

            // <--- Centering image in draw area

            imW = image.width();
            imH = image.height();

            glDrawPixels(imW, imH, GL_RGBA, GL_UNSIGNED_BYTE, image.bits());
        }

        glPopMatrix();

        // end
        glFlush();
        //glFinish();


        if (!noVideo)
            videoReceiver->imageMutex.unlock();

        image = QImage();
    }
}


void video::VideoWidget::FillBlack()
{
    renderQtImg = QImage(640, 480, QImage::Format_RGB888);
    renderQtImg.fill(bgColor.rgb());
    //std::cout << "showImage = " << mRenderQtImg.width() << std::endl;
    //std::cout << "showImage = " << mRenderQtImg.height() << std::endl;
    sceneChanged = true;
    UpdateScene();

}


void video::VideoWidget::DisconnectReceiver()
{
    if (!noVideo)
    {
        std::deque<VideoBase*>::iterator videoWidget = std::find(videoReceiver->videoWidgets.begin(), videoReceiver->videoWidgets.end(), (VideoBase*)this);
        if (videoWidget != videoReceiver->videoWidgets.end())
        {
            videoReceiver->videoWidgets.erase(videoWidget);
            videoReceiver = NULL;
        }
    }

    noVideo = true;
}


bool video::VideoWidget::ShowImage()
{
        if (!noVideo)
        {
            videoReceiver->imageMutex.lock();

            renderQtImg = videoReceiver->convertedImage;

            imageStarted = true;
            sceneChanged = true;

            //std::cout << "showImage = " << mRenderQtImg.width() << deviceNumber << std::endl;
            //std::cout << "showImage = " << mRenderQtImg.height() << std::endl;

            if (renderQtImg.width() != 0 && renderQtImg.height() != 0)
                UpdateScene();

            videoReceiver->imageMutex.unlock();
        }
        else
            FillBlack();

        return true;
}


video::VideoWidget::~VideoWidget()
{
    DisconnectReceiver();
}
