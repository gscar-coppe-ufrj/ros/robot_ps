// nodelets_plugins.cpp



#include <pluginlib/class_list_macros.h>
#include <video/receiver_nodelet.h>
#include <video/web_camera.h>
#include <video/url_camera.h>



PLUGINLIB_EXPORT_CLASS(video::ReceiverNodelet, nodelet::Nodelet)
PLUGINLIB_EXPORT_CLASS(video::WebCamera, nodelet::Nodelet)
PLUGINLIB_EXPORT_CLASS(video::URLCamera, nodelet::Nodelet)
