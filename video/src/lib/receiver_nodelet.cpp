// receiver_nodelet.cpp



#include <video/receiver_nodelet.h>



video::ReceiverNodelet::ReceiverNodelet()
{
    stopReceiving = true;
}


void video::ReceiverNodelet::onInit()
{
    privateNodeHandle = getPrivateNodeHandle();
    senderName;
    privateNodeHandle.param("sender_name", senderName, getName());

    ros::NodeHandle & nodeHandle = getNodeHandle();

    msgStartStopSub = nodeHandle.subscribe(getName() + "/stop_receiving", 1000, &ReceiverNodelet::TreatStopReceiving, this);
    msgRecordingSub = nodeHandle.subscribe(senderName + "/recording", 1000, &ReceiverNodelet::TreatRecording, this);
    msgResolutionSub = nodeHandle.subscribe(senderName + "/resolution", 1000, &ReceiverNodelet::TreatResolution, this);

    msgRecordPub = nodeHandle.advertise<std_msgs::Bool>(senderName + "/record", 1000);
    msgResolutionPub = nodeHandle.advertise<video::Resolution>(senderName + "/set_resolution", 1000);


    StopReceiving(false);
}


void video::ReceiverNodelet::TreatRecording(const std_msgs::BoolPtr & msg)
{
    if (msg->data == true)
        recording = true;
    else
        recording = false;

    for (unsigned int i = 0; i < videoWidgets.size(); i++)
        (videoWidgets[i])->ChangeRecordStatus();
}


void video::ReceiverNodelet::ChangeRecordStatus(bool statusInput)
{
    std_msgs::Bool msg;

    if (statusInput)
    {
        if (!recording)
        {
            msg.data = true;
            msgRecordPub.publish(msg);

            recording = true;
        }
    }
    else
    {
        if (recording)
        {
            msg.data = false;
            msgRecordPub.publish(msg);

            recording = false;
        }
    }
}

void video::ReceiverNodelet::ChangeResolution(int w, int h)
{
    video::Resolution msg;
    msg.width = w;
    msg.height = h;
    msgResolutionPub.publish(msg);
}


std::string video::ReceiverNodelet::SenderName()
{
    return senderName;
}


void video::ReceiverNodelet::TreatStopReceiving(const std_msgs::BoolPtr & msg)
{
    StopReceiving(msg->data);
}


void video::ReceiverNodelet::TreatResolution(const video::ResolutionConstPtr & res)
{
    for (unsigned int i = 0; i < videoWidgets.size(); i++)
        (videoWidgets[i])->GotResolution(res->width, res->height);
}

void video::ReceiverNodelet::TreatStream(const sensor_msgs::ImageConstPtr & originalImage)
{
    cv_bridge::CvImagePtr cvPtr;
    sensor_msgs::Image msg2 = *originalImage;

    try
    {
        //Always copy, returning a mutable CvImage
        //OpenCV expects color images to use BGR channel order.
        cvPtr = cv_bridge::toCvCopy(msg2, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
        //if there is an error during conversion, display it
        ROS_ERROR("tutorialROSOpenCV::main.cpp::cv_bridge exception: %s", e.what());
        return;
    }

    ImageConverter(cvPtr->image);
}


void video::ReceiverNodelet::StopReceiving(bool stop)
{
    if (stop)
    {
        if (stop != stopReceiving)
        {
            stopReceiving = stop;

            msgStreamSub.shutdown();
        }
    }
    else
    {
        if (stop != stopReceiving)
        {
            stopReceiving = stop;

            image_transport::ImageTransport it(getNodeHandle());
            msgStreamSub = it.subscribe(senderName + "/stream", 1, &ReceiverNodelet::TreatStream, this, image_transport::TransportHints("raw", ros::TransportHints(), privateNodeHandle));
        }
    }
}


video::ReceiverNodelet::~ReceiverNodelet()
{
}
