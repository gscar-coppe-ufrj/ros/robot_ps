// sender_nodelet.cpp



#include <video/sender_nodelet.h>



video::SenderNodelet::SenderNodelet()
{
    videoWriterStarted = false;
    startRecord = false;

    stopThread = false;
    Load();
}


void video::SenderNodelet::onInit()
{
    ros::NodeHandle & privateNH = getPrivateNodeHandle();
    int frameRate;
    if (privateNH.getParam("frame_rate", frameRate))
        SetDelay((1.0/frameRate)*1000);

    privateNH.param<std::string>("output_path", outputPath, "");

    privateNH.getParam("width", width);
    privateNH.getParam("height", height);
    privateNH.getParam("config", savePath);

    nodeHandle = getNodeHandle();

    msgFrameRateSub = nodeHandle.subscribe(getName() + "/frame_rate", 1, &SenderNodelet::TreatFrameRate, this);
    msgPlaySub = nodeHandle.subscribe(getName() + "/play", 1000, &SenderNodelet::TreatPlay, this);
    msgRecordSub = nodeHandle.subscribe(getName() + "/record", 1000, &SenderNodelet::TreatRecord, this);
    msgResolutionSub = nodeHandle.subscribe(getName() + "/set_resolution", 1000, &SenderNodelet::TreatResolution, this);

    image_transport::ImageTransport it(nodeHandle);
    msgImagePub = it.advertise(getName() + "/stream", 1, true);
    msgRecordingPub = nodeHandle.advertise<std_msgs::Bool>(getName() + "/recording", 1000, true);
    msgResolutionPub = nodeHandle.advertise<video::Resolution>(getName() + "/resolution", 1, true);

    SendResolution();
    internalThread = new boost::thread(Sender::InternalThreadFunction, this);
}


std::string video::SenderNodelet::GetTimeStr()
{
    std::time_t rawtime;
    std::tm* timeinfo;
    char buffer [80];

    std::time(&rawtime);
    timeinfo = std::localtime(&rawtime);

    std::strftime(buffer, 80 ,"%Y-%m-%d-%H-%M-%S", timeinfo);

    std::string strTime = buffer;

    return strTime;
}


void video::SenderNodelet::TreatFrameRate(const std_msgs::Int32Ptr & msg)
{
    if (msg->data==0)
        SetDelay(10000);
    else
        SetDelay((1.0/msg->data)*1000);
}


void video::SenderNodelet::TreatPlay(const std_msgs::BoolPtr & msg)
{
    SetStart(msg->data);
}


void video::SenderNodelet::TreatRecord(const std_msgs::BoolPtr & msg)
{
    startRecord = msg->data;
    if (startRecord)
    {
        if (!videoWriterStarted)
            StartVideoWriter(frame);
    }
    else
    {
        if(videoWriterStarted)
        {
            videoWriterStarted = false;

            std_msgs::Bool msg;
            msg.data = false;
            msgRecordingPub.publish(msg);

            mutex.lock();
            bag.close();
            videoWriter->release();
            delete recordThread;
            delete videoWriter;

            mutex.unlock();
         }
    }
}

void video::SenderNodelet::TreatResolution(const video::ResolutionPtr &msg)
{
    width = msg->width;
    height = msg->height;

    stopThread = true;
    internalThread->join();
    vCapture.release();
    delete internalThread;
    stopThread = false;
    internalThread = new boost::thread(Sender::InternalThreadFunction, this);
}


void video::SenderNodelet::RecordThreadFunction(SenderNodelet* videoSenderNodelet)
{
    videoSenderNodelet->WriteFrame();
}


void video::SenderNodelet::WriteFrame()
{
    double frameRate = 23.9;
    boost::posix_time::ptime init, final;

    while (videoWriterStarted)
    {
        init = boost::posix_time::microsec_clock::local_time();

        cv_bridge::CvImage outMsg;

        outMsg.encoding = sensor_msgs::image_encodings::BGR8;

        outMsg.image = frame;

        mutex.lock();

        // Save frame to video
        videoWriter->write(frame);

        bag.write(getName() + "/stream", ros::Time::now(), outMsg.toImageMsg());

        mutex.unlock();

        final = boost::posix_time::microsec_clock::local_time();

        tDifference = final - init;

        float diff = 1000000.0/frameRate - tDifference.total_microseconds();

        if(diff < 0)
            diff = 0;

        //std::cout << "diff = " << diff << std::endl;

        usleep(diff);
    }
}

void video::SenderNodelet::SendResolution()
{
    video::Resolution msg;
    msg.height = height;
    msg.width = width;
    msgResolutionPub.publish(msg);
}


void video::SenderNodelet::StartVideoWriter(cv::Mat image)
{
    bag.open(outputPath + GetTimeStr() + ".bag", rosbag::bagmode::Write);

    videoWriter = new cv::VideoWriter(outputPath + GetTimeStr() + ".avi", CV_FOURCC('D','I','V','X'), 23.9, cv::Size(image.cols, image.rows));
    videoWriterStarted = true;

    nextFrameTimestamp = boost::posix_time::microsec_clock::local_time();
    currentFrameTimestamp = nextFrameTimestamp;
    tDifference = (currentFrameTimestamp - nextFrameTimestamp);

    std_msgs::Bool msg;
    msg.data = true;
    msgRecordingPub.publish(msg);

    recordThread = new boost::thread(SenderNodelet::RecordThreadFunction, this);
}


void video::SenderNodelet::PublishImage()
{
    cv_bridge::CvImage outMsg;

    outMsg.encoding = sensor_msgs::image_encodings::BGR8;

    outMsg.image = frame;

    msgImagePub.publish(outMsg.toImageMsg());
}


video::SenderNodelet::~SenderNodelet()
{
    Save();
    stopThread = true;
    internalThread->join();
    delete internalThread;

    if (videoWriterStarted)
    {
        videoWriterStarted = false;

        mutex.lock();
        bag.close();
        videoWriter->release();
        delete videoWriter;

        mutex.unlock();

        delete recordThread;
     }
}
