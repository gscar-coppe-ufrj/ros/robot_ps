// url_camera.cpp



#include <video/url_camera.h>



void video::URLCamera::onInit()
{
    ros::NodeHandle & privateNH = getPrivateNodeHandle();
    privateNH.param("url", url, std::string(""));

    video::SenderNodelet::onInit();
}


void video::URLCamera::CaptureImage()
{
    bool bSuccess = vCapture.read(frame);

    if (!bSuccess)
    {
        std::cout << "Cannot read a frame from video stream " << url << std::endl;
        cameraIsOpened = true;
        //capturefail = true;
        vCapture.release();
        OpenCamera();
    }
}


void video::URLCamera::OpenCamera()
{
    while (!vCapture.isOpened() && !stopThread)
    {
        if (url != "")
        {
            if (!vCapture.open(url))
            {
                cameraIsOpened = false;
                std::cout << "Couldn't open from url: " << url << std::endl;
            }
            else
            {
                cameraIsOpened = true;
                vCapture.set(CV_CAP_PROP_FRAME_WIDTH, width);
                vCapture.set(CV_CAP_PROP_FRAME_HEIGHT, height);
            }
        }
        sleep(1);
    }
}


video::URLCamera::~URLCamera()
{
    stopThread = true;
    vCapture.release();
}
