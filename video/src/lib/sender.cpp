// sender.cpp



#include <video/sender.h>


video::Sender::Sender()
{
    captureFail = false;
    delay = boost::posix_time::milliseconds(33);

    start = true;

    cameraIsOpened = false;

    width = 640;
    height = 480;
}


void video::Sender::StartThread()
{
}


void video::Sender::InternalThreadFunction(Sender *videoSender)
{
    videoSender->ThreadFunction();
}


void video::Sender::SetDelay(int milli)
{
    delay = boost::posix_time::milliseconds(milli);
}


void video::Sender::SetStart(bool startInput)
{
    start = startInput;
}


void video::Sender::ThreadFunction()
{
    Time now, startTimer;
    TimeDuration end;
    while (!stopThread)
    {
        if (start)
        {
            startTimer = boost::posix_time::microsec_clock::local_time();

            if(!cameraIsOpened)
                OpenCamera();
            CaptureImage();
            PublishImage();

            now = boost::posix_time::microsec_clock::local_time();

            startTimer = startTimer + delay;
            end = startTimer - now;
            boost::this_thread::sleep(end);
        }
    }
}

void video::Sender::Load()
{
    if (savePath == "")
        return;
    SimpleXMLSettings settings(savePath + "_settings", "VideoSender");

    settings.BeginLoad();
    settings.BeginGroup("video");
    width = settings.LoadNumber("width", width);
    height = settings.LoadNumber("height", height);
    settings.EndGroup();
    settings.EndLoad();
}

void video::Sender::Save()
{
    if (savePath == "")
        return;
    SimpleXMLSettings settings(savePath + "_settings", "VideoSender");

    settings.BeginSave();
    settings.BeginGroup("video");
    settings.SaveNumber("width", width);
    settings.SaveNumber("height", height);
    settings.EndGroup();
    settings.EndSave();
}


void video::Sender::CaptureImage()
{
}


void video::Sender::OpenCamera()
{
}


video::Sender::~Sender()
{

}
