// web_camera.cpp



#include <video/web_camera.h>



video::WebCamera::WebCamera()
{
    cameraIsOpened = false;
}


void video::WebCamera::onInit()
{
    ros::NodeHandle & privateNH = getPrivateNodeHandle();
    if (privateNH.getParam("open_number", openNumber))
    {
        std::stringstream ss;
        ss << openNumber;
        device = "/dev/video" + ss.str();
    }
    else
        openNumber = 0;
    privateNH.param("config", configPath, std::string(""));
    privateNH.param("field_name", fieldName, std::string(""));

    netLink = "udev";
    subSystem = "video4linux";
    std::vector<std::string> list;
    list.push_back(device);
    ConfigLinuxDev(getNodeHandle(), list, getName());
    video::SenderNodelet::onInit();
}


void video::WebCamera::CaptureImage()
{
    bool bSuccess = vCapture.read(frame);

    if (!bSuccess)
    {
        std::cout << "Cannot read a frame from video stream" << openNumber << std::endl;
        cameraIsOpened = true;
        //capturefail = true;
        vCapture.release();
        OpenCamera();
    }
}


void video::WebCamera::OpenCamera()
{
    while (!vCapture.isOpened() && !stopThread)
    {
        if (device != "")
        {
            if( !vCapture.open(openNumber) )
            {
                cameraIsOpened = false;
                std::cout << "The device number " << openNumber << " is already in use or doesn't exist." << std::endl;
                sleep(2);
            }
            else
            {
                cameraIsOpened = true;
                vCapture.set(CV_CAP_PROP_FRAME_WIDTH, width);
                vCapture.set(CV_CAP_PROP_FRAME_HEIGHT, height);
            }
        }
        sleep(1);
    }
}


void video::WebCamera::SelectDevices(const std::vector<std::string> & list)
{
    if (list.size() > 0)
    {
        auto temp = list.front();

        std::string str = "video";
        auto i = temp.find(str);
        if (i != std::string::npos)
        {
            openNumber = std::stoi(temp.substr(i + str.size()));
            vCapture.release();
            device = temp;
            return;
        }
    }

    vCapture.release();
    device = "";
}


video::WebCamera::~WebCamera()
{
    stopThread = true;
    vCapture.release();
}
