// web_camera.h



#ifndef VIDEO_WEB_CAMERA_H
#define VIDEO_WEB_CAMERA_H



#include <linux_devs/linux_dev_manager.h>
#include "sender_nodelet.h"



namespace video {



class WebCamera : public SenderNodelet, public linux_devs::LinuxDevManager
{
    std::string device;
    int openNumber;

protected:
    void onInit();

    virtual void OpenCamera();
    void CaptureImage();

    virtual void SelectDevices(const std::vector<std::string> & list);

public:
    WebCamera();
    virtual ~WebCamera();
};



}



#endif // WEB_CAMERA_H
