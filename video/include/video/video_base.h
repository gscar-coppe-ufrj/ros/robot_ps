// video_base.h




#ifndef VIDEO_VIDEO_BASE_H
#define VIDEO_VIDEO_BASE_H



namespace video {



class VideoBase
{
public:
    VideoBase(){}
    virtual ~VideoBase(){}

    virtual bool ShowImage() = 0;
    virtual void ChangeRecordStatus() = 0;
    virtual void GotResolution(int h, int w) = 0;
};



}



#endif // VIDEO_BASE_H
