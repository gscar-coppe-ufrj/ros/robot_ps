// sender_nodelet.h



#ifndef VIDEO_SENDER_NODELET_H
#define VIDEO_SENDER_NODELET_H



#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <rosbag/bag.h>
#include <image_transport/image_transport.h>
#include <theora_image_transport/Packet.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Empty.h>
#include <std_msgs/Int32.h>
#include "sender.h"
#include <video/Resolution.h>



namespace video {



class SenderNodelet : public Sender, public nodelet::Nodelet
{
    ros::NodeHandle nodeHandle;

    ros::Subscriber msgFrameRateSub;
    ros::Subscriber msgPlaySub;
    ros::Subscriber msgRecordSub;
    ros::Subscriber msgResolutionSub;

    image_transport::Publisher msgImagePub;
    ros::Publisher msgRecordingPub;
    ros::Publisher msgResolutionPub;

    boost::thread* recordThread;

    rosbag::Bag bag;

    int quality;

    boost::posix_time::time_duration tDifference;
    boost::posix_time::ptime nextFrameTimestamp, currentFrameTimestamp, initialLoopTimestamp, finalLoopTimestamp;

    std::string GetTimeStr();
    void StartVideoWriter(cv::Mat image);
    void WriteFrame();
    void SendResolution();
    static void RecordThreadFunction(SenderNodelet *videoSenderNodelet);
    virtual void TreatFrameRate(const std_msgs::Int32Ptr & msg);
    virtual void TreatPlay(const std_msgs::BoolPtr & msg);
    void TreatRecord(const std_msgs::BoolPtr & msg);
    void TreatResolution(const video::ResolutionPtr &msg);

protected:
    virtual void onInit();

    virtual void PublishImage();

public:
    SenderNodelet();
    virtual ~SenderNodelet();
};



}



#endif // SENDER_NODELET_H
