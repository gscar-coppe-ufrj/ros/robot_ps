// sender.h



#ifndef VIDEO_SENDER_H
#define VIDEO_SENDER_H



#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <boost/thread.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <simple_xml_settings/simple_xml_settings.h>



typedef boost::posix_time::ptime Time;
typedef boost::posix_time::time_duration TimeDuration;



namespace video {



class Sender
{
    bool captureFail;
    bool start;

    TimeDuration delay;

    void StartThread();
    void CreateThread();
    void StartVideoWriter(cv::Mat image);
    void WriteFrame(cv::Mat image);
    void ThreadFunction();
    void FindTools();

protected:
    std::string savePath;
    boost::thread* internalThread;
    cv::Mat frame;

    bool stopThread;
    bool startRecord;
    bool videoWriterStarted;

    bool cameraIsOpened;

    cv::VideoCapture vCapture;

    boost::mutex mutex;
    cv::VideoWriter* videoWriter;

    std::string outputPath;

    int width;
    int height;


    void Load();
    void Save();
    virtual void PublishImage() = 0;
    virtual void CaptureImage();
    virtual void OpenCamera();

    void SetDelay(int milli);
    void SetStart(bool startInput);

    static void InternalThreadFunction(Sender* videoSender);

public:
    Sender();
    virtual ~Sender();
};



}



#endif // SENDER_H
