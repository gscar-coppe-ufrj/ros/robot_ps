// receiver_nodelet.h



#ifndef VIDEO_RECEIVER_NODELET_H
#define VIDEO_RECEIVER_NODELET_H



#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <theora_image_transport/Packet.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Empty.h>
#include <sensor_msgs/image_encodings.h>
#include <custom_loader/custom_nodelet.h>
#include <video/receiver.h>
#include <video/Resolution.h>



namespace video {



class ReceiverNodelet : public Receiver, public custom_loader::CustomNodelet
{
    ros::NodeHandle privateNodeHandle;

    image_transport::Subscriber msgStreamSub;
    ros::Subscriber msgStartStopSub;
    ros::Subscriber msgRecordingSub;
    ros::Subscriber msgResolutionSub;

    ros::Publisher msgRecordPub;
    ros::Publisher msgResolutionPub;

    std::string instanceName;
    std::string senderName;

    bool stopReceiving; //false = stop | true = start

    void TreatResolution(const video::ResolutionConstPtr & res);
    void TreatStream(const sensor_msgs::ImageConstPtr & originalImage);
    void TreatStopReceiving(const std_msgs::BoolPtr & msg);
    void TreatRecording(const std_msgs::BoolPtr & msg);
    void StopReceiving(bool stop);

public:

    ReceiverNodelet();

    void onInit();

    virtual void ChangeRecordStatus(bool statusInput);
    virtual void ChangeResolution(int w, int h);
    std::string SenderName();

    virtual ~ReceiverNodelet();
};



}



#endif // RECEIVER_NODELET_H
