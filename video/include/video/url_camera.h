// url_camera.h



#ifndef VIDEO_URL_CAMERA_H
#define VIDEO_URL_CAMERA_H



#include <linux_devs/linux_dev_manager.h>
#include "sender_nodelet.h"



namespace video {



class URLCamera : public SenderNodelet
{
    std::string url;

protected:
    void onInit();

    virtual void OpenCamera();
    void CaptureImage();

public:
    virtual ~URLCamera();
};



}



#endif // VIDEO_URL_CAMERA_H
