// viewer.h



#ifndef VIDEO_VIEWER_H
#define VIDEO_VIEWER_H



#include <robot_ps_core/tool.h>
#include "../receiver.h"



namespace video {



class Receiver;



class Viewer : public robot_ps::Tool
{
protected:
    void TreatAddVideoReceiver(Receiver* c){}
    void TreatRemoveVideoReceiver(Receiver* c){}

public:
    virtual void FindComponents()
    {
        AddMeToComponents<Viewer, Receiver>(&Viewer::TreatAddVideoReceiver, &Viewer::TreatRemoveVideoReceiver);
    }
};



}



#endif // VIEWER_H
