// viewer_qwidget.h



#ifndef VIDEO_VIEWER_QWIDGET_H
#define VIDEO_VIEWER_QWIDGET_H



#include "viewer.h"
#include "viewer_config_qwidget.h"
#include "video_widget.h"



namespace video {



class ViewerConfigQWidget;
class VideoWidget;




class ViewerQWidget : public QWidget, public Viewer
{
    Q_OBJECT

public:
    int sizeHintW; //width of QWidget
    int sizeHintH; //height of QWidget

    QGridLayout* gridLayout;

    std::deque<VideoWidget*> videoWidgetObjectList;

    int numRows;
    int numColumns;

    ViewerConfigQWidget* configurator;
    bool configuratorCreated;

    QSize sizeHint() const;
    void DeleteConfigurator();
    void CreateVideoWidget(std::deque<VideoWidget*> videoWidgetObjectListInput, int numRowsInput, int numColumnsInput);
    void CreateCells();
    void DeleteVideoWidgets();

    ViewerQWidget();
    ~ViewerQWidget();

public slots:
    void TreatDoubleClick();

signals:
    void mouseDoubleClickEvent(QMouseEvent *);
};



}



#endif // VIEWER_QWIDGET_H
