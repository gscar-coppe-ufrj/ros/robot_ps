// cell.h



#ifndef VIDEO_CELL_H
#define VIDEO_CELL_H



#include <iostream>
#include <opencv2/core/core.hpp>
#include <QGroupBox>
#include <QLineEdit>
#include "viewer_config_qwidget.h"
#include "video_widget.h"



#define ROTATE_BUTTON_CLOCKWISE "/usr/share/icons/Humanity/actions/16/object-rotate-right.svg"
#define ROTATE_BUTTON_COUNTERCLOCKWISE "/usr/share/icons/Humanity/actions/16/object-rotate-left.svg"
#define RECORD_BUTTON_START "/usr/share/icons/Humanity/actions/16/gtk-media-record.svg"
#define RECORD_BUTTON_STOP "/usr/share/icons/Humanity/actions/16/gtk-media-stop.svg"



namespace video {



class ViewerConfigQWidget;
class VideoWidget;



class Cell : public QGroupBox
{
    Q_OBJECT

    int sizeHintW; //widht of QWidget
    int sizeHintH; //height of QWidget

    QHBoxLayout* widgetBody;
    QVBoxLayout* infoBox;
    QToolButton* rotateButtonCCW;
    QToolButton* rotateButtonCW;
    QToolButton* recordButton;
    QLabel* recordingGifLabel;
    QMovie* recordingGif;
    QHBoxLayout* rotateLayout;
    QLineEdit* widthInput;
    QLineEdit* heightInput;
    QToolButton* applySize;
    QLabel* xLabel;
    QHBoxLayout* sizeLayout;

    bool hasRecordButton;

    QString senderName;

    QLabel* videoName;

public:    
    Cell(ViewerConfigQWidget* originConfigInput, QWidget* parent = 0);

    ViewerConfigQWidget* originConfig;

    int rotateCount;

    VideoWidget* videoWidgetObject;

    virtual QSize sizeHint() const;
    void SetDeviceName(bool noVideo);
    void ChangeRecordStatus();
    void SetResolution(int w, int h);

signals:
    void TreatDoubleClickEvent(QMouseEvent*);
    void mousePressEvent(QMouseEvent*);

private slots:
    void RotateImageCCW();
    void RotateImageCW();
    void TreatRecordButton();
    void TreatResolutionButton();

public slots:
    void TreatDoubleClick();

};



}



#endif // CELL_H
