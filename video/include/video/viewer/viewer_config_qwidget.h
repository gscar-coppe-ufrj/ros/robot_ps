// viewer_config_qwidget.h



#ifndef VIDEO_VIEWER_CONFIG_QWIDGET_H
#define VIDEO_VIEWER_CONFIG_QWIDGET_H



#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QScrollArea>
#include <QLabel>
#include <QLineEdit>
#include <QToolButton>
#include <deque>
#include "viewer_config_qwidget.h"
#include "cell.h"




#define CONFIG_NUM_COLUMNS_ROWS_APPLY "/usr/share/icons/Humanity/actions/16/media-playback-start.svg"



namespace video {



class ViewerQWidget;
class Cell;
class VideoWidget;



class ViewerConfigQWidget : public QWidget
{
    Q_OBJECT

    int sizeHintW; //widht of QWidget
    int sizeHintH; //height of QWidget

    typedef QImage  MyImage;

public:
    QHBoxLayout* hMainLayout;
    QVBoxLayout* vBodyLayout;
    QHBoxLayout* hConfigLayout;
    QGridLayout* gridLayout;
    QHBoxLayout* hConfigApply;
    QHBoxLayout* hWindowLayout;
    QVBoxLayout* vCellListLayout;
    QScrollArea* sACellList;
    QWidget* sAWidget;
    QVBoxLayout* sALayout;

    QLabel* columnLabel;
    QLabel* rowsLabel;
    QLineEdit* numColumns;
    QLineEdit* numRows;
    QToolButton* buttonGridApply;
    QToolButton* buttonApplyAll;

    std::deque<VideoWidget*> videoWidgetObjectList;
    std::deque<Cell*> videoCellList;
    std::deque<QWidget*> blankQWidgetList;

    bool videoObjectCreated;

    ViewerQWidget* originTool;

    int oldNumRows;
    int oldNumColumns;

    bool isCellClicked;
    bool isVideoClicked;
    VideoWidget* videoWidget;
    VideoWidget* videoWidgetCell;
    Cell* videoCell;

    ViewerConfigQWidget(ViewerQWidget* tool);
    virtual QSize sizeHint() const;
    void CreateCells();
    void DeleteCells();
    void CreateVideoWidget();
    void AddCellsInScroll();
    void ChangeVideoWidget();
    void SetCellClicked(VideoWidget* videoWidget, Cell* videoCellInput);
    void SetVideoClicked(VideoWidget* videoWidgetInput);
    void ClearBackgroundsCellList(Cell* videoCellInput);
    void ClearBordersBlankQWidgetList();
    void Rotate(Cell* videoCellInput);
    virtual ~ViewerConfigQWidget();

public slots:
    void ConfigNumRowsApply();
    void ApplyAll();

protected:
     void closeEvent(QCloseEvent *event);


};



}



#endif // VIEWER_CONFIG_QWIDGET_H
