// video_widget.h



#ifndef VIDEO_VIDEO_WIDGET_H
#define VIDEO_VIDEO_WIDGET_H



#include <iostream>
#include <time.h>
#include <opencv2/core/core.hpp>
#include <QGLWidget>
#include "../video_base.h"
#include "viewer_qwidget.h"



namespace video {



class Receiver;
class ViewerQWidget;
class Cell;
class ViewerConfigQWidget;



class VideoWidget : public QGLWidget, public VideoBase
{
    Q_OBJECT

    /// widht of QWidget
    int sizeHintW;
    /// height of QWidget
    int sizeHintH;

    /// Indicates when OpenGL view is to be redrawn
    bool sceneChanged;

    /// Qt image to be rendered
    QImage renderQtImg;
    /// original OpenCV image to be shown
    //cv::Mat mOrigImage;

    /// Background color
    QColor bgColor;

    /// Resized Image height
    int outH;
    /// Resized Image width
    int outW;
    /// height/width ratio
    float imgRatio;

    /// Top left X position to render image in the center of widget
    int posX;
    /// Top left Y position to render image in the center of widget
    int posY;

    int widgetHeight;
    int widgetWidth;

    int internalH;
    int internalW;

    bool imageStarted;

    int widgetType;

    /**
     * @brief initializeGL OpenGL initialization
     */
    void initializeGL();
    /**
     * @brief paintGL OpenGL Rendering
     */
    void paintGL();
    /**
     * @brief resizeGL Widget Resize Event
     * @param width
     * @param height
     */
    void resizeGL(int width, int height);

    void UpdateScene();
    void RenderImage();

public:
    explicit VideoWidget(int type, QWidget* parent = 0);
    virtual ~VideoWidget();

    Receiver* videoReceiver;

    bool noVideo;

    int rotateCount;

    QWidget* blankQWidget;

    ViewerConfigQWidget* originConfig;
    ViewerQWidget* originVideoWidget;
    Cell* originVideoCell;

    virtual QSize sizeHint() const;
    void FillBlack();
    void DisconnectReceiver();
    void ConnectReceiver(Receiver* videoReceiverInput);
    void SetBlankQwidget(QWidget* blankQWidgetInput);
    void GotResolution(int w, int h);

    virtual void ChangeRecordStatus();

signals:
    void mouseDoubleClickEvent(QMouseEvent*);
    /**
     * @brief ImageSizeChanged is used to resize the image outside the widget
     * @param outW
     * @param outH
     */
    void ImageSizeChanged(int outW, int outH);
    void mousePressEvent(QMouseEvent *);

private slots:
    void TreatClick();
    void TreatDoubleClick();

public slots:
    /**
     * @brief ShowImage is used to set the image to be viewed
     * @return
     */
    virtual bool ShowImage();
};



}



#endif // VIDEO_WIDGET_H
