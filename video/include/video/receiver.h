// receiver.h



#ifndef VIDEO_RECEIVER_H
#define VIDEO_RECEIVER_H



#include <boost/thread.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <QImage>
#include <device/device_on_off.h>
#include "video_base.h"
#include "viewer/viewer.h"



namespace video {



class Viewer;



class Receiver : public device::DeviceOnOff
{
    cv::Mat originalImage;
    cv::Mat originalImageCopied;
    float imageRatio;

protected:
    virtual void TreatAddVideoViewer(Viewer* t){}
    virtual void TreatRemoveVideoViewer(Viewer* t){}
    bool ImageConverter(cv::Mat & image);

public:
    int width;
    int height;
    std::deque<VideoBase*> videoWidgets;

    QImage convertedImage;

    boost::mutex imageMutex;

    bool recording;

    Receiver();

    virtual void FindTools()
    {
        device::DeviceOnOff::FindTools();
        AddMeToTools<Receiver, Viewer>(&Receiver::TreatAddVideoViewer, &Receiver::TreatRemoveVideoViewer);
    }

    virtual void ChangeRecordStatus(bool statusInput) = 0;
    virtual void ChangeResolution(int w, int h) = 0;
    virtual std::string SenderName() = 0;

    virtual ~Receiver();
};



}



#endif // RECEIVER_H
