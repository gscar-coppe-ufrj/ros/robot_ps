// tool_plugins.cpp



#include <pluginlib/class_list_macros.h>
#include <input_mapper/input_config_qwidget.h>
#include <input_mapper/input_viewer_qwidget.h>



PLUGINLIB_EXPORT_CLASS(input_mapper::InputConfigQWidget, robot_ps::Tool)
PLUGINLIB_EXPORT_CLASS(input_mapper::InputViewerQWidget, robot_ps::Tool)
