// input_buttons.cpp



#include <input_mapper/input_buttons.h>



using namespace std;
using namespace input_mapper;



InputButtons::InputButtons() :
    ok(true)
{
}


const double input_mapper::InputButtons::GetAxisScale() const
{
    return 1.;
}


std::string InputButtons::GetButtonNameFromId(int id)
{ 
    return GetValueFromOther<std::string, int>(GetButtonsNames(), GetButtonsIds(), "", id);
}


int InputButtons::GetButtonIdFromName(std::string name)
{ 
    return GetValueFromOther<int, std::string>(GetButtonsIds(), GetButtonsNames(), -1, name);
}


string InputButtons::GetAxisNameFromId(int id)
{ 
    return GetValueFromOther<std::string, int>(GetAxesNames(), GetAxesIds(), "", id);
}


int InputButtons::GetAxisIdFromName(std::string name)
{ 
    return GetValueFromOther<int, std::string>(GetAxesIds(), GetAxesNames(), -1, name);
}


void InputButtons::GetFirstButton(int& id, std::string& name) 
{ 
    GetButton(0, id, name);
}


void InputButtons::GetLastButton(int& id, std::string& name) 
{ 
    GetButton(-1, id, name);
}


void InputButtons::GetFirstAxis(int& id, std::string& name) 
{ 
    GetAxis(0, id, name);
}


void InputButtons::GetLastAxis(int& id, std::string& name) 
{ 
    GetAxis(-1, id, name);
}


void InputButtons::GetButton(int index, int& id, std::string& name)
{
    const std::deque<int>& ids = GetButtonsIds();
    ok = ids.size() > 0;
    if (!ok) return;
    if (index == -1) index = ids.size() - 1;
    id = ids[index];
    name = GetButtonsNames()[index];
}


void InputButtons::GetAxis(int index, int& id, std::string& name)
{
    const std::deque<int>& ids = GetAxesIds();
    ok = ids.size() > 0;
    if (!ok) return;
    if (index == -1) index = ids.size() - 1;
    id = ids[index];
    name = GetAxesNames()[index];
}


bool InputButtons::HasButton(int id)
{
    const std::deque<int>& ids = GetButtonsIds();
    return std::find(ids.begin(), ids.end(), id) != ids.end();
}


bool InputButtons::HasAxis(int id)
{
    const std::deque<int>& ids = GetAxesIds();
    return std::find(ids.begin(), ids.end(), id) != ids.end();
}


bool InputButtons::IsOk() 
{ 
    return ok; 
}


string InputButtons::GetButtonStateName(ButtonState state)
{
    switch (state)
    {
    case StateReleased:
        return "R (released)";
    case StatePressed:
        return "P (pressed)";
    case StateHolding:
        return "H (holding)";
    case StateNotHolding:
        return "NH (not holding)";
    case StateNone:
    default:
        return "";
    }
}


InputButtons::ButtonState InputButtons::GetButtonStateFromName(const std::string& state)
{
    if(state == "P")
        return StatePressed;
    else if(state == "H")
        return StateHolding;
    else if(state == "R")
        return StateReleased;
    else if(state == "NH")
        return StateNotHolding;
    else
        return StateNone;
}


unsigned int InputButtons::SizeButtons()
{
    return GetButtonsIds().size();
}


unsigned int InputButtons::SizeAxes()
{
    return GetAxesIds().size();
}
