// input_viewer_qwidget.cpp


#include <iostream>
#include <deque>
#include <input_mapper/input_viewer_qwidget.h>



input_mapper::InputViewerQWidget::Tab::Tab()
{
    gamepadActive = false;
    keyboardActive = false;
    gamepad = new QLabel("Gamepad");
    keyboard = new QLabel("Keyboard");
}


input_mapper::InputViewerQWidget::ControlGroupWidget::ControlGroupWidget(ControlGroup* controlGroup)
{
    group = new QLabel(controlGroup->name.c_str());
    group->setStyleSheet("QLabel { font : bold }");

    QVBoxLayout* l1 = new QVBoxLayout;
    l1->setContentsMargins(QMargins(0, 0, 0, 0));
    l1->setAlignment(Qt::AlignCenter);
    l1->addWidget(group, 0, Qt::AlignCenter);

    for (auto i = controlGroup->modes.begin(); i != controlGroup->modes.end(); i++)
    {
        QLabel* mode = new QLabel((*i)->name.c_str());
        modes.push_back(mode);
        l1->addWidget(mode, 0, Qt::AlignCenter);
    }

    setLayout(l1);
}


input_mapper::InputViewerQWidget::InputViewerQWidget()
{
    sizeHintW = 200;
    sizeHintH = 200;

    vLayout = new QVBoxLayout;
    vLayout->setContentsMargins(QMargins(0, 0, 0, 0));
    setLayout(vLayout);

    tabWidget = new QTabWidget();
    vLayout->addWidget(tabWidget);

    QObject::connect(this, SIGNAL(UpdateInputActiveSignal(unsigned int)), this, SLOT(UpdateInputActiveSlot(unsigned int)));
    QObject::connect(this, SIGNAL(UpdateModeSignal(InputMapper*, unsigned int, ControlGroup*)), this, SLOT(UpdateModeSlot(InputMapper*, unsigned int, ControlGroup*)));
}


QSize input_mapper::InputViewerQWidget::sizeHint() const
{
    return QSize(sizeHintW, sizeHintH);
}


void input_mapper::InputViewerQWidget::Init()
{
    QObject::connect(this, SIGNAL(AddTabSignal(InputMapper*)), this, SLOT(AddTab(InputMapper*)), Qt::BlockingQueuedConnection);
    QObject::connect(this, SIGNAL(RemoveTabSignal(InputMapper*)), this, SLOT(RemoveTab(InputMapper*)), Qt::BlockingQueuedConnection);

    auto components = this->GetListOfComponents<InputViewer, InputMapper>();
    if (components->size() > 0)
    {
        for (auto iter = components->begin(); iter != components->end(); ++iter)
            AddTab(*iter);
    }
    else
        std::cout << "no InputMapper component found" << std::endl;
}


void input_mapper::InputViewerQWidget::AddTab(InputMapper* component)
{
    QVBoxLayout* l = new QVBoxLayout;
    l->setSizeConstraint(QLayout::SetFixedSize);
    QWidget* w = new QWidget;
    w->setLayout(l);
    l->setAlignment(Qt::AlignCenter);
    QScrollArea* scrollArea = new QScrollArea;
    scrollArea->setAlignment(Qt::AlignHCenter);
    scrollArea->setWidget(w);

    Tab* tab = new Tab;
    tabs.push_back(tab);
    tab->scrollArea = scrollArea;
    tabWidget->addTab(scrollArea, QString(component->name.c_str()));

    QHBoxLayout* l2 = new QHBoxLayout;
    l2->setContentsMargins(QMargins(0, 0, 0, 0));
    l2->setAlignment(Qt::AlignCenter);
    l2->addWidget(tab->gamepad, 0, Qt::AlignCenter);
    l2->addWidget(tab->keyboard, 0, Qt::AlignCenter);
    l->addLayout(l2);

    for (auto group = component->groups.begin(); group != component->groups.end(); group++)
    {
        auto cGW = new ControlGroupWidget(*group);
        tab->groups.push_back(cGW);
        l->addWidget(cGW);
    }
    l->addWidget(new QWidget, 1);
}


void input_mapper::InputViewerQWidget::RemoveTab(InputMapper* component)
{
    auto components = this->GetListOfComponents<InputViewer, InputMapper>();
    auto found = std::find(components->begin(), components->end(), component);
    if (found != components->end())
    {
        if ((found - components->begin()) < tabs.size())
        {
            tabWidget->removeTab(found - components->begin());
            tabs.erase(found - components->begin() + tabs.begin());
        }
    }
}


/*void input_mapper::InputViewerQWidget::FindComponents()
{
    InputViewer::FindComponents();

    if (components.size() > 0)
    {
        for (auto iter = components.begin(); iter != components.end(); ++iter)
            AddTab(*iter);
    }
    else
        cout << "no GPCBGamepadAndKeyboardMapper component found" << endl;
}*/


void input_mapper::InputViewerQWidget::TreatAddInputMapper(InputMapper* component)
{
    InputViewer::TreatAddInputMapper(component);

    auto list = this->GetListOfComponents<InputViewer, InputMapper>();
    std::deque<InputMapper*>::iterator componentit = std::find(list->begin(), list->end(), component);
    if (componentit != list->end())
        emit AddTabSignal(component);
}


void input_mapper::InputViewerQWidget::TreatRemoveInputMapper(InputMapper* component)
{
    emit RemoveTabSignal(component);

    InputViewer::TreatRemoveInputMapper(component);
}


void input_mapper::InputViewerQWidget::UpdateGamepadActive(InputMapper* component, bool active)
{
    auto components = this->GetListOfComponents<InputViewer, InputMapper>();
    auto found = std::find(components->begin(), components->end(), component);
    if (found != components->end())
    {
        if (tabs[found - components->begin()]->gamepadActive != active)
        {
            tabs[found - components->begin()]->gamepadActive = active;
            emit UpdateInputActiveSignal(found - components->begin());
        }
    }
}


void input_mapper::InputViewerQWidget::UpdateKeyboardActive(InputMapper* component, bool active)
{
    auto components = this->GetListOfComponents<InputViewer, InputMapper>();
    auto found = std::find(components->begin(), components->end(), component);
    if (found != components->end())
    {
        if (tabs[found - components->begin()]->keyboardActive != active)
        {
            tabs[found - components->begin()]->keyboardActive = active;
            emit UpdateInputActiveSignal(found - components->begin());
        }
    }
}


void input_mapper::InputViewerQWidget::UpdateMode(InputMapper* component, unsigned int groupNumber, ControlGroup* controlGroup)
{
    emit UpdateModeSignal(component, groupNumber, controlGroup);
}


void input_mapper::InputViewerQWidget::UpdateInputActiveSlot(unsigned int tabNumber)
{
    if (tabs[tabNumber]->gamepadActive)
        tabs[tabNumber]->gamepad->setStyleSheet("QLabel { font : bold; color : white; background-color: green }");
    else
        tabs[tabNumber]->gamepad->setStyleSheet("QLabel { font : normal; color : normal; background-color: normal }");
    if (tabs[tabNumber]->keyboardActive)
        tabs[tabNumber]->keyboard->setStyleSheet("QLabel { font : bold; color : white; background-color: green }");
    else
        tabs[tabNumber]->keyboard->setStyleSheet("QLabel { font : normal; color : normal; background-color: normal }");
}


void input_mapper::InputViewerQWidget::UpdateModeSlot(InputMapper* component, unsigned int groupNumber, ControlGroup* controlGroup)
{
    auto components = this->GetListOfComponents<InputViewer, InputMapper>();
    auto found = std::find(components->begin(), components->end(), component);
    if (found != components->end())
    {
        ControlGroupWidget* groupWidget = tabs[found - components->begin()]->groups[groupNumber];
        /*if (controlGroup->inputMode == GamepadAndKeyboard::TypeGamepad)
        {
            groupWidget->gamepad->setStyleSheet("QLabel { font : bold; color : white; background-color: green }");
            groupWidget->keyboard->setStyleSheet("QLabel { font : normal; color : normal; background-color: normal }");
        }
        else if (controlGroup->inputMode == GamepadAndKeyboard::TypeKeyboard)
        {
            groupWidget->gamepad->setStyleSheet("QLabel { font : normal; color : normal; background-color: normal }");
            groupWidget->keyboard->setStyleSheet("QLabel { font : bold; color : white; background-color: green }");
        }
        else
        {
            groupWidget->gamepad->setStyleSheet("QLabel { font : normal; color : normal; background-color: normal }");
            groupWidget->keyboard->setStyleSheet("QLabel { font : normal; color : normal; background-color: normal }");
        }*/
        for (unsigned int i = 0; i < groupWidget->modes.size(); i++)
            groupWidget->modes[i]->setStyleSheet("QLabel { font : normal; color : normal; background-color: normal }");
        if (controlGroup->actualMode != CONTROL_GROUP_MODE_NONE_ID)
            groupWidget->modes[controlGroup->actualMode - 1]->setStyleSheet("QLabel { font : bold; color : white; background-color: green }");
    }
}


input_mapper::InputViewerQWidget::~InputViewerQWidget()
{
    delete vLayout;
    delete tabWidget;
}
