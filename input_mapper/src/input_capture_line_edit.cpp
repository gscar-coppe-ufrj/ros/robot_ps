// input_capture_line_edit.cpp



#include <input_mapper/input_capture_line_edit.h>

#include <QFocusEvent>

#include <iostream>


using namespace input_mapper;



InputCaptureLineEdit::InputCaptureLineEdit(InputMapper* component, QWidget* parent) :
    InputCaptureLineEdit::InputCaptureLineEdit(component, "", parent)
{
}


InputCaptureLineEdit::InputCaptureLineEdit(InputMapper* component, const QString& text, QWidget* parent) :
    QLineEdit(text, parent), Item(component->variable_last_command),
    component(component), capturing(false), variable(component->variable_last_command), input_type(ControlValue::None)
{
    QLineEdit::setReadOnly(true);
    QObject::connect(this, SIGNAL(UpdateItemSignal()), this, SLOT(UpdateItemSlot()));

    setFocusPolicy(Qt::ClickFocus);

    DarkenText();
}


bool InputCaptureLineEdit::SetValueFromButton(int id, bool using_keyboard)
{
    if (!capturing)
        return false;

    std::string name;
    InputButtons* gamepad_keyboard = (using_keyboard) ? component->GetKeyboard() : component->GetGamepad();
    if (gamepad_keyboard != 0)
        name = gamepad_keyboard->GetButtonNameFromId(id);
    if (gamepad_keyboard->IsOk())
    {
        QLineEdit::setText(name.c_str());
        clearFocus();
    }
    return gamepad_keyboard->IsOk();
}


bool InputCaptureLineEdit::SetValueFromAxis(int id, bool using_keyboard)
{
    if (!capturing)
        return false;

    std::string name;
    InputButtons* gamepad_keyboard = (using_keyboard) ? component->GetKeyboard() : component->GetGamepad();
    if (gamepad_keyboard != 0)
        name = gamepad_keyboard->GetAxisNameFromId(id);
    if (gamepad_keyboard->IsOk())
    {
        QLineEdit::setText(name.c_str());
        clearFocus();
    }
    return gamepad_keyboard->IsOk();
}


void InputCaptureLineEdit::focusInEvent(QFocusEvent* evt)
{
    evt->accept();
    capturing = true;
    BrightenText();
}


void InputCaptureLineEdit::focusOutEvent(QFocusEvent* evt)
{
    evt->accept();
    capturing = false;
    DarkenText();
}


void InputCaptureLineEdit::keyPressEvent(QKeyEvent* evt)
{
    evt->ignore();
}


void InputCaptureLineEdit::DarkenText()
{
    QLineEdit::setStyleSheet("color: black");
}


void InputCaptureLineEdit::BrightenText()
{
    QLineEdit::setStyleSheet("color: gray");
}


void InputCaptureLineEdit::UpdateItemSlot()
{
    if (!capturing)
        return;

    ControlValue::ControlValueInput input = variable->GetValue<0>();
    int id = variable->GetValue<1>();
std::cout << ControlValue::GetInputTypeString(input) << std::endl;
    bool ok;

    if (input != ControlValue::GamepadAxis)
        ok = SetValueFromButton(id, input == ControlValue::KeyboardButton);
    else
        ok = SetValueFromAxis(id, false);

    if (ok)
        SetInputType(input);
}


void InputCaptureLineEdit::UpdateItemFromVariable(bool)
{
    emit UpdateItemSignal();
}


bool InputCaptureLineEdit::UpdateVariableFromItem()
{
    return false;
}


InputCaptureLineEdit::~InputCaptureLineEdit()
{
}
