// control_mode.cpp



#include <input_mapper/control_mode.h>



using namespace std;
using namespace input_mapper;



ControlMode::ControlMode() :
    id(-1), name(""), isToggle(false), gamepadStringSequence(""), keyboardStringSequence(""), releasedMode(false), release(false), canToggle(true)
{
    active = false;
}


ControlMode::ControlMode(unsigned int id, string name, bool isToggle, unsigned char numberValues) :
    id(id), name(name), isToggle(isToggle), gamepadStringSequence(""), keyboardStringSequence(""), releasedMode(false), release(false), canToggle(true)
{
    active = false;
    
    for (unsigned char i = 0; i < numberValues; i++)
        values.push_back(new ControlValue);
}


bool ControlMode::isActive()
{
    if (gamepadSequence.empty() && keyboardSequence.empty())
        return true;

    return active;
}


bool ControlMode::SetModeActive(bool sequenceActive)
{
    if (isToggle)
    {
        if (canToggle && sequenceActive)
        {
            canToggle   = false;
            active      = !active;
        }
        else if (!sequenceActive)
        {
            canToggle = true;
        }
    }
    else
    {
        active = sequenceActive;
    }
}


ControlMode::~ControlMode()
{
    for (auto iter = values.begin(); iter != values.end(); ++iter)
        delete *iter;
}
