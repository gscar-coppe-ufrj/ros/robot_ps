// control_value.h



#include <input_mapper/control_value.h>



using namespace input_mapper;



const std::string ControlValue::ControlValueInput_None           = "None";
const std::string ControlValue::ControlValueInput_GamepadAxis    = "GamepadAxis";
const std::string ControlValue::ControlValueInput_GamepadButton  = "GamepadButton";
const std::string ControlValue::ControlValueInput_KeyboardButton = "KeyboardButton";



ControlValue::ControlValue() :
    gamepad(0), keyboard(0), valid(false), associatedButtonOrAxis(-1), inputType(ControlValue::None), positive(true)
{
}


std::string ControlValue::GetInputTypeString(ControlValue::ControlValueInput input_type)
{
    switch (input_type)
    {
    case GamepadAxis:
        return ControlValueInput_GamepadAxis;
    case GamepadButton:
        return ControlValueInput_GamepadButton;
    case KeyboardButton:
        return ControlValueInput_KeyboardButton;
    case None:
    default:
        return ControlValueInput_None;
    }
}


std::string ControlValue::GetInputTypeString()
{
    return GetInputTypeString(inputType);
}


void ControlValue::SetInputTypeFromString(const std::string& type)
{
    if (type == ControlValueInput_GamepadAxis)
        inputType = GamepadAxis;
    else if (type == ControlValueInput_GamepadButton)
        inputType = GamepadButton;
    else if (type == ControlValueInput_KeyboardButton)
        inputType = KeyboardButton;
    else
        inputType = None;
}


void ControlValue::SetGamepadAndKeyboard(InputButtons* gamepad, InputButtons* keyboard)
{
    this->gamepad  = gamepad;
    this->keyboard = keyboard;
}


std::string ControlValue::GetNameFromId(ControlValueInput input_type, int id)
{
    switch (input_type)
    {
    case GamepadAxis:
        if (!gamepad) return "";
        return gamepad->GetAxisNameFromId(id);
    case GamepadButton:
        if (!gamepad) return "";
        return gamepad->GetButtonNameFromId(id);
    case KeyboardButton:
        if (!keyboard) return "";
        return keyboard->GetButtonNameFromId(id);
    case None:
    default:
        return "";
    }
}


std::string ControlValue::GetNameFromId()
{
    return GetNameFromId(inputType, associatedButtonOrAxis);
}


bool ControlValue::SetIdFromName(const std::string& name)
{
    switch (inputType)
    {
    case GamepadAxis:
        if (!gamepad) return false;
        SetId(gamepad->GetAxisIdFromName(name));
        return gamepad->IsOk();
    case GamepadButton:
        if (!gamepad) return false;
        SetId(gamepad->GetButtonIdFromName(name));
        return gamepad->IsOk();
    case KeyboardButton:
        if (!keyboard) return false;
        SetId(keyboard->GetButtonIdFromName(name));
        return gamepad->IsOk();
    case None:
    default:
        return false;
    }
}


bool ControlValue::SetId(int id)
{
    valid = false;
    switch (inputType)
    {
    case GamepadAxis:
        if (!gamepad) return false;
        valid = gamepad->HasAxis(id);
        break;
    case GamepadButton:
        if (!gamepad) return false;
        valid = gamepad->HasButton(id);
        break;
    case KeyboardButton:
        if (!keyboard) return false;
        valid = keyboard->HasButton(id);
        break;
    case None:
    default:
        return false;
    }

    associatedButtonOrAxis = id;
    return valid;
}


int ControlValue::GetId()
{
    return associatedButtonOrAxis;
}


bool ControlValue::Ok()
{
    return valid;
}


ControlValue::~ControlValue()
{
}
