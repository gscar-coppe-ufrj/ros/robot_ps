// keyboard.cpp



#include <input_mapper/keyboard.h>

#include <boost/assign/list_of.hpp>



using namespace input_mapper;



const std::deque<std::string> Keyboard::empty_name = std::deque<std::string>();
const std::deque<int> Keyboard::empty_id   = std::deque<int>();
const std::deque<InputButtons::TupleMap> Keyboard::empty_map = std::deque<InputButtons::TupleMap>();


const std::deque<std::string> Keyboard::names = boost::assign::list_of
        ("ESCAPE")
        ("TAB")
        ("BACKSPACE")
        ("INSERT")
        ("DELETE")
        ("HOME")
        ("END")
        ("LEFT")
        ("UP")
        ("RIGHT")
        ("DOWN")
        ("PAGEUP")
        ("PAGEDOWN")
        ("SHIFT")
        ("CONTROL")
        ("ALT")
        ("ALTGR")
        ("CAPSLOCK")
        ("F1")
        ("F2")
        ("F3")
        ("F4")
        ("F5")
        ("F6")
        ("F7")
        ("F8")
        ("F9")
        ("F10")
        ("F11")
        ("F12")
        ("SPACE")
        ("APOSTROPHE")
        ("COMMA")
        ("MINUS")
        ("SLASH")
        ("0")
        ("1")
        ("2")
        ("3")
        ("4")
        ("5")
        ("6")
        ("7")
        ("8")
        ("9")
        ("SEMICOLON")
        ("EQUAL")
        ("A")
        ("B")
        ("C")
        ("D")
        ("E")
        ("F")
        ("G")
        ("H")
        ("I")
        ("J")
        ("K")
        ("L")
        ("M")
        ("N")
        ("O")
        ("P")
        ("Q")
        ("R")
        ("S")
        ("T")
        ("U")
        ("V")
        ("W")
        ("X")
        ("Y")
        ("Z")
        ("BRACKETLEFT")
        ("BACKSLASH")
        ("BRACKETRIGHT")
        ("BAR")
        ("ASCIITILDE")
        ("DIVISION")
        ("DEAD_ACUTE")
        ("RETURN")
        ("CCEDILLA")
        ("DEAD_TILDE")
        ("PERIOD")
        ("ASTERISK")
        ("NUMLOCK")
        ("NUMPAD_SLASH")
        ("NUMPAD_ASTERISK")
        ("NUMPAD_MINUS")
        ("NUMPAD_7")
        ("NUMPAD_HOME")
        ("NUMPAD_8")
        ("NUMPAD_UP")
        ("NUMPAD_9")
        ("NUMPAD_PAGEUP")
        ("NUMPAD_PLUS")
        ("NUMPAD_4")
        ("NUMPAD_LEFT")
        ("NUMPAD_5")
        ("NUMPAD_CLEAR")
        ("NUMPAD_6")
        ("NUMPAD_RIGHT")
        ("NUMPAD_PERIOD")
        ("NUMPAD_1")
        ("NUMPAD_END")
        ("NUMPAD_2")
        ("NUMPAD_DOWN")
        ("NUMPAD_3")
        ("NUMPAD_PAGEDOWN")
        ("NUMPAD_0")
        ("NUMPAD_INSERT")
        ("NUMPAD_DELETE")
        ("NUMPAD_COMMA")
        ("NUMPAD_ENTER");


const std::deque<int> Keyboard::ids = boost::assign::list_of
        (0)(1)(2)(3)(4)(5)(6)(7)(8)(9)(10)(11)(12)(13)(14)(15)
        (16)(17)(18)(19)(20)(21)(22)(23)(24)(25)(26)(27)(28)(29)(30)(31)
        (32)(33)(34)(35)(36)(37)(38)(39)(40)(41)(42)(43)(44)(45)(46)(47)
        (48)(49)(50)(51)(52)(53)(54)(55)(56)(57)(58)(59)(60)(31)(62)(63)
        (64)(65)(66)(67)(68)(69)(70)(71)(72)(73)(74)(75)(76)(77)(78)(79)
        (80)(81)(82)(83)(84)(85)(86)(87)(88)(89)(90)(91)(92)(93)(94)(95)
        (96)(97)(98)(99)(100)(101)(102)(103)(104)(105)(106)(107)(108)(109)(110)(111)
        (100)(101)(102)(103)(104)(105)(106)(107)(108)(109)(110)(111)
        (112)(113);


const std::deque<std::string>& Keyboard::GetButtonsNames() const
{
    return Keyboard::names;
}


const std::deque<int>& Keyboard::GetButtonsIds() const
{
    return Keyboard::ids;
}


const std::deque<std::string>& Keyboard::GetAxesNames() const
{
    return Keyboard::empty_name;
}


const std::deque<int>& Keyboard::GetAxesIds() const
{
    return Keyboard::empty_id;
}


const std::deque<InputButtons::TupleMap>& Keyboard::GetAxisToButtonMap() const
{
    return Keyboard::empty_map;
}
