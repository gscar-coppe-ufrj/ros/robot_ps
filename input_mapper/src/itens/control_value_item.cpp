// control_value_item.cpp



#include <input_mapper/itens/control_value_item.h>

#include <QApplication>



using namespace input_mapper;



const char * ControlValueItem::STYLE_ERROR   = "color: red";
const char * ControlValueItem::STYLE_REGULAR = "color: black";



ControlValueItem::ControlValueItem(ControlValue* control_value, InputMapper* component) :
    device::Item::Item(component->variable_last_command), control_value(control_value), component(component), configured(false)
{
    std::string value = control_value->GetNameFromId();

    lb_input         = new QLabel(control_value->GetInputTypeString().c_str());
    le_value         = new InputCaptureLineEdit(component, value.c_str());
    lb_value_current = new QLabel(value.c_str());
    cb_positive      = new QCheckBox("positive");
    lb_positive      = new QLabel;

    le_value->SetInputType(control_value->inputType);

    widgets.push_back(lb_input);
    widgets.push_back(le_value);
    widgets.push_back(lb_value_current);
    widgets.push_back(cb_positive);
    widgets.push_back(lb_positive);

    QObject::connect(this, SIGNAL(UpdateInputTypeSignal()), this, SLOT(UpdateInputTypeSlot()));
}


unsigned int ControlValueItem::ConfigureGridLayout(QGridLayout* layout, unsigned int from_row, unsigned int from_col)
{
    if (configured) return from_row;
    configured = true;

    cb_positive->setChecked(control_value->positive);
    UpdateSign();

    layout->addWidget(lb_input,         from_row, from_col++);
    layout->addWidget(le_value,         from_row, from_col++);
    layout->addWidget(lb_value_current, from_row, from_col++);
    layout->addWidget(cb_positive,      from_row, from_col++);
    layout->addWidget(lb_positive,      from_row, from_col++, Qt::AlignCenter);

    return from_row + 1;
}


void ControlValueItem::UpdateSign()
{
    lb_positive->setText( (control_value->positive) ? "+" : "-" );
}


void ControlValueItem::SetShown(bool flag)
{
    for (auto iter = widgets.begin(); iter != widgets.end(); ++iter)
        (*iter)->setVisible(flag);
}


bool ControlValueItem::Update()
{
    // Update sign
    control_value->positive = cb_positive->isChecked();
    UpdateSign();

    // Update input type
    control_value->inputType = le_value->GetCurrentInputType();

    // Update sequence
    QString text = le_value->text();
    if ( !control_value->SetIdFromName(text.toStdString()) )
    {
        le_value->setStyleSheet(ControlValueItem::STYLE_ERROR);
        return false;
    }
    le_value->setStyleSheet(ControlValueItem::STYLE_REGULAR);
    lb_value_current->setText(text);
    return true;
}


void ControlValueItem::UpdateInputTypeSlot()
{
    if (!le_value->hasFocus())
        return;
    //
    ControlValue::ControlValueInput input_type = component->variable_last_command->GetValue<0>();
    lb_input->setText(ControlValue::GetInputTypeString(input_type).c_str());
}


void ControlValueItem::UpdateItemFromVariable(bool)
{
    emit UpdateInputTypeSignal();
}


bool ControlValueItem::UpdateVariableFromItem()
{
    return false;
}


ControlValueItem::~ControlValueItem()
{
    if (!configured)
    {
        for (auto iter = widgets.begin(); iter != widgets.end(); ++iter)
            delete *iter;
    }
}
