// group_item.cpp



#include <input_mapper/itens/group_item.h>

#include <QApplication>

using namespace input_mapper;



GroupItem::GroupItem(ControlGroup* group, InputMapper* component) :
    group(group), configured(false), component(component), name(group->name)
{
    groupbox = new QGroupBox(name.c_str());
    gridlayout= new QGridLayout();
    groupbox->setLayout(gridlayout);
    //
    groupbox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
}


void GroupItem::ConfigureItem()
{
    if (configured) return;
    configured = true;

    int grid_row = 0;
    for (auto iter = group->modes.begin(); iter != group->modes.end(); ++iter)
    {
        ModeItem* modeitem = new ModeItem(*iter, component);
        modeitems.push_back(modeitem);
        grid_row = modeitem->ConfigureGridLayout(gridlayout, grid_row, 0);
    }
}


bool GroupItem::Update()
{
    bool flag = true;
    for (auto iter = modeitems.begin(); iter != modeitems.end(); ++iter)
        flag &= (*iter)->Update();
    return flag;
}


void GroupItem::SetShown(bool flag)
{
    groupbox->setVisible(flag);
}


GroupItem::~GroupItem()
{
    for (auto iter = modeitems.begin(); iter != modeitems.end(); ++iter)
        delete *iter;
}
