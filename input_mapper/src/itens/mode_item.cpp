// mode_item.cpp



#include <input_mapper/itens/mode_item.h>

#include <QApplication>


using namespace std;
using namespace input_mapper;



ModeItem::ModeItem(ControlMode* mode, InputMapper* component) :
    configured(false), component(component), name(mode->name), mode(mode)
{
    modenamelabel = new QLabel(name.c_str());
    tooglecheckbox = new QCheckBox("toggle");
    tooglecheckbox->setChecked(mode->isToggle);

    togglelabel = new QLabel;

    joysequence = mode->gamepadStringSequence.c_str();
    keysequence = mode->keyboardStringSequence.c_str();
    keysequencestate = new QLabel;
    joysequencestate = new QLabel;
    keysequencelabel = new QLabel("Keyboard Sequence");
    joysequencelabel = new QLabel("Joystick Sequence");

    if(joysequence.compare(""))
        joysequencestate->setText(joysequence.c_str());

    if(keysequence.compare(""))
        keysequencestate->setText(keysequence.c_str());

    keysequenceledit = new QLineEdit;
    joysequenceledit = new QLineEdit;

    SetToggleLabel(mode->isToggle);

    widgets.push_back(modenamelabel);
    widgets.push_back(togglelabel);
    widgets.push_back(keysequencelabel);
    widgets.push_back(joysequencelabel);
    widgets.push_back(keysequencestate);
    widgets.push_back(joysequencestate);
    widgets.push_back(keysequenceledit);
    widgets.push_back(joysequenceledit);
    widgets.push_back(tooglecheckbox);
}


unsigned int ModeItem::ConfigureGridLayout(QGridLayout* layout, unsigned int from_row, unsigned int from_col)
{
    if (configured) return from_row;
    configured = true;

    layout->addWidget(modenamelabel,    from_row,   from_col);
    layout->addWidget(keysequencelabel, from_row,   from_col+1);
    layout->addWidget(keysequenceledit, from_row,   from_col+2);
    layout->addWidget(keysequencestate, from_row,   from_col+3);
    layout->addWidget(joysequencelabel, from_row+1, from_col+1);
    layout->addWidget(joysequenceledit, from_row+1, from_col+2);
    layout->addWidget(joysequencestate, from_row+1, from_col+3);
    layout->addWidget(tooglecheckbox,   from_row,   from_col+4, 2, 1);
    layout->addWidget(togglelabel,      from_row,   from_col+5, 2, 1);

    from_row += 2;
    for (auto iter = mode->values.begin(); iter != mode->values.end(); ++iter)
    {
        ControlValueItem* item_value = new ControlValueItem(*iter, component);
        items_values.push_back(item_value);
        from_row = item_value->ConfigureGridLayout(layout, from_row, 1);
    }

    joysequenceledit->setText(mode->gamepadStringSequence.c_str());
    keysequenceledit->setText(mode->keyboardStringSequence.c_str());

    return from_row;
}


bool ModeItem::UpdateToggleState()
{
    bool flag = tooglecheckbox->isChecked();
    mode->isToggle = flag;
    SetToggleLabel(flag);
    return flag;
}


void ModeItem::SetToggleLabel(bool flag)
{
    togglelabel->setText( (flag) ? "Yes" : "No" );
}


string ModeItem::GetSequence(bool is_keyboard_sequence)
{
    if (is_keyboard_sequence)
        return keysequenceledit->text().toStdString();
    else
        return joysequenceledit->text().toStdString();
}


void ModeItem::SetSequenceOk(bool flag, bool is_keyboard_sequence)
{
    QLineEdit* edit = (is_keyboard_sequence) ? keysequenceledit : joysequenceledit;
    if (flag)
        edit->setStyleSheet("color: black");
    else
        edit->setStyleSheet("color: red");
}


void ModeItem::SetActualSequences(const std::string & sequence , bool is_keyboard_sequence)
{
    if(is_keyboard_sequence)
    {
        keysequence = sequence;
        keysequencestate->setText(keysequence.c_str());
    }
    else
    {
        joysequence = sequence;
        joysequencestate->setText(joysequence.c_str());
    }
}


bool ModeItem::Update()
{
    // Update toggle state
    UpdateToggleState();

    // Update keyboard
    std::string sequence = GetSequence(true);
    bool ok = component->CreateSequence(sequence, component->GetKeyboard(), mode, true);
    if (ok)
        SetActualSequences(sequence, true);
    SetSequenceOk(ok, true);

    // Update gamepad
    sequence = GetSequence(false);
    ok = component->CreateSequence(sequence, component->GetGamepad(), mode, false);
    if (ok)
        SetActualSequences(sequence, false);
    SetSequenceOk(ok, false);

    // Update children
    bool flag = true;
    for (auto iter = items_values.begin(); iter != items_values.end(); ++iter)
        flag &= (*iter)->Update();
    return flag;
}


void ModeItem::SetShown(bool flag)
{
    if (configured)
    {
        for (auto iter = widgets.begin(); iter != widgets.end(); ++iter)
            (*iter)->setVisible(flag);
        for (auto iter = items_values.begin(); iter != items_values.end(); ++iter)
            (*iter)->SetShown(flag);
    }
}


ModeItem::~ModeItem()
{
    if (!configured)
    {
        for (auto iter = widgets.begin(); iter != widgets.end(); ++iter)
            delete *iter;
    }

    for (auto iter = items_values.begin(); iter != items_values.end(); ++iter)
        delete *iter;
}
