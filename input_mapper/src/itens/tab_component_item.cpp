// tab_component_item.cpp



#include <input_mapper/itens/tab_component_item.h>

#include <QTableWidget>
#include <QApplication>



using namespace std;
using namespace input_mapper;



TabComponentItem::TabComponentItem(InputMapper* component) :
    name(component->componentName), component(component)
{
    // Create widgets
    QHBoxLayout* lout_header   = new QHBoxLayout;
    QHBoxLayout* lout_split    = new QHBoxLayout;
    QVBoxLayout* lout_contents = new QVBoxLayout;
    QPushButton* bt_update     = new QPushButton("Update");
    //
    cb_joy       = new QComboBox;
    bt_help      = new QPushButton("?");
    lout_main    = new QVBoxLayout;
    gbox_options = new QGroupBox("Options");
    lout_options = new QVBoxLayout;
    widget_main  = new QWidget;

    // Configure widgets
    this->setWidgetResizable(true);
    this->setWidget(widget_main);
    this->setFrameShape(QFrame::NoFrame);
    //
    cb_joy->setMaximumWidth(200);
    cb_joy->addItem("None");
    //
    bt_help->setMaximumWidth(30);
    //
    lout_options->setContentsMargins(4, 0, 4, 0);
    lout_options->setSpacing(4);
    //
    gbox_options->setLayout(lout_options);
    gbox_options->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);

    AddJoysticks(component->gamepadsNamesList);

    // Place widgets
    lout_header->addWidget(cb_joy,    0, Qt::AlignLeft);
    lout_header->addStretch(1);
    lout_header->addWidget(bt_update, 0, Qt::AlignRight);
    lout_header->addWidget(bt_help, 0, Qt::AlignRight);
    //
    lout_split->addWidget(gbox_options, 0, Qt::AlignLeft | Qt::AlignTop);
    //
    lout_contents->addLayout(lout_header);
    lout_contents->addLayout(lout_split);
    //
    lout_split->addLayout(lout_main);
    //
    for (auto iter_group = component->groups.begin(); iter_group!= component->groups.end(); ++iter_group)
        AddGroupItem(*iter_group);
    //
    lout_contents->addStretch(1);

    // Set the layout
    widget_main->setLayout(lout_contents);
    
    // Connect signals
    connect(bt_update, SIGNAL(clicked()), this, SLOT(UpdateButtonPressed()));
    connect(this, SIGNAL(UpdateJoyNames()), this, SLOT(HandleUpdateJoyNames()));
    connect(cb_joy, SIGNAL(currentIndexChanged(QString)), this, SLOT(ConnectJoystick(QString)));

    // Configure help button
    ConfigureHelpWindow();
}


void TabComponentItem::AddJoysticks(const std::deque<std::string> & joys)
{
    int items_before_adding = cb_joy->count();

    for (auto iter = joys.begin(); iter != joys.end(); ++iter)
        cb_joy->addItem(iter->c_str());

    int items_after_adding = cb_joy->count();

    // We check against 1 because there is a None option, so the combo box is never empty
    if (items_before_adding == 1 && items_after_adding > 1)
    {
        cb_joy->setCurrentIndex(1);
        if (items_after_adding == 2)
            ConnectJoystick(cb_joy->currentText());
    }
}


void TabComponentItem::AddGroupItem(ControlGroup* group)
{
    GroupItem* groupitem = new GroupItem(group, component);
    groupitem->ConfigureItem();
    groupitems.push_back(groupitem);
    lout_main->addWidget(groupitem->groupbox);
    //
    QCheckBox* cb_group = new QCheckBox(group->name.c_str());
    cb_group->setChecked(true);
    lout_options->addWidget(cb_group, 0, Qt::AlignLeft);
    //
    for (auto iter_modes = groupitem->modeitems.begin(); iter_modes != groupitem->modeitems.end(); ++iter_modes)
    {
        ModeItem* mode = *iter_modes;
        //
        QCheckBox  * cb_mode     = new QCheckBox(mode->name.c_str());
        QHBoxLayout* lout_spacer = new QHBoxLayout;
        //
        lout_spacer->setContentsMargins(20, 0, 0, 0);
        cb_mode->setChecked(true);
        //
        lout_spacer->addWidget(cb_mode, 0, Qt::AlignLeft);
        lout_options->addLayout(lout_spacer);
        //
        QObject::connect(cb_group, SIGNAL(clicked(bool)), cb_mode, SLOT(setVisible(bool)));
        QObject::connect(cb_mode, SIGNAL(clicked(bool)), mode, SLOT(SetShown(bool)));
    }
    //
    lout_options->addSpacing(4);
    //
    QObject::connect(cb_group, SIGNAL(clicked(bool)), groupitem, SLOT(SetShown(bool)));
}


void TabComponentItem::UpdateButtonPressed()
{
    for (auto iter = groupitems.begin(); iter != groupitems.end(); ++iter)
        (*iter)->Update();
}


void TabComponentItem::ShowExample()
{
    widget_example->show();
}


void TabComponentItem::HandleUpdateJoyNames()
{
    cb_joy->clear();
    cb_joy->clearEditText();
    for (deque<string>::iterator joylistit = component->gamepadsNamesList.begin(); joylistit!= component->gamepadsNamesList.end(); joylistit++)
        cb_joy->addItem((*joylistit).c_str());
}


void TabComponentItem::ConnectJoystick(QString joy_name)
{
    // Please do it properly.
    if (joy_name.toStdString() == "None")
        return;

   component->ConnectWithGamepad(joy_name.toStdString());
}


void TabComponentItem::ConfigureHelpWindow()
{
    widget_example = new QWidget;

    widget_example->setWindowTitle("Buttons Options");

    // Create widgets
    QGridLayout* lout_example   = new QGridLayout;
    QLabel* lb_example          = new QLabel;
    QListWidget* list_keyboard  = new QListWidget;
    QListWidget* list_joystick  = new QListWidget;
    QListWidget* list_action    = new QListWidget;
    QLabel* keylabel            = new QLabel("Keyboard Button Strings");
    QLabel* joylabel            = new QLabel("Joystick Button Strings");
    QLabel* actionlabel         = new QLabel("Actions");

    // Configure layout
    lout_example->addWidget(lb_example    , 0, 0, 2, Qt::AlignLeft);
    lout_example->addWidget(list_keyboard , 1, 1, 1, Qt::AlignLeft);
    lout_example->addWidget(list_joystick , 1, 2, 1, Qt::AlignLeft);
    lout_example->addWidget(list_action   , 1, 3, 1, Qt::AlignLeft);
    lout_example->addWidget(keylabel      , 0, 1, 1, Qt::AlignLeft);
    lout_example->addWidget(joylabel      , 0, 2, 1, Qt::AlignLeft);
    lout_example->addWidget(actionlabel   , 0, 3, 1, Qt::AlignLeft);

    // Set the layout
    widget_example->setLayout(lout_example);
    
    QPixmap pixmap;
    if (pixmap.load("seqexample.png"))
        lb_example->setPixmap(pixmap);

    const std::deque<std::string> & keyboard_buttons = component->GetKeyboard()->GetButtonsNames();
    for (auto iter = keyboard_buttons.begin() ; iter != keyboard_buttons.end() ; ++iter)
        list_keyboard->addItem(iter->c_str());

    const std::deque<std::string> & gamepad_buttons = component->GetGamepad()->GetButtonsNames();
    for (auto iter = gamepad_buttons.begin() ; iter != gamepad_buttons.end() ; ++iter)
        list_joystick->addItem(iter->c_str());

    list_action->addItem(InputButtons::GetButtonStateName(InputButtons::StatePressed).c_str());
    list_action->addItem(InputButtons::GetButtonStateName(InputButtons::StateReleased).c_str());
    list_action->addItem(InputButtons::GetButtonStateName(InputButtons::StateHolding).c_str());
    list_action->addItem(InputButtons::GetButtonStateName(InputButtons::StateNotHolding).c_str());
    
    connect(bt_help, SIGNAL(clicked()), this, SLOT(ShowExample()));
}


void TabComponentItem::ChangeJoystickNamesList()
{
    emit(UpdateJoyNames());
}


TabComponentItem::~TabComponentItem()
{
    for (auto iter = groupitems.begin(); iter != groupitems.end(); ++iter)
        delete *iter;
}
