// control_button.h



#include <input_mapper/control_button.h>



using namespace std;
using namespace input_mapper;



ControlButton::ControlButton() :
    button_id(0), button_state(InputButtons::StateNone)
{
}


ControlButton::ControlButton(unsigned int button, InputButtons::ButtonState button_state) :
    button_id(button), button_state(button_state)
{
}


ControlButton::ControlButton(unsigned int button, const string& state) :
    button_id(button)
{
    SetButtonStateFromString(state);
}


bool ControlButton::SetButtonStateFromString(const string& state)
{
    button_state = InputButtons::GetButtonStateFromName(state);
    return button_state != InputButtons::StateNone;
}
