// input_mapper_nodelet.cpp



#include <input_mapper/input_mapper_nodelet.h>



using namespace std;
using namespace input_mapper;



InputMapperNodelet::InputMapperNodelet() : holdingThread(NULL)
{
    msgGamepadSub = new ros::Subscriber();
    msgGamepadListSub = new ros::Subscriber();
    msgKeyboardSub = new ros::Subscriber();
}


void InputMapperNodelet::onInit()
{
    nodehandle = getNodeHandle();

    char name[100];
    gethostname(name, 100);
    string strname = name;
    for (unsigned char i = 0; i < strname.size(); i++)
    {
        if (strname[i] == '-')
            strname[i] = '/';
    }
    ros::NodeHandle& privateNH = getPrivateNodeHandle();
    string gamepadManagerNamespace;
    privateNH.param("gamepad_manager", gamepadManagerNamespace, string(""));
    privateNH.param<double>("holding_wait", holdingWaitTime, 1000.0);
    privateNH.param("name", componentName, string(""));

    runHoldingThread = true;
    holdingThread = new boost::thread(InputMapperNodelet::HoldingThread, this);

    *msgGamepadListSub = nodehandle.subscribe(gamepadManagerNamespace + "/selected_devices_list", 1000, &InputMapperNodelet::TreatGamepadList, this);
    *msgKeyboardSub = nodehandle.subscribe(strname + "/keyboard", 1000, &InputMapperNodelet::TreatKeyboardButton, this);
}


void InputMapperNodelet::TreatGamepadList(const linux_devs::DevicesList::ConstPtr& msg)
{
    gamepadsNamesList.clear();
    std::copy (msg->devices.begin(), msg->devices.end(), std::inserter(gamepadsNamesList, gamepadsNamesList.begin()));
    SetToolGamepadListNames();
}

/*
 * \todo Check for a holding time for every key. Resetting the time when it is released/pressed.
 */
void InputMapperNodelet::HoldingThread(InputMapperNodelet* mapper)
{
    while (mapper->runHoldingThread)
    {
        mapper->updatemutex.lock();
        //mapper->UpdateGamepadButtonStates();
        // Check for pressed to holding and released to not holding transitions.
        //for (unsigned int i = 0; i < mapper->gamepad->SizeButtons(); i++)
        for (unsigned int i = 0; i < mapper->buttonStates.size(); i++)
        {
            switch (mapper->buttonStates[i])
            {
            case InputButtons::StateReleased:
                if (!mapper->actualButtonValues[i])
                mapper->buttonStates[i] = InputButtons::StateNotHolding;
                break;
            case InputButtons::StatePressed:
                if (mapper->actualButtonValues[i])
                mapper->buttonStates[i] = InputButtons::StateHolding;
                break;
            default:
                break;
            }
        }

        mapper->UpdateGamepadFlags();
        mapper->UpdateKeyboardFlags();

        if (mapper->gamepadActivationMode.isActive())
            mapper->Update(InputButtons::TypeGamepad);
        if (mapper->keyboardActivationMode.isActive())
            mapper->Update(InputButtons::TypeKeyboard);

        mapper->updatemutex.unlock();
        boost::this_thread::sleep(boost::posix_time::milliseconds(mapper->holdingWaitTime));
    }
}


void InputMapperNodelet::TreatGamepadButton(const sensor_msgs::Joy::ConstPtr& msg)
{
    updatemutex.lock();

    std::deque<int> buttons;
    std::deque<int> axes;

    // Get list of active buttons
    for (unsigned int i = 0; i < msg->buttons.size(); ++i)
    {
        if (msg->buttons[i] != 0)
            buttons.push_back(i);
    }

    // Get list of active axes
    for (unsigned int i = 0; i < msg->axes.size(); ++i)
    {
        if (msg->axes[i] != 0)
            axes.push_back(i);
    }

    // Search for buttons not previously pressed. Use the first one found
    for (auto iter = buttons.begin(); iter != buttons.end(); ++iter)
    {
        if (std::find(gamepad_buttons_pressed.begin(), gamepad_buttons_pressed.end(), *iter) == gamepad_buttons_pressed.end())
        {
            variable_last_command->SetValue(ControlValue::GamepadButton, *iter);
            variable_last_command->Update();
        }
    }

    // Search for axes not previously active. Use the first one found
    for (auto iter = axes.begin(); iter != axes.end(); ++iter)
    {
        if (std::find(gamepad_axes_pressed.begin(), gamepad_axes_pressed.end(), *iter) == gamepad_axes_pressed.end())
        {
            variable_last_command->SetValue(ControlValue::GamepadAxis, *iter);
            variable_last_command->Update();
        }
    }

    gamepad_buttons_pressed = buttons;
    gamepad_axes_pressed = axes;

    const InputButtons* gamepad = this->GetGamepad();
    for (unsigned int i = 0; i < (gamepad->GetButtonsIds().size() - gamepad->GetAxisToButtonMap().size()) ; i++)
        actualButtonValues[i] = msg->buttons[i];
    axisValues.clear();
    //for (unsigned int j = 0; j < gamepad->GetAxesIds().size(); j++)
    for (unsigned int j = 0; j < msg->axes.size(); j++)
        axisValues.push_back(msg->axes[j]);

    UpdateGamepadButtonStates();
    Update(InputButtons::TypeGamepad);

    updatemutex.unlock();
}


void InputMapperNodelet::TreatKeyboardButton(const keyboard::KeyboardKeys::ConstPtr& msg)
{
    updatemutex.lock();

    if (!msg->pressedKeys.empty())
    {
        variable_last_command->SetValue(ControlValue::KeyboardButton, *msg->pressedKeys.rbegin());
        variable_last_command->Update();
    }

    for (int i = gamepad->SizeButtons(); i < actualButtonValues.size(); i++) 
    {
        actualButtonValues[i] = 0;
    }

    for (auto it = msg->pressedKeys.begin(); it != msg->pressedKeys.end(); it++)
    {
        actualButtonValues[*it + gamepad->SizeButtons()] = 1;
    }

    UpdateKeyboardButtonStates();
    Update(InputButtons::TypeKeyboard);

    lastPressedKey.clear();
    std::copy(msg->pressedKeys.begin(), msg->pressedKeys.end(), std::inserter(lastPressedKey, lastPressedKey.begin()));

    updatemutex.unlock();
}


void InputMapperNodelet::ConnectWithGamepad(string gamepadName)
{
    bool valid_name = find(gamepadsNamesList.begin(), gamepadsNamesList.end(), gamepadName) != gamepadsNamesList.end();
    bool not_connected = true; //find(connectedGamepadsNames.begin(), connectedGamepadsNames.end(), gamepadName) == connectedGamepadsNames.end();
    if (valid_name && not_connected)
    {
        //connectedGamepadsNames.push_back(gamepadName);

        char hostname[100];
        gethostname(hostname, 100);

        string topicName = hostname;
        replace(topicName.begin(), topicName.end(), '-', '/');

        topicName += string("/gamepads/");

        // Get only the last part of the device path.
        string devicePath = gamepadName;
        auto slashPosition = devicePath.find_last_of("/\\");

        topicName += devicePath.substr(slashPosition + 1);

        *msgGamepadSub = nodehandle.subscribe<sensor_msgs::Joy>(topicName, 10, &InputMapperNodelet::TreatGamepadButton, this);
    }
    else
       cout << "Unable to connect with gamepad: " << gamepadName << endl;
}


void InputMapperNodelet::DisconnectFromGamepad(string gamepadName)
{
    /*
    deque<string>::iterator nameIt = find(connectedGamepadsNames.begin(), connectedGamepadsNames.end(),gamepadName);
    if(nameIt != connectedGamepadsNames.end())
    {
        connectedGamepadsNames.erase(nameIt);
        msgGamepadSub->shutdown();
    }
    else
       cout << "Unable to disconnect from gamepad: " << gamepadName << endl;
    */
    msgGamepadSub->shutdown();
}


InputMapperNodelet::~InputMapperNodelet()
{
    runHoldingThread = false;
    holdingThread->join();

    delete msgGamepadSub;
    delete msgGamepadListSub;
    delete msgKeyboardSub;
}
