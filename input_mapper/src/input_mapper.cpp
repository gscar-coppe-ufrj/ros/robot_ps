// input_mapper.cpp



#include <input_mapper/input_mapper.h>
#include <simple_xml_settings/simple_xml_settings.h>
#include <input_mapper/keyboard.h>
#include <input_mapper/xbox_gamepad.h>



using namespace std;
using namespace device;
using namespace input_mapper;



InputMapper::InputMapper() :
    keyboard(new Keyboard), gamepad(new XboxGamepad), variable_last_command(0)
{
    for(unsigned int i = 0; i < keyboard->SizeButtons() + gamepad->SizeButtons(); ++i)
    {
        buttonStates.push_back(InputButtons::StateNotHolding);
        actualButtonValues.push_back(0);
    }

    variable_last_command = new VariableTemplate<ControlValue::ControlValueInput, int>("LastCommand", "", false);
}


InputButtons* InputMapper::GetKeyboard() const
{
    return keyboard;
}


InputButtons* InputMapper::GetGamepad() const
{
    return gamepad;
}


bool InputMapper::CreateSequence(const std::string& sequence, InputButtons* gamepad_keyboard, ControlMode* mode, bool is_keyboard_sequence)
{
    // Clean keyboard or gamepad last sequence
    if (is_keyboard_sequence)
    {
        mode->keyboardStringSequence = sequence;
        mode->keyboardSequence.clear();
    }
    else
    {
        mode->gamepadStringSequence = sequence;
        mode->gamepadSequence.clear();
    }

    // Remove white space
    int stringIndex = 0;
    std::string cleanSequence;

    for (std::string::const_iterator i = sequence.begin(); i < sequence.end(); i++)
    {
        if (string(1,sequence[stringIndex]) != " ")
           cleanSequence += sequence[stringIndex];

        stringIndex ++;
    }

    bool validSequence = true;

    // Split the sequence
    unsigned int indexMin = 0, plusPos = 0;
    std::vector<std::string> splitSequence;

    while (indexMin < cleanSequence.size())
    {
        plusPos = cleanSequence.find("+", indexMin);
        if (plusPos == cleanSequence.size() - 1)
        {
            validSequence = false;
            break;
        }
        else if (plusPos == -1)
        {
            splitSequence.push_back(cleanSequence.substr(indexMin));
            break;
        }
        else
            splitSequence.push_back(cleanSequence.substr(indexMin, plusPos - indexMin));
        indexMin = plusPos + 1;
    }

    // Treat sequence
    int id;
    unsigned int index = 0;

    for (int i = 0; i < splitSequence.size(); i++)
    {
        ControlButton button;

        std::string buttonName;
        std::string buttonState;

        index = splitSequence[i].find("/");

        buttonName = splitSequence[i].substr(0, index);
        buttonState = splitSequence[i].substr(index + 1);

        id = gamepad_keyboard->GetButtonIdFromName(buttonName);

        if (id == -1)
        {
            validSequence = false;
            break;
        }

        button.button_id = id;

        bool state = button.SetButtonStateFromString(buttonState);

        if (state)
        {
            if (is_keyboard_sequence)
            {
                mode->keyboardSequence.push_back(button);
            }
            else
            {
                mode->gamepadSequence.push_back(button);
            }
        }
        else
        {
            validSequence = false;
            break;
        }
    }

    if (validSequence == false)
    {
        mode->gamepadSequence.clear();
        mode->keyboardSequence.clear();
    }

    return validSequence;
}


void InputMapper::ReadModeSequenceXml()
{
    SimpleXMLSettings settings(xmlSequenceFileName, "", SimpleXMLSettings::YesNo);
    if (!settings.HasLock())
        return;

    settings.BeginLoad();
    //
    std::string sequence;
    for (auto iter_groups = groups.begin(); iter_groups != groups.end(); ++iter_groups)
    {
        ControlGroup* group = *iter_groups;
        if (settings.BeginGroup("Group", "Name", group->name))
        {
            for (auto iter_modes = group->modes.begin(); iter_modes != group->modes.end(); ++iter_modes)
            {
                ControlMode* mode = *iter_modes;
                if (settings.BeginGroup("Mode", "Name", mode->name))
                {
                    mode->isToggle = settings.LoadBool("ToggleMode", mode->isToggle);
                    //
                    sequence = settings.LoadText("GamepadSequence", mode->gamepadStringSequence);
                    //if( !CreateSequence(sequence, InputButtons::ModeGamepad, *mode ) )
                    if( !CreateSequence(sequence, gamepad, mode, false) )
                        std::cout << "Wrong gamepad sequence = " << sequence << std::endl;
                    //
                    sequence = settings.LoadText("KeyboardSequence", mode->keyboardStringSequence);
                    //if( !CreateSequence(sequence, InputButtons::ModeKeyboard, *mode ) )
                    if( !CreateSequence(sequence, keyboard, mode, true) )
                        std::cout << "Wrong keyboard sequence = " << sequence << std::endl;
                    //
                    unsigned int i = 0;
                    while (settings.BeginGroup("Value", i) && i < mode->values.size())
                    {
                        std::string input = settings.LoadText("Input", "");
                        std::string sign  = settings.LoadText("Sign", "");
                        std::string value = settings.LoadText("Value", "");
                        //
                        ControlValue* control_value = mode->values[i];
                        control_value->SetInputTypeFromString(input);
                        control_value->positive = (sign != "Negative");
                        control_value->SetGamepadAndKeyboard(gamepad, keyboard);
                        control_value->SetIdFromName(value);
                        //
                        settings.EndGroup();
                        //
                        i++;
                    }
                    settings.EndGroup();
                }
            }
            settings.EndGroup();
        }
    }
    //
    settings.EndLoad();
}


void InputMapper::WriteModeSequenceXml()
{
    SimpleXMLSettings settings(xmlSequenceFileName, "", SimpleXMLSettings::YesNo);
    if (!settings.HasLock())
        return;
    //
    settings.BeginSave();
    //
    int i = 0;
    for (auto iter_group = groups.begin(); iter_group != groups.end(); ++iter_group)
    {
        ControlGroup* group = *iter_group;
        settings.BeginGroup("Group", i++);
        settings.SaveText("Name", group->name);
        //
        int j = 0;
        for (auto iter_mode = group->modes.begin(); iter_mode != group->modes.end(); ++iter_mode)
        {
            ControlMode* mode = *iter_mode;
            settings.BeginGroup("Mode", j++);
            settings.SaveText("Name", mode->name);
            settings.SaveText("GamepadSequence", mode->gamepadStringSequence);
            settings.SaveText("KeyboardSequence", mode->keyboardStringSequence);
            settings.SaveBool("ToggleMode", mode->isToggle);
            //
            int k = 0;
            for (auto iter_value = mode->values.begin(); iter_value != mode->values.end(); ++iter_value)
            {
                ControlValue* value = *iter_value;
                settings.BeginGroup("Value", k++);
                settings.SaveNumber("ID", k-1);
                settings.SaveText("Input", value->GetInputTypeString());
                if (!value->positive)
                    settings.SaveText("Sign", "Negative");
                settings.SaveText("Value", value->GetNameFromId());
                settings.EndGroup();
            }
            //
            settings.EndGroup();
        }
        //
        settings.EndGroup();
    }
    //
    settings.EndSave();
}


void InputMapper::SetToolGamepadListNames()
{/*
    for (deque<InputConfig*>::iterator toolit = tools.begin(); toolit < tools.end(); toolit++)
        (*toolit)->ChangeJoystickNamesList();*/
}


void InputMapper::UpdateGamepadButtonStates()
{
    // This loop emulates axes as buttons.
    const std::deque<InputButtons::TupleMap>& tuples = gamepad->GetAxisToButtonMap();
    for (auto iter = tuples.begin(); iter != tuples.end(); ++iter)
    {
        const InputButtons::TupleMap& tuple = *iter;
        int id_axis     = tuple.get<0>();
        int id_button   = tuple.get<1>();
        int sign        = (tuple.get<2>()) ? 1 : -1;
        double deadzone = tuple.get<3>();
        actualButtonValues[id_button] = (sign*axisValues[id_axis] > deadzone) ? 1 : 0;
    }

    for(unsigned int i = 0; i < gamepad->SizeButtons(); i++)
    {
        switch(buttonStates[i])
        {
        case InputButtons::StateReleased:
            if(actualButtonValues[i])
                buttonStates[i] = InputButtons::StatePressed;
            //else
            //    buttonStates[i] = InputButtons::StateNotHolding;
            break;
        case InputButtons::StatePressed:
            //if(actualButtonValues[i])
            //{buttonStates[i] = InputButtons::StateHolding;}
            if (!actualButtonValues[i])
                buttonStates[i] = InputButtons::StateReleased;
            break;
        case InputButtons::StateHolding:
            if(actualButtonValues[i])
                buttonStates[i] = InputButtons::StateHolding;
            else
                buttonStates[i] = InputButtons::StateReleased;
            break;
        case InputButtons::StateNotHolding:
            if(actualButtonValues[i])
                buttonStates[i] = InputButtons::StatePressed;
            else
                buttonStates[i] = InputButtons::StateNotHolding;
            break;
        case InputButtons::StateNone:
        default:
            std::cout << "Gamepad button state not defined (name: " ;
            std::cout << gamepad->GetButtonNameFromId(i) << ", id: " << i << ')' << std::endl;
            break;
        }
    }

    UpdateGamepadFlags();
}


void InputMapper::UpdateKeyboardButtonStates()
{
    //for(unsigned int i = gamepad->SizeButtons(); i < keyboard->SizeButtons(); i++)
    for (unsigned int i = gamepad->SizeButtons(); i < buttonStates.size(); i++)
    {
        switch(buttonStates[i])
        {
        case InputButtons::StateReleased:
            if(actualButtonValues[i])
                buttonStates[i] = InputButtons::StatePressed;
            //else
            //    buttonStates[i] = InputButtons::StateNotHolding;
            break;
        case InputButtons::StatePressed:
            //if(actualButtonValues[i])
            //    buttonStates[i] = InputButtons::StateHolding;
            //else
            if (!actualButtonValues[i])
                buttonStates[i] = InputButtons::StateReleased;
            break;
        case InputButtons::StateHolding:
            if(actualButtonValues[i])
                buttonStates[i] = InputButtons::StateHolding;
            else
                buttonStates[i] = InputButtons::StateReleased;
            break;
        case InputButtons::StateNotHolding:
            if(actualButtonValues[i])
                buttonStates[i] = InputButtons::StatePressed;
            else
                buttonStates[i] = InputButtons::StateNotHolding;
            break;
        case InputButtons::StateNone:
        default:
            std::cout << "Keyboard button state not defined (name: " ;
            std::cout << keyboard->GetButtonNameFromId(i) << ", id: " << i << ')' << std::endl;
            break;
        }
    }

    UpdateKeyboardFlags();
}


void InputMapper::UpdateGamepadFlags()
{
    if (gamepadActivationMode.gamepadSequence.empty() == false)
    {
        bool validSequence = ValidateSequence(gamepadActivationMode.gamepadSequence, InputButtons::TypeGamepad);

        gamepadActivationMode.SetModeActive(validSequence);
    }
    else
    {
        gamepadActivationMode.SetModeActive(true);
    }

    auto viewers = this->GetListOfTools<InputMapper, InputViewer>();
    if (viewers != NULL)
        for (auto i = viewers->begin(); i != viewers->end(); i++)
        (*i)->UpdateGamepadActive(this, gamepadActivationMode.isActive());

    for (vector<ControlGroup*>::iterator groupsit = groups.begin(); groupsit < groups.end(); groupsit++)
    {
        (*groupsit)->lastInputMode = (*groupsit)->inputMode;

        if (gamepadActivationMode.isActive())
        {
            if ((*groupsit)->inputMode == InputButtons::TypeGamepad || (*groupsit)->inputMode == InputButtons::TypeNone)
            {
                for (vector<ControlMode*>::iterator modesit = (*groupsit)->modes.begin(); modesit < (*groupsit)->modes.end(); modesit++)
                {
                    if (SetActualMode(*(*groupsit), *(*modesit), (*modesit)->gamepadSequence, InputButtons::TypeGamepad))
                        break;
                }
            }
        }
        else if ((*groupsit)->inputMode == InputButtons::TypeGamepad)
        {
            if ((*groupsit)->actualMode != CONTROL_GROUP_MODE_NONE_ID)
                (*groupsit)->lastMode = (*groupsit)->actualMode;
            (*groupsit)->actualMode = CONTROL_GROUP_MODE_NONE_ID;
            (*groupsit)->inputMode = InputButtons::TypeNone;
            (*groupsit)->lastInputMode = InputButtons::TypeGamepad;
        }

        if ((*groupsit)->actualMode != (*groupsit)->lastMode)
        {
            auto viewers = this->GetListOfTools<InputMapper, InputViewer>();
            if (viewers != NULL)
                for (auto i = viewers->begin(); i != viewers->end(); i++)
                    (*i)->UpdateMode(this, groupsit - groups.begin(), *groupsit);
        }
    }
}


void InputMapper::UpdateKeyboardFlags()
{
    if (keyboardActivationMode.keyboardSequence.empty() == false)
    {
        bool validSequence = ValidateSequence(keyboardActivationMode.keyboardSequence, InputButtons::TypeKeyboard);

        keyboardActivationMode.SetModeActive(validSequence);
    }
    else
    {
        keyboardActivationMode.SetModeActive(true);
    }

    auto viewers = this->GetListOfTools<InputMapper, InputViewer>();
    if (viewers != NULL)
        for (auto i = viewers->begin(); i != viewers->end(); i++)
            (*i)->UpdateKeyboardActive(this, keyboardActivationMode.isActive());

    for (vector<ControlGroup*>::iterator groupsit = groups.begin(); groupsit < groups.end(); groupsit++)
    {
        (*groupsit)->lastInputMode = (*groupsit)->inputMode;

        if (keyboardActivationMode.isActive())
        {
            if ((*groupsit)->inputMode == InputButtons::TypeKeyboard || (*groupsit)->inputMode == InputButtons::TypeNone)
            {
                for (vector<ControlMode*>::iterator modesit = (*groupsit)->modes.begin(); modesit < (*groupsit)->modes.end(); modesit++)
                {
                    if (SetActualMode(*(*groupsit), *(*modesit), (*modesit)->keyboardSequence, InputButtons::TypeKeyboard))
                        break;
                }
            }
        }
        else if ((*groupsit)->inputMode == InputButtons::TypeKeyboard)
        {
            if ((*groupsit)->actualMode != CONTROL_GROUP_MODE_NONE_ID)
                (*groupsit)->lastMode = (*groupsit)->actualMode;

            (*groupsit)->actualMode = CONTROL_GROUP_MODE_NONE_ID;
            (*groupsit)->inputMode = InputButtons::TypeNone;
            (*groupsit)->lastInputMode = InputButtons::TypeKeyboard;
        }

        if ((*groupsit)->actualMode != (*groupsit)->lastMode)
        {
            auto viewers = this->GetListOfTools<InputMapper, InputViewer>();
            if (viewers != NULL)
                for (auto i = viewers->begin(); i != viewers->end(); i++)
                    (*i)->UpdateMode(this, groupsit - groups.begin(), *groupsit);
        }
    }
}


void InputMapper::SetKeyboard(InputButtons* keyboard)
{
    delete this->keyboard;
    this->keyboard = keyboard;
    //
    for(unsigned int i = 0; i < keyboard->SizeButtons()+gamepad->SizeButtons(); ++i)
    {
        buttonStates.push_back(InputButtons::StateNotHolding);
        actualButtonValues.push_back(0);
    }
}


void InputMapper::SetGamepad(InputButtons* gamepad)
{
    delete this->gamepad;
    this->gamepad = gamepad;

    for(unsigned int i = 0; i < keyboard->SizeButtons()+gamepad->SizeButtons(); ++i)
    {
        buttonStates.push_back(InputButtons::StateNotHolding);
        actualButtonValues.push_back(0);
    }
}


bool InputMapper::SetActualMode(ControlGroup& group, ControlMode& mode, vector<ControlButton>& sequence, InputButtons::InputType input_type)
{
    bool sequenceok = ValidateSequence(sequence, input_type);

    if (sequenceok)
    {
        if(mode.isToggle == true && mode.canToggle == true)
        {
            if (group.actualMode == mode.id)
            {
            // Mode is toggle and currently active.
                mode.canToggle = false;

                // Make the mode inactive.
                group.lastMode = group.actualMode;
                group.actualMode = CONTROL_GROUP_MODE_NONE_ID;

                //mode.release = true;
                group.lastInputMode = group.inputMode;
                group.inputMode = InputButtons::TypeNone;
            }
            else
            // Mode is toggle and currently inactive.
            {
                group.lastMode = group.actualMode;
                group.actualMode = mode.id;

                mode.canToggle = false;

                if (group.inputMode != input_type)
                {
                    group.lastInputMode = group.inputMode;
                    group.inputMode = input_type;
                }
            }
        }
        else if (mode.isToggle == false)
        {
        // Mode is not toggle.
            // Mode is not currently active, save current mode information.
            if (mode.id != group.actualMode)
                group.lastMode = group.actualMode;

            // Activate this mode.
            group.actualMode = mode.id;


            if (group.inputMode != input_type)
            {
                group.lastInputMode = group.inputMode;
                group.inputMode = input_type;
            }
        }
    }
    else
    {
        if(mode.isToggle)
        {
            mode.canToggle = true;
/*
            if(group.actualMode == mode.id)
                mode.releasedMode = true;
            else if(group.actualMode == CONTROL_GROUP_MODE_NONE_ID && group.lastMode == mode.id)
            {
                if(input_type == group.inputMode && mode.release)
                {
                    group.inputMode = InputButtons::TypeNone;
                    if (group.inputMode != input_type)
                    {
                        group.lastInputMode = group.inputMode;
                        group.inputMode = input_type;
                    }
                    mode.release = false;
                    group.lastMode = CONTROL_GROUP_MODE_NONE_ID;
                }
            }
            */
        }
        else
        {
            if (group.actualMode == mode.id)
            {
                group.lastMode = group.actualMode;
                group.actualMode = CONTROL_GROUP_MODE_NONE_ID;

                // Set this group as free !
                group.lastInputMode = group.inputMode;
                group.inputMode = InputButtons::TypeNone;
            }
        }
    }

    return sequenceok;
}


bool InputMapper::ValidateSequence(std::vector<ControlButton>& sequence, InputButtons::InputType input_type)
{
    if(sequence.empty())
        return false;

    int offset = 0;

    if (input_type == InputButtons::TypeKeyboard)
        offset = gamepad->SizeButtons();

    for(auto iter = sequence.begin(); iter != sequence.end(); ++iter)
    {
        ControlButton& button = *iter;
        if(buttonStates[button.button_id + offset] != button.button_state)
            return false;
    }
    return true;
}


InputMapper::~InputMapper()
{
    /*for (deque<InputConfig*>::iterator toolit = tools.begin(); toolit != tools.end(); toolit++)
        (*toolit)->RemoveComponent(this);
    */tools.clear();
    //
    WriteModeSequenceXml();
    //
    for (auto iter_group = groups.begin(); iter_group != groups.end(); ++iter_group)
        delete *iter_group;
    //
    delete keyboard;
    delete gamepad;
    //
    delete variable_last_command;
}
