// control_group.cpp



#include <input_mapper/control_group.h>



using namespace std;
using namespace input_mapper;



ControlGroup::ControlGroup() :
    actualMode(CONTROL_GROUP_MODE_NONE_ID), lastMode(CONTROL_GROUP_MODE_NONE_ID), inputMode(InputButtons::TypeNone), lastInputMode(InputButtons::TypeNone)
{
}


ControlGroup::ControlGroup(unsigned int id, string name) :
    id(id), name(name), actualMode(CONTROL_GROUP_MODE_NONE_ID), lastMode(CONTROL_GROUP_MODE_NONE_ID), inputMode(InputButtons::TypeNone), lastInputMode(InputButtons::TypeNone)
{
}


ControlGroup::~ControlGroup()
{
    for (auto iter = modes.begin(); iter != modes.end(); ++iter)
        delete *iter;
}
