// input_config_qwidget.cpp



#include <input_mapper/input_config_qwidget.h>
#include <iostream>
#include <QApplication>



using namespace std;
using namespace input_mapper;



InputConfigQWidget::InputConfigQWidget()
{
    sizehintw = 1600;
    sizehinth = 800;

    connect(this, SIGNAL(CreatingNewTabComponent(InputMapper*)), this, SLOT(CreateNewTabComponent(InputMapper*)));

    setStyleSheet("QLineEdit { background-color: white }");
}

void InputConfigQWidget::Init()
{
    vlayout = new QVBoxLayout;
    setLayout(vlayout);

    tabwidget = new QTabWidget();
    vlayout->addWidget(tabwidget);

    auto list = this->GetListOfComponents<InputConfig, InputMapper>();
    if (list->size() > 0)
    {
        for (auto iter = list->begin(); iter != list->end(); iter++)
            emit CreateNewTabComponent(*iter);
    }
    else
        cout << "no InputMapper component found" << endl;
}


void InputConfigQWidget::TreatAddMapper(InputMapper* component)
{
    InputConfig::TreatAddMapper(component);

    deque<InputMapper*>::iterator componentit = std::find(components.begin(),components.end(),component);
    if (componentit == components.end())
        emit CreatingNewTabComponent(component);
}


void InputConfigQWidget::TreatRemoveMapper(InputMapper* component)
{
    TabComponentItem* temptabcomponent;

    for (deque<TabComponentItem*>::iterator tabcomponentit = tabcomponentlist.begin() ; tabcomponentit < tabcomponentlist.end() ; tabcomponentit++)
    {
        if ((*tabcomponentit)->component == component)
        {
            temptabcomponent = (*tabcomponentit);
            tabcomponentlist.erase(tabcomponentit);
            delete temptabcomponent;
            break;
        }
    }

    InputConfig::TreatRemoveMapper(component);
}


QSize InputConfigQWidget::sizeHint() const
{
    return QSize(sizehintw, sizehinth);
}


void InputConfigQWidget::ChangeJoystickNamesList()
{
    for (deque<TabComponentItem*>::iterator tabitemit = tabcomponentlist.begin() ; tabitemit < tabcomponentlist.end() ; tabitemit++)
        (*tabitemit)->ChangeJoystickNamesList();
}


bool InputConfigQWidget::CreateNewTabComponent(InputMapper* component)
{
    bool tab_exists = false;

    // Check if tab based on this InputMapper component already exists
    for (auto iter = tabcomponentlist.begin(); iter != tabcomponentlist.end(); iter++)
    {
        TabComponentItem* tab_item = *iter;
        if (tab_item->component == component)
        {
            tab_exists = true;
            break;
        }
    }

    // If it doesn't, create a new tab
    if (!tab_exists)
    {
        TabComponentItem* tab_item = new TabComponentItem(component);
        tabcomponentlist.push_back(tab_item);
        tabwidget->addTab(tab_item, component->name.c_str());
        ConfigureTabComponent(tab_item, component);
        return true;
    }
    else
        return false;
}


void InputConfigQWidget::ConfigureTabAllComponents()
{
    int i = 0;
    for (auto iter_component = components.begin(); iter_component != components.end(); ++iter_component)
        ConfigureTabComponent(tabcomponentlist[i++], *iter_component);
}


void InputConfigQWidget::ConfigureTabComponent(TabComponentItem* item, InputMapper* component)
{
}


InputConfigQWidget::~InputConfigQWidget()
{
    for (auto it = tabcomponentlist.begin(); it != tabcomponentlist.end(); it++)
        delete (*it);
    delete vlayout;
    delete tabwidget;
}
