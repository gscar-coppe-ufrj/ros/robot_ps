// xbox_gamepad.cpp



#include <input_mapper/xbox_gamepad.h>

#include <boost/assign/list_of.hpp>



using namespace input_mapper;



const std::deque<std::string>XboxGamepad::names_buttons = boost::assign::list_of
        ("A")
        ("B")
        ("X")
        ("Y")
        ("LB")
        ("RB")
        ("BACK")
        ("START")
        ("XBOX")
        ("LEFT_STICK_BUTTON")
        ("RIGHT_STICK_BUTTON")
        ("LEFT_STICK_ALL_LEFT")
        ("LEFT_STICK_ALL_RIGHT")
        ("LEFT_STICK_ALL_UP")
        ("LEFT_STICK_ALL_DOWN")
        ("LT_AXIS_ALL_DOWN")
        ("RIGHT_STICK_ALL_LEFT")
        ("RIGHT_STICK_ALL_RIGHT")
        ("RIGHT_STICK_ALL_UP")
        ("RIGHT_STICK_ALL_DOWN")
        ("RT_AXIS_ALL_DOWN")
        ("DPAD_LEFT")
        ("DPAD_RIGHT")
        ("DPAD_UP")
        ("DPAD_DOWN");

const std::deque<std::string>XboxGamepad::names_axes = boost::assign::list_of
        ("LEFT_STICK_LR")
        ("LEFT_STICK_UD")
        ("LT_AXIS")
        ("RIGHT_STICK_LR")
        ("RIGHT_STICK_UD")
        ("RT_AXIS")
        ("DPAD_LR")
        ("DPAD_UD");


const std::deque<int>XboxGamepad::ids_buttons = boost::assign::list_of
        (0)(1)(2)(3)(4)(5)(6)(7)(8)(9)(10)(11)(12)
        (13)(14)(15)(16)(17)(18)(19)(20)(21)(22)(23)(24);


const std::deque<int>XboxGamepad::ids_axes = boost::assign::list_of(0)(1)(2)(3)(4)(5)(6)(7);


const std::deque<InputButtons::TupleMap>XboxGamepad::map_axis_button = boost::assign::tuple_list_of
        (0, 11,  true, 0.7)
        (0, 12, false, 0.7)
        (1, 13,  true, 0.7)
        (1, 14, false, 0.7)
        (2, 15, false, 0.7)
        (3, 16,  true, 0.7)
        (3, 17, false, 0.7)
        (4, 18,  true, 0.7)
        (4, 19, false, 0.7)
        (5, 20, false, 0.7)
        (6, 21,  true, 0.7)
        (6, 22, false, 0.7)
        (7, 23,  true, 0.0)
        (7, 24, false, 0.0);


const double input_mapper::XboxGamepad::GetAxisScale() const
{
    return -1. / 32767.;
}


const std::deque<std::string>&XboxGamepad::GetButtonsNames() const
{
    return XboxGamepad::names_buttons;
}


const std::deque<int>&XboxGamepad::GetButtonsIds() const
{
    return XboxGamepad::ids_buttons;
}


const std::deque<std::string>&XboxGamepad::GetAxesNames() const
{
    return XboxGamepad::names_axes;
}


const std::deque<int>&XboxGamepad::GetAxesIds() const
{
    return XboxGamepad::ids_axes;
}

const std::deque<InputButtons::TupleMap>&XboxGamepad::GetAxisToButtonMap() const
{
    return map_axis_button;
}
