/*!
 * \file input_mapper_nodelet.h
 */



#ifndef INPUT_MAPPER_INPUT_MAPPER_NODELET_H
#define INPUT_MAPPER_INPUT_MAPPER_NODELET_H



#include <linux_devs/DevicesList.h>
#include <keyboard/KeyboardKeys.h>
#include <sensor_msgs/Joy.h>
#include <input_mapper/input_mapper.h>
#include <custom_loader/custom_nodelet.h>
#include <ros/ros.h>



namespace input_mapper {



/*!
 * \class InputMapperNodelet
 * \brief The InputMapperNodelet class
 */
class InputMapperNodelet: public InputMapper, public custom_loader::CustomNodelet
{
    ///
    std::deque<int> gamepad_buttons_pressed;
    ///
    std::deque<int> gamepad_axes_pressed;

    ///
    ros::Subscriber* msgGamepadSub;
    ///
    ros::Subscriber* msgGamepadListSub;
    ///
    ros::Subscriber* msgKeyboardSub;

    /*!
     * \brief TreatGamepadButton
     * \param msg
     */
    void TreatGamepadButton(const sensor_msgs::Joy::ConstPtr& msg);
    /*!
     * \brief TreatKeyboardButton
     * \param msg
     */
    void TreatKeyboardButton(const keyboard::KeyboardKeys::ConstPtr& msg);
    /*!
     * \brief TreatGamepadList
     * \param msg
     */
    void TreatGamepadList(const linux_devs::DevicesList::ConstPtr& msg);

    ///
    double holdingWaitTime;

    static void HoldingThread(InputMapperNodelet* mapper);

protected:
    ///
    boost::mutex updatemutex;

    /// This thread is used to toggle the button state from pressed to holding.
    boost::thread* holdingThread;

public:
    ///
    ros::NodeHandle nodehandle;

    ///
    bool runHoldingThread;

    /*!
     * \brief InputMapperNodelet
     */
    InputMapperNodelet();

    /*!
     * \brief onInit
     */
    void onInit();

    /*!
     * \brief ConnectWithGamepad
     * \param gamepadName
     */
    virtual void ConnectWithGamepad(std::string gamepadName);
    /*!
     * \brief DisconnectFromGamepad
     * \param gamepadName
     */
    virtual void DisconnectFromGamepad(std::string gamepadName);

    /*!
     * \brief ~InputMapperNodelet
     */
    virtual ~InputMapperNodelet();
};



}



#endif
