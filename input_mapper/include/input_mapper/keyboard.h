/*!
 * \file keyboard.h
 */



#ifndef INPUT_MAPPER_KEYBOARD_H
#define INPUT_MAPPER_KEYBOARD_H



#include <input_mapper/input_buttons.h>



namespace input_mapper {



/*!
 * \class Keyboard
 * \brief The Keyboard class maps and treats the Keyboard gamepad inputs.
 */
class Keyboard: public InputButtons
{
public:
    /// Keyboard buttons names.
    static const std::deque<std::string> names;
    /// Keyboard buttons ids.
    static const std::deque<int> ids;
    /// Deque with the keyboard axis names.
    static const std::deque<std::string> empty_name;
    /// Deque with the keyboard axis ids.
    static const std::deque<int> empty_id;
    /// Deque that treats the keyboard buttons as axis.
    static const std::deque<InputButtons::TupleMap> empty_map;

    /// Indicates the start of the numpad buttons ids.
    static const unsigned int NUMPAD_KEYCODE_FIRST_DISTANCE = 86;

    /*!
     * \brief GetButtonsNames returns the Keyboard buttons names.
     * \return Returns the buttons names.
     */
    const std::deque<std::string>& GetButtonsNames() const;
    /*!
     * \brief GetButtonsIds returns the Keyboard buttons ids.
     * \return Returns the buttons ids.
     */
    const std::deque<int>& GetButtonsIds() const;
    /*!
     * \brief GetAxesNames returns the Keyboard axis names.
     * \return Returns the axis names.
     */
    const std::deque<std::string>& GetAxesNames() const;
    /*!
     * \brief GetAxesIds reutrns the Keyboard axis ids.
     * \return Returns the axis ids.
     */
    const std::deque<int>& GetAxesIds() const;
    /*!
     * \brief GetAxisToButtonMap returns the buttons as an axis.
     * \return Returns a deque that treats the buttons as an axis.
     */
    const std::deque<InputButtons::TupleMap>& GetAxisToButtonMap() const;
};



}



#endif
