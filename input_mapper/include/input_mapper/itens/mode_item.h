/*!
 * \file mode_item.h
 */



#ifndef INPUT_MAPPER_MODE_ITEM_H
#define INPUT_MAPPER_MODE_ITEM_H



#include <input_mapper/control_button.h>
#include <input_mapper/control_mode.h>
#include <input_mapper/input_mapper.h>
#include <input_mapper/input_buttons.h>
#include <input_mapper/itens/control_value_item.h>



namespace input_mapper {



/*!
 * \class ModeItem
 * \brief The ModeItem class
 */
class ModeItem : public QObject
{
Q_OBJECT

private:
    bool configured;
    InputMapper* component;

    std::deque<ControlValueItem*> items_values;
    std::deque<QWidget*> widgets;

public:
    const std::string name;

    ControlMode* mode;

    QLabel* modenamelabel;
    QLabel* togglelabel;
    QLabel* keysequencelabel;
    QLabel* joysequencelabel;
    QLabel* keysequencestate;
    QLabel* joysequencestate;
    QLineEdit* keysequenceledit;
    QLineEdit* joysequenceledit;
    QCheckBox* tooglecheckbox;

    std::string joysequence;
    std::string keysequence;

    ModeItem(ControlMode* mode, InputMapper* component);

    unsigned int ConfigureGridLayout(QGridLayout* layout, unsigned int from_row, unsigned int from_col);

    bool UpdateToggleState();
    void SetToggleLabel(bool flag);

    std::string GetSequence(bool is_keyboard_sequence = false);
    void SetSequenceOk(bool flag, bool is_keyboard_sequence = false);

    void SetActualSequences(const std::string & sequence, bool is_keyboard_sequence = false);

    /**
     * \brief Updates this mode and its children, values.
     * \return True if all children were successfuly updated. False otherwise.
     */
    bool Update();

    ~ModeItem();

public slots:
    void SetShown(bool flag);
};



}



#endif
