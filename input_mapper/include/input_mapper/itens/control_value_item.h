/*!
 * \file control_value_item.h
 */



#ifndef INPUT_MAPPER_CONTROLVALUEITEM_H
#define INPUT_MAPPER_CONTROLVALUEITEM_H



#include <device/item.h>

#include <input_mapper/control_value.h>
#include <input_mapper/input_mapper.h>
#include <input_mapper/input_capture_line_edit.h>

#include <QLabel>
#include <QObject>
#include <QLineEdit>
#include <QCheckBox>
#include <QGridLayout>



namespace input_mapper {



/*!
 * \class ControlValueItem
 * \brief The ControlValueItem class
 */
class ControlValueItem : public QObject, public device::Item
{
Q_OBJECT

private:
    static const char *STYLE_ERROR, *STYLE_REGULAR;

    QLabel* lb_input;
    QLabel* lb_value_current;
    QLabel* lb_positive;
    InputCaptureLineEdit* le_value;
    QCheckBox* cb_positive;

    std::deque<QWidget*> widgets;

    ControlValue* control_value;
    InputMapper* component;

    bool configured;

public slots:
    void UpdateInputTypeSlot();

public:
    ControlValueItem(ControlValue* control_value, InputMapper* component);

    unsigned int ConfigureGridLayout(QGridLayout* layout, unsigned int from_row, unsigned int from_col);

    void UpdateSign();
    void SetShown(bool flag);

    /*!
     * \brief Updates this value.
     * \return True if the value was successfuly updated. False otherwise.
     */
    bool Update();

    void UpdateItemFromVariable(bool);
    bool UpdateVariableFromItem();

    ~ControlValueItem();

signals:
    void UpdateInputTypeSignal();
};



}



#endif
