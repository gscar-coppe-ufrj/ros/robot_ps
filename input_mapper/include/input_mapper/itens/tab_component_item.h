/*!
 * \file tab_component_item.h
 */



#ifndef INPUT_MAPPER_TAB_COMPONENT_ITEM_H
#define INPUT_MAPPER_TAB_COMPONENT_ITEM_H



#include <input_mapper/input_mapper.h>
#include <input_mapper/itens/group_item.h>

#include <QComboBox>
#include <QScrollArea>
#include <QPushButton>
#include <QListWidget>



namespace input_mapper {



/*!
 * \class TabComponentItem
 * \brief The TabComponentItem class
 */
class TabComponentItem: public QScrollArea
{
Q_OBJECT

private:
    QWidget* widget_main;
    QComboBox* cb_joy;
    QPushButton* bt_help;
    QVBoxLayout* lout_main;
    QGroupBox* gbox_options;
    QVBoxLayout* lout_options;

    void AddJoysticks(const std::deque<std::string> & joys);

public:
    const std::string name;
    InputMapper* component;

    std::deque<GroupItem*> groupitems;


    QWidget* widget_example;

    TabComponentItem(InputMapper* gamepadandkeyboardcomponent);

    void AddGroupItem(ControlGroup* group);

    void ConfigureHelpWindow();
    void ChangeJoystickNamesList();

    ~TabComponentItem();

signals:
    void UpdateJoyNames();

public slots:
    void UpdateButtonPressed();
    void ShowExample();
    void HandleUpdateJoyNames();
    void ConnectJoystick(QString);
};



}



#endif
