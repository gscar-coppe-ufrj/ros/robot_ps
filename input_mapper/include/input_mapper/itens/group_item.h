/*!
 * \file group_item.h
 */



#ifndef INPUT_MAPPER_GROUP_ITEM_H
#define INPUT_MAPPER_GROUP_ITEM_H



#include <input_mapper/itens/mode_item.h>
#include <input_mapper/input_mapper.h>
#include <input_mapper/control_group.h>

#include <QGroupBox>



namespace input_mapper {



/*!
 * \class GroupItem
 * \brief The GroupItem class
 */
class GroupItem : public QObject
{
Q_OBJECT

private:
    ControlGroup* group;
    bool configured;
    InputMapper* component;

public:
    const std::string name;

    std::deque<ModeItem*> modeitems;
    QGroupBox* groupbox;
    QGridLayout* gridlayout;

    GroupItem(ControlGroup* group, InputMapper* component);

    void ConfigureItem();

    /**
     * \brief Updates this group and its children, i.e. modes and values.
     * \return True if all children were successfuly updated. False otherwise.
     */
    bool Update();

    ~GroupItem();

public slots:
    void SetShown(bool flag);
};



}



#endif
