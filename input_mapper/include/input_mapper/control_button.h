/*!
 * \file control_button.h
 */



#ifndef INPUT_MAPPER_CONTROL_BUTTON_H
#define INPUT_MAPPER_CONTROL_BUTTON_H



#include <input_mapper/input_buttons.h>



namespace input_mapper {



/*!
 * \class ControlButton
 * \brief The ControlButton class initializes a button with a specific or not id and state.
 */
class ControlButton
{
public:
    /// Button id.
    unsigned int button_id;
    /// Actual button state.
    InputButtons::ButtonState button_state;

    /*!
     * \brief ControlButton initializes a button without state and id.
     */
    ControlButton();
    /*!
     * \brief ControlButton initializes a button with a specific id and state.
     * \param button_id Button id.
     * \param button_state Button state.
     */
    ControlButton(unsigned int button_id, InputButtons::ButtonState button_state);
    /*!
     * \brief ControlButton initializes a button with a specific id and sets
     *      the button state to the string passed, if it's valid. If the string
     *      isn't valid the button state is set to InputButtons::StateNone.
     * \param button_id Button id.
     * \param state State string to be checked.
     */
    ControlButton(unsigned int button_id, const std::string& state);

    /*!
     * \brief SetButtonStateFromString sets the button state to the string
     *      value if the string passed is valid. If the string is not valid
     *      the button state is set to InputButtons::StateNone.
     * \param state State string to be checked.
     * \return Returns true if the state string is valid.
     */
    bool SetButtonStateFromString(const std::string& state);
};



}



#endif
