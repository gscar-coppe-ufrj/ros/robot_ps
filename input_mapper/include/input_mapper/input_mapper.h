/*!
 * \file input_mapper.h
 */



#ifndef INPUT_MAPPER_INPUT_MAPPER_H
#define INPUT_MAPPER_INPUT_MAPPER_H



#include <robot_ps_core/component.h>

#include <device/variables/variable_template.h>

#include "input_viewer.h"
#include <input_mapper/input_config.h>
#include <input_mapper/input_buttons.h>
#include <input_mapper/control_group.h>

#include <boost/thread.hpp>



namespace input_mapper {



class InputConfig;
class InputViewer;



/*!
 * \class InputMapper
 * \brief The InputMapper class
 */
class InputMapper: public robot_ps::Component
{
protected:
    /*!
     * \brief UpdateGamepadFlags
     */
    void UpdateGamepadFlags();
    /*!
     * \brief UpdateKeyboardFlags
     */
    void UpdateKeyboardFlags();

    ///
    InputButtons* keyboard;
    ///
    InputButtons* gamepad;
    ///
    std::deque<InputConfig*> tools;

    /*!
     * \brief ReadModeSequenceXml
     */
    void ReadModeSequenceXml();
    /*!
     * \brief WriteModeSequenceXml
     */
    void WriteModeSequenceXml();

    /*!
     * \brief SetKeyboard
     * \param keyboard
     */
    void SetKeyboard(InputButtons* keyboard);
    /*!
     * \brief SetGamepad
     * \param gamepad
     */
    void SetGamepad(InputButtons* gamepad);

    /*!
     * \brief UpdateGamepadButtonStates
     */
    virtual void UpdateGamepadButtonStates();
    /*!
     * \brief UpdateKeyboardButtonStates
     */
    virtual void UpdateKeyboardButtonStates();

    virtual void Update(InputButtons::InputType input_type) = 0;
    /*!
     * \brief SetActualMode
     * \param group
     * \param mode
     * \param sequence
     * \param input_type
     * \return
     */
    virtual bool SetActualMode(ControlGroup& group, ControlMode& mode, std::vector<ControlButton>& sequence, InputButtons::InputType input_type);
    /*!
     * \brief ValidateSequence
     * \param sequence
     * \return
     */
    bool ValidateSequence(std::vector<ControlButton>& sequence, InputButtons::InputType input_type);

    /*!
     * \brief TreatAddMapper
     * \param toolInputConfig
     */
    virtual void TreatAddConfig(InputConfig* toolInputConfig) {};

    /*!
     * \brief TreatRemoveMapper
     * \param toolInputConfig
     */
    virtual void TreatRemoveConfig(InputConfig* toolInputConfig) {};

    /*!
     * \brief TreatAddViewer
     * \param toolInputViewer
     */
    virtual void TreatAddViewer(InputViewer* toolInputViewer) {};

    /*!
     * \brief TreatRemoveViewer
     * \param toolInputViewer
     */
    virtual void TreatRemoveViewer(InputViewer* toolInputViewer) {};

public:
    /** \brief Input type (keyboard button, gamepad button or gamepad axis) int (id) */
    device::VariableTemplate<ControlValue::ControlValueInput, int>* variable_last_command;

    ///
    std::string xmlSequenceFileName;

    std::string componentName;

    ///
    std::deque<InputButtons::ButtonState> buttonStates;
    ///
    std::deque<bool> actualButtonValues;
    ///
    std::deque<float> axisValues;
    ///
    std::deque<int> lastPressedKey;

    ///
    std::deque<std::string> connectedGamepadsNames;
    ///
    std::deque<std::string> gamepadsNamesList;

    ///
    std::vector<ControlGroup*> groups;

    ControlMode gamepadActivationMode;
    ControlMode keyboardActivationMode;

    /*!
     * \brief InputMapper
     */
    InputMapper();

    /*!
     * \brief GetKeyboard
     * \return
     */
    InputButtons* GetKeyboard() const;
    /*!
     * \brief GetGamepad
     * \return
     */
    InputButtons* GetGamepad() const;

    /*!
     * \brief CreateSequence
     * \param sequence
     * \param gamepad_keyboard
     * \param mode
     * \param is_keyboard_sequence
     * \return
     */
    bool CreateSequence(const std::string& sequence, InputButtons* gamepad_keyboard, ControlMode* mode, bool is_keyboard_sequence = false);

    /*!
     * \brief SetToolGamepadListNames
     */
    void SetToolGamepadListNames();

    /*!
     * \brief ConnectWithGamepad
     * \param gamepadName
     */
    virtual void ConnectWithGamepad(std::string gamepadName) = 0;
    /*!
     * \brief DisconnectFromGamepad
     * \param gamepadName
     */
    virtual void DisconnectFromGamepad(std::string gamepadName) = 0;

    /*!
     * \brief FindTools
     */
    virtual void FindTools()
    {
        AddMeToTools<InputMapper, InputConfig>(&InputMapper::TreatAddConfig, &InputMapper::TreatRemoveConfig);
        AddMeToTools<InputMapper, InputViewer>(&InputMapper::TreatAddViewer, &InputMapper::TreatRemoveViewer);
    }

    /*!
     * \brief ~InputMapper
     */
    virtual ~InputMapper();
};



}



#endif
