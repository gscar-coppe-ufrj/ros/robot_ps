/*!
 * \file input_buttons.h
 */



#ifndef INPUT_MAPPER_INPUT_BUTTONS_H
#define INPUT_MAPPER_INPUT_BUTTONS_H



#include <deque>
#include <string>
#include <algorithm>
#include <boost/tuple/tuple.hpp>



namespace input_mapper {



/*!
 * \class InputButtons
 * \brief The InputButtons class returns the inputs of the buttons searched.
 */
class InputButtons
{
    /// Flag to indicates if a search was sucessful or not.
    bool ok;

     /*!
     * \brief GetValueFromOther searchs and returns a equivalent value of one deque from
     *      another deque.
     * \param deque1 Deque of the returned value.
     * \param deque2 Deque from within the search is executed.
     * \param value_default Default value in case the search fails.
     * \param value Value to be searched in the second deque.
     * \return Returns the value searched or deault in case of fail.
     */
    template <typename T1, typename T2>
    T1 GetValueFromOther(const std::deque<T1>& deque1, const std::deque<T2>& deque2, T1 value_default, T2 value)
    {
        auto iter = std::find(deque2.begin(), deque2.end(), value);
        ok = iter != deque2.end();
        if (!ok)
            return value_default;
        return deque1[std::distance(deque2.begin(), iter)];
    }

public:
    /*! \brief <id_axis, id_button, true_if_positive, deadzone> */
    typedef boost::tuple<int, int, bool, double> TupleMap;

    enum ButtonState
    {
        StateReleased = 0,
        StatePressed,
        StateHolding,
        StateNotHolding,
        StateNone
    };

    enum InputType
    {
        TypeKeyboard = 0,
        TypeGamepad,
        TypeNone
    };

    /*!
     * \brief InputButtons sets flag to true.
     */
    InputButtons();

    /*!
     * \brief GetAxisScale returns the axis scale.
     *
     * The axis scale is such that the maximum value corresponds to 1, i.e. axis_scale = 1 / maximum_value.
     *
     * \return the axis scale.
     */
    virtual const double GetAxisScale() const;

    /*!
     * \brief GetButtonsNames returns a deque with the input buttons names.
     * \return Deque with the buttons names.
     */
    virtual const std::deque<std::string>& GetButtonsNames() const = 0;
    /*!
     * \brief GetButtonsIds returns a deque with the input buttons ids.
     * \return Deque with the buttons ids.
     */
    virtual const std::deque<int>& GetButtonsIds() const = 0;
    /*!
     * \brief GetAxesNames returns a deque with the input axis names.
     * \return Deque with the axis names.
     */
    virtual const std::deque<std::string>& GetAxesNames() const = 0;
    /*!
     * \brief GetAxesIds returns a deque with the input acis ids.
     * \return Deque with the axis ids.
     */
    virtual const std::deque<int>& GetAxesIds() const = 0;
    /*!
     * \brief GetAxisToButtonMap maps the axis as buttons.
     * \return Returns the button map.
     */
    virtual const std::deque<TupleMap>& GetAxisToButtonMap() const = 0;

    /*!
     * \brief GetButtonNameFromId returns the correspondent name from a
     *      specific id.
     * \param id Id of the name being searched.
     * \return Returns the name if the id was found, blank string otherwise.
     */
     std::string GetButtonNameFromId(int id);

    /*!
     * \brief GetButtonIdFromName returns the correspondent id from a
     *      specific name.
     * \param name Name of the id being searched.
     * \return Returns the id if the name was found, -1 otherwise.
     */
    int GetButtonIdFromName(std::string name);

    /*!
     * \brief GetAxisNameFromId returns the correspondent name form a
     *      specific id.
     * \param id Id of the name being searched.
     * \return Returns the name if the id was found, blank stirng otherwise.
     */
    std::string GetAxisNameFromId(int id);

    /*!
     * \brief GetAxisIdFromName returns the correpondent id from a
     *      specific name.
     * \param name  Name of the id being searched.
     * \return Returns the id if the name was found, -1 otherwise.
     */
    int GetAxisIdFromName(std::string name);

    /*!
     * \brief GetFirstButton gets the id and name of the first button.
     * \param id Id of the first button.
     * \param name Name of the first button.
     */
    void GetFirstButton(int& id, std::string& name);

    /*!
     * \brief GetLastButton gets the id and name of the last button.
     * \param id Id of the last button.
     * \param name Name of the last button.
     */
    void GetLastButton(int& id, std::string& name);

    /*!
     * \brief GetFirstAxis gets the id and name of the first axis.
     * \param id Id of the first axis.
     * \param name Name of the first axis.
     */
    void GetFirstAxis(int& id, std::string& name);

    /*!
     * \brief GetLastAxis gets the id and name of the last axis.
     * \param id Id of the last axis.
     * \param name Name of the last axis.
     */
    void GetLastAxis(int& id, std::string& name);

    /*!
     * \brief GetButton gets the id and name of the first or last button.
     * \param index Zero for the first button, -1 for the last button.
     * \param id Id of the first or last button.
     * \param name Name of the first or last button.
     */
    void GetButton(int index, int& id, std::string& name);

    /*!
     * \brief GetAxis gets the id and name of the first or last button.
     * \param index Zero for the first button, -1 for the last button.
     * \param id Id of the first or last button.
     * \param name Name of the first or last button.
     */
    void GetAxis(int index, int& id, std::string& name);

    /*!
     * \brief HasButton checks if the button exist by it's id.
     * \param id Id of the button being searched.
     * \return Returns true if the button exist, false otherwise.
     */
    bool HasButton(int id);

    /*!
     * \brief HasAxis checks if the axis exist by it's id.
     * \param id Id of the axis being searched.
     * \return Returns true if the axis exist, false otherwise.
     */
    bool HasAxis(int id);

    /*!
     * \brief IsOk returns true or false informing if the last call for a
     *       method of this class was successful.
     * \return Returns true or false.
     */
    bool IsOk();

    /*!
     * \brief GetButtonStateName returns the string of the state of the button.
     * \param state State of the button.
     * \return Returns the string of the state, blank in case the state is none
     *       or invalid.
     */
    static std::string GetButtonStateName(ButtonState state);

    /*!
     * \brief GetButtonStateFromName returns the state of the button.
     * \param state String of the state of the button.
     * \return Returns the state of the button, StateNone in case the string
     *      is invalid.
     */
    static ButtonState GetButtonStateFromName(const std::string& state);

    /*!
     * \brief SizeButtons returns the number of buttons.
     * \return Returns the number of buttons.
     */
    unsigned int SizeButtons();

    /*!
     * \brief SizeAxes returns the number of axes.
     * \return Returns the number of axes.
     */
    unsigned int SizeAxes();
};



}



#endif
