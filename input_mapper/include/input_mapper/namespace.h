/*!
 * \file namespace.h
 */



#ifndef INPUT_MAPPER_NAMESPACE_H
#define INPUT_MAPPER_NAMESPACE_H



/*!
 * \namespace input_mapper
 * \brief This namespace contains classes responsible for mapping the inputs of
 *      diferents joysticks.
 */
namespace input_mapper {
}



#endif
