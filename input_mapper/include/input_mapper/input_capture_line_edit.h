/*!
 * \file input_capture_line_edit.h
 */



#ifndef INPUT_MAPPER_INPUT_CAPTURE_LINE_EDIT_H
#define INPUT_MAPPER_INPUT_CAPTURE_LINE_EDIT_H



#include <input_mapper/input_mapper.h>
#include <input_mapper/input_buttons.h>
#include <device/item.h>
#include <device/variables/variable_template.h>

#include <QLineEdit>
#include <QString>



namespace input_mapper {



/*!
 * \class InputCaptureLineEdit
 * \brief The InputCaptureLineEdit class
 */
class InputCaptureLineEdit : public QLineEdit, public device::Item
{
Q_OBJECT

private:
    InputMapper* component;
    bool capturing;
    device::VariableTemplate<ControlValue::ControlValueInput, int>* variable;
    ControlValue::ControlValueInput input_type;

    void DarkenText();
    void BrightenText();

private slots:
    void UpdateItemSlot();

protected:
    virtual void focusInEvent(QFocusEvent*);
    virtual void focusOutEvent(QFocusEvent*);
    virtual void keyPressEvent(QKeyEvent*);

public:
    explicit InputCaptureLineEdit(InputMapper* component, QWidget* parent = 0);
    InputCaptureLineEdit(InputMapper* component, const QString& text, QWidget* parent = 0);

    bool SetValueFromButton(int id, bool using_keyboard);
    bool SetValueFromAxis(int id, bool using_keyboard);

    void UpdateItemFromVariable(bool);
    bool UpdateVariableFromItem();

    inline void SetInputType(ControlValue::ControlValueInput type)
    { input_type = type; }

    inline ControlValue::ControlValueInput GetCurrentInputType()
    { return input_type; }

    virtual ~InputCaptureLineEdit();

signals:
    void UpdateItemSignal();
};



}



#endif
