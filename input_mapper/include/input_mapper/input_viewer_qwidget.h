// input_viewer_qwidget.h



#ifndef INPUT_MAPPER_INPUT_VIEWER_QWIDGET_H
#define INPUT_MAPPER_INPUT_VIEWER_QWIDGET_H



#include <QLabel>
#include <QVBoxLayout>
#include <QTabWidget>
#include <QScrollArea>
#include "input_viewer.h"



namespace input_mapper {



class InputViewerQWidget : public QWidget, public InputViewer
{
    Q_OBJECT

    int sizeHintW;
    int sizeHintH;

    QVBoxLayout* vLayout;
    QTabWidget* tabWidget;

    class ControlGroupWidget : public QWidget
    {
    public:
        QLabel* group;
        std::deque<QLabel*> modes;

        ControlGroupWidget(ControlGroup* controlGroup);
    };

    class Tab
    {
    public:
        bool gamepadActive;
        bool keyboardActive;
        QLabel* keyboard;
        QLabel* gamepad;

        QScrollArea* scrollArea;
        std::deque<ControlGroupWidget*> groups;

        Tab();
    };

    std::deque<Tab*> tabs;

public:
    InputViewerQWidget();

    virtual QSize sizeHint() const;

    void Init();

    //void FindComponents();
    virtual void TreatAddInputMapper(InputMapper* component);
    virtual void TreatRemoveInputMapper(InputMapper* component);

    void UpdateGamepadActive(InputMapper* component, bool active);
    void UpdateKeyboardActive(InputMapper* component, bool active);
    void UpdateMode(InputMapper* component, unsigned int groupNumber, ControlGroup* controlGroup);

    virtual ~InputViewerQWidget();

signals:
    void UpdateInputActiveSignal(unsigned int tabNumber);
    void UpdateModeSignal(InputMapper* component, unsigned int groupNumber, ControlGroup* controlGroup);
    void AddTabSignal(InputMapper* component);
    void RemoveTabSignal(InputMapper* component);
public slots:
    void UpdateInputActiveSlot(unsigned int tabNumber);
    void UpdateModeSlot(InputMapper* component, unsigned int groupNumber, ControlGroup* controlGroup);
    void AddTab(InputMapper* component);
    void RemoveTab(InputMapper* component);
};



}



#endif
