/*!
 * \file control_mode.h
 */



#ifndef INPUT_MAPPER_CONTROL_MODE_H
#define INPUT_MAPPER_CONTROL_MODE_H

#include <vector>

#include <input_mapper/control_button.h>
#include <input_mapper/control_value.h>



namespace input_mapper {



/*!
 * \class ControlMode
 * \brief The ControlMode class sets the mode of the button and it's values.
 */
class ControlMode
{
    /// True if this mode is currently active, false otherwise.
    bool active;

public:
    /// Mode' id.
    unsigned int id;
    /// Mode's name.
    std::string name;

    /// Indicates if a mode is toogle or not.
    bool isToggle;

    /// Vector with the buttons of a sequence from a gamepad.
    std::vector<ControlButton> gamepadSequence;
    /// Vector with the buttons of a sequence from a keyboard.
    std::vector<ControlButton> keyboardSequence;
    /// String containing the gamepad button sequence.
    std::string gamepadStringSequence;
    /// String containing the keyboard button sequence.
    std::string keyboardStringSequence;
    /// Deque with the mode's values.
    std::deque<ControlValue*> values;
    /// States if the mode was released or not.
    bool releasedMode;
    /// States that the mode should be released.
    bool release;
    
    /// True if this mode can toggle, false otherwise. This doesn't set the mode as toggleable.
    bool canToggle;

    /*!
     * \brief ControlMode sets the mode to an invalid one.
     */
    ControlMode();
    /*!
     * \brief ControlMode sets the mode and it's values.
     * \param id Mode' id.
     * \param name Mode's name.
     * \param isToogle Sets the mode to toogle or not.
     * \param numberValues Sets the values of the mode.
     */
    ControlMode(unsigned int id, std::string name, bool isToggle = false, unsigned char numberValues = 0);

    /*!
     * \brief isActive
     * \return True if this mode is active, false otherwise. 
     */
    bool isActive();

    /*!
     * \brief SetModeActive sets this mode to active or inactive.
     *      This function will set the mode as active or inactive based on the
     *      current state of the mode and if its sequence is active or not.
     *
     * \param sequenceActive True if the sequence for this mode is active. False
     *          otherwise.
     *
     * \return True if the mode is active, false otherwise. 
     */ 
    bool SetModeActive(bool sequenceActive);
        

    /*!
     * \brief ~ControlMode clears the mode's values.
     */
    ~ControlMode();
};



}



#endif
