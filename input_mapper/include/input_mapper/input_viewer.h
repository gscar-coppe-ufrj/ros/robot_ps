// input_viewer.h



#ifndef INPUT_MAPPER_INPUT_VIEWER_H
#define INPUT_MAPPER_INPUT_VIEWER_H



#include <robot_ps_core/tool.h>
#include "control_group.h"
#include "input_mapper.h"



namespace input_mapper {



class InputMapper;



class InputViewer : public robot_ps::Tool
{
protected:
    virtual void TreatAddInputMapper(InputMapper* c){}
    virtual void TreatRemoveInputMapper(InputMapper* c){}

public:
    InputViewer(){}

    virtual void FindComponents()
    {
        AddMeToComponents<InputViewer, InputMapper>(&InputViewer::TreatAddInputMapper, &InputViewer::TreatRemoveInputMapper);
    }

    virtual void UpdateGamepadActive(InputMapper* component, bool active){}
    virtual void UpdateKeyboardActive(InputMapper* component, bool active){}
    virtual void UpdateMode(InputMapper* component, unsigned int groupNumber, ControlGroup* controlGroup){}

    virtual ~InputViewer(){}
};



}



#endif
