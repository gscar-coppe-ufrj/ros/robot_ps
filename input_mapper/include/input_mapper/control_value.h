/*!
 * \file control_value.h
 */



#ifndef INPUT_MAPPER_CONTROL_VALUE_H
#define INPUT_MAPPER_CONTROL_VALUE_H



#include <input_mapper/input_buttons.h>



namespace input_mapper {



/*!
 * \class ControlValue
 * \brief The ControlValue class manipulates the information about gamepad and keyboard's buttons and axis.
 */
class ControlValue
{
    /// Gamepad object.
    InputButtons* gamepad;
    /// Keyboard object.
    InputButtons* keyboard;
    /// Flag that indicates if the keyboard or gamepad has the button or axis.
    bool valid;
    /// Id of the button or axis.
    int associatedButtonOrAxis;

public:
    enum ControlValueInput
    {
        None = 0,
        GamepadAxis,
        GamepadButton,
        KeyboardButton
    };

    /// Invalid input.
    static const std::string ControlValueInput_None;
    /// Gamepad' axis input type.
    static const std::string ControlValueInput_GamepadAxis;
    /// Gamepad's button input type.
    static const std::string ControlValueInput_GamepadButton;
    /// Keyboard's button input type.
    static const std::string ControlValueInput_KeyboardButton;

    /// Input type.
    ControlValueInput inputType;
    /// Flag for positive or negative symbol.
    bool positive;

    /*!
     * \brief ControlValue sets default values.
     */
    ControlValue();

    /*!
     * \brief GetInputTypeString returns the string of the input type.
     * \param input_type Type of the input.
     * \return String that contains the type of the input.
     */
    static std::string GetInputTypeString(ControlValueInput input_type);

    /*!
     * \brief GetInputTypeString returns the string of the actual input type.
     * \return String that conatins the type of the actual input.
     */
    std::string GetInputTypeString();

    /*!
     * \brief SetInputTypeFromString sets the input to the new type.
     * \param type New input type.
     */
    void SetInputTypeFromString(const std::string& type);

    /*!
     * \brief SetGamepadAndKeyboard sets the gamepad and keyboard.
     * \param gamepad Pointer to a gamepad object.
     * \param keyboard Pointer to a keyboard object.
     */
    void SetGamepadAndKeyboard(InputButtons* gamepad, InputButtons* keyboard);

    /*!
     * \brief GetNameFromId returns the name of the input associated with a specific type and id.
     * \param input_type The input type, e.g. GamepadAxis, GamepadButton and KeyboardButton.
     * \param id Id of the input.
     * \return Returns the name of the input.
     */
    std::string GetNameFromId(ControlValueInput input_type, int id);

    /*!
     * \brief GetNameFromId returns the name of the input from the variable
     *      ControlValue::associatedButtonOrAxis.
     * \return Returns the name of the input.
     */
    std::string GetNameFromId();

    /*!
     * \brief SetIdFromName sets the id of the button or axis from the name.
     * \param name Name of the button or axis to be setted.
     * \return Returns true if sucessful, false otherwise.
     */
    bool SetIdFromName(const std::string& name);

    /*!
     * \brief SetId sets the id of the button or axis.
     * \param id Id to be setted.
     * \return Returns true if sucessful, false otherwise.
     */
    bool SetId(int id);

    /*!
     * \brief GetId gets the id of the button or axis.
     * \return Returns the id.
     */
    int GetId();

    /*!
     * \brief Ok returns true if the axis or button exist.
     * \return Returns the value of the variable ControlValue::positive.
     */
    bool Ok();

    /*!
     * \brief ~ControlValue does nothing.
     */
    ~ControlValue();
};



}



#endif
