/*!
 * \file xbox_gamepad.h
 */



#ifndef INPUT_MAPPER_XBOX_GAMEPAD_H
#define INPUT_MAPPER_XBOX_GAMEPAD_H



#include <input_mapper/input_buttons.h>



namespace input_mapper {


/*!
 * \class XboxGamepad
 * \brief The XboxGamepad class controls and treats the xboxgamepad inputs.
 */
class XboxGamepad: public InputButtons
{
    /// Xbox buttons names.
    static const std::deque<std::string> names_buttons;
    /// Xbox buttons ids.
    static const std::deque<int> ids_buttons;
    /// Xbox axis names.
    static const std::deque<std::string> names_axes;
    /// Xbox axis ids.
    static const std::deque<int> ids_axes;
    /// Xbox axis as buttons.
    static const std::deque<InputButtons::TupleMap> map_axis_button;

public:
    /*!
     * \brief GetAxisScale returns the Xbox axis scale.
     * \return the axis scale.
     */
    const double GetAxisScale() const;
    /*!
     * \brief GetButtonsNames returns the Xbox buttons names.
     * \return Returns the buttons names.
     */
    const std::deque<std::string>& GetButtonsNames() const;
    /*!
     * \brief GetButtonsIds returns the Xbox buttons ids.
     * \return Returns the buttons ids.
     */
    const std::deque<int>& GetButtonsIds() const;
    /*!
     * \brief GetAxesNames reutrns the Xbox axis names.
     * \return Returns the axis names.
     */
    const std::deque<std::string>& GetAxesNames() const;
    /*!
     * \brief GetAxesIds returns the Xbox axis ids.
     * \return Returns the axis names.
     */
    const std::deque<int>& GetAxesIds() const;
    /*!
     * \brief GetAxisToButtonMap returns Xbox axis as buttons.
     * \return Returns the map that treats axis as buttons.
     */
    const std::deque<InputButtons::TupleMap>& GetAxisToButtonMap() const;
};



}



#endif
