/*!
 * \file input_config.h
 */



#ifndef INPUT_MAPPER_INPUT_CONFIG_H
#define INPUT_MAPPER_INPUT_CONFIG_H



//#include "../../generalpackagebase.h"
#include <input_mapper/input_mapper.h>
#include <robot_ps_core/tool.h>



namespace input_mapper {



class InputMapper;



/*!
 * \class InputConfig
 * \brief The InputConfig class
 */
class InputConfig: public robot_ps::Tool
{
protected:
    ///
    std::deque<InputMapper*> components;

    /*!
     * \brief TreatAddMapper
     * \param componentInputMapper
     */
    virtual void TreatAddMapper(InputMapper* componentInputMapper) {};

    /*!
     * \brief TreatRemoveMapper
     * \param componentInputMapper
     */
    virtual void TreatRemoveMapper(InputMapper* componentInputMapper) {};

public:
    /*!
     * \brief InputConfig
     */
    InputConfig();

    /*!
     * \brief SaveConfigInText
     */
    void SaveConfigInText();
    /*!
     * \brief LoadConfigInText
     */
    void LoadConfigInText();

    /*!
     * \brief FindComponents
     */
    virtual void FindComponents()
    {
        AddMeToComponents<InputConfig, InputMapper>(&InputConfig::TreatAddMapper, &InputConfig::TreatRemoveMapper);
    }

    /*!
     * \brief ChangeJoystickNamesList
     */
    virtual void ChangeJoystickNamesList() = 0;

    /*!
     * \brief ~InputConfig
     */
     virtual ~InputConfig();
};



}



#endif
