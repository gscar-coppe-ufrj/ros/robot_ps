/*!
 * \file input_config_qwidget.h
 */



#ifndef INPUT_MAPPER_INPUT_CONFIG_QWIDGET_H
#define INPUT_MAPPER_INPUT_CONFIG_QWIDGET_H



#include <input_mapper/input_config.h>
#include <input_mapper/itens/tab_component_item.h>

#include <QTabWidget>



namespace input_mapper {



/*!
 * \class InputConfigQWidget
 * \brief The InputConfigQWidget class
 */
class InputConfigQWidget: public QWidget, public InputConfig
{
    Q_OBJECT

    int sizehintw;
    int sizehinth;

    unsigned char numberofcomponents;
    QVBoxLayout* vlayout;
    QTabWidget* tabwidget;
    std::deque<TabComponentItem*> tabcomponentlist;

public:
    unsigned char currentcomponenttab;

    InputConfigQWidget();

    void Init();

//    void FindComponents();
    virtual void TreatAddMapper(InputMapper* component);
    virtual void TreatRemoveMapper(InputMapper* component);
//    void RemoveComponent(InputMapper* component);

    virtual QSize sizeHint() const;

    void ChangeJoystickNamesList();


    void ConfigureTabAllComponents();
    void ConfigureTabComponent(TabComponentItem* item, InputMapper* component);

    virtual ~InputConfigQWidget();

signals:
    void CreatingNewTabComponent(InputMapper* component);

public slots:
    bool CreateNewTabComponent(InputMapper* component);
};



}



#endif
