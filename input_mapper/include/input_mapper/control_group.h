/*!
 * \file control_group.h
 */



#ifndef INPUT_MAPPER_CONTROL_GROUP_H
#define INPUT_MAPPER_CONTROL_GROUP_H



#include <input_mapper/control_mode.h>
#include <vector>
#include <string>


namespace input_mapper {



#define CONTROL_GROUP_MODE_NONE_ID 0



/*!
 * \class ControlGroup
 * \brief The ControlGroup class configures the group of a button and it's modes.
 */
class ControlGroup
{
public:
    /// Group' id.
    unsigned int id;
    /// Group's name.
    std::string name;
    /// Vector that cotains the group's modes.
    std::vector<ControlMode*> modes;
    /// Actual group's mode.
    unsigned int actualMode;
    /// Last group's mode.
    unsigned int lastMode;
    /// Type of the input.
    InputButtons::InputType inputMode;
    /// Type of the last input.
    InputButtons::InputType lastInputMode;

    /*!
     * \brief ControlGroup sets the modes and input types to none.
     */
    ControlGroup();
    /*!
     * \brief ControlGroup sets the group' id and name to a specific value.
     * \param id Group' id passed.
     * \param name Group's name passed.
     */
    ControlGroup(unsigned int id, std::string name);
    /*!
     * \brief ~ControlGroup clears the group's modes vector.
     */
    ~ControlGroup();
};



}



#endif
