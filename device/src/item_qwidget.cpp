// item_qwidget.cpp



#include <device/item_qwidget.h>



device::ItemQWidget::ItemQWidget(Variable *variable) : Item(variable)
{
    QObject::connect(this, SIGNAL(SignalUpdateItemFromVariable(bool)), this, SLOT(SlotUpdateItemFromVariable(bool)));
}


void device::ItemQWidget::UpdateItemFromVariable(bool reset)
{
    emit(SignalUpdateItemFromVariable(reset));
}


void device::ItemQWidget::SlotUpdateItemFromVariable(bool reset)
{
    InternalUpdateItemFromVariable(reset);
}


device::ItemQWidget::~ItemQWidget()
{

}

