// tool_plugins.cpp



#include <pluginlib/class_list_macros.h>
#include <device/device_management/device_management_qwidget.h>
#include <device/device_management/device_on_off_management_qwidget.h>
#include <device/data_table/data_table_qwidget.h>
#include <device/plotter/plotter_qwidget.h>


PLUGINLIB_EXPORT_CLASS(device::DeviceManagementQWidget, robot_ps::Tool)
PLUGINLIB_EXPORT_CLASS(device::DeviceOnOffManagementQWidget, robot_ps::Tool)
PLUGINLIB_EXPORT_CLASS(device::DataTableQWidget, robot_ps::Tool)
PLUGINLIB_EXPORT_CLASS(device::PlotterQWidget, robot_ps::Tool)

