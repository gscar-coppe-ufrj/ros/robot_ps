// device_on_off.cpp



#include <device/device_on_off.h>



device::DeviceOnOff::DeviceOnOff()
{
    deviceOnOff = new VariableBool("On/Off", "", true);
    deviceOnOff->SetTrueString1("On");
    deviceOnOff->SetTrueString2("Turn On");
    deviceOnOff->SetFalseString1("Off");
    deviceOnOff->SetFalseString2("Turn Off");
    deviceOnOff->SetState(false);
    onOffVariables.push_back(deviceOnOff);
    variables.push_back(deviceOnOff);
}


void device::DeviceOnOff::TreatAddDeviceOnOffManagement(device::DeviceOnOffManagement *t)
{
}


void device::DeviceOnOff::TreatRemoveDeviceOnOffManagement(device::DeviceOnOffManagement *t)
{
}


void device::DeviceOnOff::FindTools()
{
    Device::FindTools();
    AddMeToTools<DeviceOnOff, DeviceOnOffManagement>(&DeviceOnOff::TreatAddDeviceOnOffManagement, &DeviceOnOff::TreatRemoveDeviceOnOffManagement);
}


device::DeviceOnOff::~DeviceOnOff()
{
}


