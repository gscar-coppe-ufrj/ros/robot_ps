// item.cpp



#include <device/item.h>



device::Item::Item(Variable* variable)
{
    this->variable = variable;
    variable->AddItem(this);
}


device::Item::~Item()
{
    if (variable != NULL)
    {
        variable->RemoveItem(this);
        variable = NULL;
    }
}
