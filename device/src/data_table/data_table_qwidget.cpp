// data_table_qwidget.cpp



#include <QPushButton>
#include <QGroupBox>
#include <QButtonGroup>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSignalMapper>
#include <QHeaderView>
#include <QDockWidget>
#include <QToolButton>
#include <QSizePolicy>
#include <QSplitter>
#include <device/data_table/data_table_qwidget.h>
#include <device/data_table/device_signal.h>



device::DataTableQWidget::DataTableQWidget()
{
    sizeHintW = 400;
    sizeHintH = 400;

    auto layout = new QVBoxLayout;
    this->setLayout(layout);

    generalHidden = false;

    FillConfigDialog();

    table = new QTableWidget;
    table->setEditTriggers( QAbstractItemView::NoEditTriggers );
    layout->addWidget(table);

    /*auto configButton = new QPushButton("Config");
    layout->addWidget(configButton);

    connect(configButton, SIGNAL(clicked(bool)), config, SLOT(show()));*/
}


/*QSize device::DataTableQWidget::MyGetQTableWidgetSize(QTableWidget *t)
{
   int w = t->verticalHeader()->width(); // +4 seems to be needed
   for (int i = 0; i < t->columnCount(); i++)
      w += t->columnWidth(i); // seems to include gridline (on my machine)
   int h = t->horizontalHeader()->height();
   for (int i = 0; i < t->rowCount(); i++)
      h += t->rowHeight(i);
   return QSize(w, h);
}


void device::DataTableQWidget::MyRedoGeometry(QWidget* w)
{
    const bool vis = w->isVisible();
    bool isHidden = w->isHidden();
    const QPoint pos = w->pos();
    emit w->hide();
    emit w->show();
    emit w->hide();
    w->setVisible(vis);
    if (vis && !pos.isNull())
        w->move(pos);

    if (isHidden)
        emit w->hide();
    else
        emit w->show();
}*/


std::deque<QRadioButton*> device::DataTableQWidget::NewRadioButtonGroup(int row, std::string name, QTableWidget* table)
{
    std::deque<QRadioButton*> rs;
    if (row >= 0)
    {
        table->insertRow(row);
        table->setVerticalHeaderItem(row, new QTableWidgetItem(QString::fromStdString(name)));

        auto buttonGroup = new QButtonGroup;
        for (int i = 0; i < 3; i++)
        {
            auto r = new QRadioButton;
            auto w = new QWidget;
            auto lR = new QHBoxLayout;
            lR->setAlignment(Qt::AlignCenter);
            w->setLayout(lR);
            lR->addWidget(r);
            lR->setSpacing(0);
            lR->setMargin(0);

            table->setCellWidget(row, i, w);
            buttonGroup->addButton(r);
            rs.push_back(r);
        }
    }
    rs[0]->setChecked(true);
    table->resizeColumnsToContents();
    return rs;
}


void device::DataTableQWidget::FillConfigDialog()
{
    config = new QDialog(this);
    config->setWindowTitle("Table Config");
    config->resize(700, 400);

    auto l = new QVBoxLayout;
    config->setLayout(l);

    auto groupGeneral = new QGroupBox("General");
    l->addWidget(groupGeneral);
    auto generalShow = new QRadioButton("Show");
    generalShow->setChecked(true);
    auto generalHide = new QRadioButton("Hide");
    auto lGeneral = new QVBoxLayout;
    lGeneral->addWidget(generalShow);
    lGeneral->addWidget(generalHide);
    groupGeneral->setLayout(lGeneral);

    connect(generalShow, SIGNAL(clicked(bool)), this, SLOT(GeneralShow(bool)));
    connect(generalHide, SIGNAL(clicked(bool)), this, SLOT(GeneralHide(bool)));

    auto lDevices = new QVBoxLayout;
    auto lVariables = new QVBoxLayout;
    auto splitter = new QSplitter;
    l->addWidget(splitter, 1);
    auto lDevicesWidget = new QWidget;
    lDevicesWidget->setLayout(lDevices);
    splitter->addWidget(lDevicesWidget);
    auto lVariablesWidget = new QWidget;
    lVariablesWidget->setLayout(lVariables);
    splitter->addWidget(lVariablesWidget);

    auto text = new QLabel("Devices");
    text->setFont(groupGeneral->font());
    lDevices->addWidget(text, 0);
    devicesTable = new QTableWidget(this);
    //lDevices->addWidget(devicesTable, 1, Qt::AlignTop);
    lDevices->addWidget(devicesTable);
    text = new QLabel("Variables");
    text->setFont(groupGeneral->font());
    lVariables->addWidget(text, 0);
    variablesTable = new QTableWidget(this);
    //lVariables->addWidget(variablesTable, 1, Qt::AlignTop);
    lVariables->addWidget(variablesTable);

    QStringList header;
    header.push_back("Normal");
    header.push_back("Show");
    header.push_back("Hide");
    devicesTable->setColumnCount(3);
    devicesTable->setHorizontalHeaderLabels(header);
    devicesTable->setSelectionMode(QAbstractItemView::NoSelection);
    //devicesTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    //devicesTable->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    variablesTable->setColumnCount(3);
    variablesTable->setHorizontalHeaderLabels(header);
    variablesTable->setSelectionMode(QAbstractItemView::NoSelection);
    //variablesTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    //variablesTable->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}


QSize device::DataTableQWidget::sizeHint() const
{
    return QSize(sizeHintW, sizeHintH);
}


void device::DataTableQWidget::TreatAddDevice(device::Device *c)
{
    DataTable::TreatAddDevice(c);

    auto list = GetListOfComponents<DataTable, Device>();
    auto found = std::find(list->begin(), list->end(), c);

    if (found != list->end())
    {
        auto deviceSignal = new DeviceSignal;
        connect(deviceSignal, SIGNAL(SendDevice(Device*)), this, SLOT(SlotAddDevice(Device*)), Qt::BlockingQueuedConnection);
        emit deviceSignal->SendDevice(c);
        delete deviceSignal;
    }
}


void device::DataTableQWidget::TreatRemoveDevice(device::Device *c)
{
    DataTable::TreatRemoveDevice(c);

    auto deviceSignal = new DeviceSignal;
    connect(deviceSignal, SIGNAL(SendDevice(Device*)), this, SLOT(SlotRemoveDevice(Device*)), Qt::BlockingQueuedConnection);
    emit deviceSignal->SendDevice(c);
    delete deviceSignal;
}


void device::DataTableQWidget::FindComponents()
{
    qRegisterMetaType<Qt::Orientation>("Qt::Orientation");
    qRegisterMetaType<QVector<int>>("QVector<int>");

    DataTable::FindComponents();

    auto list = GetListOfComponents<DataTable, Device>();

    std::deque<Device*>::iterator device = list->begin();

    while (device != list->end())
    {
        auto deviceSignal = new DeviceSignal;
        connect(deviceSignal, SIGNAL(SendDevice(Device*)), this, SLOT(SlotAddDevice(Device*)));
        emit deviceSignal->SendDevice(*device);
        delete deviceSignal;
        device++;
    }
}


void device::DataTableQWidget::Init()
{
    QDockWidget* dock;
    QWidget* parent = this;
    bool found = true;

    while (true)
    {
        parent = parent->parentWidget();
        if (parent == NULL)
        {
            found = false;
            break;
        }
        dock = dynamic_cast<QDockWidget*>(parent);
        if (dock != NULL)
            break;
    }

    if (found)
    {
        auto title = new QWidget;
        auto layout = new QHBoxLayout;
        layout->setMargin(0);
        layout->setContentsMargins(2, 0, 2, 0);
        layout->setSpacing(5);
        title->setLayout(layout);
        auto label = new QLabel(dock->windowTitle());
        label->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        label->setMinimumWidth(1);
        layout->addWidget(label, 1);

        QToolButton* configButton = new QToolButton;
        configButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/document-properties.svg"));
        layout->addWidget(configButton);

        connect(configButton, SIGNAL(clicked()), config, SLOT(show()));

        QToolButton* floatButton = new QToolButton;
        floatButton->setIcon(QIcon("/usr/share/icons/gnome/scalable/actions/window-maximize-symbolic.svg"));
        layout->addWidget(floatButton);

        connect(floatButton, SIGNAL(clicked()), dock, SLOT(_q_toggleTopLevel()));

        QToolButton* closeButton = new QToolButton;
        closeButton->setIcon(QIcon("/usr/share/icons/Humanity/actions/48/window-close.svg"));
        layout->addWidget(closeButton);

        connect(closeButton, SIGNAL(clicked()), dock, SLOT(close()));

        dock->setTitleBarWidget(title);
    }
}


void device::DataTableQWidget::SlotAddDevice(device::Device *d)
{
    bool columnFound = false;
    int columnOfDevice = 0;

    int columnIndex = 0;
    for (auto column = 0; column != table->columnCount(); column++)
    {
        if (table->horizontalHeaderItem(column)->text().toStdString() == d->name)
        {
            columnFound = true;
            columnOfDevice = column;
            break;
        }
        if ( !(table->horizontalHeaderItem(column)->text().toStdString() < d->name) )
            break;
        columnIndex++;
    }
    columnOfDevice = columnIndex;

    if (!columnFound)
    {
        table->insertColumn(columnOfDevice);
        table->setHorizontalHeaderItem(columnOfDevice, new QTableWidgetItem(QString::fromStdString(d->name)));

        auto rs = NewRadioButtonGroup(columnOfDevice, d->name, devicesTable);

        QSignalMapper* signalMapperNormal = new QSignalMapper(this);
        QObject::connect(rs[0], SIGNAL(clicked(bool)), signalMapperNormal, SLOT(map()));
        signalMapperNormal->setMapping(rs[0], d->name.c_str());
        QObject::connect(signalMapperNormal, SIGNAL(mapped(const QString &)), this, SLOT(DeviceNormal(const QString &)));

        QSignalMapper* signalMapperShow = new QSignalMapper(this);
        QObject::connect(rs[1], SIGNAL(clicked(bool)), signalMapperShow, SLOT(map()));
        signalMapperShow->setMapping(rs[1], d->name.c_str());
        QObject::connect(signalMapperShow, SIGNAL(mapped(const QString &)), this, SLOT(DeviceShow(const QString &)));

        QSignalMapper* signalMapperHide = new QSignalMapper(this);
        QObject::connect(rs[2], SIGNAL(clicked(bool)), signalMapperHide, SLOT(map()));
        signalMapperHide->setMapping(rs[2], d->name.c_str());
        QObject::connect(signalMapperHide, SIGNAL(mapped(const QString &)), this, SLOT(DeviceHide(const QString &)));

        /*MyRedoGeometry(config);
        devicesTable->setMaximumSize(MyGetQTableWidgetSize(devicesTable));
        devicesTable->setMinimumSize(devicesTable->maximumSize());*/
    }

    for (auto v = d->variables.begin(); v != d->variables.end(); v++)
    {
        bool rowFound = false;
        int rowOfVariable = 0;

        std::string tempName = (*v)->GetName() + (((*v)->GetUnit() == "") ? "" : (" " + (*v)->GetUnit()));

        int rowIndex = 0;
        for (auto row = 0; row != table->rowCount(); row++)
        {
            if (table->verticalHeaderItem(row)->text().toStdString() == tempName)
            {
                rowFound = true;
                rowOfVariable = row;
                break;
            }
            if ( !(table->verticalHeaderItem(row)->text().toStdString() < tempName) )
                break;
            rowIndex++;
        }
        rowOfVariable = rowIndex;
        if (!rowFound)
        {
            variablesOrder.insert(variablesOrder.begin()+rowIndex, tempName);
            table->insertRow(rowOfVariable);
            table->setVerticalHeaderItem(rowOfVariable, new QTableWidgetItem(QString::fromStdString(tempName)));

            auto rs = NewRadioButtonGroup(rowOfVariable, tempName, variablesTable);

            QSignalMapper* signalMapperNormal = new QSignalMapper(this);
            QObject::connect(rs[0], SIGNAL(clicked(bool)), signalMapperNormal, SLOT(map()));
            signalMapperNormal->setMapping(rs[0], tempName.c_str());
            QObject::connect(signalMapperNormal, SIGNAL(mapped(const QString &)), this, SLOT(VariableNormal(const QString &)));

            QSignalMapper* signalMapperShow = new QSignalMapper(this);
            QObject::connect(rs[1], SIGNAL(clicked(bool)), signalMapperShow, SLOT(map()));
            signalMapperShow->setMapping(rs[1], tempName.c_str());
            QObject::connect(signalMapperShow, SIGNAL(mapped(const QString &)), this, SLOT(VariableShow(const QString &)));

            QSignalMapper* signalMapperHide = new QSignalMapper(this);
            QObject::connect(rs[2], SIGNAL(clicked(bool)), signalMapperHide, SLOT(map()));
            signalMapperHide->setMapping(rs[2], tempName.c_str());
            QObject::connect(signalMapperHide, SIGNAL(mapped(const QString &)), this, SLOT(VariableHide(const QString &)));

            /*MyRedoGeometry(config);
            variablesTable->setMaximumSize(MyGetQTableWidgetSize(variablesTable));
            variablesTable->setMinimumSize(variablesTable->maximumSize());*/
        }

        auto item = new TableItemQWidget(*v);
        table->setItem(rowOfVariable, columnOfDevice, item);
        item->UpdateItemFromVariable(true);
    }
}


void device::DataTableQWidget::SlotRemoveDevice(device::Device *d)
{
    auto found = std::find(devicesShown.begin(), devicesShown.end(), d->name);
    if (found != devicesShown.end())
        devicesShown.erase(found);
    found = std::find(devicesHidden.begin(), devicesHidden.end(), d->name);
    if (found != devicesHidden.end())
        devicesHidden.erase(found);

    bool columnFound = false;
    int columnOfDevice = 0;

    for (auto column = 0; column != table->columnCount(); column++)
    {
        if (table->horizontalHeaderItem(column)->text().toStdString() == d->name)
        {
            columnFound = true;
            columnOfDevice = column;
            break;
        }
    }

    if (columnFound)
    {
        devicesTable->removeRow(columnOfDevice);
        /*MyRedoGeometry(config);
        devicesTable->setMaximumSize(MyGetQTableWidgetSize(devicesTable));
        devicesTable->setMinimumSize(devicesTable->maximumSize());*/

        std::deque<int> rowsOfVariables;

        for (auto v = d->variables.begin(); v != d->variables.end(); v++)
        {
            bool rowFound = false;
            int rowOfVariable = 0;

            std::string tempName = (*v)->GetName() + (((*v)->GetUnit() == "") ? "" : (" " + (*v)->GetUnit()));

            for (auto row = 0; row != table->rowCount(); row++)
            {
                if (table->verticalHeaderItem(row)->text().toStdString() == tempName)
                {
                    rowFound = true;
                    rowsOfVariables.push_back(row);
                    rowOfVariable = row;
                    break;
                }
            }

            if (rowFound)
            {
                bool rowEmpty = true;
                delete table->item(rowOfVariable, columnOfDevice);

                for (auto c = 0; c != table->columnCount(); c++)
                {
                    if (table->item(rowOfVariable, c) != NULL)
                    {
                        rowEmpty = false;
                        break;
                    }
                }
                if (rowEmpty)
                {
                    std::string tempName = variablesOrder[rowOfVariable];
                    auto found = std::find(variablesShown.begin(), variablesShown.end(), tempName);
                    if (found != variablesShown.end())
                        variablesShown.erase(found);
                    found = std::find(variablesHidden.begin(), variablesHidden.end(), tempName);
                    if (found != variablesHidden.end())
                        variablesHidden.erase(found);

                    emit table->removeRow(rowOfVariable);
                    variablesTable->removeRow(rowOfVariable);
                    variablesOrder.erase(variablesOrder.begin() + (rowOfVariable));
                    /*MyRedoGeometry(config);
                    variablesTable->setMaximumSize(MyGetQTableWidgetSize(variablesTable));
                    variablesTable->setMinimumSize(variablesTable->maximumSize());*/
                }
            }
        }

        bool columnEmpty = true;
        for (auto r = 0; r != table->rowCount(); r++)
        {
            if (table->item(r, columnOfDevice) != NULL)
            {
                columnEmpty = false;
                break;
            }
        }
        if (columnEmpty)
            emit table->removeColumn(columnOfDevice);
    }
}


void device::DataTableQWidget::NormalShowHide()
{
    for (auto i = 0; i < table->columnCount(); i++)
    {
        auto label = table->horizontalHeaderItem(i)->text().toStdString();
        auto foundShown = std::find(devicesShown.begin(), devicesShown.end(), label);
        auto foundHidden = std::find(devicesHidden.begin(), devicesHidden.end(), label);
        if (foundShown == devicesShown.end() && foundHidden == devicesHidden.end())
            table->setColumnHidden(i, generalHidden);
    }
    for (auto i = 0; i < table->rowCount(); i++)
    {
        auto label = table->verticalHeaderItem(i)->text().toStdString();
        auto foundShown = std::find(variablesShown.begin(), variablesShown.end(), label);
        auto foundHidden = std::find(variablesHidden.begin(), variablesHidden.end(), label);
        if (foundShown == variablesShown.end() && foundHidden == variablesHidden.end())
            table->setRowHidden(i, generalHidden);
    }
}


void device::DataTableQWidget::GeneralShow(bool checked)
{
    generalHidden = false;
    NormalShowHide();
}


void device::DataTableQWidget::GeneralHide(bool checked)
{
    generalHidden = true;
    NormalShowHide();
}


void device::DataTableQWidget::DeviceNormal(const QString & name)
{
    bool columnFound = false;
    int columnOfDevice = 0;

    for (auto column = 0; column != table->columnCount(); column++)
    {
        if (table->horizontalHeaderItem(column)->text() == name)
        {
            columnFound = true;
            columnOfDevice = column;
            break;
        }
    }

    auto found = std::find(devicesShown.begin(), devicesShown.end(), name.toStdString());
    if (found != devicesShown.end())
        devicesShown.erase(found);
    found = std::find(devicesHidden.begin(), devicesHidden.end(), name.toStdString());
    if (found != devicesHidden.end())
        devicesHidden.erase(found);

    if (columnFound)
        table->setColumnHidden(columnOfDevice, generalHidden);
}


void device::DataTableQWidget::DeviceShow(const QString & name)
{
    bool columnFound = false;
    int columnOfDevice = 0;

    for (auto column = 0; column != table->columnCount(); column++)
    {
        if (table->horizontalHeaderItem(column)->text() == name)
        {
            columnFound = true;
            columnOfDevice = column;
            break;
        }
    }

    auto found = std::find(devicesShown.begin(), devicesShown.end(), name.toStdString());
    if (found == devicesShown.end())
        devicesShown.push_back(name.toStdString());
    found = std::find(devicesHidden.begin(), devicesHidden.end(), name.toStdString());
    if (found != devicesHidden.end())
        devicesHidden.erase(found);

    if (columnFound)
        table->setColumnHidden(columnOfDevice, false);
}


void device::DataTableQWidget::DeviceHide(const QString & name)
{
    bool columnFound = false;
    int columnOfDevice = 0;

    for (auto column = 0; column != table->columnCount(); column++)
    {
        if (table->horizontalHeaderItem(column)->text() == name)
        {
            columnFound = true;
            columnOfDevice = column;
            break;
        }
    }

    auto found = std::find(devicesShown.begin(), devicesShown.end(), name.toStdString());
    if (found != devicesShown.end())
        devicesShown.erase(found);
    found = std::find(devicesHidden.begin(), devicesHidden.end(), name.toStdString());
    if (found == devicesHidden.end())
        devicesHidden.push_back(name.toStdString());

    if (columnFound)
        table->setColumnHidden(columnOfDevice, true);
}


void device::DataTableQWidget::VariableNormal(const QString & name)
{
    bool rowFound = false;
    int rowOfVariable = 0;

    for (auto row = 0; row != table->rowCount(); row++)
    {
        if (table->verticalHeaderItem(row)->text() == name)
        {
            rowFound = true;
            rowOfVariable = row;
            break;
        }
    }

    auto found = std::find(variablesShown.begin(), variablesShown.end(), name.toStdString());
    if (found != variablesShown.end())
        variablesShown.erase(found);
    found = std::find(variablesHidden.begin(), variablesHidden.end(), name.toStdString());
    if (found != variablesHidden.end())
        variablesHidden.erase(found);

    if (rowFound)
        table->setRowHidden(rowOfVariable, generalHidden);
}


void device::DataTableQWidget::VariableShow(const QString & name)
{
    bool rowFound = false;
    int rowOfVariable = 0;

    for (auto row = 0; row != table->rowCount(); row++)
    {
        if (table->verticalHeaderItem(row)->text() == name)
        {
            rowFound = true;
            rowOfVariable = row;
            break;
        }
    }

    auto found = std::find(variablesShown.begin(), variablesShown.end(), name.toStdString());
    if (found == variablesShown.end())
        variablesShown.push_back(name.toStdString());
    found = std::find(variablesHidden.begin(), variablesHidden.end(), name.toStdString());
    if (found != variablesHidden.end())
        variablesHidden.erase(found);

    if (rowFound)
        table->setRowHidden(rowOfVariable, false);
}


void device::DataTableQWidget::VariableHide(const QString & name)
{
    bool rowFound = false;
    int rowOfVariable = 0;

    for (auto row = 0; row != table->rowCount(); row++)
    {
        if (table->verticalHeaderItem(row)->text() == name)
        {
            rowFound = true;
            rowOfVariable = row;
            break;
        }
    }

    auto found = std::find(variablesShown.begin(), variablesShown.end(), name.toStdString());
    if (found != variablesShown.end())
        variablesShown.erase(found);
    found = std::find(variablesHidden.begin(), variablesHidden.end(), name.toStdString());
    if (found == variablesHidden.end())
        variablesHidden.push_back(name.toStdString());

    if (rowFound)
        table->setRowHidden(rowOfVariable, true);
}
