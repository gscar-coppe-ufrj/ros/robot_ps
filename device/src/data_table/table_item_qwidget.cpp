// table_item_qwidget.cpp



#include <sstream>
#include <device/data_table/table_item_qwidget.h>



device::TableItemQWidget::TableItemQWidget(Variable* variable) : Item(variable)
{
    if(variable->IsEditable())
        setBackgroundColor(Qt::lightGray);

    setFlags(flags() & ~Qt::ItemIsEditable);
}


void device::TableItemQWidget::UpdateItemFromVariable(bool reset)
{
    if (variable != NULL)
    {
        if (variable->updating)
            setTextColor(Qt::darkBlue);
        else
            setTextColor(Qt::black);

        std::stringstream ss;
        switch (variable->GetType())
        {
        case Variable::VariableTypes::typeChar:
            ss << (short)std::get<0>(((VariableTemplate<char>*)variable)->GetValue());
            setText(QString::fromUtf8(ss.str().c_str()));
            break;
        case Variable::VariableTypes::typeUnsignedChar:
            ss << (unsigned short)std::get<0>(((VariableTemplate<unsigned char>*)variable)->GetValue());
            setText(QString::fromUtf8(ss.str().c_str()));
            break;
        case Variable::VariableTypes::typeShort:
            ss << std::get<0>(((VariableTemplate<short>*)variable)->GetValue());
            setText(QString::fromUtf8(ss.str().c_str()));
            break;
        case Variable::VariableTypes::typeUnsignedShort:
            ss << std::get<0>(((VariableTemplate<unsigned short>*)variable)->GetValue());
            setText(QString::fromUtf8(ss.str().c_str()));
            break;
        case Variable::VariableTypes::typeInt:
            ss << std::get<0>(((VariableTemplate<int>*)variable)->GetValue());
            setText(QString::fromUtf8(ss.str().c_str()));
            break;
        case Variable::VariableTypes::typeUnsignedInt:
            ss << std::get<0>(((VariableTemplate<unsigned int>*)variable)->GetValue());
            setText(QString::fromUtf8(ss.str().c_str()));
            break;
        case Variable::VariableTypes::typeDouble:
            ss << std::get<0>(((VariableTemplate<double>*)variable)->GetValue());
            setText(QString::fromUtf8(ss.str().c_str()));
            break;
        case Variable::VariableTypes::typeBool:
            if(((VariableBool*)variable)->GetState())
                setText(QString::fromUtf8(((VariableBool*)variable)->GetTrueString1().c_str()));
            else
                setText(QString::fromUtf8(((VariableBool*)variable)->GetFalseString1().c_str()));
            break;
        case Variable::VariableTypes::typeCombo:
        {
            unsigned int i = ((VariableCombo*)variable)->GetSelectedElementNumber();
            setText(QString::fromUtf8(((VariableCombo*)variable)->GetElementString(i).c_str()));
            break;
        }
        default:
            break;
        }
    }
}

bool device::TableItemQWidget::UpdateVariableFromItem()
{
    return true;
}


bool device::TableItemQWidget::IsEditable()
{
    return variable->IsEditable();
}


device::TableItemQWidget::~TableItemQWidget()
{
}
