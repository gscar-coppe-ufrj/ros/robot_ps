// config_var_image_item.cpp



#include <device/device_management/items/config_var_image_item.h>



device::ConfigVarImageItem::ConfigVarImageItem(Variable* variable) : ConfigVarItem(variable)
{
    label = new QLabel;
}


void device::ConfigVarImageItem::OrganizeWidget()
{
    delete layout;
    auto vLayout = new QVBoxLayout;
    vLayout->setContentsMargins(0, 0, 0, 0);
    vLayout->setSpacing(10);
    vLayout->addWidget(name);
    vLayout->addWidget(label);

    setLayout(vLayout);
}


void device::ConfigVarImageItem::InternalUpdateItemFromVariable(bool reset)
{
    if (variable != NULL)
    {
        VariableImage* var = (VariableImage*)variable;

        QImage img;
        img.load(QString(var->src.c_str()));
        label->setPixmap(QPixmap::fromImage(img));
    }
}


bool device::ConfigVarImageItem::UpdateVariableFromItem()
{
    if (variable != NULL)
    {
        VariableImage* var = (VariableImage*)variable;
        return true;
    }
    else
        return false;
}


void device::ConfigVarImageItem::InternalApply()
{
    if (UpdateVariableFromItem())
        ConfigVarItem::InternalApply();
}


device::ConfigVarImageItem::~ConfigVarImageItem()
{
}

