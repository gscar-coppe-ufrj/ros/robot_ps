// config_var_double_item.cpp



#include <sstream>
#include <QDoubleValidator>
#include <device/device_management/items/config_var_double_item.h>



device::ConfigVarDoubleItem::ConfigVarDoubleItem(Variable* variable) : ConfigVarItem(variable)
{
    currentDoubleValue = new QLabel;

    if (((VariableTemplate<double>*)variable)->IsRanged()[0])
    {
        slider = new QSlider(Qt::Horizontal);
        sliderMax = std::get<0>(((VariableTemplate<double>*)variable)->GetMaxValue());
        sliderMin = std::get<0>(((VariableTemplate<double>*)variable)->GetMinValue());
        connect(slider, SIGNAL(valueChanged(int)), this, SLOT(RangedNewValueChanged(int)));
    }
    else
        slider = NULL;

    newDoubleValue = new QLineEdit;
    newDoubleValue->setValidator(new QDoubleValidator);
    connect(newDoubleValue, SIGNAL(editingFinished()), this, SLOT(NewValueChanged()));

    updateNewValueSlider = true;
    updateNewValue = true;
}


void device::ConfigVarDoubleItem::AddWidgets()
{
    layout->addWidget(currentDoubleValue);

    if (slider != NULL)
        layout->addWidget(slider);

    layout->addWidget(newDoubleValue);
}


void device::ConfigVarDoubleItem::NewValueChanged()
{
    if (updateNewValueSlider)
    {
        if (slider != NULL)
        {
            double v = newDoubleValue->text().toDouble();
            updateNewValue = false;
            if (v > sliderMax)
            {
                newDoubleValue->setText(QString::number(sliderMax));
                slider->setValue(slider->maximum());
            }
            else if (v < sliderMin)
            {
                newDoubleValue->setText(QString::number(sliderMin));
                slider->setValue(slider->minimum());
            }
            else
            {
                double slidervalue = (v - sliderMin)*(slider->maximum() - slider->minimum())/(sliderMax- sliderMin);
                slider->setValue((int)slidervalue);
            }
            updateNewValue = true;
        }

        if (newDoubleValue->text().toDouble() == currentDoubleValue->text().toDouble())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarDoubleItem::RangedNewValueChanged(int sliderpos)
{
    if (updateNewValue)
    {
        double value = (sliderpos - slider->minimum())*(sliderMax- sliderMin)/(slider->maximum() - slider->minimum());
        updateNewValueSlider = false;
        newDoubleValue->setText(QString::number(value + sliderMin));
        updateNewValueSlider = true;

        if (newDoubleValue->text().toDouble() == currentDoubleValue->text().toDouble())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarDoubleItem::InternalUpdateItemFromVariable(bool reset)
{
    if (variable != NULL)
    {
        VariableTemplate<double>* varDouble = (VariableTemplate<double>*)variable;
        std::stringstream ss;
        ss << std::get<0>(varDouble->GetValue());
        std::string str = ss.str();
        currentDoubleValue->setText(QString::fromStdString(str));
        if (varDouble->updating)
            currentDoubleValue->setStyleSheet("QLabel {color : blue;}");
        else
            currentDoubleValue->setStyleSheet("QLabel {color : black;}");
        if (newDoubleValue->text() == "" || reset)
        {
            newDoubleValue->setText(QString::fromStdString(str));
            if (slider != NULL)
            {
                double slidervalue = (std::get<0>(varDouble->GetValue()) - sliderMin)*(slider->maximum() - slider->minimum())/(sliderMax- sliderMin);
                updateNewValue = false;
                slider->setValue((int)slidervalue);
                updateNewValue = true;
            }
        }

        if (newDoubleValue->text().toDouble() == currentDoubleValue->text().toDouble())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


bool device::ConfigVarDoubleItem::UpdateVariableFromItem()
{
    if (variable != NULL)
    {
        VariableTemplate<double>* varDouble = (VariableTemplate<double>*)variable;
        if (newDoubleValue->text() != "")
            varDouble->SetNewValue(newDoubleValue->text().toDouble());
        else
            varDouble->SetNewValue(std::get<0>(varDouble->GetValue()));
        return true;
    }
    else
        return false;
}


void device::ConfigVarDoubleItem::InternalApply()
{
    if (UpdateVariableFromItem())
        ConfigVarItem::InternalApply();
}


device::ConfigVarDoubleItem::~ConfigVarDoubleItem()
{
}

