// config_var_null_item.cpp



#include <device/device_management/items/config_var_null_item.h>



device::ConfigVarNullItem::ConfigVarNullItem(Variable* variable) : ConfigVarItem(variable)
{
    apply->hide();
    name->setStyleSheet("QLabel {font: bold}");
}


void device::ConfigVarNullItem::AddWidgets()
{
}


device::ConfigVarNullItem::~ConfigVarNullItem()
{
}

