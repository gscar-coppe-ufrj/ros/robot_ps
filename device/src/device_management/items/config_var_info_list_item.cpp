// config_var_info_list_item.cpp



#include <iostream>
#include <device/device_management/items/config_var_info_list_item.h>

using namespace std;



device::ConfigVarInfoListItem::ConfigVarInfoListItem(Variable* variable) : ConfigVarItem(variable)
{
    firstTime = true;
    reselecting = false;

    list = new QListWidget;
    info = new QTableWidget;
    
    info->setColumnCount(2);
    columnHeaders << "Attribute" << "Value";
    info->setHorizontalHeaderLabels(columnHeaders);
    
    info->setEditTriggers(QAbstractItemView::NoEditTriggers);
    info->verticalHeader()->setVisible(false);
    info->setShowGrid(false);
    
    connect(this, SIGNAL(NewApply()), apply, SLOT(click()), Qt::QueuedConnection);
    connect(list, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(ListItemChanged(QListWidgetItem*, QListWidgetItem*)));
}


void device::ConfigVarInfoListItem::OrganizeWidget()
{
    delete layout;
    auto hLayout = new QHBoxLayout;
    hLayout->setContentsMargins(0, 0, 0, 0);
    hLayout->setSpacing(10);

    auto layoutColumn1 = new QVBoxLayout;
    layoutColumn1->setContentsMargins(0, 0, 0, 0);
    layoutColumn1->setSpacing(10);
    layoutColumn1->addWidget(name);
    layoutColumn1->addWidget(list);

    hLayout->addLayout(layoutColumn1);
    hLayout->addWidget(info, 1);

    setLayout(hLayout);
}


void device::ConfigVarInfoListItem::ListItemChanged(QListWidgetItem* current, QListWidgetItem* previous)
{
    if (current != NULL && !reselecting)
        emit NewApply();
}


void device::ConfigVarInfoListItem::InternalUpdateItemFromVariable(bool reset)
{
    if (variable != NULL)
    {
        QStringList labels;

        VariableInfoList* var = (VariableInfoList*)variable;

        QString tempSelectedElement;
        auto item = list->currentItem();
        if (item != NULL)
            tempSelectedElement = item->text();

        list->clear();
        for (auto i = var->elements.begin(); i != var->elements.end(); i++)
            labels.push_back(QString((*i).c_str()));
        list->addItems(labels);

        if (item != NULL)
        {
            auto itensFound = list->findItems(tempSelectedElement, Qt::MatchExactly);

            if (itensFound.size() > 0)
            {
                reselecting = true;

                list->setCurrentItem(itensFound.first());

                if (var->elementSelected == tempSelectedElement.toStdString())
                {
                    map<string, string> infoMap = var->info;
                    info->setRowCount(infoMap.size());

                    int row = 0;
                    for (auto it = infoMap.begin(); it != infoMap.end(); it++)
                    {
                        info->setItem(row, 0, new QTableWidgetItem(it->first.c_str()));
                        info->setItem(row, 1, new QTableWidgetItem(it->second.c_str()));
                        row++;
                    }
                    info->resizeColumnsToContents();
                }
                //if (var->elementSelected == tempSelectedElement.toStdString())
                //    info->setText(QString(var->info.c_str()));

                reselecting = false;
            }
            else
            {
                info->clear();
                info->setHorizontalHeaderLabels(columnHeaders);
            }
        }
        else
        {
            info->clear();
            info->setHorizontalHeaderLabels(columnHeaders);
        }
    }
}


bool device::ConfigVarInfoListItem::UpdateVariableFromItem()
{
    if (variable != NULL)
    {
        VariableInfoList* var = (VariableInfoList*)variable;
        if (list->selectedItems().size() > 0)
            var->elementSelected = list->selectedItems().first()->text().toStdString();
        return true;
    }
    else
        return false;
}


void device::ConfigVarInfoListItem::InternalApply()
{
    if (UpdateVariableFromItem())
        ConfigVarItem::InternalApply();
}


device::ConfigVarInfoListItem::~ConfigVarInfoListItem()
{
}

