// config_var_u_short_item.cpp



#include <sstream>
#include <QIntValidator>
#include <device/device_management/items/config_var_u_short_item.h>



device::ConfigVarUShortItem::ConfigVarUShortItem(Variable* variable) : ConfigVarItem(variable)
{
    currentUShortValue = new QLabel;

    if (((VariableTemplate<unsigned short>*)variable)->IsRanged()[0])
    {
        slider = new QSlider(Qt::Horizontal);
        sliderMax = std::get<0>(((VariableTemplate<unsigned short>*)variable)->GetMaxValue());
        sliderMin = std::get<0>(((VariableTemplate<unsigned short>*)variable)->GetMinValue());
        connect(slider, SIGNAL(valueChanged(int)), this, SLOT(RangedNewValueChanged(int)));
    }
    else
        slider = NULL;

    newUShortValue = new QLineEdit;
    newUShortValue->setValidator(new QIntValidator(0, 65535));
    connect(newUShortValue, SIGNAL(editingFinished()), this, SLOT(NewValueChanged()));

    updateNewValueSlider = true;
    updateNewValue = true;
}


void device::ConfigVarUShortItem::AddWidgets()
{
    layout->addWidget(currentUShortValue);

    if (slider != NULL)
        layout->addWidget(slider);

    layout->addWidget(newUShortValue);
}


void device::ConfigVarUShortItem::NewValueChanged()
{
    if (updateNewValueSlider)
    {
        if (slider != NULL)
        {
            unsigned short v = newUShortValue->text().toUInt();
            updateNewValue = false;
            if (v > sliderMax)
            {
                newUShortValue->setText(QString::number(sliderMax));
                slider->setValue(slider->maximum());
            }
            else if (v < sliderMin)
            {
                newUShortValue->setText(QString::number(sliderMin));
                slider->setValue(slider->minimum());
            }
            else
            {
                unsigned short slidervalue = (v - sliderMin)*(slider->maximum() - slider->minimum())/(sliderMax- sliderMin);
                slider->setValue((unsigned short)slidervalue);
            }
            updateNewValue = true;
        }

        if (newUShortValue->text().toUInt() == currentUShortValue->text().toUInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarUShortItem::RangedNewValueChanged(int sliderpos)
{
    if (sliderpos < 0)
        sliderpos = 0;

    if (updateNewValue)
    {
        unsigned short value = (sliderpos - slider->minimum())*(sliderMax- sliderMin)/(slider->maximum() - slider->minimum());
        updateNewValueSlider = false;
        newUShortValue->setText(QString::number(value + sliderMin));
        updateNewValueSlider = true;

        if (newUShortValue->text().toUInt() == currentUShortValue->text().toUInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarUShortItem::InternalUpdateItemFromVariable(bool reset)
{
    if (variable != NULL)
    {
        VariableTemplate<unsigned short>* varUShort = (VariableTemplate<unsigned short>*)variable;
        std::stringstream ss;
        ss << std::get<0>(varUShort->GetValue());
        std::string str = ss.str();
        currentUShortValue->setText(QString::fromStdString(str));
        if (varUShort->updating)
            currentUShortValue->setStyleSheet("QLabel {color : blue;}");
        else
            currentUShortValue->setStyleSheet("QLabel {color : black;}");
        if (newUShortValue->text() == "" || reset)
        {
            newUShortValue->setText(QString::fromStdString(str));
            if (slider != NULL)
            {
                // a bug may happen because of the conversion of the int (slider) to unsigned int (everything else)
                unsigned short slidervalue = (std::get<0>(varUShort->GetValue()) - sliderMin)*(slider->maximum() - slider->minimum())/(sliderMax- sliderMin);
                updateNewValue = false;
                slider->setValue((unsigned short)slidervalue);
                updateNewValue = true;
            }
        }

        if (newUShortValue->text().toUInt() == currentUShortValue->text().toUInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


bool device::ConfigVarUShortItem::UpdateVariableFromItem()
{
    if (variable != NULL)
    {
        VariableTemplate<unsigned short>* varUShort = (VariableTemplate<unsigned short>*)variable;
        if (newUShortValue->text() != "")
            varUShort->SetNewValue(newUShortValue->text().toUInt());
        else
            varUShort->SetNewValue(std::get<0>(varUShort->GetValue()));
        return true;
    }
    else
        return false;
}


void device::ConfigVarUShortItem::InternalApply()
{
    if (UpdateVariableFromItem())
        ConfigVarItem::InternalApply();
}


device::ConfigVarUShortItem::~ConfigVarUShortItem()
{
}

