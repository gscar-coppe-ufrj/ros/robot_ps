// on_off_item.cpp



#include <string>
#include <device/device_management/items/on_off_item.h>



device::OnOffItem::OnOffItem(Variable* variable) : ItemQWidget(variable)
{
    layout = new QHBoxLayout;
    layout->setMargin(0);
    layout->setSpacing(0);

    std::string strName = variable->GetName();
    if (variable->GetUnit() != "")
        strName += " (" + variable->GetUnit() + ")";
    name = new QLabel(QString::fromStdString(strName));

    status = new QLabel;
    status->setPixmap(QPixmap(ON_OFF_ITEM_STATUS_ICON_OFF));

    button = new QToolButton;
    connect(button, SIGNAL(clicked()), this, SLOT(HandleClick()));

    layout->addWidget(name, 1);
    layout->addWidget(status);
    layout->addWidget(button);

    setLayout(layout);

    currentStatus = false;
    fromClick = false;

    deviceOnOff = NULL;

    VariableBool* varBool = static_cast<VariableBool*>(variable);
    this->setState(varBool->GetState());
}


void device::OnOffItem::setState(bool flag)
{
    if (flag)
    {
        status->setPixmap(QPixmap(ON_OFF_ITEM_STATUS_ICON_ON));
        button->setIcon(QIcon(ON_OFF_ITEM_STATUS_ICON_STOP));
    }
    else
    {
        status->setPixmap(QPixmap(ON_OFF_ITEM_STATUS_ICON_OFF));
        button->setIcon(QIcon(ON_OFF_ITEM_STATUS_ICON_PLAY));
    }
}


void device::OnOffItem::InternalUpdateItemFromVariable(bool reset)
{
    if (variable != NULL)
    {
        // VariableBool is different from others Variables.
        // It's like reset procedure must always happen

        bool oldCurrentStatus = currentStatus;

        VariableBool* varBool = (VariableBool*)variable;
        currentStatus = varBool->GetState();
        if (currentStatus != oldCurrentStatus)
            this->setState(currentStatus);
        std::string s2;
        if (currentStatus)
            s2 = varBool->GetFalseString2();
        else
            s2 = varBool->GetTrueString2();
        if (s2 == "")
            button->hide();
        else
            button->show();
    }
}


bool device::OnOffItem::UpdateVariableFromItem()
{
    if (variable != NULL)
    {
        VariableBool* varBool = (VariableBool*)variable;
        if (fromClick)
            varBool->SetNewState(!currentStatus);
        else
            varBool->SetNewState(varBool->GetState());
        return true;
    }
    else
        return false;
}


void device::OnOffItem::HandleClick()
{
    fromClick = true;
    if(UpdateVariableFromItem())
    {
        if (deviceOnOff != NULL)
            deviceOnOff->UpdateVariable(variable);
    }
    fromClick = false;
}

