// config_var_number_item.cpp


#include <QIntValidator>
#include <sstream>
#include <device/device_management/items/config_var_int_item.h>



device::ConfigVarIntItem::ConfigVarIntItem(Variable* variable) : ConfigVarItem(variable)
{
    currentIntValue = new QLabel;

    if (((VariableTemplate<int>*)variable)->IsRanged()[0])
    {
        slider = new QSlider(Qt::Horizontal);
        sliderMax = std::get<0>(((VariableTemplate<int>*)variable)->GetMaxValue());
        sliderMin = std::get<0>(((VariableTemplate<int>*)variable)->GetMinValue());
        connect(slider, SIGNAL(valueChanged(int)), this, SLOT(RangedNewValueChanged(int)));
    }
    else
        slider = NULL;

    newIntValue = new QLineEdit;
    newIntValue->setValidator(new QIntValidator);
    connect(newIntValue, SIGNAL(editingFinished()), this, SLOT(NewValueChanged()));

    updateNewValueSlider = true;
    updateNewValue = true;
}


void device::ConfigVarIntItem::AddWidgets()
{
    layout->addWidget(currentIntValue);

    if (slider != NULL)
        layout->addWidget(slider);

    layout->addWidget(newIntValue);
}


void device::ConfigVarIntItem::NewValueChanged()
{
    if (updateNewValueSlider)
    {
        if (slider != NULL)
        {
            int v = newIntValue->text().toInt();
            updateNewValue = false;
            if (v > sliderMax)
            {
                newIntValue->setText(QString::number(sliderMax));
                slider->setValue(slider->maximum());
            }
            else if (v < sliderMin)
            {
                newIntValue->setText(QString::number(sliderMin));
                slider->setValue(slider->minimum());
            }
            else
            {
                int slidervalue = (v - sliderMin)*(slider->maximum() - slider->minimum())/(sliderMax- sliderMin);
                slider->setValue((int)slidervalue);
            }
            updateNewValue = true;
        }

        if (newIntValue->text().toInt() == currentIntValue->text().toInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarIntItem::RangedNewValueChanged(int sliderpos)
{
    if (updateNewValue)
    {
        int value = (sliderpos - slider->minimum())*(sliderMax- sliderMin)/(slider->maximum() - slider->minimum());
        updateNewValueSlider = false;
        newIntValue->setText(QString::number(value + sliderMin));
        updateNewValueSlider = true;

        if (newIntValue->text().toInt() == currentIntValue->text().toInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarIntItem::InternalUpdateItemFromVariable(bool reset)
{
    if (variable != NULL)
    {
        VariableTemplate<int>* varInt = (VariableTemplate<int>*)variable;
        std::stringstream ss;
        ss << std::get<0>(varInt->GetValue());
        std::string str = ss.str();
        currentIntValue->setText(QString::fromStdString(str));
        if (varInt->updating)
            currentIntValue->setStyleSheet("QLabel {color : blue;}");
        else
            currentIntValue->setStyleSheet("QLabel {color : black;}");
        if (newIntValue->text() == "" || reset)
        {
            newIntValue->setText(QString::fromStdString(str));
            if (slider != NULL)
            {
                int slidervalue = (std::get<0>(varInt->GetValue()) - sliderMin)*(slider->maximum() - slider->minimum())/(sliderMax- sliderMin);
                updateNewValue = false;
                slider->setValue((int)slidervalue);
                updateNewValue = true;
            }
        }

        if (newIntValue->text().toInt() == currentIntValue->text().toInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


bool device::ConfigVarIntItem::UpdateVariableFromItem()
{
    if (variable != NULL)
    {
        VariableTemplate<int>* varint = (VariableTemplate<int>*)variable;
        if (newIntValue->text() != "")
            varint->SetNewValue(newIntValue->text().toInt());
        else
            varint->SetNewValue(std::get<0>(varint->GetValue()));
        return true;
    }
    else
        return false;
}


void device::ConfigVarIntItem::InternalApply()
{
    if (UpdateVariableFromItem())
        ConfigVarItem::InternalApply();
}


device::ConfigVarIntItem::~ConfigVarIntItem()
{
}

