// config_var_char_item.cpp


#include <QIntValidator>
#include <sstream>
#include <device/device_management/items/config_var_char_item.h>



device::ConfigVarCharItem::ConfigVarCharItem(Variable* variable) : ConfigVarItem(variable)
{
    currentCharValue = new QLabel;

    if (((VariableTemplate<char>*)variable)->IsRanged()[0])
    {
        slider = new QSlider(Qt::Horizontal);
        sliderMax = std::get<0>(((VariableTemplate<char>*)variable)->GetMaxValue());
        sliderMin = std::get<0>(((VariableTemplate<char>*)variable)->GetMinValue());
        connect(slider, SIGNAL(valueChanged(int)), this, SLOT(RangedNewValueChanged(int)));
    }
    else
        slider = NULL;

    newCharValue = new QLineEdit;
    newCharValue->setValidator(new QIntValidator(-128,127));
    connect(newCharValue, SIGNAL(editingFinished()), this, SLOT(NewValueChanged()));

    updateNewValueSlider = true;
    updateNewValue = true;
}


void device::ConfigVarCharItem::AddWidgets()
{
    layout->addWidget(currentCharValue);

    if (slider != NULL)
        layout->addWidget(slider);

    layout->addWidget(newCharValue);
}


void device::ConfigVarCharItem::NewValueChanged()
{
    if (updateNewValueSlider)
    {
        if (slider != NULL)
        {
            char v = newCharValue->text().toInt();
            updateNewValue = false;
            if (v > sliderMax)
            {
                newCharValue->setText(QString::number(sliderMax));
                slider->setValue(slider->maximum());
            }
            else if (v < sliderMin)
            {
                newCharValue->setText(QString::number(sliderMin));
                slider->setValue(slider->minimum());
            }
            else
            {
                int slidervalue = (v - sliderMin)*(slider->maximum() - slider->minimum())/(sliderMax- sliderMin);
                slider->setValue((int)slidervalue);
            }
            updateNewValue = true;
        }

        if (newCharValue->text().toInt() == currentCharValue->text().toInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarCharItem::RangedNewValueChanged(int sliderpos)
{
    if (updateNewValue)
    {
        int value = (sliderpos - slider->minimum())*(sliderMax- sliderMin)/(slider->maximum() - slider->minimum());
        updateNewValueSlider = false;
        newCharValue->setText(QString::number(value + sliderMin));
        updateNewValueSlider = true;

        if (newCharValue->text().toInt() == currentCharValue->text().toInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarCharItem::InternalUpdateItemFromVariable(bool reset)
{
    if (variable != NULL)
    {
        VariableTemplate<char>* varChar = (VariableTemplate<char>*)variable;
        std::stringstream ss;
        ss << (short)std::get<0>(varChar->GetValue());
        std::string str = ss.str();
        currentCharValue->setText(QString::fromStdString(str));
        if (varChar->updating)
            currentCharValue->setStyleSheet("QLabel {color : blue;}");
        else
            currentCharValue->setStyleSheet("QLabel {color : black;}");
        if (newCharValue->text() == "" || reset)
        {
            newCharValue->setText(QString::fromStdString(str));
            if (slider != NULL)
            {
                int slidervalue = (std::get<0>(varChar->GetValue()) - sliderMin)*(slider->maximum() - slider->minimum())/(sliderMax- sliderMin);
                updateNewValue = false;
                slider->setValue((int)slidervalue);
                updateNewValue = true;
            }
        }

        if (newCharValue->text().toInt() == currentCharValue->text().toInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


bool device::ConfigVarCharItem::UpdateVariableFromItem()
{
    if (variable != NULL)
    {
        VariableTemplate<char>* varChar = (VariableTemplate<char>*)variable;
        if (newCharValue->text() != "")
            varChar->SetNewValue(newCharValue->text().toInt());
        else
            varChar->SetNewValue(std::get<0>(varChar->GetValue()));
        return true;
    }
    else
        return false;
}


void device::ConfigVarCharItem::InternalApply()
{
    if (UpdateVariableFromItem())
        ConfigVarItem::InternalApply();
}


device::ConfigVarCharItem::~ConfigVarCharItem()
{
}

