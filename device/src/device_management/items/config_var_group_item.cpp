// config_var_bool_item.cpp



#include <string>
#include <device/device_management/items/config_var_group_item.h>



device::ConfigVarGroupItem::ConfigVarGroupItem(Variable* variable) : ConfigVarItem(variable)
{
    apply->hide();

    name->setStyleSheet("QLabel {font: bold}");

    maximize = new QToolButton;
    maximize->setIcon(QIcon("/usr/share/icons/Humanity/actions/32/add.svg"));
    //maximize->setFixedSize(35, 35);
    maximize->hide();
    minimize = new QToolButton;
    minimize->setIcon(QIcon("/usr/share/icons/Humanity/actions/32/remove.svg"));
    //minimize->setFixedSize(35, 35);
    connect(maximize, SIGNAL(clicked(bool)), this, SLOT(InternalApply()));
    connect(minimize, SIGNAL(clicked(bool)), this, SLOT(InternalApply()));
}


void device::ConfigVarGroupItem::OrganizeWidget()
{
    delete layout;
    auto hLayout = new QHBoxLayout;
    hLayout->setContentsMargins(0, 0, 0, 0);
    hLayout->setSpacing(10);

    hLayout->addWidget(maximize);
    hLayout->addWidget(minimize);
    hLayout->addWidget(name, 1, Qt::AlignHCenter);

    setLayout(hLayout);
}


void device::ConfigVarGroupItem::InternalUpdateItemFromVariable(bool reset)
{
}


bool device::ConfigVarGroupItem::UpdateVariableFromItem()
{
    if (variable != NULL)
    {
        maximize->setHidden(!maximize->isHidden());
        minimize->setHidden(!minimize->isHidden());

        QWidget* parent = parentWidget();
        while (parent != NULL)
        {
            auto devManagement = dynamic_cast<DeviceManagementQWidget*>(parent);
            if (devManagement!= NULL)
            {
                devManagement->ShowGroup(maximize->isHidden(), this);
                break;
            }
            parent = parent->parentWidget();
        }

        return true;
    }
    else
        return false;
}


void device::ConfigVarGroupItem::Maximize()
{

}


void device::ConfigVarGroupItem::Minimize()
{

}


void device::ConfigVarGroupItem::InternalApply()
{
    if (UpdateVariableFromItem())
        ConfigVarItem::InternalApply();
}


device::ConfigVarGroupItem::~ConfigVarGroupItem()
{
}

