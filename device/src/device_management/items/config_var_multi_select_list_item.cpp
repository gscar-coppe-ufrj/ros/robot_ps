// config_var_multi_select_list_item.cpp



#include <iostream>
#include <device/device_management/items/config_var_multi_select_list_item.h>



device::ConfigVarMultiSelectListItem::ConfigVarMultiSelectListItem(Variable* variable) : ConfigVarItem(variable)
{
    firstTime = true;

    elements = new QListWidget;
    selectedElements = new QListWidget;
    newElementSelection = new QListWidget;

    add = new QToolButton();
    add->setIcon(QIcon("/usr/share/icons/Humanity/actions/16/add.svg"));
    remove = new QToolButton();
    remove->setIcon(QIcon("/usr/share/icons/Humanity/actions/16/remove.svg"));

    connect(add, SIGNAL(clicked()), this, SLOT(Add()));
    connect(remove, SIGNAL(clicked()), this, SLOT(Remove()));
    connect(elements, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(ElementsItemChanged(QListWidgetItem*, QListWidgetItem*)));
    connect(selectedElements, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(SelectedElementsItemChanged(QListWidgetItem*, QListWidgetItem*)));
    connect(newElementSelection, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(NewElementSelectionItemChanged(QListWidgetItem*, QListWidgetItem*)));
}


void device::ConfigVarMultiSelectListItem::OrganizeWidget()
{
    delete layout;
    auto vLayout = new QVBoxLayout;
    vLayout->setContentsMargins(0, 0, 0, 0);
    vLayout->setSpacing(10);
    auto layoutLine1 = new QHBoxLayout;
    layoutLine1->setContentsMargins(0, 0, 0, 0);
    layoutLine1->setSpacing(10);
    auto layoutLine2 = new QHBoxLayout;
    layoutLine2->setContentsMargins(0, 0, 0, 0);
    layoutLine2->setSpacing(10);

    layoutLine1->addWidget(name);
    layoutLine1->addWidget(add);
    layoutLine1->addWidget(remove);
    layoutLine1->addWidget(apply);
    layoutLine2->addWidget(selectedElements);
    layoutLine2->addWidget(elements);
    layoutLine2->addWidget(newElementSelection);

    vLayout->addLayout(layoutLine1);
    vLayout->addLayout(layoutLine2);

    setLayout(vLayout);
}


void device::ConfigVarMultiSelectListItem::Add()
{
    auto temp = elements->selectedItems();
    if (temp.size() > 0)
    {
        if (newElementSelection->findItems(temp.front()->text(), Qt::MatchCaseSensitive).size() == 0)
            newElementSelection->addItem(temp.front()->text());
    }
}


void device::ConfigVarMultiSelectListItem::Remove()
{
    QList<QListWidgetItem*> temp = newElementSelection->selectedItems();
    for (auto i = temp.begin(); i!= temp.end(); i++)
    {
        delete newElementSelection->takeItem(newElementSelection->row(*i));
    }
}


void device::ConfigVarMultiSelectListItem::ElementsItemChanged(QListWidgetItem* current, QListWidgetItem* previous)
{
    if (current != NULL)
    {
        selectedElements->setCurrentItem(NULL);
        newElementSelection->setCurrentItem(NULL);
    }
}


void device::ConfigVarMultiSelectListItem::SelectedElementsItemChanged(QListWidgetItem* current, QListWidgetItem* previous)
{
    if (current != NULL)
    {
        elements->setCurrentItem(NULL);
        newElementSelection->setCurrentItem(NULL);
    }
}


void device::ConfigVarMultiSelectListItem::NewElementSelectionItemChanged(QListWidgetItem* current, QListWidgetItem* previous)
{
    if (current != NULL)
    {
        elements->setCurrentItem(NULL);
        selectedElements->setCurrentItem(NULL);
    }
}


void device::ConfigVarMultiSelectListItem::InternalUpdateItemFromVariable(bool reset)
{
    QStringList labels;
    if (variable != NULL)
    {
        VariableMultiSelectList* var = (VariableMultiSelectList*)variable;

        elements->clear();
        for (auto i = var->elements.begin(); i != var->elements.end(); i++)
            labels.push_back(QString((*i).c_str()));
        elements->addItems(labels);

        labels.clear();
        selectedElements->clear();
        for (auto i = var->selectedElements.begin(); i != var->selectedElements.end(); i++)
            labels.push_back(QString((*i).c_str()));
        selectedElements->addItems(labels);

        labels.clear();
        if (firstTime)
        {
            firstTime = false;

            newElementSelection->clear();
            for (auto i = var->selectedElements.begin(); i != var->selectedElements.end(); i++)
                labels.push_back(QString((*i).c_str()));
            newElementSelection->addItems(labels);
        }
        else
        {
            for (auto i = 0; i < newElementSelection->count();)
            {
                std::string text = newElementSelection->item(i)->text().toStdString();
                auto found = std::find(var->elements.begin(), var->elements.end(), text);
                if (found == var->selectedElements.end())
                    newElementSelection->removeItemWidget(newElementSelection->item(i));
                else
                    i++;
            }
        }
    }
}


bool device::ConfigVarMultiSelectListItem::UpdateVariableFromItem()
{
    if (variable != NULL)
    {
        VariableMultiSelectList* var = (VariableMultiSelectList*)variable;
        var->newElementSelection.clear();
        for (auto i = 0; i != newElementSelection->count(); i++)
            var->newElementSelection.push_back(newElementSelection->item(i)->text().toStdString());
        return true;
    }
    else
        return false;
}


void device::ConfigVarMultiSelectListItem::InternalApply()
{
    if (UpdateVariableFromItem())
        ConfigVarItem::InternalApply();
}


device::ConfigVarMultiSelectListItem::~ConfigVarMultiSelectListItem()
{
}

