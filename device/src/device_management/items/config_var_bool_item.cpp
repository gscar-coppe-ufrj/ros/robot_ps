// config_var_bool_item.cpp



#include <string>
#include <device/device_management/items/config_var_bool_item.h>



device::ConfigVarBoolItem::ConfigVarBoolItem(Variable* variable) : ConfigVarItem(variable)
{
    //apply->hide();

    currentBoolString = new QLabel;

    button = new QPushButton;
    button->setCheckable(true);
    connect(button, SIGNAL(clicked(bool)), this, SLOT(ButtonClicked(bool)));
}


void device::ConfigVarBoolItem::AddWidgets()
{
    layout->addWidget(currentBoolString);
    layout->addWidget(button);
}


void device::ConfigVarBoolItem::ButtonClicked(bool checked)
{
    if (button->isChecked())
        name->setStyleSheet("QLabel {font: italic}");
    else
        name->setStyleSheet("QLabel {font: normal}");
}


void device::ConfigVarBoolItem::InternalUpdateItemFromVariable(bool reset)
{
    if (variable != NULL)
    {
        VariableBool* varBool = (VariableBool*)variable;
        currentBoolValue = varBool->GetState();
        std::string s1;
        if (currentBoolValue)
            s1 = varBool->GetTrueString1();
        else
            s1 = varBool->GetFalseString1();
        currentBoolString->setText(QString::fromStdString(s1));
        if (varBool->updating)
            currentBoolString->setStyleSheet("QLabel {color : blue;}");
        else
            currentBoolString->setStyleSheet("QLabel {color : black;}");

        // VariableBool is different from others Variables.
        // It's like reset procedure must always happen

        button->setChecked(false);
        std::string s2;
        if (currentBoolValue)
            s2 = varBool->GetFalseString2();
        else
            s2 = varBool->GetTrueString2();
        button->setText(QString::fromStdString(s2));

        if (s2 == "")
        {
            button->hide();
            apply->hide();
        }
        else
        {
            button->show();
            apply->show();
        }

        if (button->isChecked())
            name->setStyleSheet("QLabel {font: italic}");
        else
            name->setStyleSheet("QLabel {font: normal}");
    }
}


bool device::ConfigVarBoolItem::UpdateVariableFromItem()
{
    if (variable != NULL)
    {
        VariableBool* varbool = (VariableBool*)variable;
        if (button->isChecked())
            varbool->SetNewState(!currentBoolValue);
        else
            varbool->SetNewState(varbool->GetState());
        return true;
    }
    else
        return false;
}


void device::ConfigVarBoolItem::InternalApply()
{
    if (UpdateVariableFromItem())
        ConfigVarItem::InternalApply();
}


device::ConfigVarBoolItem::~ConfigVarBoolItem()
{
}

