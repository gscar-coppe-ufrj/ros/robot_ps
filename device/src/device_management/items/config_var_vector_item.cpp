// config_var_double_item.cpp



#include <sstream>
#include <functional>
#include <iostream>
#include <QDoubleValidator>
#include <device/device_management/items/config_var_vector_item.h>


device::ConfigVarVectorItem::ConfigVarVectorItem(VariableVector* variable) : ConfigVarItem(variable)
{
    size = ((VariableVector*)variable)->GetSize();
    for (unsigned int i = 0; i < variable->GetSize(); i++)
    {
        currentVectorValue.push_back(new QLabel);
        newVectorValue.push_back(new QLineEdit);
        QToolButton* apl = new QToolButton();
        apl->setIcon(QIcon("/usr/share/icons/Humanity/actions/16/dialog-apply.svg"));
        applys.push_back(apl);
        QObject::connect(apl, SIGNAL(clicked()), this, SLOT(InternalApply()));
        names.push_back(new QLabel(QString::fromStdString(variable->GetName(i))));
        newVectorValue[i]->setValidator(new QDoubleValidator);
        connect(newVectorValue[i], &QLineEdit::editingFinished, this,  [=]() {
            NewValueChanged(i);
        });
        if (variable->IsRanged())
        {
            slider.push_back(new QSlider(Qt::Horizontal));
            //sliderMax = std::get<0>(((VariableTemplate<double>*)variable)->GetMaxValue());
            //sliderMin = std::get<0>(((VariableTemplate<double>*)variable)->GetMinValue());
            //connect(slider, SIGNAL(valueChanged(int)), this, SLOT(RangedNewValueChanged(int)));
        }
        else
            slider.push_back(NULL);
    }

    updateNewValueSlider.push_back(true);
    updateNewValue.push_back(true);
}


void device::ConfigVarVectorItem::AddWidgets()
{
    QVBoxLayout* v = new QVBoxLayout;
    v->setAlignment(Qt::AlignTop);
    name->setStyleSheet("font-weight: bold;");
    layout->removeWidget(name);
    apply->hide();
    //v->addWidget(name);

    for (unsigned int i; i < size; i++)
    {
        QHBoxLayout* h = new QHBoxLayout;
        //h->addSpacing(10);
        h->addWidget(names[i],1);
        h->addWidget(currentVectorValue[i]);
        h->setContentsMargins(0, 0, 0, 0);
        h->setSpacing(10);
        if (slider[i] != NULL)
            h->addWidget(slider[i]);

        h->addWidget(newVectorValue[i]);
        h->addWidget(applys[i]);
        v->addLayout(h);
        v->setContentsMargins(0, 0, 0, 0);
        v->setSpacing(3);
    }
    layout->addLayout(v);
}


void device::ConfigVarVectorItem::NewValueChanged(unsigned int i)
{
    if (updateNewValueSlider[i])
    {
        if (slider[i] != NULL)
        {
            double v = newVectorValue[i]->text().toDouble();
            updateNewValue[i] = false;
            if (v > sliderMax[i])
            {
                newVectorValue[i]->setText(QString::number(sliderMax[i]));
                slider[i]->setValue(slider[i]->maximum());
            }
            else if (v < sliderMin[i])
            {
                newVectorValue[i]->setText(QString::number(sliderMin[i]));
                slider[i]->setValue(slider[i]->minimum());
            }
            else
            {
                double slidervalue = (v - sliderMin[i])*(slider[i]->maximum() - slider[i]->minimum())/(sliderMax[i]- sliderMin[i]);
                slider[i]->setValue((int)slidervalue);
            }
            updateNewValue[i] = true;
        }

        if (newVectorValue[i]->text().toDouble() == currentVectorValue[i]->text().toDouble())
            names[i]->setStyleSheet("QLabel {font: normal}");
        else
            names[i]->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarVectorItem::RangedNewValueChanged(int sliderpos, int i)
{
    if (updateNewValue[i])
    {
        double value = (sliderpos - slider[i]->minimum())*(sliderMax[i]- sliderMin[i])/(slider[i]->maximum() - slider[i]->minimum());
        updateNewValueSlider[i] = false;
        newVectorValue[i]->setText(QString::number(value + sliderMin[i]));
        updateNewValueSlider[i] = true;

        if (newVectorValue[i]->text().toDouble() == currentVectorValue[i]->text().toDouble())
            names[i]->setStyleSheet("QLabel {font: normal}");
        else
            names[i]->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarVectorItem::InternalUpdateItemFromVariable(bool reset)
{
    if (variable != NULL)
    {
        for (unsigned int i = 0; i < size; i++)
        {
            VariableVector* varVec = (VariableVector*)variable;
            std::stringstream ss;
            ss << varVec->GetValue(i);
            std::string str = ss.str();
            currentVectorValue[i]->setText(QString::fromStdString(str));
            if (varVec->updating)
                currentVectorValue[i]->setStyleSheet("QLabel {color : blue;}");
            else
                currentVectorValue[i]->setStyleSheet("QLabel {color : black;}");
            if (newVectorValue[i]->text() == "" || reset)
            {
                newVectorValue[i]->setText(QString::fromStdString(str));
                if (slider[i] != NULL)
                {
                    double slidervalue = (varVec->GetValue(i) - sliderMin[i])*(slider[i]->maximum() - slider[i]->minimum())/(sliderMax[i] - sliderMin[i]);
                    updateNewValue[i] = false;
                    slider[i]->setValue((int)slidervalue);
                    updateNewValue[i] = true;
                }
            }

            if (newVectorValue[i]->text().toDouble() == currentVectorValue[i]->text().toDouble())
                names[i]->setStyleSheet("QLabel {font: normal}");
            else
                names[i]->setStyleSheet("QLabel {font: italic}");
        }
    }
}


bool device::ConfigVarVectorItem::UpdateVariableFromItem()
{
    if (variable != NULL)
    {
        // Why does it enters here 4 times?
        for (int i = 0; i < size; i++)
        {
            VariableVector* varVec = (VariableVector*)variable;
            if (newVectorValue[i]->text() != "")
                varVec->SetNewValue(i, newVectorValue[i]->text().toDouble());
            else
                varVec->SetNewValue(i, varVec->GetValue(i));
        }
        return true;
    }
    else
        return false;
}

void device::ConfigVarVectorItem::HideApply()
{
    apply->hide();
    for (QToolButton* b : applys)
        b->hide();
}


void device::ConfigVarVectorItem::InternalApply()
{
    if (UpdateVariableFromItem())
        ConfigVarItem::InternalApply();
}


device::ConfigVarVectorItem::~ConfigVarVectorItem()
{
}

