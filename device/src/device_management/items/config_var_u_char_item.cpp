// config_var_u_char_item.cpp



#include <sstream>
#include <QIntValidator>
#include <device/device_management/items/config_var_u_char_item.h>



device::ConfigVarUCharItem::ConfigVarUCharItem(Variable* variable) : ConfigVarItem(variable)
{
    currentUCharValue = new QLabel;

    if (((VariableTemplate<unsigned char>*)variable)->IsRanged()[0])
    {
        slider = new QSlider(Qt::Horizontal);
        sliderMax = std::get<0>(((VariableTemplate<unsigned char>*)variable)->GetMaxValue());
        sliderMin = std::get<0>(((VariableTemplate<unsigned char>*)variable)->GetMinValue());
        connect(slider, SIGNAL(valueChanged(int)), this, SLOT(RangedNewValueChanged(int)));
    }
    else
        slider = NULL;

    newUCharValue = new QLineEdit;
    newUCharValue->setValidator(new QIntValidator(0, 255));
    connect(newUCharValue, SIGNAL(editingFinished()), this, SLOT(NewValueChanged()));

    updateNewValueSlider = true;
    updateNewValue = true;
}


void device::ConfigVarUCharItem::AddWidgets()
{
    layout->addWidget(currentUCharValue);

    if (slider != NULL)
        layout->addWidget(slider);

    layout->addWidget(newUCharValue);
}


void device::ConfigVarUCharItem::NewValueChanged()
{
    if (updateNewValueSlider)
    {
        if (slider != NULL)
        {
            unsigned char v = newUCharValue->text().toUInt();
            updateNewValue = false;
            if (v > sliderMax)
            {
                newUCharValue->setText(QString::number(sliderMax));
                slider->setValue(slider->maximum());
            }
            else if (v < sliderMin)
            {
                newUCharValue->setText(QString::number(sliderMin));
                slider->setValue(slider->minimum());
            }
            else
            {
                unsigned int slidervalue = (v - sliderMin)*(slider->maximum() - slider->minimum())/(sliderMax- sliderMin);
                slider->setValue((unsigned int)slidervalue);
            }
            updateNewValue = true;
        }

        if (newUCharValue->text().toUInt() == currentUCharValue->text().toUInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarUCharItem::RangedNewValueChanged(int sliderpos)
{
    if (sliderpos < 0)
        sliderpos = 0;

    if (updateNewValue)
    {
        unsigned char value = (sliderpos - slider->minimum())*(sliderMax- sliderMin)/(slider->maximum() - slider->minimum());
        updateNewValueSlider = false;
        newUCharValue->setText(QString::number(value + sliderMin));
        updateNewValueSlider = true;

        if (newUCharValue->text().toUInt() == currentUCharValue->text().toUInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarUCharItem::InternalUpdateItemFromVariable(bool reset)
{
    if (variable != NULL)
    {
        VariableTemplate<unsigned char>* varUChar = (VariableTemplate<unsigned char>*)variable;
        std::stringstream ss;
        ss << (unsigned short)std::get<0>(varUChar->GetValue());
        std::string str = ss.str();
        currentUCharValue->setText(QString::fromStdString(str));
        if (varUChar->updating)
            currentUCharValue->setStyleSheet("QLabel {color : blue;}");
        else
            currentUCharValue->setStyleSheet("QLabel {color : black;}");
        if (newUCharValue->text() == "" || reset)
        {
            newUCharValue->setText(QString::fromStdString(str));
            if (slider != NULL)
            {
                // a bug may happen because of the conversion of the int (slider) to unsigned int (everything else)
                unsigned int slidervalue = (std::get<0>(varUChar->GetValue()) - sliderMin)*(slider->maximum() - slider->minimum())/(sliderMax- sliderMin);
                updateNewValue = false;
                slider->setValue((unsigned int)slidervalue);
                updateNewValue = true;
            }
        }

        if (newUCharValue->text().toUInt() == currentUCharValue->text().toUInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


bool device::ConfigVarUCharItem::UpdateVariableFromItem()
{
    if (variable != NULL)
    {
        VariableTemplate<unsigned char>* varUChar = (VariableTemplate<unsigned char>*)variable;
        if (newUCharValue->text() != "")
            varUChar->SetNewValue(newUCharValue->text().toUInt());
        else
            varUChar->SetNewValue(std::get<0>(varUChar->GetValue()));
        return true;
    }
    else
        return false;
}


void device::ConfigVarUCharItem::InternalApply()
{
    if (UpdateVariableFromItem())
        ConfigVarItem::InternalApply();
}


device::ConfigVarUCharItem::~ConfigVarUCharItem()
{
}

