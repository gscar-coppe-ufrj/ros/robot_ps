// config_var_item.cpp



#include <string>
#include <device/device_management/items/config_var_item.h>



device::ConfigVarItem::ConfigVarItem(Variable* variable) : ItemQWidget(variable)
{
    layout = new QHBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(10);

    std::string strName = variable->GetName();
    if (variable->GetUnit() != "")
        strName += " (" + variable->GetUnit() + ")";
    name = new QLabel(QString::fromStdString(strName));
    name->setStyleSheet("font-weight: bold;");

    apply = new QToolButton();
    apply->setIcon(QIcon("/usr/share/icons/Humanity/actions/16/dialog-apply.svg"));

    QObject::connect(apply, SIGNAL(clicked()), this, SLOT(InternalApply()));

    setObjectName(name->text());
}


void device::ConfigVarItem::InternalUpdateItemFromVariable(bool reset)
{
}


bool device::ConfigVarItem::UpdateVariableFromItem()
{
    return true;
}


void device::ConfigVarItem::OrganizeWidget()
{
    layout->addWidget(name, 1);
    AddWidgets();
    layout->addWidget(apply);

    setLayout(layout);
}


void device::ConfigVarItem::InternalApply()
{
    emit Apply(variable);
}


device::ConfigVarItem::~ConfigVarItem()
{
}

