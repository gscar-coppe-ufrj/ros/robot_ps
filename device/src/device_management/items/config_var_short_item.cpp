// config_var_short_item.cpp


#include <QIntValidator>
#include <sstream>
#include <device/device_management/items/config_var_short_item.h>



device::ConfigVarShortItem::ConfigVarShortItem(Variable* variable) : ConfigVarItem(variable)
{
    currentShortValue = new QLabel;

    if (((VariableTemplate<short>*)variable)->IsRanged()[0])
    {
        slider = new QSlider(Qt::Horizontal);
        sliderMax = std::get<0>(((VariableTemplate<short>*)variable)->GetMaxValue());
        sliderMin = std::get<0>(((VariableTemplate<short>*)variable)->GetMinValue());
        connect(slider, SIGNAL(valueChanged(int)), this, SLOT(RangedNewValueChanged(int)));
    }
    else
        slider = NULL;

    newShortValue = new QLineEdit;
    newShortValue->setValidator(new QIntValidator(-32768, 32767));
    connect(newShortValue, SIGNAL(editingFinished()), this, SLOT(NewValueChanged()));

    updateNewValueSlider = true;
    updateNewValue = true;
}


void device::ConfigVarShortItem::AddWidgets()
{
    layout->addWidget(currentShortValue);

    if (slider != NULL)
        layout->addWidget(slider);

    layout->addWidget(newShortValue);
}


void device::ConfigVarShortItem::NewValueChanged()
{
    if (updateNewValueSlider)
    {
        if (slider != NULL)
        {
            short v = newShortValue->text().toInt();
            updateNewValue = false;
            if (v > sliderMax)
            {
                newShortValue->setText(QString::number(sliderMax));
                slider->setValue(slider->maximum());
            }
            else if (v < sliderMin)
            {
                newShortValue->setText(QString::number(sliderMin));
                slider->setValue(slider->minimum());
            }
            else
            {
                short slidervalue = (v - sliderMin)*(slider->maximum() - slider->minimum())/(sliderMax- sliderMin);
                slider->setValue((short)slidervalue);
            }
            updateNewValue = true;
        }

        if (newShortValue->text().toShort() == currentShortValue->text().toShort())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarShortItem::RangedNewValueChanged(int sliderpos)
{
    if (updateNewValue)
    {
        short value = (sliderpos - slider->minimum())*(sliderMax- sliderMin)/(slider->maximum() - slider->minimum());
        updateNewValueSlider = false;
        newShortValue->setText(QString::number(value + sliderMin));
        updateNewValueSlider = true;

        if (newShortValue->text().toInt() == currentShortValue->text().toInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarShortItem::InternalUpdateItemFromVariable(bool reset)
{
    if (variable != NULL)
    {
        VariableTemplate<short>* varShort = (VariableTemplate<short>*)variable;
        std::stringstream ss;
        ss << std::get<0>(varShort->GetValue());
        std::string str = ss.str();
        currentShortValue->setText(QString::fromStdString(str));
        if (varShort->updating)
            currentShortValue->setStyleSheet("QLabel {color : blue;}");
        else
            currentShortValue->setStyleSheet("QLabel {color : black;}");
        if (newShortValue->text() == "" || reset)
        {
            newShortValue->setText(QString::fromStdString(str));
            if (slider != NULL)
            {
                int slidervalue = (std::get<0>(varShort->GetValue()) - sliderMin)*(slider->maximum() - slider->minimum())/(sliderMax- sliderMin);
                updateNewValue = false;
                slider->setValue((int)slidervalue);
                updateNewValue = true;
            }
        }

        if (newShortValue->text().toShort() == currentShortValue->text().toShort())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


bool device::ConfigVarShortItem::UpdateVariableFromItem()
{
    if (variable != NULL)
    {
        VariableTemplate<short>* varShort = (VariableTemplate<short>*)variable;
        if (newShortValue->text() != "")
            varShort->SetNewValue(newShortValue->text().toInt());
        else
            varShort->SetNewValue(std::get<0>(varShort->GetValue()));
        return true;
    }
    else
        return false;
}


void device::ConfigVarShortItem::InternalApply()
{
    if (UpdateVariableFromItem())
        ConfigVarItem::InternalApply();
}


device::ConfigVarShortItem::~ConfigVarShortItem()
{
}

