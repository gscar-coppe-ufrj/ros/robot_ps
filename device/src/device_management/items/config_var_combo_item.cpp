// config_var_combo_item.cpp



#include <device/device_management/items/config_var_combo_item.h>



device::ConfigVarComboItem::ConfigVarComboItem(Variable* variable) : ConfigVarItem(variable)
{
    currentSelection = new QLabel;

    combo = new QComboBox;
    connect(combo, SIGNAL(currentIndexChanged(int)), this, SLOT(IndexChanged(int)));
}


void device::ConfigVarComboItem::AddWidgets()
{
    layout->addWidget(currentSelection);
    layout->addWidget(combo);
}


void device::ConfigVarComboItem::IndexChanged(int i)
{
    if (currentSelection->text() == combo->currentText())
        name->setStyleSheet("QLabel {font: normal}");
    else
        name->setStyleSheet("QLabel {font: italic}");
}


void device::ConfigVarComboItem::InternalUpdateItemFromVariable(bool reset)
{
    if (variable != NULL)
    {
        VariableCombo* varcombo = (VariableCombo*)variable;
        currentSelection->setText(QString::fromStdString(varcombo->GetElementString(varcombo->GetSelectedElementNumber())));
        if (varcombo->updating)
            currentSelection->setStyleSheet("QLabel {color : blue;}");
        else
            currentSelection->setStyleSheet("QLabel {color : black;}");
        if (combo->count() == 0 || reset)
        {
            unsigned int size = varcombo->GetSize();
            if (size > 0)
            {
                combo->clear();
                for (unsigned int i = 1; i <= size; i++)
                    combo->addItem(QString::fromStdString(varcombo->GetElementString(i)));
                unsigned int n = varcombo->GetSelectedElementNumber();
                currentSelection->setText(QString::fromStdString(varcombo->GetElementString(n)));
                combo->setCurrentIndex(n - 1);
            }
        }

        if (currentSelection->text() == combo->currentText())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


bool device::ConfigVarComboItem::UpdateVariableFromItem()
{
    if (variable != NULL)
    {
        VariableCombo* varcombo = (VariableCombo*)variable;
        if (combo->count() > 0)
            varcombo->SetNewSelection(combo->currentIndex() + 1);
        else
            varcombo->SetNewSelection(varcombo->GetSelectedElementNumber());
        return true;
    }
    else
        return false;
}


void device::ConfigVarComboItem::InternalApply()
{
    if (UpdateVariableFromItem())
        ConfigVarItem::InternalApply();
}


device::ConfigVarComboItem::~ConfigVarComboItem()
{
}

