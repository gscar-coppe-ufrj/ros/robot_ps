// config_var_u_int_item.cpp



#include <sstream>
#include <QIntValidator>
#include <device/device_management/items/config_var_u_int_item.h>



device::ConfigVarUIntItem::ConfigVarUIntItem(Variable* variable) : ConfigVarItem(variable)
{
    currentUIntValue = new QLabel;

    if (((VariableTemplate<unsigned int>*)variable)->IsRanged()[0])
    {
        slider = new QSlider(Qt::Horizontal);
        sliderMax = std::get<0>(((VariableTemplate<unsigned int>*)variable)->GetMaxValue());
        sliderMin = std::get<0>(((VariableTemplate<unsigned int>*)variable)->GetMinValue());
        connect(slider, SIGNAL(valueChanged(int)), this, SLOT(RangedNewValueChanged(int)));
    }
    else
        slider = NULL;

    newUIntValue = new QLineEdit;
    newUIntValue->setValidator(new QIntValidator);
    connect(newUIntValue, SIGNAL(editingFinished()), this, SLOT(NewValueChanged()));

    updateNewValueSlider = true;
    updateNewValue = true;
}


void device::ConfigVarUIntItem::AddWidgets()
{
    layout->addWidget(currentUIntValue);

    if (slider != NULL)
        layout->addWidget(slider);

    layout->addWidget(newUIntValue);
}


void device::ConfigVarUIntItem::NewValueChanged()
{
    if (updateNewValueSlider)
    {
        if (slider != NULL)
        {
            unsigned int v = newUIntValue->text().toUInt();
            updateNewValue = false;
            if (v > sliderMax)
            {
                newUIntValue->setText(QString::number(sliderMax));
                slider->setValue(slider->maximum());
            }
            else if (v < sliderMin)
            {
                newUIntValue->setText(QString::number(sliderMin));
                slider->setValue(slider->minimum());
            }
            else
            {
                unsigned int slidervalue = (v - sliderMin)*(slider->maximum() - slider->minimum())/(sliderMax- sliderMin);
                slider->setValue((unsigned int)slidervalue);
            }
            updateNewValue = true;
        }

        if (newUIntValue->text().toUInt() == currentUIntValue->text().toUInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarUIntItem::RangedNewValueChanged(int sliderpos)
{
    if (sliderpos < 0)
        sliderpos = 0;

    if (updateNewValue)
    {
        unsigned int value = (sliderpos - slider->minimum())*(sliderMax- sliderMin)/(slider->maximum() - slider->minimum());
        updateNewValueSlider = false;
        newUIntValue->setText(QString::number(value + sliderMin));
        updateNewValueSlider = true;

        if (newUIntValue->text().toUInt() == currentUIntValue->text().toUInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


void device::ConfigVarUIntItem::InternalUpdateItemFromVariable(bool reset)
{
    if (variable != NULL)
    {
        VariableTemplate<unsigned int>* varUInt = (VariableTemplate<unsigned int>*)variable;
        std::stringstream ss;
        ss << std::get<0>(varUInt->GetValue());
        std::string str = ss.str();
        currentUIntValue->setText(QString::fromStdString(str));
        if (varUInt->updating)
            currentUIntValue->setStyleSheet("QLabel {color : blue;}");
        else
            currentUIntValue->setStyleSheet("QLabel {color : black;}");
        if (newUIntValue->text() == "" || reset)
        {
            newUIntValue->setText(QString::fromStdString(str));
            if (slider != NULL)
            {
                // a bug may happen because of the conversion of the int (slider) to unsigned int (everything else)
                unsigned int slidervalue = (std::get<0>(varUInt->GetValue()) - sliderMin)*(slider->maximum() - slider->minimum())/(sliderMax- sliderMin);
                updateNewValue = false;
                slider->setValue((unsigned int)slidervalue);
                updateNewValue = true;
            }
        }

        if (newUIntValue->text().toUInt() == currentUIntValue->text().toUInt())
            name->setStyleSheet("QLabel {font: normal}");
        else
            name->setStyleSheet("QLabel {font: italic}");
    }
}


bool device::ConfigVarUIntItem::UpdateVariableFromItem()
{
    if (variable != NULL)
    {
        VariableTemplate<unsigned int>* varUInt = (VariableTemplate<unsigned int>*)variable;
        if (newUIntValue->text() != "")
            varUInt->SetNewValue(newUIntValue->text().toUInt());
        else
            varUInt->SetNewValue(std::get<0>(varUInt->GetValue()));
        return true;
    }
    else
        return false;
}


void device::ConfigVarUIntItem::InternalApply()
{
    if (UpdateVariableFromItem())
        ConfigVarItem::InternalApply();
}


device::ConfigVarUIntItem::~ConfigVarUIntItem()
{
}

