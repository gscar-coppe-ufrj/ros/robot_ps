// device_on_off_management_qwidget.cpp



#include <device/device_management/device_on_off_management_qwidget.h>


device::DeviceOnOffManagementQWidget::DeviceOnOffManagementQWidget()
{
    sizeHintW = 320; //sets the width of QWidget
    sizeHintH = 200; //sets the height of QWidget

    itensQWidget = new QWidget;
    itensArea = new QScrollArea;
    ver = new QVBoxLayout;
    ver->setMargin(0);
    ver->setSpacing(0);
    itensLayout = new QVBoxLayout;
    itensLayout->setMargin(0);
    itensLayout->setSpacing(0);

    setLayout(ver);

    QObject::connect(this, SIGNAL(FillingTool()), this, SLOT(TreatFillingTool()), Qt::QueuedConnection);
    QObject::connect(this,SIGNAL(RemovingTool(int)),this, SLOT(TreatRemovingTool(int)), Qt::BlockingQueuedConnection);

    ver->addWidget(itensArea, 1);
    itensArea->setWidgetResizable(true);
    itensArea->setWidget(itensQWidget);
    itensQWidget->setLayout(itensLayout);
    itensLayout->setAlignment(Qt::AlignTop);
}


QSize device::DeviceOnOffManagementQWidget::sizeHint() const
{
    return QSize(sizeHintW, sizeHintH);
}


device::OnOffItem* device::DeviceOnOffManagementQWidget::NewItemFromVar(Variable* var)
{
    OnOffItem* i = new OnOffItem(var);
    var->Update();

    onOffItems.push_back(i);
    itensLayout->addWidget(i);

    return i;
}


void device::DeviceOnOffManagementQWidget::FillTool()
{
    for (std::deque<OnOffItem*>::iterator i = onOffItems.begin(); i != onOffItems.end(); i++)
        delete (OnOffItem*)(*i); //deletes all existing OnOffItens
    onOffItems.clear();
    for (std::deque<NameItem*>::iterator i = devicesNames.begin(); i != devicesNames.end(); i++)
        delete (NameItem*)(*i); //deletes all existing names
    devicesNames.clear();

    for (auto d = deviceOnOffList.begin(); d != deviceOnOffList.end(); d++)
        //re-adds all devices that exist in this instance
    {
        NameItem* deviceName = new NameItem(QString::fromStdString((*d)->name));
        deviceName->deviceOnOff = *d; //tells to the NameItem which device it names
        deviceName->setStyleSheet("QLabel {font: bold}"); //set the display as bold
        devicesNames.push_back(deviceName);
        itensLayout->addWidget(deviceName, 0, Qt::AlignCenter);

        for (std::deque<Variable*>::iterator var = (*d)->onOffVariables.begin(); var != (*d)->onOffVariables.end(); var++)
            //re-adds all devices of this instance to interface
        {
            if ((*var)->IsEditable()) //deviceonoff is editable by default when it's created
            {
                OnOffItem* i = NewItemFromVar(*var);
                if (i !=NULL)
                    i->deviceOnOff = *d; //tells to the item to which device it belongs
            }
        }
    }
}

void device::DeviceOnOffManagementQWidget::RemoveTool(int index)
{
    delete devicesNames[index];
    delete onOffItems[index];

    devicesNames.erase(devicesNames.begin() + index);
    onOffItems.erase(onOffItems.begin() + index);

}


void device::DeviceOnOffManagementQWidget::TreatAddDeviceOnOff(device::DeviceOnOff* c)
{
    //    deviceOnOffList.clear();
    //    auto list = GetListOfComponents<DeviceOnOffManagement, DeviceOnOff>();
    //    if (list != NULL)
    //        std::copy(list->begin(), list->end(), std::back_inserter(deviceOnOffList));
    int index = 0;
    if (std::find(deviceOnOffList.begin(), deviceOnOffList.end(), c) == deviceOnOffList.end())
    {
        for (int i = 0; i < deviceOnOffList.size(); i++)
        {
            if (!(deviceOnOffList.at(i)->name < c->name))
                break;
            index++;
        }
        deviceOnOffList.insert(deviceOnOffList.begin()+index, c);
    }

    emit FillingTool();
}


void device::DeviceOnOffManagementQWidget::TreatRemoveDeviceOnOff(device::DeviceOnOff* c)
{
    auto found = std::find(deviceOnOffList.begin(), deviceOnOffList.end(), c);
    int index = found - deviceOnOffList.begin();
    if (found != deviceOnOffList.end())
    {
        deviceOnOffList.erase(found);
        emit RemovingTool(index);
    }
}


void device::DeviceOnOffManagementQWidget::FindComponents()
{
    deviceOnOffList.clear();

    DeviceOnOffManagement::FindComponents();

    auto list = GetListOfComponents<DeviceOnOffManagement, DeviceOnOff>();
    for (auto c : (*list))
    {
        int index = 0;
        for (int i = 0; i < deviceOnOffList.size(); i++)
        {
            if (!(deviceOnOffList.at(i)->name < c->name))
                break;
            index++;
        }
        deviceOnOffList.insert(deviceOnOffList.begin()+index, c);
    }

//    if (list != NULL)
//        std::copy(list->begin(), list->end(), std::back_inserter(deviceOnOffList));

    emit FillingTool();
}


void device::DeviceOnOffManagementQWidget::TreatFillingTool()
{
    FillTool();
}

void device::DeviceOnOffManagementQWidget::TreatRemovingTool(int index)
{
    RemoveTool(index);
}

