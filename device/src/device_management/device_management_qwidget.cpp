// device_management_qwidget.cpp



#include <device/device_management/device_management_qwidget.h>



device::DeviceManagementQWidget::DeviceManagementQWidget()
{
    sizeHintW = 200;
    sizeHintH = 200;

    comboDevices = new QComboBox;
    itensQWidget = new QWidget;
    itensArea = new QScrollArea;
    cancel = new QPushButton;
    apply = new QPushButton;
    ver = new QVBoxLayout;
    devicesLayout = new QHBoxLayout;
    itensLayout = new QVBoxLayout;
    buttonsLayout = new QHBoxLayout;

    currentSelectedDevice = NULL;

    currentSelectedDeviceIndex = 0;

    redoingComboDevices = false;

    comboDevices->setSizeAdjustPolicy(QComboBox::AdjustToContents);

    QObject::connect(this, SIGNAL(EmitSelectDevice(int)), this, SLOT(SelectDevice(int)));

    QObject::connect(comboDevices, SIGNAL(currentIndexChanged(int)), this, SLOT(SelectDevice(int)));

    QObject::connect(apply, SIGNAL(clicked()), this, SLOT(Apply()));
    QObject::connect(cancel, SIGNAL(clicked()), this, SLOT(Cancel()));

    setLayout(ver);
    cancel->setText("Cancel");
    apply->setText("Apply");

    ver->addLayout(devicesLayout);
    devicesLayout->addWidget(comboDevices , 0, Qt::AlignTop);
    devicesLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
    ver->addWidget(itensArea, 1);
    itensArea->setWidgetResizable(true);
    itensArea->setWidget(itensQWidget);
    itensQWidget->setLayout(itensLayout);
    itensLayout->setAlignment(Qt::AlignTop);
    // The Item objects should be added in the itenslayout
    ver->addLayout(buttonsLayout);
    buttonsLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding));
    buttonsLayout->addWidget(cancel);
    buttonsLayout->addWidget(apply);
}


QSize device::DeviceManagementQWidget::sizeHint() const
{
    return QSize(sizeHintW, sizeHintH);
}


void device::DeviceManagementQWidget::DeviceApply(Variable* variable)
{
    currentSelectedDevice->UpdateVariable(variable);
}


void device::DeviceManagementQWidget::Apply()
{
    for (std::deque<ConfigVarItem*>::iterator item = configVarItems.begin(); item != configVarItems.end(); item++)
        (*item)->UpdateVariableFromItem();

    if (currentSelectedDevice != NULL)
        currentSelectedDevice->UpdateAllVariables();
}


void device::DeviceManagementQWidget::Cancel()
{
    for (std::deque<ConfigVarItem*>::iterator item = configVarItems.begin(); item != configVarItems.end(); item++)
        (*item)->UpdateItemFromVariable(true);
}


device::ConfigVarItem* device::DeviceManagementQWidget::NewItemFromVar(Variable* var)
{
    ConfigVarItem* i = NULL;
    switch (var->GetType())
    {
    case Variable::VariableTypes::typeChar:
        i = new ConfigVarCharItem(var);
        break;
    case Variable::VariableTypes::typeUnsignedChar:
        i = new ConfigVarUCharItem(var);
        break;
    case Variable::VariableTypes::typeShort:
        i = new ConfigVarShortItem(var);
        break;
    case Variable::VariableTypes::typeUnsignedShort:
        i = new ConfigVarUShortItem(var);
        break;
    case Variable::VariableTypes::typeInt:
        i = new ConfigVarIntItem(var);
        break;
    case Variable::VariableTypes::typeUnsignedInt:
        i = new ConfigVarUIntItem(var);
        break;
    case Variable::VariableTypes::typeDouble:
        i = new ConfigVarDoubleItem(var);
        break;
    case Variable::VariableTypes::typeBool:
        i = new ConfigVarBoolItem(var);
        break;
    case Variable::VariableTypes::typeCombo:
        i = new ConfigVarComboItem(var);
        break;
    case Variable::VariableTypes::typeMultiSelectList:
        i = new ConfigVarMultiSelectListItem(var);
        break;
    case Variable::VariableTypes::typeInfoList:
        i = new ConfigVarInfoListItem(var);
        break;
    case Variable::VariableTypes::typeImage:
        i = new ConfigVarImageItem(var);
        break;
    case Variable::VariableTypes::typeGroup:
        i = new ConfigVarGroupItem(var);
        break;
    case Variable::VariableTypes::typeVector:
        i = new ConfigVarVectorItem((VariableVector*)var);
        break;
    default:
        i = new ConfigVarNullItem(var);
        break;
    }
    if (i != NULL)
    {
        var->Update();

        i->OrganizeWidget();
        configVarItems.push_back(i);
        itensLayout->addWidget(i);
    }
    return i;
}


void device::DeviceManagementQWidget::CheckSelection()
{
    comboDevices->clear();
    comboDevices->addItem("");

    for (auto i = listDevices.begin(); i != listDevices.end(); i++)
        comboDevices->addItem(QString::fromStdString((*i)->name));

    auto found = std::find(listDevices.begin(), listDevices.end(), currentSelectedDevice);
    if (found != listDevices.end())
    {
        int index = found - listDevices.begin();
        currentSelectedDeviceIndex = index + 1;
        comboDevices->setCurrentIndex(currentSelectedDeviceIndex);
    }
    else
        emit EmitSelectDevice(0);
}


void device::DeviceManagementQWidget::SelectDevice(int index)
{
    int oldCurrentSelectedDeviceIndex = currentSelectedDeviceIndex;
    currentSelectedDeviceIndex = index;

    if (index > 0)
    {
        index -= 1;
        currentSelectedDevice = listDevices[index];

        if (oldCurrentSelectedDeviceIndex != currentSelectedDeviceIndex)
        {
            for (std::deque<ConfigVarItem*>::iterator i = configVarItems.begin(); i != configVarItems.end(); i++)
                delete (ConfigVarItem*)(*i);
            configVarItems.clear();

            Device* d = currentSelectedDevice;
            for (std::deque<Variable*>::iterator var = d->variables.begin(); var != d->variables.end(); var++)
            {
                if ((*var)->IsEditable())
                {
                    ConfigVarItem* i = NewItemFromVar(*var);
                    if (i != NULL)
                        QObject::connect(i, SIGNAL(Apply(Variable*)), this, SLOT(DeviceApply(Variable*)));
                }
            }
        }
    }
    else if (index == 0)
    {
        currentSelectedDevice = NULL;

        for (std::deque<ConfigVarItem*>::iterator i = configVarItems.begin(); i != configVarItems.end(); i++)
            delete (ConfigVarItem*)(*i);
        configVarItems.clear();
    }
}


void device::DeviceManagementQWidget::TreatAddDevice(device::Device *c)
{
    if (std::find(listDevices.begin(), listDevices.end(), c) == listDevices.end())
    {
        int index = 0;
        for (int i = 0; i < listDevices.size(); i++)
        {
            if (!(listDevices.at(i)->name < c->name))
                break;
            index++;
        }
        listDevices.insert(listDevices.begin()+index, c);
    }

    CheckSelection();
}


void device::DeviceManagementQWidget::TreatRemoveDevice(device::Device *c)
{
    auto found = std::find(listDevices.begin(), listDevices.end(), c);
    listDevices.erase(found);
    if (currentSelectedDevice == c)
        emit EmitSelectDevice(0);

    CheckSelection();
}


void device::DeviceManagementQWidget::FindComponents()
{
    DeviceManagement::FindComponents();

    listDevices.clear();
    auto list = GetListOfComponents<DeviceManagement, Device>();

    for (int i = 0; i < list->size(); i++)
    {
        int index = 0;
        Device* c = (*list)[i];
        for (int i = 0; i < listDevices.size(); i++)
        {
            if (!(listDevices.at(i)->name < c->name))
                break;
            index++;
        }
        listDevices.insert(listDevices.begin()+index, c);
    }


    CheckSelection();
}

void device::DeviceManagementQWidget::ShowGroup(bool show, device::ConfigVarGroupItem* group)
{
    auto groupFound = std::find(configVarItems.begin(), configVarItems.end(), (device::ConfigVarItem*)group);
    if (groupFound != configVarItems.end())
    {
        auto i = groupFound;
        for (i++; i != configVarItems.end(); i++)
        {
            if (dynamic_cast<device::ConfigVarGroupItem*>(*i) != NULL)
                break;
            (*i)->setHidden(!show);
        }
    }
}


