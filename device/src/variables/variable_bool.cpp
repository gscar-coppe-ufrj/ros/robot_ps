// variablebool.cpp



#include <device/variables/variable_bool.h>



device::VariableBool::VariableBool(std::string name, std::string unit, bool editable) : Variable(name, unit, editable)
{
    type = VariableTypes::typeBool;

    state = false;
    newState = false;

    trueValue1 = "";
    trueValue2 = "";
    falseValue1 = "";
    falseValue2 = "";
}


void device::VariableBool::SetTrueString1(std::string trueValue1)
{
    this->trueValue1 = trueValue1;
}


std::string device::VariableBool::GetTrueString1()
{
    return trueValue1;
}


void device::VariableBool::SetTrueString2(std::string trueValue2)
{
    this->trueValue2 = trueValue2;
}


std::string device::VariableBool::GetTrueString2()
{
    return trueValue2;
}


void device::VariableBool::SetFalseString1(std::string falseValue1)
{
    this->falseValue1 = falseValue1;
}


std::string device::VariableBool::GetFalseString1()
{
    return falseValue1;
}


void device::VariableBool::SetFalseString2(std::string falseValue2)
{
    this->falseValue2 = falseValue2;
}


std::string device::VariableBool::GetFalseString2()
{
    return falseValue2;
}


void device::VariableBool::SetState(bool state)
{
    this->state = state;
}


bool device::VariableBool::GetState()
{
    return state;
}


void device::VariableBool::SetNewState(bool newState)
{
    this->newState = newState;
}


bool device::VariableBool::GetNewState()
{
    return newState;
}


device::VariableBool::~VariableBool()
{
}


