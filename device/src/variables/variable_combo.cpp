// variablecombo.cpp



#include <device/variables/variable_combo.h>




device::VariableCombo::VariableCombo(std::string name, std::string unit, bool editable) : Variable(name, unit, editable)
{
    type = VariableTypes::typeCombo;

    elementSelected = elements.end();
}


unsigned int device::VariableCombo::GetSize()
{
    return elements.size();
}


void device::VariableCombo::AddElement(std::string e)
{
    elements.push_back(e);
    if (elements.size() == 1)
        elementSelected = elements.begin();
}


std::string device::VariableCombo::GetElementString(unsigned int i)
{
    if (i != 0 && i <= elements.size())
    {
        std::deque<std::string>::iterator it = elements.begin() + (i - 1);
        return *it;
    }
    else
        return "";
}


void device::VariableCombo::SelectElement(unsigned int i)
{
    if (i != 0 && i <= elements.size())
        elementSelected = elements.begin() + (i - 1);
}


unsigned int device::VariableCombo::GetSelectedElementNumber()
{
    std::deque<std::string>::iterator e = elements.begin();
    unsigned int i = 1;
    while (e != elements.end())
    {
        if (e == elementSelected)
            return i;
        e++;
        i++;
    }
    return 0;
}


void device::VariableCombo::SetNewSelection(unsigned int newI)
{
    if (newI != 0 && newI <= elements.size())
        newElementSelection = elements.begin() + (newI - 1);
}


unsigned int device::VariableCombo::GetNewSelection()
{
    std::deque<std::string>::iterator e = elements.begin();
    unsigned int i = 1;
    while (e != elements.end())
    {
        if (e == newElementSelection)
            return i;
        e++;
        i++;
    }
    return 0;
}


device::VariableCombo::~VariableCombo()
{
}


