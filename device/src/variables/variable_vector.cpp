#include <device/variables/variable_vector.h>



device::VariableVector::VariableVector(int size, std::string name, std::vector<std::string> names = std::vector<std::string>(), std::vector<std::string> units = std::vector<std::string>(), bool editable = true):
    Variable(name, "", editable)
{
    this->size = size;
    this->names = names;
    this->units = units;
    this->editable = editable;
    type = VariableTypes::typeVector;

    updating = false;

    for (unsigned int i = 0; i < size; i++)
    {
        vector.push_back(0);
        newVector.push_back(0);
        Range r;
        ranges.push_back(r);
    }
}

device::VariableVector::VariableVector(int size, std::string name, std::string unit, bool editable = true):
    Variable(name, unit, editable)
{
    this->size = size;
    this->editable = editable;
    type = VariableTypes::typeVector;

    updating = false;

    for (unsigned int i = 0; i < size; i++)
    {
        names.push_back(name + " " + std::to_string(i));
        units.push_back(unit);
        vector.push_back(0);
        newVector.push_back(0);
        Range r;
        ranges.push_back(r);
    }
}


void device::VariableVector::SetNames(int index, std::string name)
{
    if (index < names.size())
        names[index] = name;
}

std::string device::VariableVector::GetName(int index)
{
    if (index < names.size())
        return names[index];
    else
        return "";
}

std::vector<std::string> device::VariableVector::GetNames()
{
    return names;
}

void device::VariableVector::SetUnits(int index, std::string unit)
{
    if (index < units.size())
        units[index] = unit;
}

double device::VariableVector::GetValue(int index)
{
    if (index < vector.size())
        return vector[index];
    else
        return 0;
}

std::vector<double> device::VariableVector::GetVector()
{
    return vector;
}

double device::VariableVector::GetNewValue(int index)
{
    if (index < newVector.size())
        return newVector[index];
    else
        return 0;
}

std::vector<double> device::VariableVector::GetNewVector()
{
    return newVector;
}

void device::VariableVector::SetValue(int index, double value)
{
    if (index < vector.size())
        vector[index] = value;
}

void device::VariableVector::SetVector(std::vector<double> vec)
{
    if (vec.size() == vector.size())
        vector = vec;
}

void device::VariableVector::SetNewValue(int index, double value)
{
    if (index < vector.size())
        newVector[index] = value;
}

int device::VariableVector::GetSize()
{
    return size;
}

void device::VariableVector::SetRange(int index, double max, double min)
{
    Range r;
    r.max = max;
    r.min = min;
    ranges[index] = r;
}

void device::VariableVector::SetRange(double max, double min)
{
    for (unsigned int i; i < size; i++)
    {
        Range r;
        r.max = max;
        r.min = min;
        ranges[i] = r;
    }
}

bool device::VariableVector::IsRanged()
{
    return false;
}
