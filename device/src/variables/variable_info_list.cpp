// variable_info_list.cpp



#include <device/variables/variable_info_list.h>




device::VariableInfoList::VariableInfoList(std::string name, std::string unit, bool editable) : Variable(name, unit, editable)
{
    type = VariableTypes::typeInfoList;
}


device::VariableInfoList::~VariableInfoList()
{
}

