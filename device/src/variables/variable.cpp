// variable.cpp



#include <algorithm>
#include <device/variables/variable.h>



device::Variable::Variable(std::string name, std::string unit, bool editable, bool plottable)
{
    this->name =  name;
    this->unit =  unit;
    this->editable = editable;
    this->plottable = plottable;
    type = VariableTypes::typeNull;

    updating = false;
}


std::string device::Variable::GetName()
{
    return name;
}


void device::Variable::SetName(std::string n)
{
    name = n;
}


std::string device::Variable::GetUnit()
{
    return unit;
}


void device::Variable::SetUnit(std::string u)
{
    unit = u;
}


bool device::Variable::IsEditable()
{
    return editable;
}

bool device::Variable::IsPlottable()
{
    return plottable;
}

void device::Variable::SetPlottable(bool value)
{
    plottable = value;
}

unsigned char device::Variable::GetType()
{
    return type;
}


void device::Variable::AddItem(Item* item)
{
    std::deque<Item*>::iterator itemit = std::find(items.begin(), items.end(), item);
    if (itemit == items.end())
        items.push_back(item);
}


void device::Variable::RemoveItem(Item* item)
{
    std::deque<Item*>::iterator itemit = std::find(items.begin(), items.end(), item);
    if (itemit != items.end())
        items.erase(itemit);
}


void device::Variable::Update()
{
    for (std::deque<Item*>::iterator item = items.begin(); item != items.end(); item++)
        (*item)->UpdateItemFromVariable();
}


device::Variable::~Variable()
{
}


