// variable_image.cpp



#include <device/variables/variable_image.h>




device::VariableImage::VariableImage(std::string name, std::string unit, bool editable) : Variable(name, unit, editable)
{
    type = VariableTypes::typeImage;
}


device::VariableImage::~VariableImage()
{
}

