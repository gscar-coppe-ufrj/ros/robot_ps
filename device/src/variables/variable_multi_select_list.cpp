// VariableMultiSelectList.cpp



#include <device/variables/variable_multi_select_list.h>



device::VariableMultiSelectList::VariableMultiSelectList(std::string name, std::string unit, bool editable) : Variable(name, unit, editable)
{
    type = VariableTypes::typeMultiSelectList;
}


device::VariableMultiSelectList::~VariableMultiSelectList()
{
}

