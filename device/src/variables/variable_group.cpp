// variable_group.cpp



#include <device/variables/variable_group.h>



device::VariableGroup::VariableGroup(std::string name, std::string unit, bool editable) : Variable(name, unit, editable)
{
    type = VariableTypes::typeGroup;
    this->name = name;
}


