// variable_string.cpp



#include <device/variables/variable_string.h>



device::VariableString::VariableString(std::string name, std::string unit, bool editable) : Variable(name, unit, editable, false)
{
    type = VariableTypes::typeString;
}

void device::VariableString::SetString(std::string str)
{
    value = str;
}

std::string device::VariableString::GetString()
{
    return value;
}

device::VariableString::~VariableString()
{
}

