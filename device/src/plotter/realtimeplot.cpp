// realtimeplot.cpp



#include <device/plotter/realtimeplot.h>
#define SLIDER_MAXIMUM 1000


device::RealTimePlot::RealTimePlot():
    play(true),
    rangePosition(SLIDER_MAXIMUM),
    usingSlider(false)
{
    // Settings
    settings.dataRange = 100;
    settings.visibleRange = 8;
    settings.refreshRate = 10;
    settings.unlimitedData = false;
    settings.tickStep = 2;
    settings.lineWidth = 2;
    settings.fullName = true;

    // Widgets
    sliderRange = new QSlider(Qt::Horizontal, this);
    sliderRange->setRange(0, SLIDER_MAXIMUM);
    sliderRange->setValue(SLIDER_MAXIMUM);

    indexColors =   {0x0072BD, 0xD95319, 0xEDB120, 0x7E2F8E, 0x77AC30, 0x4DBEEE, 0xA2142F};

    customPlot = new QCustomPlot();

    customPlot->xAxis->setTickLabelType(QCPAxis::ltDateTime);
    customPlot->xAxis->setDateTimeFormat("hh:mm:ss");
    customPlot->xAxis->setAutoTickStep(false);
    customPlot->xAxis->setTickStep(settings.tickStep);
    customPlot->axisRect()->setupFullAxesBox();
    customPlot->setInteractions(customPlot->interactions() | QCP::iRangeDrag |
                                QCP::iRangeZoom | QCP::iSelectAxes |
                                QCP::iSelectLegend | QCP::iSelectPlottables);
    customPlot->setContextMenuPolicy(Qt::CustomContextMenu);
    customPlot->legend->setFont(QFont(font().family(), 10));
    customPlot->legend->setSelectedFont(QFont(font().family(), 10));
    customPlot->legend->setIconSize(50, 20);
    customPlot->legend->setVisible(true);
    customPlot->addLayer("selected", customPlot->layer("main"));
    customPlot->show();

    QVBoxLayout* layout = new QVBoxLayout(this);
    layout->addWidget(customPlot);
    layout->addSpacing(-5);
    layout->addWidget(sliderRange);
    layout->setContentsMargins(0,0,0,0);
    layout->setMargin(0);

    connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
    connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));
    connect(sliderRange, SIGNAL(sliderMoved(int)), this, SLOT(SetVisibleRange(int)));
    connect(sliderRange, SIGNAL(sliderPressed()),this, SLOT(SliderPressed()));
    connect(sliderRange, SIGNAL(sliderReleased()),this, SLOT(SliderReleased()));
    connect(customPlot, SIGNAL(selectionChangedByUser()), this, SLOT(SelectionChanged()));
    connect(customPlot, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(ContextMenuRequest(QPoint)));

    connect(customPlot, SIGNAL(mousePress(QMouseEvent*)),this, SLOT(PlotPressed(QMouseEvent*)));
    connect(customPlot, SIGNAL(mouseMove(QMouseEvent*)),this, SLOT(MouseMoved(QMouseEvent*)));
    connect(customPlot, SIGNAL(mouseRelease(QMouseEvent*)),this, SLOT(PlotReleased(QMouseEvent*)));

    customPlot->axisRect()->insetLayout()->setInsetAlignment(0, (Qt::AlignTop|Qt::AlignLeft));

    // Slider style
    QFile file(":/device/qss/plot_slider");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    sliderRange->setStyleSheet(styleSheet);

    //timer to replot
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(Replot()));
    timer->start(1000/settings.refreshRate);
    lastKey = 0;

    line = new QCPItemLine(customPlot);
    line->setVisible(false);
    line->setHead(QCPLineEnding(QCPLineEnding::EndingStyle::esBar));
    line->setTail(QCPLineEnding(QCPLineEnding::EndingStyle::esBar));
    zoomY = false;
    holding = false;

    // layouts to add after the plot axis to put the legend centered
    subLayout = new QCPLayoutGrid;
    QCPLayoutElement *dummyElement = new QCPLayoutElement;
    QCPLayoutElement *dummyElement2 = new QCPLayoutElement;
    QCPLayoutElement *dummyElement3 = new QCPLayoutElement;
    customPlot->plotLayout()->addElement(1, 0, subLayout); // add sub-layout in the cell bellow the main axis rect
    subLayout->addElement(0, 0, dummyElement); // add dummy element on the left
    subLayout->addElement(0, 2, dummyElement2); // add dummy element on the right
    subLayout->addElement(1, 0, dummyElement3); // add element bellow
    customPlot->plotLayout()->setRowStretchFactor(1, 0.01);
    legendOut = false;
}

void device::RealTimePlot::UpdateGraph(QCPGraph* graph, double value)
{
    double key = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
    static bool first = true;
    if (first)
    {
        initialTime = key;
        first = false;
    }
    // add data to line:
    graph->addData(key, value);
    // remove data that's outside data range:
    if (!settings.unlimitedData)
    {
        double removal_point = key - settings.dataRange;
        graph->removeDataBefore(removal_point);
        if (removal_point > initialTime) initialTime = removal_point;
    }
    // rescale value (vertical) axis to fit the current data:
    // customplot->graph(graph)->rescaleValueAxis(true);

    if (rangePosition == sliderRange->maximum())
    {
        customPlot->xAxis->setRange(key+0.25, settings.visibleRange, Qt::AlignRight);
    }
    else
    {
        int newvalue = MapValue(lastDataPosition, initialTime,
                                lastUpdate, sliderRange->minimum(),
                                sliderRange->maximum());
        if (!usingSlider)
            sliderRange->setValue(newvalue);
    }
}

QCPGraph* device::RealTimePlot::NewGraph(std::string name = "")
{
    customPlot->addGraph();
    QCPGraph* graph = customPlot->graph(); // gets the newest graph
    QColor c = QColor(indexColors[lastKey%indexColors.size()]);
    QPen pen = QPen(c);
    pen.setWidth(settings.lineWidth);
    graph->setPen(pen);
    graph->setAntialiasedFill(false);
    graph->setName(QString::fromStdString(name));
    graphMap[graph] = name;
    if (!settings.fullName)
    {
         std::size_t pos = name.find_last_of("/");
         std::string shortName = name.substr(pos+1);
         graph->setName(QString::fromStdString(shortName));
    }
    else
    {
        graph->setName(QString::fromStdString((name)));
    }
    QPen selectedPen = QPen(c, settings.lineWidth, Qt::DashLine);
    graph->setSelectedPen(selectedPen);
    lastKey++;
    if (legendOut)
    {
        int index = customPlot->graphCount();
        customPlot->legend->addElement(0, index, customPlot->legend->itemWithPlottable(graph));
    }
    return graph;
}

void device::RealTimePlot::RemoveGraph(QCPGraph* graph)
{
    graph->clearData();
    auto it = graphMap.find(graph);
    graphMap.erase(it);
    customPlot->removeGraph(graph);
    customPlot->replot();
}

void device::RealTimePlot::FitGraphs()
{
    customPlot->yAxis->setRange(GetLargestRange());
}

void device::RealTimePlot::SetDataRange(double range)
{
    settings.dataRange = range;
}

void device::RealTimePlot::SetVisibleRange(int range)
{
    rangePosition = range;
    double value = MapValue(range, sliderRange->minimum(), sliderRange->maximum(),
                            initialTime, lastUpdate);
    customPlot->xAxis->setRange(value, settings.visibleRange, Qt::AlignRight);
    lastDataPosition = value;
}

void device::RealTimePlot::PlayPause(bool b)
{
    play = !play;
}

void device::RealTimePlot::Configure()
{
    // Update Settings pens
    settings.pens.clear();
    for (int i=0; i<customPlot->graphCount(); i++)
        settings.pens.push_back(customPlot->graph(i)->pen());
    ConfigDialog* cfgdialog = new ConfigDialog(this, settings);
    cfgdialog->setWindowModality(Qt::ApplicationModal);
    connect(cfgdialog, SIGNAL(SendSettings(PlotSettings)),this, SLOT(ApplySettings(PlotSettings)));
    cfgdialog->show();
}

void device::RealTimePlot::Save()
{
    QFileDialog::Options options;
    QString selectedFilter;
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    "Save",
                                                    "~/",
                                                    "PNG (*.png);;JPEG (*.jpg);;BMP (*.bmp);;PDF (*.pdf)",
                                                    &selectedFilter,
                                                    options);
    if (!fileName.isEmpty())
    {
        if (selectedFilter == "PNG (*.png)")
            customPlot->savePng(fileName, 500, 400, 1);
        else if (selectedFilter == "JPEG (*.jpg)")
            customPlot->saveJpg(fileName, 500, 400, 1);
        else if (selectedFilter == "BMP (*.bmp)")
            customPlot->saveBmp(fileName, 500, 400, 1);
        else if (selectedFilter == "PDF (*.pdf)")
            customPlot->savePdf(fileName, 0, 500, 400, "PdfCreator", "Title");
    }
}

void device::RealTimePlot::SliderPressed()
{
    usingSlider = true;
}

void device::RealTimePlot::SliderReleased()
{
    usingSlider = false;
}

void device::RealTimePlot::ApplySettings(PlotSettings newsettings)
{
    settings = newsettings;
    customPlot->xAxis->setTickStep(settings.tickStep);
    for (int i=0; i<customPlot->graphCount(); ++i)
    {
        // Graph line style
        customPlot->graph(i)->setPen(settings.pens[i]);
        QPen selected = settings.pens[i];
        // Selected Pen is always dashed because I want
        selected.setStyle(Qt::DashLine);
        customPlot->graph(i)->setSelectedPen(selected);
        // Full Name
        std::string fullName =  graphMap[customPlot->graph(i)];
        if (!settings.fullName)
        {
             std::size_t pos =fullName.find_last_of("/");
             std::string shortName = fullName.substr(pos+1);
             customPlot->graph(i)->setName(QString::fromStdString(shortName));
        }
        else
        {
            customPlot->graph(i)->setName(QString::fromStdString((fullName)));
        }
    }
    this->timer->setInterval(1000/settings.refreshRate);
}

void device::RealTimePlot::SelectionChanged()
{
    for (int i=0; i<customPlot->graphCount(); ++i)
    {
        QCPGraph *graph = customPlot->graph(i);
        QCPPlottableLegendItem *item = customPlot->legend->itemWithPlottable(graph);
        if (item->selected() || graph->selected())
        {
            item->setSelected(true);
            graph->setSelected(true);
            QPen p = graph->pen();
            p.setStyle(Qt::DashLine);
            graph->setPen(p);
            // bring selected graph to front
            graph->setLayer("selected");
            emit SelectedGraph(graph);
        }
        else
        {
            QPen p = graph->pen();
            p.setStyle(Qt::SolidLine);
            graph->setPen(p);
            graph->setLayer("main");
        }
    }
}

void device::RealTimePlot::ContextMenuRequest(QPoint pos)
{
    QMenu* menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    if (customPlot->legend->selectTest(pos, false) >= 0) // context menu on legend requested
    {
        if (customPlot->selectedLegends().size() > 0)
        {
            menu->addAction("Remove selected graph", this, SLOT(RequestRemove()));
        }
        menu->addAction("Move to top left", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignLeft));
        menu->addAction("Move to top center", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignHCenter));
        menu->addAction("Move to top right", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignRight));
        menu->addAction("Move to bottom right", this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignRight));
        menu->addAction("Move to bottom left", this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignLeft));
        menu->addAction("Move out", this, SLOT(MoveOut()));
        menu->popup(customPlot->mapToGlobal(pos));
    }
}

void device::RealTimePlot::moveLegend()
{
    LegendPosition(false);
    // make sure this slot is really called by a context menu action, so it carries the data we need
    if (QAction* contextAction = qobject_cast<QAction*>(sender()))
    {
        bool ok;
        int dataInt = contextAction->data().toInt(&ok);
        if (ok)
        {

            customPlot->axisRect()->insetLayout()->addElement(customPlot->legend, Qt::AlignRight|Qt::AlignTop);
            //customPlot->axisRect()->insetLayout()->addElement(customPlot->legend);
            customPlot->axisRect()->insetLayout()->setInsetAlignment(0, (Qt::Alignment)dataInt);
            customPlot->replot();
        }
    }
}

void device::RealTimePlot::PlotPressed(QMouseEvent* ev)
{
    if(zoomY)
    {
        if (ev->buttons() == Qt::RightButton)
        {
            line->setVisible(true);
            double x = customPlot->xAxis->pixelToCoord(ev->pos().x());
            double y = customPlot->yAxis->pixelToCoord(ev->pos().y());
            line->start->setCoords(x,y);
            line->end->setCoords(x,y);
            holding = true;
        }
    }
}

void device::RealTimePlot::MouseMoved(QMouseEvent *ev)
{
    if(zoomY)
    {
        if (ev->buttons() == Qt::RightButton)
        {
            //double x = customPlot->xAxis->pixelToCoord(ev->pos().x());
            //double y = customPlot->yAxis->pixelToCoord(ev->pos().y());
            //line->end->setCoords(x,y);
            //line->start->setCoords(x,line->start->coords().y());
        }
    }
}

void device::RealTimePlot::PlotReleased(QMouseEvent *ev)
{
    if(zoomY)
    {
        if (ev->button() == Qt::RightButton)
        {
            line->setVisible(false);
            double y_start = line->start->coords().y();
            double y_end = line->end->coords().y();
            customPlot->yAxis->setRange(y_start, y_end);
            holding = false;
        }
    }
}

void device::RealTimePlot::RequestRemove()
{
    emit RemoveSignal(customPlot->selectedGraphs());
}

QCPRange device::RealTimePlot::GetLargestRange()
{
    QCPRange largest_range;
    std::vector<QCPRange> ranges;
    for (int i = 0; i < customPlot->graphCount(); i++)
    {
        bool haveLower = false;
        bool haveUpper = false;
        QCPRange range;
        double current;
        QCPDataMap* data = customPlot->graph(i)->data();
        QCPDataMap::const_iterator it = data->constBegin();
        while (it != data->constEnd())
        {
            current = it.value().value;
            if (!qIsNaN(current))
            {
                if (current < range.lower || !haveLower)
                {
                    range.lower = current;
                    haveLower = true;
                }
                if (current > range.upper || !haveUpper)
                {
                    range.upper = current;
                    haveUpper = true;
                }
            }
            it++;
        }
        ranges.push_back(range);
        if ( i == 0 || (range.lower < (ranges.at(i-1)).lower))
            largest_range.lower = range.lower;
        if ( i == 0 || (range.upper > (ranges.at(i-1).upper)))
            largest_range.upper = range.upper;
    }
    return largest_range;
}

double device::RealTimePlot::MapValue(double value, double min_orig,
                                      double max_orig, double min_dest, double max_dest)
{
    return (value - min_orig) / (max_orig - min_orig) * (max_dest - min_dest) + min_dest ;
}

std::vector<QRgb> device::RealTimePlot::loadColors()
{
    std::ifstream file;
    file.open(":/qss/colors.txt");
    std::vector<QRgb> colors;
    int number;
    while(file >> std::hex >> number)
    {
        colors.push_back(number);
        file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << number << std::endl;
    }
}

void device::RealTimePlot::Replot()
{
    lastUpdate = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
    if (zoomY && holding)
    {
        QPoint p = customPlot->mapFromGlobal(QCursor::pos());
        QPointF end = QPointF(line->start->pixelPoint().x(),p.y());
        line->end->setPixelPoint(end);
    }
    if (play)
        customPlot->replot();
}

void device::RealTimePlot::ActivateZoomY(bool active)
{
    zoomY = active;
}

void device::RealTimePlot::MoveOut()
{
    LegendPosition(true);
}

void device::RealTimePlot::LegendPosition(bool inOut)
{
    legendOut = inOut;

    QList<QCPAbstractLegendItem*> items;
    for (int i = 0; i < customPlot->graphCount(); i++)
    {
       QCPAbstractLegendItem* item(customPlot->legend->itemWithPlottable(customPlot->graph(i)));
       items.append(item);
       customPlot->legend->take(item);
    }

    if (!inOut)
    {
        customPlot->axisRect()->insetLayout()->addElement(customPlot->legend, Qt::AlignRight|Qt::AlignTop);
        for (int i = 0; i < customPlot->graphCount(); i++)
            customPlot->legend->addElement(i,0,items.at(i));
    }
    else
    {
        for (int i = 0; i < customPlot->graphCount(); i++)
        {
            //if (!(customPlot->legend->element(0,i) == customPlot->legend->item(i)))
            customPlot->legend->addElement(0,i,items.at(i));//customPlot->legend->item(i));
        }
        subLayout->addElement(0, 1, customPlot->legend); // add legend
        customPlot->legend->setColumnStretchFactor(0, 0.01);
        subLayout->setColumnStretchFactor(0,100);
        subLayout->setColumnStretchFactor(2,100);
    }
    //subLayout->setRowStretchFactor(0, 0.01); // make legend cell (in row 0) take up as little vertical space as possible
    customPlot->removeGraph(customPlot->addGraph()); // trick to fix layout indefinetely increasing
}

device::RealTimePlot::~RealTimePlot()
{
}
