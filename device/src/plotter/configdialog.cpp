// configdialog.cpp



#include <device/plotter/configdialog.h>



device::ConfigDialog::ConfigDialog(QWidget* parent, PlotSettings settings) : QDialog (parent, Qt::Dialog), mSettings(settings)
{
    indexColors =   {0x0072BD,0xD95319,0xEDB120, 0x7E2F8E, 0x77AC30, 0x4DBEEE, 0xA2142F};
    QVBoxLayout* mainLayout = new QVBoxLayout(this);

    // General group
    QGroupBox* generalBox = new QGroupBox("General Settings",this);
    QGridLayout* layout = new QGridLayout();
    QHBoxLayout* lytButtons = new QHBoxLayout;
    QPushButton* btOk = new QPushButton("Ok");
    QPushButton* btCancel = new QPushButton("Cancel");
    QLabel* lbVisibleRange = new QLabel("Visible Range");
    QLabel* lbDataRange = new QLabel("Data Range");
    QLabel* lbRefreshRate = new QLabel("Refresh Rate");
    QLabel* lbTickStep = new QLabel("Tick Step");
    QLabel* lbLineWidth = new QLabel("Default line width");
    leDataRange = new QLineEdit(this);
    leRefreshRate = new QLineEdit;
    leVisibleRange = new QLineEdit;
    leTickStep = new QLineEdit;
    leLineWidth = new QLineEdit;
    cbUnlimitedData = new QCheckBox("Limit Data to:");
    leVisibleRange->setValidator(new QIntValidator(this));
    leDataRange->setValidator(new QIntValidator(this));
    leRefreshRate->setValidator(new QDoubleValidator(this));
    leRefreshRate->setValidator(new QDoubleValidator(this));
    leTickStep->setValidator(new QIntValidator(this));
    leLineWidth->setValidator(new QIntValidator(this));
    cbFullName = new QCheckBox("Display full name");
    lytButtons->addWidget(btOk, 0, Qt::AlignCenter);
    lytButtons->addWidget(btCancel, 0, Qt::AlignCenter);
    layout->addWidget(lbVisibleRange, 0, 0);
    layout->addWidget(leVisibleRange, 0, 1);
    layout->addWidget(cbUnlimitedData, 1, 0,1,2);
    layout->addWidget(lbDataRange, 2, 0);
    layout->addWidget(leDataRange, 2, 1);
    layout->addWidget(lbRefreshRate, 3, 0);
    layout->addWidget(leRefreshRate, 3, 1);
    layout->addWidget(lbTickStep, 4, 0);
    layout->addWidget(leTickStep, 4, 1);
    layout->addWidget(lbLineWidth, 5, 0);
    layout->addWidget(leLineWidth, 5, 1);
    layout->addWidget(cbFullName, 6, 0, 1, 2);

    // Style Group
    QGroupBox* styleBox = new QGroupBox("Style Settings",this);
    QLabel* lbProperties = new QLabel("Properties for graph:",this);
    QLabel* lblineStyle = new QLabel("Line Style");
    QLabel* lblineWidth= new QLabel("Line Width");
    QLabel* lbColor = new QLabel("Color");
    QGridLayout* styleLayout = new QGridLayout;
    btColor = new QToolButton;
    btColor->setFixedSize(48,24);


    QMenu* colorMenu = new QMenu;

    for (QRgb color : indexColors)
    {
        QPixmap pixmap(16,16);
        pixmap.fill(QColor(color));
        QIcon colorIcon(pixmap);
        actions.push_back(colorMenu->addAction(colorIcon, ""));
    }
    actions.push_back(colorMenu->addAction("More Colors"));
    btColor->setMenu(colorMenu);
    cblineNumber = new QComboBox;
    cblineStyle = new QComboBox;
    leLineWidthNumber = new QLineEdit;
    leLineWidthNumber->setValidator(new QDoubleValidator(this));

    for (int i = 0; i < settings.pens.size(); i++)
        cblineNumber->addItem(QString::number(i));

    cblineStyle->addItem("Qt::SolidLine");
    cblineStyle->addItem("Qt::DashLine");
    cblineStyle->addItem("Qt::DotLine");
    cblineStyle->addItem("Qt::DashDotLine");
    cblineStyle->addItem("Qt::DashDotDotLine");
    cblineStyle->addItem("Qt::NoPen");

    styleLayout->addWidget(lbProperties,0,0);
    styleLayout->addWidget(cblineNumber,0,1);
    styleLayout->addWidget(lblineStyle,1,0);
    styleLayout->addWidget(cblineStyle,1,1);
    styleLayout->addWidget(lbColor, 2, 0);
    styleLayout->addWidget(btColor, 2, 1);
    styleLayout->addWidget(lblineWidth,3,0);
    styleLayout->addWidget(leLineWidthNumber,3,1);

    generalBox->setLayout(layout);
    styleBox->setLayout(styleLayout);
    mainLayout->addWidget(generalBox);
    mainLayout->addWidget(styleBox);
    mainLayout->addLayout(lytButtons);

    LoadSettings(settings);

    connect(btOk, SIGNAL(clicked(bool)), this, SLOT(OkPressed()));
    connect(btCancel, SIGNAL(clicked(bool)), this, SLOT(CancelPressed()));
    connect(cbUnlimitedData, SIGNAL(clicked(bool)), this, SLOT(UnlimitedPressed(bool)));
    connect(cblineNumber, SIGNAL(currentIndexChanged(int)), this, SLOT(ChangeLine(int)));
    connect(colorMenu, SIGNAL(triggered(QAction*)), this, SLOT(ColorPressed(QAction*)));
    connect(leLineWidthNumber, SIGNAL(textChanged(QString)),this, SLOT(LineWidthChanged(QString)));
    connect(cblineStyle, SIGNAL(activated(int)),this, SLOT(StyleChanged(int)));
    connect(btColor, SIGNAL(clicked(bool)),btColor,SLOT(showMenu()));

}

void device::ConfigDialog::LoadSettings(PlotSettings settings)
{
    // General Settings
    leDataRange->setText(QString::number(settings.dataRange));
    leRefreshRate->setText(QString::number(settings.refreshRate));
    leVisibleRange->setText(QString::number(settings.visibleRange));
    cbUnlimitedData->setChecked(!settings.unlimitedData);
    leTickStep->setText(QString::number(settings.tickStep));
    leLineWidth->setText(QString::number(settings.lineWidth));
    cbFullName->setChecked(settings.fullName);
    ChangeLine(0);
}

void device::ConfigDialog::OkPressed()
{
    mSettings.dataRange = leDataRange->text().toDouble();
    mSettings.refreshRate = leRefreshRate->text().toDouble();
    mSettings.visibleRange = leVisibleRange->text().toDouble();
    mSettings.unlimitedData = !cbUnlimitedData->isChecked();
    mSettings.tickStep = leTickStep->text().toInt();
    mSettings.lineWidth = leLineWidth->text().toInt();
    mSettings.fullName = cbFullName->isChecked();
    emit SendSettings(mSettings);
    this->close();
}

void device::ConfigDialog::CancelPressed()
{
    this->close();
}

void device::ConfigDialog::UnlimitedPressed(bool check)
{
    leDataRange->setDisabled(!check);
}

void device::ConfigDialog::ColorPressed(QAction* a)
{
    if (mSettings.pens.size() > 0)
    {
        QColor c;
        if (a == actions.at(0))
            c = QColor(indexColors[0]);
        else if (a == actions.at(1))
            c = QColor(indexColors[1]);
        else if (a == actions.at(2))
            c = QColor(indexColors[2]);
        else if (a == actions.at(3))
            c = QColor(indexColors[3]);
        else if (a == actions.at(4))
            c = QColor(indexColors[4]);
        else if (a == actions.at(5))
            c = QColor(indexColors[5]);
        else if (a == actions.at(6))
            c = QColor(indexColors[6]);
        else if (a == actions.at(7))
            c = QColorDialog::getColor();

        QPixmap pixmap(16,16);
        pixmap.fill(c);
        QIcon colorIcon(pixmap);
        btColor->setIcon(colorIcon);
        mSettings.pens.at(cblineNumber->currentIndex()).setColor(c);
    }
}

void device::ConfigDialog::ChangeLine(int line)
{
    if (mSettings.pens.size() > 0)
    {
        QPen p = mSettings.pens.at(line);
        leLineWidthNumber->setText(QString::number(p.widthF()));
        QPixmap pixmap(16,16);
        pixmap.fill(p.color());
        QIcon colorIcon(pixmap);
        btColor->setIcon(colorIcon);
        switch (p.style())
        {
        case Qt::SolidLine:
            cblineStyle->setCurrentIndex(0);
            break;
        case Qt::DashLine:
            cblineStyle->setCurrentIndex(1);
            break;
        case Qt::DotLine:
            cblineStyle->setCurrentIndex(2);
            break;
        case Qt::DashDotLine:
            cblineStyle->setCurrentIndex(3);
            break;
        case Qt::DashDotDotLine:
            cblineStyle->setCurrentIndex(4);
            break;
        case Qt::NoPen:
            cblineStyle->setCurrentIndex(5);
            break;
        }

    }
}

void device::ConfigDialog::LineWidthChanged(QString width)
{
    if (mSettings.pens.size() > 0)
        mSettings.pens.at(cblineNumber->currentIndex()).setWidthF(width.toFloat());
}

void device::ConfigDialog::StyleChanged(int style)
{
    if (mSettings.pens.size() > 0)
    {
        switch (style)
        {
        case 0:
            mSettings.pens.at(cblineNumber->currentIndex()).setStyle(Qt::SolidLine);
            break;
        case 1:
            mSettings.pens.at(cblineNumber->currentIndex()).setStyle(Qt::DashLine);
            break;
        case 2:
            mSettings.pens.at(cblineNumber->currentIndex()).setStyle(Qt::DotLine);
            break;
        case 3:
            mSettings.pens.at(cblineNumber->currentIndex()).setStyle(Qt::DashDotLine);
            break;
        case 4:
            mSettings.pens.at(cblineNumber->currentIndex()).setStyle(Qt::DashDotDotLine);
            break;
        case 5:
            mSettings.pens.at(cblineNumber->currentIndex()).setStyle(Qt::NoPen);
            break;
        }
    }
}

device::ConfigDialog::~ConfigDialog()
{

}
