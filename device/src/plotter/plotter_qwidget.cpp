    // plotter_qwidget.cpp



#include <device/plotter/plotter_qwidget.h>
#include <device/data_table/device_signal.h>



device::PlotterQWidget::PlotterQWidget()
{
    //Window
    window = new QMainWindow;
    toolbar = new QToolBar;
    toolbarButtons = new QToolBar;

    //Icons
    iconAdd = QIcon(":/device/add-icon");
    iconRemove = QIcon(":/device/minus");
    iconPlay = QIcon(":/device/icon_play");
    iconPause = QIcon(":/device/icon_pause");
    iconFit = QIcon(":/device/fit_icon");
    iconConfigure = QIcon(":/device/icon_configure");

    //Widgets
    varSelection = new QComboBox(this);
    varSelection->setEditable(true);
    plotWidget = new RealTimePlot;
    layout = new QVBoxLayout(this);
    addRemove = new QPushButton("Add");

    stringModel = new QStringListModel();
    stringModel->setStringList(items);

    proxyModel = new QSortFilterProxyModel;
    proxyModel->setSourceModel(stringModel);
    proxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);

    proxyModelCompleter = new QSortFilterProxyModel;
    proxyModelCompleter->setSourceModel(stringModel);
    proxyModelCompleter->setFilterCaseSensitivity(Qt::CaseInsensitive);

    varSelection->setModel(proxyModel);
    completer = new QCompleter(proxyModelCompleter,this);
    completer->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
    varSelection->setCompleter(completer);
    QObject::connect(varSelection->lineEdit(), SIGNAL(textEdited(QString)), this, SLOT(onTextChanged(QString)));
    QObject::connect(completer, SIGNAL(activated(const QString &)), this, SLOT(onCompleterActivated(const QString &)));

    buttonPlayPause = new QToolButton(this);
    buttonFit = new QToolButton(this);
    buttonConfigure = new QToolButton(this);
    buttonSave = new QToolButton(this);
    buttonZoomY = new QToolButton(this);

    addRemove->setIcon(iconAdd);
    addRemove->setIconSize(QSize(16,16));
    addRemove->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    varSelection->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
    toolbar->addWidget(varSelection);
    toolbar->addWidget(addRemove);
    toolbar->setAllowedAreas(Qt::TopToolBarArea | Qt::BottomToolBarArea);

    buttonPlayPause->setIcon(iconPause);
    buttonPlayPause->setCheckable(true);
    buttonFit->setIcon(iconFit);
    buttonConfigure->setIcon(iconConfigure);
    buttonSave->setIcon(QIcon(":/device/icon_save"));
    buttonZoomY->setIcon(QIcon(":/device/zoom_select"));
    buttonSave->setCheckable(false);
    buttonZoomY->setCheckable(true);

    toolbarButtons->addWidget(buttonPlayPause);
    toolbarButtons->addWidget(buttonFit);
    toolbarButtons->addWidget(buttonConfigure);
    toolbarButtons->addWidget(buttonSave);
    toolbarButtons->addWidget(buttonZoomY);

    window->addToolBar(Qt::TopToolBarArea, toolbar);
    window->addToolBarBreak();
    window->addToolBar(Qt::TopToolBarArea, toolbarButtons);
    window->setCentralWidget(plotWidget);
    layout->addWidget(window);
    setLayout(layout);

    connect(buttonFit, SIGNAL(clicked(bool)), plotWidget, SLOT(FitGraphs()));
    connect(buttonPlayPause, SIGNAL(clicked(bool)), plotWidget, SLOT(PlayPause(bool)));
    connect(buttonPlayPause, SIGNAL(clicked(bool)), this, SLOT(PlayPause(bool)));
    connect(buttonConfigure, SIGNAL(clicked(bool)), plotWidget, SLOT(Configure()));
    connect(buttonSave, SIGNAL(clicked(bool)), plotWidget, SLOT(Save()));
    connect(buttonZoomY, SIGNAL(clicked(bool)), plotWidget, SLOT(ActivateZoomY(bool)));

    connect(addRemove,SIGNAL(clicked()),this, SLOT(AddGraphSlot()));
    connect(varSelection,SIGNAL(activated(int)), this, SLOT(VarSelected(int)));
    connect(plotWidget, SIGNAL(SelectedGraph(QCPGraph*)), this, SLOT(ChangeSelection(QCPGraph*)));
    connect(plotWidget, SIGNAL(RemoveSignal(QList<QCPGraph*>)), this, SLOT(SlotRemoveByGraph(QList<QCPGraph*>)));
    varSelection->setCurrentIndex(-1);
    LoadSettings();
}

void device::PlotterQWidget::onTextChanged(QString Text)
{
    QStringList Items = stringModel->stringList();
    QString Item;
    foreach(Item,Items)
    {
        if (Item.indexOf(Text) > -1)
        {
            proxyModelCompleter->setFilterFixedString(Text);
            return;
        }
    }
}

void device::PlotterQWidget::onCompleterActivated(const QString &index)
{
    varSelection->setCurrentIndex(items.indexOf(index));
}

void device::PlotterQWidget::AddGraphSlot()
{
    int index = varSelection->currentIndex();
    AddGraph(index);
}

void device::PlotterQWidget::FindComponents()
{
    device::Plotter::FindComponents();

    auto list = GetListOfComponents<Plotter, Device>();

    for (auto iter = list->begin(); iter != list->end(); iter++)
    {
        auto deviceSignal = new DeviceSignal;
        connect(deviceSignal, SIGNAL(SendDevice(Device*)), this, SLOT(SlotAddVariablesDevice(Device*)));
        emit deviceSignal->SendDevice(*iter);
        delete deviceSignal;
    }
}

void device::PlotterQWidget::TreatAddDevice(device::Device *c)
{
    Plotter::TreatAddDevice(c);

    auto list = GetListOfComponents<Plotter, Device>();
    auto found = std::find(list->begin(), list->end(), c);

    if (found != list->end())
    {
        auto deviceSignal = new DeviceSignal;
        connect(deviceSignal, SIGNAL(SendDevice(Device*)), this, SLOT(SlotAddVariablesDevice(Device*)), Qt::BlockingQueuedConnection);
        emit deviceSignal->SendDevice(c);
        delete deviceSignal;
    }
    LoadLastSection();
}

void device::PlotterQWidget::TreatRemoveDevice(device::Device *c)
{
    Plotter::TreatRemoveDevice(c);

    auto deviceSignal = new DeviceSignal;
    connect(deviceSignal, SIGNAL(SendDevice(Device*)), this, SLOT(SlotRemoveVariablesDevice(Device*)), Qt::BlockingQueuedConnection);
    emit deviceSignal->SendDevice(c);
    delete deviceSignal;
}


void device::PlotterQWidget::SlotAddVariablesDevice(device::Device *d)
{   
    varSelection->blockSignals(true);
    for(auto variableit = d->variables.begin(); variableit < d->variables.end() ; variableit++)
    {
        Variable* var = (*variableit);
        if (var->IsPlottable())
        {
            switch (var->GetType())
            {
            case (Variable::typeShort):
            case (Variable::typeUnsignedShort):
            case (Variable::typeInt):
            case (Variable::typeUnsignedInt):
            case (Variable::typeDouble):
                std::string varName = d->name + "/" + (*variableit)->GetName();
                VariableSelected new_var;
                new_var.var = *variableit;
                new_var.item = nullptr;
                new_var.selected = false;
                new_var.name = varName;

                int index = 0;
                for (int i = 0; i < varDeque.size(); i++)
                {
                    if (!(varDeque.at(i).name < new_var.name))
                        break;
                    index++;
                }

                // Insert in alphabetical order
                varDeque.insert(varDeque.begin()+index, new_var);
                items.insert(index, QString::fromStdString(new_var.name));
                stringModel->setStringList(items);
                //varSelection->insertItem(index,QString::fromStdString(new_var.name));
                break;
            }
        }
    }
    varSelection->blockSignals(false);

    LoadLastSection();
}

void device::PlotterQWidget::SlotRemoveVariablesDevice(device::Device *d)
{
    for (auto variableit = d->variables.begin(); variableit < d->variables.end() ; variableit++)
    {
        for (unsigned int i = 0; i < varDeque.size(); i++)
        {
            if( varDeque.at(i).var == (*variableit) )
            {
                delete varDeque.at(i).item;
                varDeque.erase(varDeque.begin()+i);
                items.removeAt(i);
                stringModel->setStringList(items);
                //varSelection->removeItem(i);
            }
        }
    }
}

QSize device::PlotterQWidget::sizeHint() const
{
    return QSize(500,400);
}

void device::PlotterQWidget::SyncSelection()
{
    if (varDeque.at(varSelection->currentIndex()).item != nullptr)
    {
        QCPGraph* graph = varDeque.at(varSelection->currentIndex()).item->GetGraph();
        QCPPlottableLegendItem* item = plotWidget->customPlot->legend->itemWithPlottable(graph);
        plotWidget->customPlot->deselectAll();
        item->setSelected(true);
        graph->setSelected(true);
        graph->setLayer("selected");
        // When selected the graph is dashed
        QPen p = graph->pen();
        p.setStyle(Qt::DashLine);
        graph->setPen(p);
        for (int i=0; i< plotWidget->customPlot->graphCount(); ++i)
        {
            if (plotWidget->customPlot->graph(i) != graph)
            {
                QPen p = plotWidget->customPlot->graph(i)->pen();
                p.setStyle(Qt::SolidLine);
                plotWidget->customPlot->graph(i)->setPen(p);
            }
        }
    }
    else // item isnt created yet so it obviously isnt selected
    {
        plotWidget->customPlot->deselectAll();
        for (int i=0; i< plotWidget->customPlot->graphCount(); ++i)
        {
            QPen p = plotWidget->customPlot->graph(i)->pen();
            p.setStyle(Qt::SolidLine);
            plotWidget->customPlot->graph(i)->setPen(p);
            plotWidget->customPlot->graph(i)->setLayer("main");
        }
    }
}

void device::PlotterQWidget::LoadSettings()
{
    QSettings settings("robot_gui", "plotter");
    if (settings.childGroups().size() > 0)
    {
        settings.beginGroup(settings.childGroups()[0]);
        int size = settings.beginReadArray("variables");
        for (int i = 0; i < size; i++)
        {
            settings.setArrayIndex(i);
            lastSection.push_back(settings.value("name").toString().toStdString());
        }
        settings.endArray();
        settings.remove("");
        settings.endGroup();
    }
}

void device::PlotterQWidget::SaveSettings()
{
    QSettings settings("robot_gui", "plotter");
    //settings.clear();
    QUuid q;
    settings.beginGroup(q.createUuid().toString());
    settings.beginWriteArray("variables");
    int j = 0;
    for (int i = 0; i < varDeque.size(); i++)
    {
        if (varDeque[i].selected)
        {
            settings.setArrayIndex(j);
            j++;
            settings.setValue("name", QString::fromStdString(varDeque[i].name));
        }
    }
    settings.endArray();
    settings.endGroup();
}

void device::PlotterQWidget::AddGraph(int index)
{
    if (index >= 0)
    {

        if (varSelection->currentIndex() != index)
            varSelection->setCurrentIndex(index);
        if ( (varDeque.at(index)).selected == false)
        {
            //Adding
            addRemove->setIcon(iconRemove);
            addRemove->setText("Remove");
            Variable* var = varDeque.at(index).var;
            switch ( (varDeque.at(index).var)->GetType() )
            {
            case Variable::typeDouble:
                varDeque.at(index).item = new PlotItemTemplate<double>((VariableTemplate<double>*)var, plotWidget, varDeque.at(index).name);
                (varDeque.at(index)).selected  = true;
                break;
            case Variable::typeShort:
                varDeque.at(index).item = new PlotItemTemplate<short int>((VariableTemplate<short int>*)var, plotWidget, varDeque.at(index).name);
                (varDeque.at(index)).selected  = true;
                break;
            case Variable::typeUnsignedShort:
                varDeque.at(index).item = new PlotItemTemplate<unsigned short>((VariableTemplate<unsigned short>*)var, plotWidget, varDeque.at(index).name);
                (varDeque.at(index)).selected  = true;
                break;
            case Variable::typeInt:
                varDeque.at(index).item = new PlotItemTemplate<int>((VariableTemplate<int>*) var, plotWidget, varDeque.at(index).name);
                (varDeque.at(index)).selected  = true;
                break;
            case Variable::typeUnsignedInt:
                varDeque.at(index).item = new PlotItemTemplate<unsigned int>((VariableTemplate<unsigned int>*)var, plotWidget, varDeque.at(index).name);
                (varDeque.at(index)).selected  = true;
                break;
            case Variable::typeBool:
                varDeque.at(index).item = new PlotItemBool((VariableBool*)var, plotWidget, varDeque.at(index).name);
                (varDeque.at(index)).selected  = true;
                break;
            }
            SyncSelection();
        }
        else
        {
            //Removing
            delete (varDeque.at(index)).item;
            varDeque.at(index).item = nullptr;
            varDeque.at(index).selected = false;
            addRemove->setText("Add");
            addRemove->setIcon(iconAdd);
        }
    }
}

void device::PlotterQWidget::LoadLastSection()
{
    auto it = lastSection.begin();
    while (it != lastSection.end())
    {
        bool found = false;

        for (int i = 0; i < varDeque.size(); i++)
        {
            if ( (*it) == varDeque[i].name )
            {
                AddGraph(i);
                found = true;
                break;
            }
        }
        if (found)
            it = lastSection.erase(it);
        else
            ++it;
    }
}

void device::PlotterQWidget::VarSelected(int index)
{
    if( (varDeque.at(index)).selected != false)
    {
        addRemove->setIcon(iconRemove);
        addRemove->setText("Remove");
    }
    else
    {
        addRemove->setIcon(iconAdd);
        addRemove->setText("Add");
    }
    SyncSelection();
}

void device::PlotterQWidget::ChangeSelection(QCPGraph *graph)
{
    int index = 0;
    for (VariableSelected var : varDeque )
    {
        if (var.item != nullptr)
        {
            if ( var.item->GetGraph() == graph)
                break;
        }
        index++;
    }
    varSelection->setCurrentIndex(index);
    VarSelected(index);
}

void device::PlotterQWidget::PlayPause(bool b)
{
    if (!b)
        buttonPlayPause->setIcon(iconPause);
    else
        buttonPlayPause->setIcon(iconPlay);
}

void device::PlotterQWidget::SlotRemoveByGraph(QList<QCPGraph*> graphList)
{
    for(int i = 0; i < varDeque.size(); i++)
    {
        if ( varDeque.at(i).item == nullptr ) continue;
        if (graphList.indexOf(varDeque.at(i).item->GetGraph()) != -1)
        {
            delete (varDeque.at(i)).item;
            varDeque.at(i).item = nullptr;
            varDeque.at(i).selected = false;
            if (i = varSelection->currentIndex())
            {
                addRemove->setText("Add");
                addRemove->setIcon(iconAdd);
            }
        }
    }
}

device::PlotterQWidget::~PlotterQWidget()
{
    SaveSettings();
    for(auto it = varDeque.begin(); it != varDeque.end(); it++)
    {
        if((*it).item != nullptr) delete (*it).item;
    }
    delete plotWidget;
    delete window;
}
