// plotitem.cpp



#include <device/plotter/plotitem.h>



device::PlotItem::PlotItem(Variable *var, RealTimePlot *plt, std::string deviceName) : ItemQWidget(var)
{
    plotObject = plt;
    graph = plotObject->NewGraph(deviceName); // + var->GetName()
}

QCPGraph* device::PlotItem::GetGraph() const
{
    return graph;
}

bool device::PlotItem::UpdateVariableFromItem()
{
    return true;
}

device::PlotItem::~PlotItem()
{
    plotObject->RemoveGraph(graph);
}
