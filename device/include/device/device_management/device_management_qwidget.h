// device_management_qwidget.h



#ifndef DEVICE_DEVICE_MANAGEMENT_QWIDGET_H
#define DEVICE_DEVICE_MANAGEMENT_QWIDGET_H



#include <deque>
#include <QWidget>
#include <QComboBox>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QScrollArea>
#include "device_management.h"
#include "items/config_var_null_item.h"
#include "items/config_var_char_item.h"
#include "items/config_var_u_char_item.h"
#include "items/config_var_short_item.h"
#include "items/config_var_u_short_item.h"
#include "items/config_var_int_item.h"
#include "items/config_var_u_int_item.h"
#include "items/config_var_double_item.h"
#include "items/config_var_bool_item.h"
#include "items/config_var_combo_item.h"
#include "items/config_var_multi_select_list_item.h"
#include "items/config_var_info_list_item.h"
#include "items/config_var_image_item.h"
#include "items/config_var_group_item.h"
#include "items/config_var_vector_item.h"



namespace device {



class ConfigVarGroupItem;



class DeviceManagementQWidget : public QWidget, public DeviceManagement
{
    Q_OBJECT

    int sizeHintW;
    int sizeHintH;

    QComboBox* comboDevices;
    QWidget* itensQWidget;
    QScrollArea* itensArea;
    QPushButton* cancel;
    QPushButton* apply;
    QVBoxLayout* ver;
    QHBoxLayout* devicesLayout;
    QVBoxLayout* itensLayout;
    QHBoxLayout* buttonsLayout;

    std::deque<Device*> listDevices;

    Device* currentSelectedDevice;

    int currentSelectedDeviceIndex;

    bool redoingComboDevices;

    std::deque<ConfigVarItem*> configVarItems;

    virtual void TreatAddDevice(Device* c);
    virtual void TreatRemoveDevice(Device* c);

    ConfigVarItem* NewItemFromVar(Variable* var);

    void CheckSelection();

public:
    DeviceManagementQWidget();

    virtual QSize sizeHint() const;

    void FindComponents();

    void ShowGroup(bool show, ConfigVarGroupItem* group);


signals:
    void EmitSelectDevice(int index);

private slots:
    void DeviceApply(Variable* variable);
    void Apply();
    void Cancel();
    void SelectDevice(int index);
};



}



#endif // DEVICE_MANAGEMENT_QWIDGET_H
