// device_on_off_management_qwidget.h



#ifndef DEVICE_DEVICE_ON_OFF_MANAGEMENT_QWIDGET_H
#define DEVICE_DEVICE_ON_OFF_MANAGEMENT_QWIDGET_H



#include <deque>
#include <QWidget>
#include <QComboBox>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include "device_on_off_management.h"
#include "items/on_off_item.h"
#include "items/name_item.h"



namespace device {



class OnOffItem;



class DeviceOnOffManagementQWidget : public QWidget, public DeviceOnOffManagement
{
    Q_OBJECT

    int sizeHintW; //widht of QWidget
    int sizeHintH; //height of QWidget

    QComboBox* comboController; //Creates a dropdown list
    QWidget* itensQWidget; //Reference in http://harmattan-dev.nokia.com/docs/platform-api-reference/xml/daily-docs/libqt4/qwidget.html#details
    QScrollArea* itensArea; //Creates an area that have scroll bars
    QVBoxLayout* ver; //The QVBoxLayout class lines up widgets vertically
    QVBoxLayout* itensLayout; //The QVBoxLayout class lines up widgets vertically

    std::deque<DeviceOnOff*> deviceOnOffList;
    std::deque<NameItem*> devicesNames;
    std::deque<OnOffItem*> onOffItems;

    /**
     * @brief Adds an OnOff item to the Tool.
     */
    OnOffItem* NewItemFromVar(Variable* var);
    void FillTool();
    void RemoveTool(int index);

protected:
    virtual void TreatAddDeviceOnOff(DeviceOnOff* c);
    virtual void TreatRemoveDeviceOnOff(DeviceOnOff* c);

public:
    DeviceOnOffManagementQWidget();

    virtual QSize sizeHint() const;

    void FindComponents(); // it looks for connections and feeds the devicelist and the devicegrouplist

private slots:
    void TreatFillingTool();
    void TreatRemovingTool(int index);

signals:
    void FillingTool();
    void RemovingTool(int index);
};



}



#endif // DEVICE_ON_OFF_MANAGEMENT_QWIDGET_H
