// device_on_off_management.h



#ifndef DEVICE_DEVICE_ON_OFF_MANAGEMENT_H
#define DEVICE_DEVICE_ON_OFF_MANAGEMENT_H



#include <robot_ps_core/tool.h>
#include "../device_on_off.h"



namespace device {



class DeviceOnOff; //notifies the GPTDeviceOnOffManagement that GPCBDeviceOnOff class exists


class DeviceOnOffManagement : public robot_ps::Tool //this class is derived from Tool defined in "RobotGUI/tool.h"
{
protected:
    virtual void TreatAddDeviceOnOff(DeviceOnOff* c){}
    virtual void TreatRemoveDeviceOnOff(DeviceOnOff* c){}

public:
    virtual void FindComponents()
    {
        AddMeToComponents<DeviceOnOffManagement, DeviceOnOff>(&DeviceOnOffManagement::TreatAddDeviceOnOff, &DeviceOnOffManagement::TreatRemoveDeviceOnOff);
    }
};



}



#endif // DEVICE_ON_OFF_MANAGEMENT_H
