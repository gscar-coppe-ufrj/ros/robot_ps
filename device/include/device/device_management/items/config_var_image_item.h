// config_var_image_item.h



#ifndef DEVICE_CONFIG_VAR_IMAGE_ITEM_H
#define DEVICE_CONFIG_VAR_IMAGE_ITEM_H



#include <string>
#include <device/variables/variable_image.h>
#include "config_var_item.h"



namespace device {



class ConfigVarImageItem : public ConfigVarItem
{
Q_OBJECT
    QLabel* label;

    void AddWidgets(){}

public:
    ConfigVarImageItem(Variable* variable);

    void OrganizeWidget();

    void InternalUpdateItemFromVariable(bool reset = false);
    bool UpdateVariableFromItem();

    ~ConfigVarImageItem();

protected slots:
    void InternalApply();
};



}



#endif
