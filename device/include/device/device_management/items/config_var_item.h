// config_var_item.h



#ifndef DEVICE_CONFIG_VAR_ITEM_H
#define DEVICE_CONFIG_VAR_ITEM_H



#include <QHBoxLayout>
#include <QLabel>
#include <QToolButton>
#include <device/item_qwidget.h>



namespace device {



class ConfigVarItem : public ItemQWidget
{
Q_OBJECT

protected:
    QHBoxLayout* layout;

    QLabel* name;
    QToolButton* apply;

    virtual void AddWidgets() = 0;

public:
    ConfigVarItem(Variable* variable);

    virtual void InternalUpdateItemFromVariable(bool reset = false);
    virtual bool UpdateVariableFromItem();

    virtual void OrganizeWidget();

    ~ConfigVarItem();

protected slots:
    virtual void InternalApply();

signals:
    void Apply(Variable* variable);
};



}



#endif // CONFIG_VAR_ITEM_H
