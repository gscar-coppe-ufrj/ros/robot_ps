// config_var_combo_item.h



#ifndef DEVICE_CONFIG_VAR_COMBO_ITEM_H
#define DEVICE_CONFIG_VAR_COMBO_ITEM_H



#include <QLabel>
#include <QComboBox>
#include <device/variables/variable_combo.h>
#include "config_var_item.h"



namespace device {



class ConfigVarComboItem : public ConfigVarItem
{
Q_OBJECT

    QLabel* currentSelection;
    QComboBox* combo;

protected:
    void AddWidgets();

public:
    ConfigVarComboItem(Variable* variable);

    void InternalUpdateItemFromVariable(bool reset = false);
    bool UpdateVariableFromItem();

    ~ConfigVarComboItem();

protected slots:
    void IndexChanged(int i);
    void InternalApply();
};



}



#endif // CONFIG_VAR_COMBO_ITEM_H
