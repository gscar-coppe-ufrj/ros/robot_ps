// config_var_group_item.h



#ifndef DEVICE_CONFIG_VAR_GROUP_ITEM_H
#define DEVICE_CONFIG_VAR_GROUP_ITEM_H



#include <QLabel>
#include <QToolButton>
#include "config_var_item.h"
#include <device/variables/variable_group.h>
#include "../device_management_qwidget.h"



namespace device {



class ConfigVarGroupItem : public ConfigVarItem
{
Q_OBJECT

    QToolButton* maximize;
    QToolButton* minimize;

    void AddWidgets(){}

public:
    ConfigVarGroupItem(Variable* variable);

    void OrganizeWidget();

    void InternalUpdateItemFromVariable(bool reset = false);
    bool UpdateVariableFromItem();

    ~ConfigVarGroupItem();

protected slots:
    void Maximize();
    void Minimize();
    void InternalApply();
};



}



#endif // DEVICE_CONFIG_VAR_GROUP_ITEM_H
