// config_var_u_int_item.h



#ifndef DEVICE_CONFIG_VAR_U_INT_ITEM_H
#define DEVICE_CONFIG_VAR_U_INT_ITEM_H



#include <QSlider>
#include <QLabel>
#include <QLineEdit>
#include <device/variables/variable_template.h>
#include "config_var_item.h"



namespace device {



class ConfigVarUIntItem : public ConfigVarItem
{
Q_OBJECT

    QSlider* slider;
    unsigned int sliderMax;
    unsigned int sliderMin;
    QLabel* currentUIntValue;
    QLineEdit* newUIntValue;

    bool updateNewValueSlider;
    bool updateNewValue;

    void AddWidgets();

public:
    ConfigVarUIntItem(Variable* variable);

    void InternalUpdateItemFromVariable(bool reset = false);
    bool UpdateVariableFromItem();

    ~ConfigVarUIntItem();

protected slots:
    void NewValueChanged();
    void RangedNewValueChanged(int sliderpos);
    void InternalApply();
};



}



#endif // CONFIG_VAR_U_INT_ITEM_H
