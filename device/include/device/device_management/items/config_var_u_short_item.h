// config_var_u_short_item.h



#ifndef DEVICE_CONFIG_VAR_U_SHORT_ITEM_H
#define DEVICE_CONFIG_VAR_U_SHORT_ITEM_H



#include <QSlider>
#include <QLabel>
#include <QLineEdit>
#include <device/variables/variable_template.h>
#include "config_var_item.h"



namespace device {



class ConfigVarUShortItem : public ConfigVarItem
{
Q_OBJECT

    QSlider* slider;
    unsigned int sliderMax;
    unsigned int sliderMin;
    QLabel* currentUShortValue;
    QLineEdit* newUShortValue;

    bool updateNewValueSlider;
    bool updateNewValue;

    void AddWidgets();

public:
    ConfigVarUShortItem(Variable* variable);

    void InternalUpdateItemFromVariable(bool reset = false);
    bool UpdateVariableFromItem();

    ~ConfigVarUShortItem();

protected slots:
    void NewValueChanged();
    void RangedNewValueChanged(int sliderpos);
    void InternalApply();
};



}



#endif // DEVICE_CONFIG_VAR_U_SHORT_ITEM_H
