// config_var_u_char_item.h



#ifndef DEVICE_CONFIG_VAR_U_CHAR_ITEM_H
#define DEVICE_CONFIG_VAR_U_CHAR_ITEM_H



#include <QSlider>
#include <QLabel>
#include <QLineEdit>
#include <device/variables/variable_template.h>
#include "config_var_item.h"



namespace device {



class ConfigVarUCharItem : public ConfigVarItem
{
Q_OBJECT

    QSlider* slider;
    unsigned char sliderMax;
    unsigned char sliderMin;
    QLabel* currentUCharValue;
    QLineEdit* newUCharValue;

    bool updateNewValueSlider;
    bool updateNewValue;

    void AddWidgets();

public:
    ConfigVarUCharItem(Variable* variable);

    void InternalUpdateItemFromVariable(bool reset = false);
    bool UpdateVariableFromItem();

    ~ConfigVarUCharItem();

protected slots:
    void NewValueChanged();
    void RangedNewValueChanged(int sliderpos);
    void InternalApply();
};



}



#endif // DEVICE_CONFIG_VAR_U_CHAR_ITEM_H
