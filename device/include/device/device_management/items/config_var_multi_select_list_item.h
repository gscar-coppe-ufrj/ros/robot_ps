// config_var_multi_select_list_item.h



#ifndef DEVICE_CONFIG_VAR_MULTI_SELECT_LIST_ITEM_H
#define DEVICE_CONFIG_VAR_MULTI_SELECT_LIST_ITEM_H



#include <QLabel>
#include <QListWidget>
#include <device/variables/variable_multi_select_list.h>
#include "config_var_item.h"



namespace device {



class ConfigVarMultiSelectListItem : public ConfigVarItem
{
Q_OBJECT
    bool firstTime;

    QListWidget* elements;
    QListWidget* selectedElements;
    QListWidget* newElementSelection;

    QToolButton* add;
    QToolButton* remove;

    void AddWidgets(){}

public:
    ConfigVarMultiSelectListItem(Variable* variable);

    void OrganizeWidget();

    void InternalUpdateItemFromVariable(bool reset = false);
    bool UpdateVariableFromItem();

    ~ConfigVarMultiSelectListItem();

protected slots:
    void Add();
    void Remove();
    void ElementsItemChanged(QListWidgetItem* current, QListWidgetItem* previous);
    void SelectedElementsItemChanged(QListWidgetItem* current, QListWidgetItem* previous);
    void NewElementSelectionItemChanged(QListWidgetItem* current, QListWidgetItem* previous);
    void InternalApply();
};



}



#endif
