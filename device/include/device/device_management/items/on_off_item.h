// on_off_item.h



#ifndef DEVICE_ON_OFF_ITEM_H
#define DEVICE_ON_OFF_ITEM_H



#include <QHBoxLayout>
#include <QLabel>
#include <QToolButton>
#include <device/item_qwidget.h>
#include <device/variables/variable_bool.h>
#include "../../device_on_off.h"



#define ON_OFF_ITEM_STATUS_ICON_ON "/usr/share/icons/Humanity/animations/22/brasero-disc-100.svg"
#define ON_OFF_ITEM_STATUS_ICON_OFF "/usr/share/icons/Humanity/animations/22/brasero-disc-00.svg"
#define ON_OFF_ITEM_STATUS_ICON_PLAY "/usr/share/icons/Humanity/actions/24/player_play.svg"
#define ON_OFF_ITEM_STATUS_ICON_STOP "/usr/share/icons/Humanity/actions/24/player_stop.svg"



namespace device {



class OnOffItem : public ItemQWidget
{
Q_OBJECT

    QHBoxLayout* layout;

    QLabel* name;
    QLabel* status;
    QToolButton* button;

    bool currentStatus, fromClick;

private:
    void setState(bool);

public:
    DeviceOnOff* deviceOnOff;

    OnOffItem(Variable* variable);

    void InternalUpdateItemFromVariable(bool reset);
    bool UpdateVariableFromItem();

protected slots:
    void HandleClick();
};



}



#endif // ON_OFF_ITEM_H
