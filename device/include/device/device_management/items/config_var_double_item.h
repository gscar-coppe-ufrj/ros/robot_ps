// config_var_double_item.h



#ifndef DEVICE_CONFIG_VAR_DOUBLE_ITEM_H
#define DEVICE_CONFIG_VAR_DOUBLE_ITEM_H



#include <QSlider>
#include <QLabel>
#include <QLineEdit>
#include <device/variables/variable_template.h>
#include "config_var_item.h"



namespace device {



class ConfigVarDoubleItem : public ConfigVarItem
{
Q_OBJECT

    QSlider* slider;
    double sliderMax;
    double sliderMin;
    QLabel* currentDoubleValue;
    QLineEdit* newDoubleValue;

    bool updateNewValueSlider;
    bool updateNewValue;

    void AddWidgets();

public:
    ConfigVarDoubleItem(Variable* variable);

    void InternalUpdateItemFromVariable(bool reset = false);
    bool UpdateVariableFromItem();

    ~ConfigVarDoubleItem();

protected slots:
    void NewValueChanged();
    void RangedNewValueChanged(int sliderpos);
    void InternalApply();
};



}



#endif // CONFIG_VAR_DOUBLE_ITEM_H
