// config_var_null_item.h



#ifndef DEVICE_CONFIG_VAR_NULL_ITEM_H
#define DEVICE_CONFIG_VAR_NULL_ITEM_H



#include "config_var_item.h"



namespace device {



class ConfigVarNullItem : public ConfigVarItem
{
Q_OBJECT

protected:
    void AddWidgets();

public:
    ConfigVarNullItem(Variable* variable);
    ~ConfigVarNullItem();
};



}



#endif // CONFIG_VAR_NULL_ITEM_H
