// config_var_vector_item.h



#ifndef DEVICE_CONFIG_VAR_VECTOR_ITEM_H
#define DEVICE_CONFIG_VAR_VECTOR_ITEM_H



#include <QSlider>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <device/variables/variable_vector.h>
#include "config_var_item.h"



namespace device {



class ConfigVarVectorItem : public ConfigVarItem
{
Q_OBJECT

    unsigned int size;
    std::vector<QSlider*> slider;
    std::vector<double> sliderMax;
    std::vector<double> sliderMin;
    std::vector<QLabel*> names;
    std::vector<QToolButton*> applys;
    std::vector<QLabel*> currentVectorValue;
    std::vector<QLineEdit*> newVectorValue;

    std::vector<bool> updateNewValueSlider;
    std::vector<bool> updateNewValue;

    void AddWidgets();

public:
    ConfigVarVectorItem(VariableVector* variable);

    void InternalUpdateItemFromVariable(bool reset = false);
    bool UpdateVariableFromItem();
    void HideApply();

    ~ConfigVarVectorItem();

protected slots:
    void NewValueChanged(unsigned int i);
    void RangedNewValueChanged(int sliderpos, int i);
    void InternalApply();
};



}



#endif // CONFIG_VAR_VECTOR_ITEM_H
