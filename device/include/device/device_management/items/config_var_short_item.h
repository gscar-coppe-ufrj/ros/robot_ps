// config_var_short_item.h



#ifndef DEVICE_CONFIG_VAR_SHORT_ITEM_H
#define DEVICE_CONFIG_VAR_SHORT_ITEM_H



#include <QLabel>
#include <QSlider>
#include <QLineEdit>
#include <device/variables/variable_template.h>
#include "config_var_item.h"



namespace device {



class ConfigVarShortItem : public ConfigVarItem
{
Q_OBJECT

    QSlider* slider;
    int sliderMax;
    int sliderMin;
    QLabel* currentShortValue;
    QLineEdit* newShortValue;

    bool updateNewValueSlider;
    bool updateNewValue;

    void AddWidgets();

public:
    ConfigVarShortItem(Variable* variable);

    void InternalUpdateItemFromVariable(bool reset = false);
    bool UpdateVariableFromItem();

    ~ConfigVarShortItem();

protected slots:
    void NewValueChanged();
    void RangedNewValueChanged(int sliderpos);
    void InternalApply();
};



}



#endif // CONFIG_VAR_SHORT_ITEM_H
