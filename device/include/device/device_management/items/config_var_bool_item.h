// configvarboolitem.h



#ifndef DEVICE_CONFIG_VAR_BOOL_ITEM_H
#define DEVICE_CONFIG_VAR_BOOL_ITEM_H



#include <QLabel>
#include <QPushButton>
#include <device/variables/variable_bool.h>
#include "config_var_item.h"



namespace device {



class ConfigVarBoolItem : public ConfigVarItem
{
Q_OBJECT

    bool currentBoolValue;
    QLabel* currentBoolString;
    QPushButton* button;

protected:
    void AddWidgets();

public:
    ConfigVarBoolItem(Variable* variable);

    void InternalUpdateItemFromVariable(bool reset = false);
    bool UpdateVariableFromItem();

    ~ConfigVarBoolItem();

protected slots:
    void ButtonClicked(bool checked);
    void InternalApply();
};



}



#endif // CONFIG_VAR_BOOL_ITEM_H
