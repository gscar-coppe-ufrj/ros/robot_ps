// nameitem.h



#ifndef DEVICE_NAME_ITEM_H
#define DEVICE_NAME_ITEM_H


#include <QLabel>
#include "../../device_on_off.h"



namespace device {



class NameItem : public QLabel
{
public:
    DeviceOnOff* deviceOnOff;

    NameItem(const QString & text) : QLabel(text) {}
};



}



#endif // NAME_ITEM_H
