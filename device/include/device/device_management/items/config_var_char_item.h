// config_var_char_item.h



#ifndef DEVICE_CONFIG_VAR_CHAR_ITEM_H
#define DEVICE_CONFIG_VAR_CHAR_ITEM_H



#include <QLabel>
#include <QSlider>
#include <QLineEdit>
#include <device/variables/variable_template.h>
#include "config_var_item.h"



namespace device {



class ConfigVarCharItem : public ConfigVarItem
{
Q_OBJECT

    QSlider* slider;
    int sliderMax;
    int sliderMin;
    QLabel* currentCharValue;
    QLineEdit* newCharValue;

    bool updateNewValueSlider;
    bool updateNewValue;

    void AddWidgets();

public:
    ConfigVarCharItem(Variable* variable);

    void InternalUpdateItemFromVariable(bool reset = false);
    bool UpdateVariableFromItem();

    ~ConfigVarCharItem();

protected slots:
    void NewValueChanged();
    void RangedNewValueChanged(int sliderpos);
    void InternalApply();
};



}



#endif // DEVICE_CONFIG_VAR_CHAR_ITEM_H
