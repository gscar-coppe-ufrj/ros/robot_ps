// config_var_info_list_item.h



#ifndef DEVICE_CONFIG_VAR_INFO_LIST_ITEM_H
#define DEVICE_CONFIG_VAR_INFO_LIST_ITEM_H



#include <QLabel>
#include <QListWidget>
#include <QTableWidget>
#include <QHeaderView>

#include <device/variables/variable_info_list.h>
#include "config_var_item.h"



namespace device {



class ConfigVarInfoListItem : public ConfigVarItem
{
Q_OBJECT
    bool firstTime;
    bool reselecting;

    QListWidget* list;
    QTableWidget* info;
    QStringList columnHeaders;

    void AddWidgets(){}

public:
    ConfigVarInfoListItem(Variable* variable);

    void OrganizeWidget();

    void InternalUpdateItemFromVariable(bool reset = false);
    bool UpdateVariableFromItem();

    ~ConfigVarInfoListItem();

protected slots:
    void ListItemChanged(QListWidgetItem* current, QListWidgetItem* previous);
    void InternalApply();

signals:
    void NewApply();
};



}



#endif
