// config_var_int_item.h



#ifndef DEVICE_CONFIG_VAR_INT_ITEM_H
#define DEVICE_CONFIG_VAR_INT_ITEM_H



#include <QLabel>
#include <QSlider>
#include <QLineEdit>
#include <device/variables/variable_template.h>
#include "config_var_item.h"



namespace device {



class ConfigVarIntItem : public ConfigVarItem
{
Q_OBJECT

    QSlider* slider;
    int sliderMax;
    int sliderMin;
    QLabel* currentIntValue;
    QLineEdit* newIntValue;

    bool updateNewValueSlider;
    bool updateNewValue;

    void AddWidgets();

public:
    ConfigVarIntItem(Variable* variable);

    void InternalUpdateItemFromVariable(bool reset = false);
    bool UpdateVariableFromItem();

    ~ConfigVarIntItem();

protected slots:
    void NewValueChanged();
    void RangedNewValueChanged(int sliderpos);
    void InternalApply();
};



}



#endif // CONFIG_VAR_INT_ITEM_H
