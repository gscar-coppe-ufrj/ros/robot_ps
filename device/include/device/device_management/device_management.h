// gptdevice_management.h



#ifndef DEVICE_DEVICE_MANAGEMENT_H
#define DEVICE_DEVICE_MANAGEMENT_H



#include <robot_ps_core/tool.h>
#include <device/device.h>




namespace device {



class Device;



class DeviceManagement : public robot_ps::Tool
{
protected:
    virtual void TreatAddDevice(Device* c){}
    virtual void TreatRemoveDevice(Device* c){}

public:
    virtual void FindComponents()
    {
        AddMeToComponents<DeviceManagement, Device>(&DeviceManagement::TreatAddDevice, &DeviceManagement::TreatRemoveDevice);
    }
};



}



#endif // DEVICE_MANAGEMENT_H
