// device_on_off.h



#ifndef DEVICE_DEVICE_ON_OFF_H
#define DEVICE_DEVICE_ON_OFF_H



#include <deque>
#include <device/device.h>
#include <device/variables/variable_bool.h>
#include "device_management/device_on_off_management.h"



namespace device {



class DeviceOnOffManagement;



class DeviceOnOff : public Device
{
protected:
    std::deque<DeviceOnOffManagement*> toolDeviceOnOffManagements;

    VariableBool* deviceOnOff;

    virtual void TreatAddDeviceOnOffManagement(DeviceOnOffManagement* t);
    virtual void TreatRemoveDeviceOnOffManagement(DeviceOnOffManagement* t);

public:
    std::deque<Variable*> onOffVariables;

    DeviceOnOff();

    virtual void FindTools();

    virtual ~DeviceOnOff();
};



}



#endif // DEVICE_ON_OFF_H
