// realtimeplot.h



#ifndef REAL_TIME_PLOT_H
#define REAL_TIME_PLOT_H



#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <map>
#include <QLayout>
#include <QToolButton>
#include <QPushButton>
#include <QSlider>
#include <QFileDialog>
#include <QToolBar>
#include "qcustomplot/qcustomplot.h"
#include "configdialog.h"



namespace device {



class RealTimePlot : public QWidget
{
    Q_OBJECT
    bool play;
    bool usingSlider;
    bool legendOut;
    bool zoomY;
    double lastDataPosition;
    double rangePosition;
    double initialTime;
    double holding;
    double lastUpdate;
    int lastKey;
    std::vector<QRgb> indexColors;
    std::map<QCPGraph*,std::string> graphMap;

    PlotSettings settings;

    QSlider* sliderRange;
    QTimer* timer;
    QCPItemLine* line;
    QCPLayoutGrid* subLayout;

    double MapValue(double value, double min_orig,
                    double max_orig, double min_dest, double max_dest);
    std::vector<QRgb> loadColors();
    QCPRange GetLargestRange();

public:
    RealTimePlot();
    ~RealTimePlot();
    void UpdateGraph(QCPGraph* graph, double value);
    QCPGraph* NewGraph(std::string name);
    void RemoveGraph(QCPGraph* graph);
    QCustomPlot* customPlot;

public slots:
    void SetDataRange(double range);
    void SetVisibleRange(int range);
    void FitGraphs();
    void PlayPause(bool b);
    void Configure();
    void Save();
    void SliderPressed();
    void SliderReleased();
    void ApplySettings(PlotSettings newsettings);
    void SelectionChanged();
    void ContextMenuRequest(QPoint p);
    void RequestRemove();
    void Replot();
    void ActivateZoomY(bool active);
    void MoveOut();
    void LegendPosition(bool inOut);

private slots:
    void moveLegend();
    void PlotPressed(QMouseEvent* ev);
    void MouseMoved(QMouseEvent* ev);
    void PlotReleased(QMouseEvent* ev);

signals:
    void RemoveSignal(QList<QCPGraph*>);
    void SelectedGraph(QCPGraph*);
};



}



#endif
