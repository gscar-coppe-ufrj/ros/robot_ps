// plotter.h



#ifndef DEVICE_PLOTTER_H
#define DEVICE_PLOTTER_H



#include <robot_ps_core/tool.h>
#include "../device.h"



namespace device {



class Device;



class Plotter : public robot_ps::Tool
{
protected:
    virtual void TreatAddDevice(Device* c){}
    virtual void TreatRemoveDevice(Device* c){}

public:
    virtual void FindComponents()
    {
        AddMeToComponents<Plotter, Device>(&Plotter::TreatAddDevice, &Plotter::TreatRemoveDevice);
    }
};



}



#endif // DEVICE_PLOTTER_H
