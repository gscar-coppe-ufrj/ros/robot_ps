// gptplotqwidget.h



#ifndef GPT_PLOT_QWIDGET_H
#define GPT_PLOT_QWIDGET_H



#include <QLayout>
#include <QComboBox>
#include <QPushButton>
#include <QToolBar>
#include <QMainWindow>
#include <QSettings>
#include <QUuid>
#include <device/plotter/plotter.h>
#include "plotitem.h"
#include "realtimeplot.h"



namespace device {



struct VariableSelected
{
    std::string name;
    bool selected;
    Variable* var;
    PlotItem* item;
};

class PlotterQWidget : public QWidget, public Plotter
{
    Q_OBJECT
    std::deque<VariableSelected> varDeque;
    std::vector<std::string> lastSection;
    RealTimePlot* plotWidget;

    QComboBox* varSelection;
    QStringList items;
    QStringListModel* stringModel;
    QSortFilterProxyModel* proxyModel;
    QSortFilterProxyModel* proxyModelCompleter;
    QCompleter* completer;
    QVBoxLayout* layout;
    QHBoxLayout* layoutSelector;
    QPushButton* addRemove;
    QToolBar* toolbar;
    QToolBar* toolbarButtons;
    QToolButton* buttonPlayPause;
    QToolButton* buttonFit;
    QToolButton* buttonConfigure;
    QToolButton* buttonSave;
    QToolButton* buttonZoomY;
    QIcon iconAdd;
    QIcon iconRemove;
    QIcon iconPlay;
    QIcon iconPause;
    QIcon iconFit;
    QIcon iconConfigure;
    QMainWindow* window;

    void SyncSelection();
    void LoadSettings();
    void SaveSettings();

public:
    PlotterQWidget();

    void FindComponents();
    void TreatAddDevice(Device* c);
    void TreatRemoveDevice(Device* c);
    void AddGraph(int index);

    /**
     * @brief LoadLastSection loads the variables from last time and adds them to the plot
     * Is called on FindComponents and TreatAddDevice
     */
    void LoadLastSection();

    virtual QSize sizeHint() const;

    virtual ~PlotterQWidget();

private slots:
    void onTextChanged(QString Text);
    void onCompleterActivated(const QString &index);
    void AddGraphSlot();
    void SlotAddVariablesDevice(Device* d);
    void SlotRemoveVariablesDevice(Device* d);
    void VarSelected(int index);
    void ChangeSelection(QCPGraph* graph);
    void PlayPause(bool);
    void SlotRemoveByGraph(QList<QCPGraph*>);
};



}



#endif
