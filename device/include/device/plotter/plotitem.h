// plotitem.h



#ifndef PLOT_ITEM_H
#define PLOT_ITEM_H


#include <vector>
#include <device/item_qwidget.h>
#include <device/variables/variable_template.h>
#include <device/variables/variable_bool.h>
#include "realtimeplot.h"



namespace device {



class PlotItem: public ItemQWidget
{
protected:
    QCPGraph* graph;
public:
    RealTimePlot* plotObject;

    PlotItem(Variable* var, RealTimePlot* plt, std::string deviceName = "");
    QCPGraph* GetGraph() const;
    virtual void InternalUpdateItemFromVariable(bool reset = false)=0;
    virtual bool UpdateVariableFromItem();

    virtual ~PlotItem();
};



class PlotItemBool: public PlotItem
{
public:
    PlotItemBool(Variable* var, RealTimePlot* plt,std::string deviceName = "") :
        PlotItem(var, plt, deviceName) {}

    virtual void InternalUpdateItemFromVariable(bool reset = false)
    {
        plotObject->UpdateGraph(graph, ((VariableBool*)variable)->GetState());
    }
};

template <typename  T>
class PlotItemTemplate: public PlotItem
{
public:
    PlotItemTemplate(VariableTemplate<T>* var, RealTimePlot* plt,std::string deviceName = "") :
        PlotItem(var, plt, deviceName)
    {
    }

    virtual void InternalUpdateItemFromVariable(bool reset = false)
    {
        plotObject->UpdateGraph(graph, std::get<0>(((VariableTemplate<T>*)variable)->GetValue()));
    }

    virtual bool UpdateVariableFromItem()
    {
        return true;
    }
};



}



#endif // PLOT_ITEM_H
