// configdialog.h



#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H



#include <QDialog>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QValidator>
#include <QTabWidget>
#include <QCheckBox>
#include <QComboBox>
#include <QGroupBox>
#include <QColorDialog>
#include <QToolButton>
#include <QMenu>
#include <iostream>


namespace device {



struct PlotSettings
{
    double dataRange;
    double visibleRange;
    double refreshRate;
    int lineWidth;
    int tickStep;
    bool unlimitedData;
    bool fullName;
    std::vector<QPen> pens;
};

class ConfigDialog : public QDialog
{
    Q_OBJECT

public:
    ConfigDialog(QWidget* parent, PlotSettings settings);
    ~ConfigDialog();

private:
    std::vector<QRgb> indexColors;

    QLineEdit* leVisibleRange;
    QLineEdit* leDataRange;
    QLineEdit* leRefreshRate;
    QLineEdit* leTickStep;
    QLineEdit* leLineWidth;
    QLineEdit* leLineWidthNumber;
    QCheckBox* cbUnlimitedData;
    QComboBox* cblineStyle;
    QComboBox* cblineNumber;
    QCheckBox* cbFullName;
    QToolButton* btColor;
    std::vector<QAction*> actions;

    PlotSettings mSettings;

    void LoadSettings(PlotSettings settings);

private slots:
    void OkPressed();
    void CancelPressed();
    void UnlimitedPressed(bool check);
    void ColorPressed(QAction* a);
    void ChangeLine(int line);
    void LineWidthChanged(QString width);
    void StyleChanged(int style);
signals:
    void SendSettings(PlotSettings settings);
};



}



#endif // CONFIGDIALOG_H
