// item.h



#ifndef DEVICE_ITEM_QWIDGET_H
#define DEVICE_ITEM_QWIDGET_H



#include <QWidget>
#include "item.h"



namespace device {



class ItemQWidget : public QWidget, public Item
{
    Q_OBJECT

public:
    ItemQWidget(Variable* variable);

    virtual void InternalUpdateItemFromVariable(bool reset) = 0;
    void UpdateItemFromVariable(bool reset = false);

    virtual ~ItemQWidget();

private slots:
    void SlotUpdateItemFromVariable(bool reset);

signals:
    void SignalUpdateItemFromVariable(bool reset);
};



}



#endif // ITEM_QWIDGET_H
