// item.h



#ifndef DEVICE_ITEM_H
#define DEVICE_ITEM_H



#include <device/variables/variable.h>



namespace device {



class Variable;



class Item
{
protected:
    Variable* variable;

public:
    Item(Variable* variable);

    virtual void UpdateItemFromVariable(bool reset = false) = 0;
    virtual bool UpdateVariableFromItem() = 0;

    virtual ~Item();
};



}



#endif // ITEM_H
