// variable_string.h



#ifndef DEVICE_VARIABLE_STRING_H
#define DEVICE_VARIABLE_STRING_H



#include <string>
#include "variable.h"



namespace device {



class VariableString : public Variable
{
    std::string value;
public:
    VariableString(std::string name, std::string unit, bool editable);
    void SetString(std::string str);
    std::string GetString();
    ~VariableString();
};



}



#endif
