// variable_info_list.h



#ifndef DEVICE_VARIABLE_INFO_LIST_H
#define DEVICE_VARIABLE_INFO_LIST_H



#include <map>
#include <vector>

#include "variable.h"



namespace device {



class VariableInfoList : public Variable
{

public:
    VariableInfoList(std::string name, std::string unit, bool editable);

    std::vector<std::string> elements;
    std::string elementSelected;
    std::map<std::string, std::string> info;

    ~VariableInfoList();
};



}



#endif // DEVICE_VARIABLE_INFO_LIST_H
