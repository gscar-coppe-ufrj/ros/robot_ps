// variable_group.h



#ifndef DEVICE_VARIABLE_GROUP_H
#define DEVICE_VARIABLE_GROUP_H



#include "variable.h"



namespace device {



class VariableGroup : public Variable
{
public:
    VariableGroup(std::string name, std::string unit, bool editable);
};



}



#endif // VARIABLE_GROUP_H
