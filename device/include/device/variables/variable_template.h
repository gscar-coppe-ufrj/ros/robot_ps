// variable_template.h



#ifndef DEVICE_VARIABLE_TEMPLATE_H
#define DEVICE_VARIABLE_TEMPLATE_H



#include <tuple>
#include <string>
#include <stdexcept>
#include <vector>

#include "variable.h"



namespace device {



template <typename... Types>
class VariableTemplate : public Variable
{
    typedef typename std::tuple<Types...> Tuple;

    public:
        static const unsigned int size = sizeof...(Types);

    private:
        Tuple tupleValue;
        Tuple tupleNewValue;

        bool isRanged[size];

        Tuple ranges[2];


        template <std::size_t index, std::size_t index_max, typename T, typename... Args>
        void SetTupleValue(Tuple &values, T value, Args... args)
        {
            std::get<index>(values) = value;
            SetTupleValue<index + 1, index_max>(values, args...);
        }


        template<std::size_t index, std::size_t indexMax>
        void SetTupleValue(Tuple &values) {}

    public:
        VariableTemplate(std::string name, std::string unit, bool editable, unsigned char type = VariableTypes::typeTemplate) : Variable(name, unit, editable)
        {
            this->type = type;

            for (unsigned int i = 0; i < size; i++)
                isRanged[i] = false;
        }


        void SetValue(Types... args)
        {
            SetTupleValue<0, size>(tupleValue, args...);
        }


        template <std::size_t index, typename T>
        void SetValue(T value)
        {
            std::get<index>(tupleValue) = value;
        }


        void SetNewValue(Types... args)
        {
            SetTupleValue<0, size>(tupleNewValue, args...);
        }


        template <std::size_t index, typename T>
        void SetNewValue(T value)
        {
            std::get<index>(tupleNewValue) = value;
        }


        void SetRanged(std::vector<bool> && flags)
        {
            for (unsigned int i = 0; i < size; i++)
                SetRanged(i, flags[i]);
        }


        void SetRanged(bool* flags)
        {
            SetRanged( std::vector<bool>(flags, flags + size) );
        }


        void SetRanged(bool flagForAll)
        {
            for (unsigned int i = 0; i < size; i++)
                SetRanged(i, flagForAll);
        }


        void SetRanged(unsigned int index, bool flag)
        {
            isRanged[index] = flag;
        }


        void SetMinValue(Types... args)
        {
            SetTupleValue<0, size>(ranges[0], args...);
        }


        template <std::size_t index, typename T>
        void SetMinValue(T value)
        {
            std::get<index>(ranges[0]) = value;
        }


        void SetMaxValue(Types... args)
        {
            SetTupleValue<0, size>(ranges[1], args...);
        }


        template <std::size_t index, typename T>
        void SetMaxValue(T value)
        {
            std::get<index>(ranges[1]) = value;
        }


        const Tuple & GetValue()
        {
            return tupleValue;
        }


        const Tuple & GetNewValue()
        {
            return tupleNewValue;
        }


        const Tuple & GetMinValue()
        {
            return ranges[0];
        }


        const Tuple & GetMaxValue()
        {
            return ranges[1];
        }


        template <unsigned int index>
        typename std::tuple_element<index, Tuple >::type GetValue()
        {
            return std::get<index>(tupleValue);
        }


        template <unsigned int index>
        typename std::tuple_element<index, Tuple >::type GetNewValue()
        {
            return std::get<index>(tupleNewValue);
        }


        template <unsigned int index>
        typename std::tuple_element<index, Tuple >::type GetMinValue()
        {
            return std::get<index>(ranges[0]);
        }


        template <unsigned int index>
        typename std::tuple_element<index, Tuple >::type GetMaxValue()
        {
            return std::get<index>(ranges[1]);
        }


        bool* IsRanged()
        {
            static bool isRangedCopy[size];
            std::copy(isRanged, isRanged + size, isRangedCopy);
            return isRangedCopy;
        }

};



}



#endif // VARIABLE_TEMPLATE_H
