// variable_bool.h



#ifndef DEVICE_VARIABLE_BOOL_H
#define DEVICE_VARIABLE_BOOL_H



#include "variable.h"



namespace device {



class VariableBool : public Variable
{
    bool state;
    bool newState;

    std::string trueValue1;
    std::string trueValue2;
    std::string falseValue1;
    std::string falseValue2;

public:
    VariableBool(std::string name, std::string unit, bool editable);

    void SetTrueString1(std::string trueValue1);
    std::string GetTrueString1();
    void SetTrueString2(std::string trueValue2);
    std::string GetTrueString2();
    void SetFalseString1(std::string falseValue1);
    std::string GetFalseString1();
    void SetFalseString2(std::string falseValue2);
    std::string GetFalseString2();
    void SetState(bool state);
    bool GetState();
    void SetNewState(bool newState);
    bool GetNewState();

    ~VariableBool();
};



}



#endif // VARIABLE_BOOL_H
