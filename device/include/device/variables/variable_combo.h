// variable_combo.h



#ifndef DEVICE_VARIABLE_COMBO_H
#define DEVICE_VARIABLE_COMBO_H



#include "variable.h"



namespace device {



class VariableCombo : public Variable
{
    std::deque<std::string> elements;
    std::deque<std::string>::iterator elementSelected;
    std::deque<std::string>::iterator newElementSelection;

public:
    VariableCombo(std::string name, std::string unit, bool editable);

    unsigned int GetSize();
    void AddElement(std::string e);
    std::string GetElementString(unsigned int i);
    void SelectElement(unsigned int i);
    unsigned int GetSelectedElementNumber();

    void SetNewSelection(unsigned int newI);
    unsigned int GetNewSelection();

    ~VariableCombo();
};



}



#endif // VARIABLE_COMBO_H
