// variable_multi_select_list.h



#ifndef DEVICE_VARIABLE_MULTI_SELECT_LIST_H
#define DEVICE_VARIABLE_MULTI_SELECT_LIST_H



#include <vector>
#include "variable.h"



namespace device {



class VariableMultiSelectList : public Variable
{
public:
    VariableMultiSelectList(std::string name, std::string unit, bool editable);

    std::vector<std::string> elements;
    std::vector<std::string> selectedElements;
    std::vector<std::string> newElementSelection;

    ~VariableMultiSelectList();
};



}



#endif
