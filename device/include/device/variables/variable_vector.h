// variable_vector.h



#ifndef DEVICE_VARIABLE_VECTOR_H
#define DEVICE_VARIABLE_VECTOR_H



#include <vector>
#include "variable.h"



namespace device {



struct Range
{
    double max;
    double min;
};

class VariableVector: public Variable
{
    int size;
    std::vector<std::string> names;
    std::vector<std::string> units;
    std::vector<double> vector;
    std::vector<double> newVector;
    std::vector<Range> ranges;

public:
    VariableVector(int size, std::string name, std::vector<std::string> names,
                   std::vector<std::string> units, bool editable);

    VariableVector(int size, std::string name, std::string unit, bool editable);

    void SetNames(int index, std::string name);

    std::string GetName(int index);
    std::vector<std::string> GetNames();

    void SetUnits(int index, std::string unit);

    double GetValue(int index);
    std::vector<double> GetVector();

    double GetNewValue(int index);
    std::vector<double> GetNewVector();

    void SetValue(int index, double value);
    void SetVector(std::vector<double> vec);

    void SetNewValue(int index, double value);

    int GetSize();

    void SetRange(int index, double max, double min);

    void SetRange(double max, double min);

    bool IsRanged();
};



}



#endif // VARIABLE_VECTOR_H
