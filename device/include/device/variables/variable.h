// variable.h



#ifndef DEVICE_VARIABLE_H
#define DEVICE_VARIABLE_H



#include <string>
#include <deque>
#include <device/item.h>



namespace device {



class Item;



class Variable
{
public:
    enum VariableTypes {
        typeNull,
        typeChar,
        typeUnsignedChar,
        typeShort,
        typeUnsignedShort,
        typeInt,
        typeUnsignedInt,
        typeDouble,
        typeBool,
        typeCombo,
        typeTemplate,
        typeMultiSelectList,
        typeInfoList,
        typeImage,
        typeGroup,
        typeVector,
        typeString
    };

protected:
    std::string name;
    std::string unit;
    bool editable;
    bool plottable;
    unsigned char type;

    std::deque<Item*> items;

public:
    // this variable indicates that the variable value is periodically been update
    bool updating;

    Variable(std::string name, std::string unit, bool editable, bool plottable = true);

    std::string GetName();
    void SetName(std::string n);
    std::string GetUnit();
    void SetUnit(std::string u);
    bool IsEditable();
    bool IsPlottable();
    void SetPlottable(bool value);
    unsigned char GetType();

    void AddItem(Item* item);
    void RemoveItem(Item* item);

    void Update();

    ~Variable();
};



}



#endif // VARIABLE_H
