// variable_image.h



#ifndef DEVICE_VARIABLE_IMAGE_H
#define DEVICE_VARIABLE_IMAGE_H



#include "variable.h"



namespace device {



class VariableImage : public Variable
{
public:
    std::string src;

    VariableImage(std::string name, std::string unit, bool editable);
    ~VariableImage();
};



}



#endif // DEVICE_VARIABLE_IMAGE_H
