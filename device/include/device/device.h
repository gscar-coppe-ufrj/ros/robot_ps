// device.h



#ifndef DEVICE_DEVICE_H
#define DEVICE_DEVICE_H



#include <deque>
#include <robot_ps_core/component.h>
#include <device/device_management/device_management.h>
#include <device/plotter/plotter.h>
#include "data_table/data_table.h"
#include "variables/variable.h"



namespace device {



class DataTable;
class DeviceManagement;
class Plotter;



class Device : public robot_ps::Component
{
protected:
    virtual void TreatAddDataTable(DataTable* t){}
    virtual void TreatRemoveDataTable(DataTable* t){}

    virtual void TreatAddDeviceManagement(DeviceManagement* t){}
    virtual void TreatRemoveDeviceManagement(DeviceManagement* t){}

    virtual void TreatAddPlotter(Plotter* t){}
    virtual void TreatRemovePlotter(Plotter* t){}

public:
    std::deque<Variable*> variables;

    virtual void FindTools()
    {
        AddMeToTools<Device, DataTable>(&Device::TreatAddDataTable, &Device::TreatRemoveDataTable);
        AddMeToTools<Device, DeviceManagement>(&Device::TreatAddDeviceManagement, &Device::TreatRemoveDeviceManagement);
        AddMeToTools<Device, Plotter>(&Device::TreatAddPlotter, &Device::TreatRemovePlotter);
    }

    virtual void UpdateVariable(Variable* variable){}
    virtual void UpdateAllVariables(){}

    virtual ~Device()
    {
        for (std::deque<Variable*>::iterator v = variables.begin(); v != variables.end(); v++)
            delete (*v);
    }
};



}



#endif // DEVICE_H
