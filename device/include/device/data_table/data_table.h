// data_table.h



#ifndef DEVICE_DATA_TABLE_H
#define DEVICE_DATA_TABLE_H



#include <robot_ps_core/tool.h>
#include "../device.h"



namespace device {



class Device;



class DataTable : public robot_ps::Tool
{
protected:
    virtual void TreatAddDevice(Device* c){}
    virtual void TreatRemoveDevice(Device* c){}

public:
    virtual void FindComponents()
    {
        AddMeToComponents<DataTable, Device>(&DataTable::TreatAddDevice, &DataTable::TreatRemoveDevice);
    }
};



}



#endif // DATA_TABLE_H
