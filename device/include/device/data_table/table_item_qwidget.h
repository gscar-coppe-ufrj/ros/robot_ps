// table_item_qwidget.h



#ifndef DEVICE_TABLE_ITEM_QWIDGET_H
#define DEVICE_TABLE_ITEM_QWIDGET_H



#include <QTableWidgetItem>
#include <device/item.h>
#include <device/variables/variable_bool.h>
#include <device/variables/variable_combo.h>
#include <device/variables/variable_template.h>



namespace device {



class TableItemQWidget: public QTableWidgetItem, public Item
{
public:
    TableItemQWidget(Variable* variable);

    virtual void UpdateItemFromVariable(bool reset = false);
    virtual bool UpdateVariableFromItem();

    bool IsEditable();

    virtual ~TableItemQWidget();
};



}



#endif // TABLE_ITEM_QWIDGET_H
