// device_signal.h



#ifndef DEVICE_DEVICE_SIGNAL
#define DEVICE_DEVICE_SIGNAL



#include <QObject>
#include <device/device.h>



namespace device {



class DeviceSignal : public QObject
{
    Q_OBJECT

signals:
    void SendDevice(Device* d);
};



}



#endif // DEVICE_SIGNAL
