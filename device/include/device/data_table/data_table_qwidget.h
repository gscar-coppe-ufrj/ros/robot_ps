// data_table_qwidget.h



#ifndef DEVICE_DATA_TABLE_QWIDGET_H
#define DEVICE_DATA_TABLE_QWIDGET_H



#include <deque>
#include <string>
#include <QRadioButton>
#include <QDialog>
#include <QTableWidget>
#include <QWidget>
#include "data_table.h"
#include "table_item_qwidget.h"



namespace device {



class DataTableQWidget : public QWidget, public DataTable
{
    Q_OBJECT

    int sizeHintW;
    int sizeHintH;

    QTableWidget* table;

    QDialog* config;
    QTableWidget* devicesTable;
    QTableWidget* variablesTable;
    bool generalHidden;
    std::deque<std::string> devicesShown;
    std::deque<std::string> devicesHidden;
    std::deque<std::string> variablesShown;
    std::deque<std::string> variablesHidden;
    std::deque<std::string> variablesOrder;

    //void MyRedoGeometry(QWidget *w);
    //QSize MyGetQTableWidgetSize(QTableWidget *t);

    std::deque<QRadioButton*> NewRadioButtonGroup(int row, std::string name, QTableWidget* table);
    void FillConfigDialog();

    void NormalShowHide();

protected:
    virtual void TreatAddDevice(Device* c);
    virtual void TreatRemoveDevice(Device* c);

public:
    DataTableQWidget();

    virtual QSize sizeHint() const;

    void FindComponents();
    void Init();

signals:
    void SignalAddDevice(Device* d);
    void SignalRemoveDevice(Device* d);

private slots:
    void SlotAddDevice(Device* d);
    void SlotRemoveDevice(Device* d);
    void GeneralShow(bool checked);
    void GeneralHide(bool checked);
    void DeviceNormal(const QString & name);
    void DeviceShow(const QString & name);
    void DeviceHide(const QString & name);
    void VariableNormal(const QString & name);
    void VariableShow(const QString & name);
    void VariableHide(const QString & name);
};



}



#endif // DATA_TABLE_QWIDGET_H
