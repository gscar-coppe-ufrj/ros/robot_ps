// gpcbaudioreceivernodelet.cpp



#include <audio/receiver_nodelet.h>



audio::ReceiverNodelet::ReceiverNodelet()
{
}


std::string audio::ReceiverNodelet::GetName()
{
    return getName();
}


void audio::ReceiverNodelet::onInit()
{
    ros::NodeHandle & privateNH = getPrivateNodeHandle();

    privateNH.param("sender_name", senderName, getName());

    privateNH.param<int>("number_channels", numberChannels, 0);
    privateNH.param<int>("number_bytes", numberBytes, 0);
    privateNH.param<int>("sample_rate", sampleRate, 0);

    std::string plugIn;
    std::string card;
    int dev;
    int subDev;
    privateNH.param<std::string>("plug_in", plugIn, "");
    privateNH.param<std::string>("card", card, "");
    bool devSet = privateNH.getParam((std::string)"dev", dev);
    bool subDevSet = privateNH.getParam((std::string)"sub_dev", subDev);

    if (plugIn != "" || card != "" || devSet || subDevSet)
    {
        std::stringstream ss;
        ss << plugIn;
        if (card != "")
            ss << ":CARD=" << card;
        if (devSet)
            ss << ",DEV=" << dev;
        if (subDevSet)
            ss << ",SUBDEV=" << subDev;
        pcmName = ss.str();
    }

    if (StartUp())
    {
        nodeHandle = getNodeHandle();

        msgRecordingSub = nodeHandle.subscribe(senderName + "/recording", 100, &ReceiverNodelet::RecordingCallback, this);

        msgRecordPub = nodeHandle.advertise<std_msgs::Bool>(senderName + "/record", 100, true);

        Play(true);
    }
}


void audio::ReceiverNodelet::Play(bool play)
{
    if (play && !playing)
    {
        msgAudioSub = nodeHandle.subscribe(senderName + "/stream", 100, &ReceiverNodelet::ReceiveAudioCallback, this);
        Playing(true);
    }
    else if (!play && playing)
    {
        msgAudioSub.shutdown();
        Playing(false);
    }
}


void audio::ReceiverNodelet::Record(bool record)
{
    std_msgs::BoolPtr msg(new std_msgs::Bool);
    msg->data = record;
    msgRecordPub.publish(msg);
}


void audio::ReceiverNodelet::RecordingCallback(const std_msgs::Bool::ConstPtr & recording)
{
    Recording(recording->data);
}


void audio::ReceiverNodelet::ReceiveAudioCallback(const std_msgs::Int16MultiArray::ConstPtr & audioArray)
{
    snd_pcm_prepare(pcmHandle); // I'm not sure why, but adding this line and taking it from below made this work properly

    int pcm = snd_pcm_writei(pcmHandle, (char*)&audioArray->data[0], (int)audioArray->data.size());

    if (pcm == -EPIPE)
    {
        //cout << "received audio packet" << endl;
        //snd_pcm_prepare(pcm_handle);
    }
    else if (pcm < 0)
        std::cout << "ERROR. Can't write to PCM device. " << snd_strerror(pcm) << std::endl;
}


audio::ReceiverNodelet::~ReceiverNodelet()
{
}
