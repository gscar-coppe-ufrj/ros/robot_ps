// sender_nodelet.cpp



#include <audio/sender_nodelet.h>



audio::SenderNodelet::SenderNodelet()
{
}


void audio::SenderNodelet::onInit()
{
    ros::NodeHandle & privateNH = getPrivateNodeHandle();

    privateNH.param<std::string>("output_path", outputPath, "");

    privateNH.param<int>("number_channels", numberChannels, 0);
    privateNH.param<int>("number_bytes", numberBytes, 0);
    privateNH.param<int>("sample_rate", sampleRate, 0);

    std::string plugIn;
    std::string card;
    int dev;
    int subDev;
    privateNH.param<std::string>("plug_in", plugIn, "");
    privateNH.param<std::string>("card", card, "");
    bool devSet = privateNH.getParam((std::string)"dev", dev);
    bool subDevSet = privateNH.getParam((std::string)"sub_dev", subDev);

    std::stringstream ss;
    ss << plugIn;
    if (card != "")
        ss << ":CARD=" << card;
    if (devSet)
        ss << ",DEV=" << dev;
    if (subDevSet)
        ss << ",SUBDEV=" << subDev;
    pcmName = ss.str();

    nodeHandle = getNodeHandle();

    msgRecordSub = nodeHandle.subscribe(getName() + "/record", 100, &SenderNodelet::SetRecordCallback, this);

    msgRecordingPub = nodeHandle.advertise<std_msgs::Bool>(getName() + "/recording", 100, true);
    msgAudioPub = nodeHandle.advertise<std_msgs::Int16MultiArray>(getName() + "/stream", 100, true);

    StartCycle();
}


void audio::SenderNodelet::SendRecording(bool recording)
{
    std_msgs::BoolPtr rec(new std_msgs::Bool);
    rec->data = recording;
    msgRecordingPub.publish(rec);
}


void audio::SenderNodelet::SendAudio(std::vector<int16_t> & audioArray)
{
    std_msgs::Int16MultiArrayPtr int16_array(new std_msgs::Int16MultiArray);
    int16_array->data = audioArray;
    msgAudioPub.publish(int16_array);
}


void audio::SenderNodelet::SetRecordCallback(const std_msgs::Bool::ConstPtr & record)
{
    this->record = record->data;
}


audio::SenderNodelet::~SenderNodelet()
{
    DestroyCycle();
}
