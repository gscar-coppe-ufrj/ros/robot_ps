// listener_qwidget.cpp



#include <include/audio/listener_qwidget.h>
#include <QTextEdit>
#include <QAction>
#include <QHBoxLayout>
#include <QMainWindow>
#include <QLabel>



audio::ListenerQWidget::ListenerQWidget()
{
    sizehintw = 200;
    sizehinth = 200;
}


void audio::ListenerQWidget::Init()
{
    audioPlay = new QAction(QIcon(GPT_AUDIO_LISTENER_QWIDGET_ICON_PLAY), "Audio Play", NULL);
    audioStop = new QAction(QIcon(GPT_AUDIO_LISTENER_QWIDGET_ICON_STOP), "Audio Stop", NULL);
    recordStart = new QAction(QIcon(GPT_AUDIO_LISTENER_QWIDGET_ICON_NOT_RECORDING), "Record Start", NULL);
    recordStop = new QAction(QIcon(GPT_AUDIO_LISTENER_QWIDGET_ICON_RECORDING), "Record Stop", NULL);

    QHBoxLayout* lay = new QHBoxLayout;
    this->setLayout(lay);

    QMainWindow* window = new QMainWindow(0);
    bar = new QToolBar(window);
    bar->addAction(audioPlay);
    bar->addAction(audioStop);
    bar->addAction(recordStart);
    bar->addAction(recordStop);
    window->addToolBar(bar);

    table = new QTableWidget(window);
    table->setShowGrid(false);
    table->setColumnCount(3);
    table->horizontalHeader()->hide();
    table->verticalHeader()->hide();
    table->setColumnWidth(0, 20);
    table->setColumnWidth(1, 20);
    table->horizontalHeader()->setStretchLastSection(true);
    //table->resizeColumnsToContents();
    //table->setSelectionBehavior(QAbstractItemView::SelectRows);
    window->setCentralWidget(table);

    lay->addWidget(window);

    connect(this, SIGNAL(SignalPlaying(Receiver*)), this, SLOT(SlotPlaying(Receiver*)));
    connect(this, SIGNAL(SignalRecording(Receiver*)), this, SLOT(SlotRecording(Receiver*)));
    connect(audioPlay, SIGNAL(triggered()), SLOT(SlotAudioPlay()));
    connect(audioStop, SIGNAL(triggered()), SLOT(SlotAudioStop()));
    connect(recordStart, SIGNAL(triggered()), SLOT(SlotRecordStart()));
    connect(recordStop, SIGNAL(triggered()), SLOT(SlotRecordStop()));

    connect(this, SIGNAL(FillTable()), SLOT(FillingTable()));

    emit FillTable();
}


QSize audio::ListenerQWidget::sizeHint() const
{
    return QSize(sizehintw, sizehinth);
}


void audio::ListenerQWidget::Playing(Receiver* receiver)
{
    emit SignalPlaying(receiver);
}


void audio::ListenerQWidget::Recording(Receiver* receiver)
{
    emit SignalRecording(receiver);
}


void audio::ListenerQWidget::SlotPlaying(Receiver* receiver)
{
    auto list = this->GetListOfComponents<Listener, Receiver>();
    auto it = std::find(list->begin(), list->end(), receiver);
    if (it != list->end())
    {
        unsigned int index = it - list->begin();

        if (receiver->Playing())
            audioReceiverPlayButtons[index]->setIcon(QIcon(GPT_AUDIO_LISTENER_QWIDGET_ICON_STOP));
        else
            audioReceiverPlayButtons[index]->setIcon(QIcon(GPT_AUDIO_LISTENER_QWIDGET_ICON_PLAY));
    }
}


void audio::ListenerQWidget::SlotRecording(Receiver* receiver)
{
    auto list = this->GetListOfComponents<Listener, Receiver>();
    auto it = std::find(list->begin(), list->end(), receiver);
    if (it != list->end())
    {
        unsigned int index = it - list->begin();

        if (receiver->Recording())
            audioReceiverRecordButtons[index]->setIcon(QIcon(GPT_AUDIO_LISTENER_QWIDGET_ICON_RECORDING));
        else
            audioReceiverRecordButtons[index]->setIcon(QIcon(GPT_AUDIO_LISTENER_QWIDGET_ICON_NOT_RECORDING));
    }
}


void audio::ListenerQWidget::SlotPlayStop()
{
    QToolButton* button = (QToolButton*)sender();
    auto it = std::find(audioReceiverPlayButtons.begin(), audioReceiverPlayButtons.end(), button);
    if (it != audioReceiverPlayButtons.end())
    {
        unsigned int index = it - audioReceiverPlayButtons.begin();
        auto list = this->GetListOfComponents<Listener, Receiver>();
        Receiver* componentAudioReceiver = (*list)[index];
        componentAudioReceiver->Play(!componentAudioReceiver->Playing());
    }
}


void audio::ListenerQWidget::SlotRecordStartStop()
{
    QToolButton* button = (QToolButton*)sender();
    auto it = std::find(audioReceiverRecordButtons.begin(), audioReceiverRecordButtons.end(), button);
    if (it != audioReceiverRecordButtons.end())
    {
        unsigned int index = it - audioReceiverRecordButtons.begin();
        auto list = this->GetListOfComponents<Listener, Receiver>();
        Receiver* componentAudioReceiver = (*list)[index];
        componentAudioReceiver->Record(!componentAudioReceiver->Recording());
    }
}


void audio::ListenerQWidget::SlotAudioPlay()
{
    auto items = table->selectedItems();
    auto list = this->GetListOfComponents<Listener, Receiver>();
    for (unsigned char i = 0; i < items.size(); i++)
    {
        for (unsigned char j = 0; j < list->size(); j++)
        {
            if (items[i] == audioReceiverString[j])
                (*list)[j]->Play(true);
        }
    }
}


void audio::ListenerQWidget::SlotAudioStop()
{
    auto items = table->selectedItems();
    auto list = this->GetListOfComponents<Listener, Receiver>();
    for (unsigned char i = 0; i < items.size(); i++)
    {
        for (unsigned char j = 0; j < list->size(); j++)
        {
            if (items[i] == audioReceiverString[j])
                (*list)[j]->Play(false);
        }
    }
}


void audio::ListenerQWidget::SlotRecordStart()
{
    auto items = table->selectedItems();
    for (unsigned char i = 0; i < items.size(); i++)
    {
        auto list = this->GetListOfComponents<Listener, Receiver>();
        for (unsigned char j = 0; j < list->size(); j++)
        {
            if (items[i] == audioReceiverString[j])
                (*list)[j]->Record(true);
        }
    }
}


void audio::ListenerQWidget::SlotRecordStop()
{
    auto items = table->selectedItems();
    for (unsigned char i = 0; i < items.size(); i++)
    {
        auto list = this->GetListOfComponents<Listener, Receiver>();
        for (unsigned char j = 0; j < list->size(); j++)
        {
            if (items[i] == audioReceiverString[j])
                (*list)[j]->Record(false);
        }
    }
}


void audio::ListenerQWidget::FillingTable()
{
    auto list = this->GetListOfComponents<Listener, Receiver>();

    audioReceiverPlayButtons.clear();
    audioReceiverRecordButtons.clear();
    audioReceiverString.clear();
    table->clear();
    if (list == NULL)
        table->setRowCount(0);
    else
    {
        table->setRowCount(list->size());

        // treat all connections
        int i = 0;
        for (std::deque<Receiver*>::iterator audioReceiver = list->begin(); audioReceiver != list->end(); audioReceiver++)
        {
            std::string str = (*audioReceiver)->GetName();
            QTableWidgetItem* stringItem = new QTableWidgetItem(QString(str.c_str()));
            audioReceiverString.push_back(stringItem);
            table->setItem(i, 2, stringItem);

            QLabel* label;
            QHBoxLayout* layout;
            QToolButton* button[i];

            for (unsigned int j = 0; j < 2; j++)
            {
                label = new QLabel;
                label->setAlignment(Qt::AlignCenter);
                label->setStyleSheet("QLabel {background-color: white}");
                label->setMargin(0);
                layout = new QHBoxLayout;
                layout->setMargin(0);
                layout->setSpacing(0);
                label->setLayout(layout);
                button[j] = new QToolButton;
                button[j]->setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding);
                layout->addWidget(button[j]);
                table->setCellWidget(i, j, label);
            }

            audioReceiverPlayButtons.push_back(button[0]);
            audioReceiverRecordButtons.push_back(button[1]);

            connect(button[0], SIGNAL(clicked()), SLOT(SlotPlayStop()));
            connect(button[1], SIGNAL(clicked()), SLOT(SlotRecordStartStop()));

            if ((*audioReceiver)->Playing())
                button[0]->setIcon(QIcon(GPT_AUDIO_LISTENER_QWIDGET_ICON_STOP));
            else
                button[0]->setIcon(QIcon(GPT_AUDIO_LISTENER_QWIDGET_ICON_PLAY));

            if ((*audioReceiver)->Recording())
                button[1]->setIcon(QIcon(GPT_AUDIO_LISTENER_QWIDGET_ICON_RECORDING));
            else
                button[1]->setIcon(QIcon(GPT_AUDIO_LISTENER_QWIDGET_ICON_NOT_RECORDING));

            i++;
        }
    }
}


void audio::ListenerQWidget::FindComponents()
{
    Listener::FindComponents();

    emit FillTable();
}


void audio::ListenerQWidget::TreatAddReceiver(Receiver* receiver)
{
    Listener::TreatAddReceiver(receiver);

    emit FillTable();
}


void audio::ListenerQWidget::TreatRemoveReceiver(Receiver* receiver)
{
    Listener::TreatRemoveReceiver(receiver);

    emit FillTable();
}


audio::ListenerQWidget::~ListenerQWidget()
{
}
