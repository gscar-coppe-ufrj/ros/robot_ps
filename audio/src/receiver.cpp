// gpcbaudioreceiver.cpp



#include <audio/receiver.h>
#include <iostream>



audio::Receiver::Receiver()
{
    playing = false;
    recording = false;

    numberChannels = 0;
    numberBytes = 0;
    sampleRate = 0;
    pcmName = "default";
}


bool audio::Receiver::StartUp()
{
    int pcm;
    unsigned int tmp;
    snd_pcm_hw_params_t *params;

    /* Open the PCM device in playback mode */
    if ((pcm = snd_pcm_open(&pcmHandle, pcmName.c_str(), SND_PCM_STREAM_PLAYBACK, 0)) < 0)
    {
        std::cout << "ERROR: Can't open " << pcmName << " PCM device. " << snd_strerror(pcm) << std::endl;
        return false;
    }

    /* Allocate parameters object and fill it with default values*/
    snd_pcm_hw_params_alloca(&params);

    snd_pcm_hw_params_any(pcmHandle, params);

    /* Set parameters */
    if ((pcm = snd_pcm_hw_params_set_access(pcmHandle, params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
    {
        std::cout << "ERROR: Can't set interleaved mode. " << snd_strerror(pcm) << std::endl;
        return false;
    }

    if ((pcm = snd_pcm_hw_params_set_format(pcmHandle, params, SND_PCM_FORMAT_S16_LE)) < 0)
    {
        std::cout << "ERROR: Can't set format. " << snd_strerror(pcm) << std::endl;
        return false;
    }

    if ((pcm = snd_pcm_hw_params_set_channels(pcmHandle, params, numberChannels)) < 0)
    {
        std::cout << "ERROR: Can't set channels number. " << snd_strerror(pcm) << std::endl;
        return false;
    }

    if ((pcm = snd_pcm_hw_params_set_rate_near(pcmHandle, params, (unsigned int*)&sampleRate, 0)) < 0)
    {
        std::cout << "ERROR: Can't set rate. " << snd_strerror(pcm) << std::endl;
        return false;
    }

    /* Write parameters */
    if ((pcm = snd_pcm_hw_params(pcmHandle, params) < 0))
    {
        std::cout << "ERROR: Can't set harware parameters. " << snd_strerror(pcm) << std::endl;
        return false;
    }

    /* Resume information */
    std::cout << "PCM name: " << snd_pcm_name(pcmHandle) << std::endl;

    std::cout << "PCM state: " << snd_pcm_state_name(snd_pcm_state(pcmHandle)) << std::endl;

    snd_pcm_hw_params_get_channels(params, &tmp);
    std::cout << "channels: " << tmp;

    if (tmp == 1)
        std::cout << " (mono)" << std::endl;
    else if (tmp == 2)
        std::cout << " (stereo)" << std::endl;

    snd_pcm_hw_params_get_rate(params, &tmp, 0);
    std::cout << "rate: " << tmp << " bps" << std::endl;

    return true;
}


void audio::Receiver::Recording(bool recording)
{
    this->recording = recording;

    // this function is called when a ROS message arrives telling if it's recording or not
    auto list = GetListOfTools<Receiver, Listener>();
    if (list != NULL)
    {
        for (std::deque<Listener*>::iterator it = list->begin(); it != list->end(); it++)
            (*it)->Recording(this);
    }
}


void audio::Receiver::Playing(bool playing)
{
    this->playing = playing;

    auto list = GetListOfTools<Receiver, Listener>();
    if (list != NULL)
    {
        for (std::deque<Listener*>::iterator it = list->begin(); it != list->end(); it++)
            (*it)->Playing(this);
    }
}


bool audio::Receiver::Playing()
{
    return playing;
}


bool audio::Receiver::Recording()
{
    return recording;
}


audio::Receiver::~Receiver()
{
}
