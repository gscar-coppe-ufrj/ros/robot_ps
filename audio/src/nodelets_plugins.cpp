// nodelets_plugins.cpp



#include <pluginlib/class_list_macros.h>
#include <audio/sender_nodelet.h>
#include <audio/receiver_nodelet.h>



PLUGINLIB_EXPORT_CLASS(audio::SenderNodelet, nodelet::Nodelet)
PLUGINLIB_EXPORT_CLASS(audio::ReceiverNodelet, nodelet::Nodelet)
