// sender.cpp



#include <audio/sender.h>
#include <iostream>


audio::Sender::Sender()
{
    destroyCycle = false;
    cycleBarrier = new boost::barrier(2);

    record = false;
    recording = false;
    sndFile = NULL;

    numberChannels = 0;
    numberBytes = 0;
    sampleRate = 0;
}


int16_t audio::Sender::CharToInt16(char x)
{
    int16_t res = (int16_t)x & 255;

    return res;
}


std::string audio::Sender::GetTimeStr()
{
    std::time_t rawtime;
    std::tm* timeinfo;
    char buffer [80];

    std::time(&rawtime);
    timeinfo = std::localtime(&rawtime);

    std::strftime(buffer, 80, "%Y-%m-%d-%H-%M-%S", timeinfo);

    std::string strTime = buffer;

    return strTime;
}


void audio::Sender::RecordAudio(std::vector<int16_t> & audioArray)
{
    if (record)
    {
        if (!recording) // open file
        {
            recording = true;

            std::string sndFileName = outputPath + GetTimeStr() + ".wav";
            SF_INFO sfinfo;
            sfinfo.channels = numberChannels;
            sfinfo.samplerate = sampleRate;
            sfinfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;

            sndFile = sf_open(sndFileName.c_str(), SFM_WRITE, &sfinfo);

            SendRecording(true);
            std::cout << "recording started" << std::endl;
        }

        if (sndFile != NULL)
            sf_write_short(sndFile, &audioArray[0], (int)audioArray.size());
    }
    else
    {
        if (recording) // close file
        {
            sf_close(sndFile);

            recording = false;
            SendRecording(false);
            std::cout << "recording stopped" << std::endl;
        }
    }
}


void audio::Sender::Cycle()
{
    bool initOK = true;
    bool configOK = true;
    Time start = boost::posix_time::microsec_clock::local_time();
    Time now;
    TimeDuration end;

    int pcm;
    int size;
    snd_pcm_t* pcmHandle;
    snd_pcm_hw_params_t* params;
    unsigned int val;
    snd_pcm_uframes_t frames;
    char *buffer;

    while (!destroyCycle)
    {
        /* Open PCM device for recording (capture). */
        pcm = snd_pcm_open(&pcmHandle, pcmName.c_str(), SND_PCM_STREAM_CAPTURE, 0);
        if (pcm < 0)
        {
            std::cout << "ERROR: Can't open " << pcmName << " PCM device. " << snd_strerror(pcm) << std::endl;
            sleep(1);
        }
        else
        {
            configOK = true;
            /* Allocate a hardware parameters object. */
            snd_pcm_hw_params_alloca(&params);

            /* Fill it in with default values. */
            snd_pcm_hw_params_any(pcmHandle, params);

            /* Set the desired hardware parameters. */

            if ((pcm = snd_pcm_hw_params_set_access(pcmHandle, params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
            {
                std::cout << "ERROR: Can't set interleaved mode. " << snd_strerror(pcm) << std::endl;
                configOK = false;
            }

            if ((pcm = snd_pcm_hw_params_set_format(pcmHandle, params, SND_PCM_FORMAT_S16_LE)) < 0)
            {
                std::cout << "ERROR: Can't set format. " << snd_strerror(pcm) << std::endl;
                configOK = false;
            }

            if ((pcm = snd_pcm_hw_params_set_channels(pcmHandle, params, numberChannels)) < 0)
            {
                std::cout << "ERROR: Can't set channels number. " << snd_strerror(pcm) << std::endl;
                configOK = false;
            }

            /* 48000 bits/second sampling rate (CD quality) */
            if ((pcm = snd_pcm_hw_params_set_rate_near(pcmHandle, params, (unsigned int*)&sampleRate, 0)) < 0)
            {
                std::cout << "ERROR: Can't set rate. " << snd_strerror(pcm) << std::endl;
                configOK = false;
            }

            /* Set period size to 32 frames. */ // IS THIS NECESSARY????????
            frames = 32;
            snd_pcm_hw_params_set_period_size_near(pcmHandle, params, &frames, 0);

            /* Write the parameters to the driver */
            pcm = snd_pcm_hw_params(pcmHandle, params);
            if (pcm < 0)
            {
                std::cout << "unable to set hw parameters: " << snd_strerror(pcm)<< std::endl;
                configOK = false;
            }
            else
            {
                /* Use a buffer large enough to hold one period */
                snd_pcm_hw_params_get_period_size(params, &frames, 0);
                size = frames * numberChannels * numberBytes; // NBYTES bytes/sample, NCHANNELS channels
                buffer = new char[size];

                /* We want to loop for 1 second */
                snd_pcm_hw_params_get_period_time(params, &val, 0);
                long device_loops = 1e6 / val;

                int device_loops_count = 0;

                while (!destroyCycle && initOK && configOK)
                {
                    now = boost::posix_time::microsec_clock::local_time();

                    std::vector<int16_t> array;
                    int16_t int16_sample;

                    /* Get device Data */
                    long device_loops_index = device_loops;
                    while (device_loops_index > 0 && configOK)
                    {
                        device_loops_index--;
                        pcm = snd_pcm_readi(pcmHandle, buffer, frames);
                        if (pcm == -EPIPE)
                        {
                            /* EPIPE means overrun */
                            std::cout <<"overrun occurred" << std::endl;
                            configOK = false;
                            snd_pcm_prepare(pcmHandle);
                        }
                        else if (pcm < 0){
                            std::cout << "error from read: " << snd_strerror(pcm) << std::endl;
                            configOK = false;}
                        else if (pcm != (int)frames){
                            fprintf(stderr, "short read, read %d frames\n", pcm);
                            configOK = false;}

                        /* Put the buffer in an array of the correct type */
                        for (int n=0; n<size/numberBytes; n++)
                        {
                            int16_sample = CharToInt16(buffer[n*numberBytes]);
                            for(int m=1; m<numberBytes; m++)
                                int16_sample = int16_sample | ( CharToInt16(buffer[n*numberBytes+m]) << m*8 );
                            array.push_back(int16_sample);
                        }
                    }
                    device_loops_count++;

                    //int L = array.size();
                    //cout << "I published " << L << "  samples in loop " << device_loops_count << endl;

                    SendAudio(array);
                    RecordAudio(array);

                    start = start + boost::posix_time::milliseconds(GPC_AUDIO_SENDER_CYCLE_PERIOD);
                    end = start - now;
                    boost::this_thread::sleep(end);
                }

                snd_pcm_drain(pcmHandle);
                snd_pcm_close(pcmHandle);
                delete[] buffer;
            }
        }
    }
    cycleBarrier->wait();
}


void audio::Sender::CycleThreadFunction(Sender *component)
{
    component->Cycle();
}


void audio::Sender::StartCycle()
{
    cycleThread = new boost::thread(Sender::CycleThreadFunction, this);
}


void audio::Sender::DestroyCycle()
{
    if (!destroyCycle)
    {
        destroyCycle = true;
        cycleBarrier->wait();
        cycleThread->join();
        delete cycleBarrier;
        delete cycleThread;
    }
}


audio::Sender::~Sender()
{
    DestroyCycle();
}
