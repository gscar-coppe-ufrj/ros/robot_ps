/*!
 * \file namespace.h
 */



#ifndef AUDIO_NAMESPACE_H
#define AUDIO_NAMESPACE_H



/*!
 * \namespace audio 
 * \brief The audio namespace provides a collection of classes to listen audio
 * 		and send/receive audio through ROS.
 */
namespace audio {
}



#endif
