/*!
 * \file receiver_nodelet.h
 */



#ifndef AUDIO_RECEIVER_NODELET_H
#define AUDIO_RECEIVER_NODELET_H



#include <ros/ros.h>
#include <custom_loader/custom_nodelet.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Int16MultiArray.h>
#include "receiver.h"



namespace audio {



/*!
 * \class ReceiverNodelet
 * \brief The audio::ReceiverNodelet class implements the same functionality as
 *		audio::Receiver, but integrating with ROS messages.
 */
class ReceiverNodelet : public Receiver, public custom_loader::CustomNodelet
{
    /// Subscribe to a channel that control the recording state.
    ros::Subscriber msgRecordingSub;
    /// Subscribe to a channel with audio data.
    ros::Subscriber msgAudioSub;

    /// Publisher of record state.
    ros::Publisher msgRecordPub;

    /*!
     * \brief RecordingCallback is a callback for the recording channel.
     * \param recording Message received.
     */
    void RecordingCallback(const std_msgs::Bool::ConstPtr & recording);
    /*!
     * \brief ReceiveAudioCallback is a callback for the audio data channel.
     * \param audioArray Audio data received.
     */
    void ReceiveAudioCallback(const std_msgs::Int16MultiArray::ConstPtr & audioArray);

protected:
    /// ROS node handle.
    ros::NodeHandle nodeHandle;

    /// Name of the sender node.
    std::string senderName;

    /*!
     * \brief Record sends the record state, using ReceiverNodelet::msgRecordPub.
     * \param record State to send.
     */
    void Record(bool record);
    /*!
     * \brief Play is used to subscribe or shutdown the
     *      ReceiverNodelet::msgAudioSub.
     * \param play Whether to subscribe or shutdown the subscriber.
     */
    void Play(bool play);

public:
    /*!
     * \brief ReceiverNodelet does nothing.
     */
    ReceiverNodelet();

    /*!
     * \brief GetName gets the name of the ROS node.
     * \return Name of this node.
     */
    virtual std::string GetName();

    /*!
     * \brief onInit sets parameters and start ROS subscriber and
     *      advertise channels.
     */
    virtual void onInit();

    /*!
     * \brief ~ReceiverNodelet does nothing.
     */
    ~ReceiverNodelet();
};



}



#endif // AUDIO_RECEIVER_NODELET_H
