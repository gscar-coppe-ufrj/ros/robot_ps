/*!
 * \file sender_nodelet.h
 */



#ifndef AUDIO_SENDER_NODELET_H
#define AUDIO_SENDER_NODELET_H



#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Int16MultiArray.h>
#include "sender.h"



namespace audio {



/*!
 * \class SenderNodelet
 * \brief The audio::SenderNodelet class implements the same functionality as
 * 		audio::Sender, but integrating with the ROS messages.
 */
class SenderNodelet : public Sender, public nodelet::Nodelet
{
    /// Subscriber to a channel that will control the state of the variable
    /// Sender::record.
    ros::Subscriber msgRecordSub;

    /// Publisher of the audio data.
    ros::Publisher msgAudioPub;
    /// Publisher of the Sender::recording.
    ros::Publisher msgRecordingPub;

protected:
    /*!
     * \brief SendRecording is called when the state of Sender::recording is
     * 		changed.
     * \param recording The current state of Sender::recording.
     */
    void SendRecording(bool recording);
    /*!
     * \brief SendAudio is called every loop of Sender::Cycle(), to send
     * 		the audio data through ROS.
     * \param audioArray The audio data.
     */
    void SendAudio(std::vector<int16_t> & audioArray);

public:
    /// ROS node handle.
    ros::NodeHandle nodeHandle;

    /*!
     * \brief SenderNodelet does nothing.
     */
    SenderNodelet();

    /*!
     * \brief onInit sets default values through ROS params and create ROS'
     *      advertise and subscribe channels.
     */
    virtual void onInit();

    /*!
     * \brief SetRecordCallback is used by ROS' subscribe channel "/record".
     * \param record The message received.
     */
    void SetRecordCallback(const std_msgs::Bool::ConstPtr & record);

    /*!
     * \brief ~SenderNodelet does nothing.
     */
    ~SenderNodelet();
};



}



#endif // SENDER_NODELET_H
