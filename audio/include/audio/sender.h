/*!
 * \file sender.h
 */



#ifndef AUDIO_SENDER_H
#define AUDIO_SENDER_H



#include <boost/thread.hpp>
#include <alsa/asoundlib.h>
#include <sndfile.h>



#define GPC_AUDIO_SENDER_CYCLE_PERIOD 100



namespace audio {



typedef boost::posix_time::ptime Time;
typedef boost::posix_time::time_duration TimeDuration;



/*!
 * \class Sender
 * \brief The audio::Sender class is responsible for connecting to the audio 
 * 		device and collecting the audio data to send and record. 
 */
class Sender
{
    /// Flag to indicate the cycle inside the thread should end.
    bool destroyCycle;
    /// Thread used by Sender::Cycle().
    boost::thread* cycleThread;

    /// Indicates whether the sound is being recorded.
    bool recording;
    /// File handle used by sndfile to create a sound file.
    SNDFILE* sndFile;

    /*!
     * \brief Cycle will continously listen to the audio device.
     *
     * Each cycle will call Sender::RecordAudio and Sender::SendAudio.
     */
    void Cycle();
    /*!
     * \brief CycleThreadFunction is an entry point to the Cycle() thread.
     *
     * Spawns a new boost::thread to continuosly listen to audio device.
     * \param[in] component The component that will execute the thread.
     */
    static void CycleThreadFunction(Sender* component);
    /*!
     * \brief GetTimeStr gets the current time as string.
     *
     * Format is %Y-%m-%d-%H-%M-%S.
     * \return The current time as string.
     */
    std::string GetTimeStr();
    /*!
     * \brief RecordAudio saves the \p audioArray data to a file.
     *
     * The file is saved in the Sender::outputPath directory, 
     * with Sender::GetTimeStr() name and .wav extension.
     * \param[in] audioArray
     */
    void RecordAudio(std::vector<int16_t> & audioArray);
    /*!
     * \brief CharToInt16 converts an int8_t to int16_t.
     * \param[in] x Char to be converted.
     * \return An int16_t version of the input.
     */
    static int16_t CharToInt16(char x);

protected:
    /// Number of channels used by the pcm device.
    int numberChannels;
    /// Number of bytes each ROS message have.
    int numberBytes;
    /// Sample rate of pcm device.
    int sampleRate;
    /// Name of pcm device.
    std::string pcmName;
    
    /// Output path of the recorded audio file.
    std::string outputPath;
    /// Barrier used to wait for the cycle thread.
    boost::barrier* cycleBarrier;
    /// Control if the audio data should be saved to a file. True to save, false
    /// otherwise.
    bool record;
    /*!
     * \brief StartCycle calls Sender::CycleThreadFunction to spawn the
     *      cycle thread.
     */
    void StartCycle();
    /*!
     * \brief DestroyCycle waits for the cycle loop to end and destroy the thread.
     */
    void DestroyCycle();
    /*!
     * \brief SendRecording sends the status of the variable
     *      Sender::recording.
     *
     * \param recording The current status of the variable.
     */
    virtual void SendRecording(bool recording) = 0;
    /*!
     * \brief SendAudio sends the audio returned by the device.
     *
     * \param audioArray Audio data returned by the device.
     */
    virtual void SendAudio(std::vector<int16_t> & audioArray) = 0;

public:
    /*!
     * \brief Sender does nothing.
     */
    Sender();
    /*!
     * \brief ~Sender does nothing.
     */
    virtual ~Sender();
};



}



#endif // AUDIO_SENDER_H
