/*!
 * \file listener.h
 */



#ifndef AUDIO_LISTENER_H
#define AUDIO_LISTENER_H



#include <robot_ps_core/tool.h>
#include "receiver.h"



namespace audio {



class Receiver;



/*!
 * \class Listener
 * \brief The audio::Listener class is responsible for playing and recording
 *      the audio.
 */
class Listener : public robot_ps::Tool
{
protected:
    /*!
     * \brief TreatAddReceiver is called whenever something is added to the
     *      layout and regenerates the table.
     * \param c Contains the information of what was added to the layout.
     */
    virtual void TreatAddReceiver(Receiver* c){}
    /*!
     * \brief TreatRemoveReceiver is called whenever something is removed of
     *      the layout and regenerates the table.
     * \param c Contains the information of what was removed of the layout.
     */
    virtual void TreatRemoveReceiver(Receiver* c){}

public:
    /*!
     * \brief Listener does nothing.
     */
    Listener(){}

    /*!
     * \brief FindComponents find all the components and add the new
     *      component to the deck.
     */
    virtual void FindComponents()
    {
        AddMeToComponents<Listener, Receiver>(&Listener::TreatAddReceiver, &Listener::TreatRemoveReceiver);
    }

    /*!
     * \brief Playing emits a signal that sets a icon depending if the audio
     *      is playing or not.
     * \param audioReceiver Variable used to set the connection between tha
     *      signal and the function.
     */
    virtual void Playing(Receiver* audioReceiver) = 0;
    /*!
     * \brief Recording emits a signal that sets a icon depending if the
     *      audio is beeing recording or not.
     * \param audioReceiver Variable used to set the connection between tha
     *      signal and the function.
     */
    virtual void Recording(Receiver* audioReceiver) = 0;

    /*!
     * \brief ~Listener does nothing.
     */
    virtual ~Listener(){}
};



}



#endif
