/*!
 * \file listener_qwidget.h
 */



#ifndef AUDIO_LISTENER_QWIDGET_H
#define AUDIO_LISTENER_QWIDGET_H



#include <deque>
#include <QListWidget>
#include <QHeaderView>
#include <QToolBar>
#include <QTableWidget>
#include <QToolButton>
#include "listener.h"



#define GPT_AUDIO_LISTENER_QWIDGET_ICON_PLAY "/usr/share/icons/Humanity/actions/16/media-playback-start.svg"
#define GPT_AUDIO_LISTENER_QWIDGET_ICON_STOP "/usr/share/icons/Humanity/actions/16/media-playback-stop.svg"
#define GPT_AUDIO_LISTENER_QWIDGET_ICON_RECORDING "/usr/share/icons/Humanity/actions/16/editdelete.svg"
#define GPT_AUDIO_LISTENER_QWIDGET_ICON_NOT_RECORDING "/usr/share/icons/Humanity/actions/16/media-record.svg"



namespace audio {



/*!
 * \class ListenerQWidget
 * \brief The audio::ListenerQWidget class provide the Qt graphical interface for
 *		audio::Listener.
 */
class ListenerQWidget : public QWidget, public Listener
{
    Q_OBJECT

    /// Size of the widget's width.
    int sizehintw;
    /// Size of the widget's height.
    int sizehinth;

    /// Bar with the actions icons.
    QToolBar* bar;
    /// Icon that starts playing the audio.
    QAction* audioPlay;
    /// Icon that stops playing the audio.
    QAction* audioStop;
    /// Icon that starts recording the audio.
    QAction* recordStart;
    /// Icon that stops recording the audio.
    QAction* recordStop;

    /// Window that displays the components linked to the tool.
    QTableWidget* table;

    /// Deque containing the icons of play's buttons.
    std::deque<QToolButton*> audioReceiverPlayButtons;
    /// Deque containing the icons of record's buttons.
    std::deque<QToolButton*> audioReceiverRecordButtons;
    /// Deque containing the names of the buttons.
    std::deque<QTableWidgetItem*> audioReceiverString;

    /*!
     * \brief Playing notifies that the \p receiver has changed its playing state.
     * \param receiver The audio::Receiver that changed its state.
     */
    void Playing(Receiver* receiver);
    /*!
     * \brief Recording notifies that the \p receiver has changed its recording state.
     * \param receiver The audio::Receiver that changed its state.
     */
    void Recording(Receiver* receiver);

    /*!
     * \brief TreatAddReceiver treats the inclusion of a new \p receiver.
     * \param receiver The receiver added to this tool.
     */
    void TreatAddReceiver(Receiver* receiver);
    /*!
     * \brief TreatRemoveReceiver treats the removal of a \p receiver.
     * \param receiver The receiver removed from this tool.
     */
    void TreatRemoveReceiver(Receiver* receiver);

public:
    /*!
     * \brief ListenerQWidget does nothing.
     */
    ListenerQWidget();

    /*!
     * \brief Init setups the Qt layout of the widget and Qt signals.
     */
    void Init();

    /*!
     * \brief sizeHint provides the dimensions of the widget.
     * \return QSize with widget dimensions.
     */
    virtual QSize sizeHint() const;

    /*!
     * \brief FindComponents finds all components connect to this tool.
     *
     * Internally it emits a signal FillTable, calling 
     * ListenerQWidget::FillingTable().
     */
    void FindComponents();

    /*!
     * \brief ~ListenerQWidget does nothing.
     */
    virtual ~ListenerQWidget();

signals:
    /*!
     * \brief FillTable emits a signal when the table needs to be updated.
     */
    void FillTable();
    /*!
     * \brief SignalPlaying emits a signal when a \p receiver changes its
     *      playing state.
     * \param receiver The receiver that changed its state.
     */
    void SignalPlaying(Receiver* receiver);
    /*!
     * \brief SignalRecording emits a signal when a \p receiver changes its
     *      recording state.
     * \param receiver The receiver that changed its state.
     */
    void SignalRecording(Receiver* receiver);

private slots:
    /*!
     * \brief FillingTable creates a table listing all components of this
     *      tool.
     */
    void FillingTable();

public slots:
    /*!
     * \brief SlotPlaying is called by ListenerQWidget::SignalPlaying().
     * \param receiver The receiver that changed its state.
     */
    void SlotPlaying(Receiver* receiver);
    /*!
     * \brief SlotRecording is called by ListenerQWidget::SIgnalRecording().
     * \param receiver The receiver that changed its state.
     */
    void SlotRecording(Receiver* receiver);
    /*!
     * \brief SlotPlayStop is used to play/stop the audio playback
     *      of a given component.
     */
    void SlotPlayStop();
    /*!
     * \brief SlotRecordStartStop is used to start/stop recording the audio
     *      of a given component.
     */
    void SlotRecordStartStop();
    /*!
     * \brief SlotAudioPlay is called when the play button is clicked.
     */
    void SlotAudioPlay();
    /*!
     * \brief SlotAudioStop is called when the stop button is clicked.
     */
    void SlotAudioStop();
    /*!
     * \brief SlotRecordStart is called when the start recording button is
     *      clicked.
     */
    void SlotRecordStart();
    /*!
     * \brief SlotRecordStop is called when the stop recording button is
     *      clicked.
     */
    void SlotRecordStop();
};



}



#endif // AUDIO_LISTENER_QWIDGET_H
