/*!
 * \file receiver.h
 */



#ifndef AUDIO_RECEIVER_H
#define AUDIO_RECEIVER_H



#include <deque>
#include <alsa/asoundlib.h>
#include <robot_ps_core/component.h>
#include "listener.h"



namespace audio {



class Listener;



/*!
 * \class Receiver
 * \brief The audio::Receiver class is responsible for receiving and
 *      treating the audio received.
 *
 * This code is heavily based on the https://gist.github.com/ghedo/963382
 */
class Receiver : public robot_ps::Component
{
protected:
    /// Handle of the pcm device.
    snd_pcm_t* pcmHandle;

    /// Variable used to identify if it's playing or not the audio.
    bool playing;
    /// Variable used to identify if it's recording or not the audio.
    bool recording;

    /// Number of channels used by the pcm device.
    int numberChannels;
    /// Number of bytes each ROS message have.
    int numberBytes;
    /// Sample rate of pcm device.
    int sampleRate;
    /// Name of pcm device.
    std::string pcmName;

    /*!
     * \brief TreatAddAudioListener handle new audio::Listener being added
     *      to this component.
     * \param toolAudioListener audio::Listener addded.
     */
    virtual void TreatAddAudioListener(Listener* toolAudioListener){}
    /*!
     * \brief TreatRemoveAudioListener handle deletion of a audio::Listener
     *      from this component.
     * \param toolAudioListener audio::Listener removed.
     */
    virtual void TreatRemoveAudioListener(Listener* toolAudioListener){}

    /*!
     * \brief StartUp initializes a new sound playback stream.
     * \return Success or failure of the initialization process.
     */
    bool StartUp();
    /*!
     * \brief Playing sets Receiver::playing to \p playing and notify every
     *      tool connected the new state.
     * \param playing The new state of Receiver::playing.
     */
    void Playing(bool playing);
    /*!
     * \brief Recording is called when a ROS message arrives and notify
     *      every tool connected about the new state.
     * \param recording New state of Receiver::recording.
     */
    void Recording(bool recording);

public:
    /*!
     * \brief Receiver sets the default values for the variables used.
     */
    Receiver();

    /*!
     * \brief GetName is used to get the name of this node.
     * \return Name of this node.
     */
    virtual std::string GetName() = 0;
    /*!
     * \brief Playing is called to check if the audio is playing or not.
     * \return Returns the valor of the variable playing.
     */
    bool Playing();
    /*!
     * \brief Play changes the playing state of this.
     * \param play The new value to set.
     */
    virtual void Play(bool play) = 0;
    /*!
     * \brief Recording is called to check if the audio is being recorded
     *         or not.
     * \return Returns the valor of the variable Receiver::recording.
     */
    bool Recording();
    /*!
     * \brief Record sends the record state using ROS.
     * \param record Record state.
     */
    virtual void Record(bool record) = 0;

    /*!
     * \brief FindTools adds this component to every compatible tool.
     */
    virtual void FindTools()
    {
        AddMeToTools<Receiver, Listener>(&Receiver::TreatAddAudioListener, &Receiver::TreatRemoveAudioListener);
    }

    /*!
     * \brief ~Receiver does nothing.
     */
    virtual ~Receiver();
};



}



#endif // AUDIO_RECEIVER_H
