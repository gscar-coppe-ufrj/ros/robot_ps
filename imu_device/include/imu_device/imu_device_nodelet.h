// imu_device_nodelet.h



#ifndef IMU_DEVICE_NODELET_H
#define IMU_DEVICE_NODELET_H



#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <custom_loader/custom_nodelet.h>
#include <imu_device/imu_device.h>



namespace imu_device {



class IMUDeviceNodelet : public IMUDevice, public custom_loader::CustomNodelet
{
    ros::NodeHandle nodeHandle;

    ros::Subscriber msgIMUSub;

public:
    void onInit();

    void DataCallback(const sensor_msgs::Imu::ConstPtr & msg);
};



}



#endif // IMU_DEVICE_NODELET_H
