// imu_device.h



#ifndef IMU_DEVICE_H
#define IMU_DEVICE_H



#include <device/device.h>
#include <device/variables/variable_template.h>



namespace imu_device {



class IMUDevice : public device::Device
{
protected:
    bool paramDegree;
    bool paramOrientationQuaternion;
    bool paramOrientationRPY;
    bool paramAngularVelocity;
    bool paramLinearAcceleration;

    device::VariableTemplate<double>* angularPosition[3];
    device::VariableTemplate<double>* quaternion[4];
    device::VariableTemplate<double>* angularVelocity[3];
    device::VariableTemplate<double>* linearAcceleration[3];

    void CreateVariables();
};



}



#endif // IMU_DEVICE_H
