// imu_device.cpp



#include <iostream>
#include <imu_device/imu_device.h>



void imu_device::IMUDevice::CreateVariables()
{
    std::string unit = paramDegree ? "º" : "rad";

    if (paramOrientationQuaternion)
    {
        quaternion[0] = new device::VariableTemplate<double>("x", "", false, device::Variable::typeDouble);
        quaternion[1] = new device::VariableTemplate<double>("y", "", false, device::Variable::typeDouble);
        quaternion[2] = new device::VariableTemplate<double>("z", "", false, device::Variable::typeDouble);
        quaternion[3] = new device::VariableTemplate<double>("w", "", false, device::Variable::typeDouble);

        for (unsigned char i = 0; i < 4; i++)
        {
            quaternion[i]->SetValue(0);
            variables.push_back(quaternion[i]);
        }
    }

    if (paramOrientationRPY)
    {
        angularPosition[0] = new device::VariableTemplate<double>("Roll", unit, false, device::Variable::typeDouble);
        angularPosition[1] = new device::VariableTemplate<double>("Pitch", unit, false, device::Variable::typeDouble);
        angularPosition[2] = new device::VariableTemplate<double>("Yaw", unit, false, device::Variable::typeDouble);

        for (unsigned char i = 0; i < 3; i++)
        {
            angularPosition[i]->SetValue(0);
            variables.push_back(angularPosition[i]);
        }
    }

    if (paramAngularVelocity)
    {
        angularVelocity[0] = new device::VariableTemplate<double>("Vel Roll", unit + "/s", false, device::Variable::typeDouble);
        angularVelocity[1] = new device::VariableTemplate<double>("Vel Pitch", unit + "/s", false, device::Variable::typeDouble);
        angularVelocity[2] = new device::VariableTemplate<double>("Vel Yaw", unit + "/s", false, device::Variable::typeDouble);

        for (unsigned char i = 0; i < 3; i++)
        {
            angularVelocity[i]->SetValue(0);
            variables.push_back(angularVelocity[i]);
        }
    }

    if (linearAcceleration)
    {
        linearAcceleration[0] = new device::VariableTemplate<double>("Acc X", "m/s²", false, device::Variable::typeDouble);
        linearAcceleration[1] = new device::VariableTemplate<double>("Acc Y", "m/s²", false, device::Variable::typeDouble);
        linearAcceleration[2] = new device::VariableTemplate<double>("Acc Z", "m/s²", false, device::Variable::typeDouble);

        for (unsigned char i = 0; i < 3; i++)
        {
            linearAcceleration[i]->SetValue(0);
            variables.push_back(linearAcceleration[i]);
        }
    }
}
