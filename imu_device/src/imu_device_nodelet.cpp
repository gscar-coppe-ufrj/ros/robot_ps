// device_a.cpp



#include <iostream>
#include <tf/tf.h>
#include <imu_device/imu_device_nodelet.h>



void imu_device::IMUDeviceNodelet::onInit()
{
    ros::NodeHandle & privateNH = getPrivateNodeHandle();

    std::string senderName;
    privateNH.param("sender_name", senderName, getName());
    privateNH.param("degree", paramDegree, false);
    privateNH.param("orientation_quaternion", paramOrientationQuaternion, false);
    privateNH.param("orientation_rpy", paramOrientationRPY, true);
    privateNH.param("angular_velocity", paramAngularVelocity, false);
    privateNH.param("linear_acceleration", paramLinearAcceleration, false);

    msgIMUSub = getNodeHandle().subscribe(senderName + "/data", 100, &IMUDeviceNodelet::DataCallback, this);

    CreateVariables();
}


void imu_device::IMUDeviceNodelet::DataCallback(const sensor_msgs::Imu::ConstPtr & msg)
{
    float gain = paramDegree ? 180/M_PI : 1;

    tf::Quaternion q;
    tf::quaternionMsgToTF(msg->orientation, q);

    if (paramOrientationQuaternion)
    {
        quaternion[0]->SetValue(q.getX());
        quaternion[1]->SetValue(q.getY());
        quaternion[2]->SetValue(q.getZ());
        quaternion[3]->SetValue(q.getW());

        for (unsigned char i = 0; i < 4; i++)
            quaternion[i]->Update();
    }

    if (paramOrientationRPY)
    {
        double r, p, y;
        tf::Matrix3x3 m(q);
        m.getRPY(r, p, y);
        angularPosition[0]->SetValue(r*gain);
        angularPosition[1]->SetValue(p*gain);
        angularPosition[2]->SetValue(y*gain);
        for (unsigned char i = 0; i < 3; i++)
            angularPosition[i]->Update();
    }

    if (paramAngularVelocity)
    {
        angularVelocity[0]->SetValue(msg->angular_velocity.x*gain);
        angularVelocity[1]->SetValue(msg->angular_velocity.y*gain);
        angularVelocity[2]->SetValue(msg->angular_velocity.z*gain);
        for (unsigned char i = 0; i < 3; i++)
            angularVelocity[i]->Update();
    }

    if (paramLinearAcceleration)
    {
        linearAcceleration[0]->SetValue(msg->linear_acceleration.x);
        linearAcceleration[1]->SetValue(msg->linear_acceleration.y);
        linearAcceleration[2]->SetValue(msg->linear_acceleration.z);
        for (unsigned char i = 0; i < 3; i++)
            linearAcceleration[i]->Update();
    }
}
