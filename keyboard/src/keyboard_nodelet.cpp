// keyboard_nodelet.cpp



#include <keyboard/keyboard_nodelet.h>



using namespace keyboard;



KeyboardNodelet::KeyboardNodelet()
{
}


void KeyboardNodelet::onInit()
{
    nodeHandle = getNodeHandle();

    char name[100];
    gethostname(name, 100);
    std::string strname = name;
    std::replace(strname.begin(), strname.end(), '-', '/');

    keyboardPub = nodeHandle.advertise<keyboard::KeyboardKeys>(strname + "/keyboard", 5);
}


void KeyboardNodelet::PublishPressedKeys()
{
    keyboard::KeyboardKeys msg;
    std::copy(pressedButtons.begin(), pressedButtons.end(), std::inserter(msg.pressedKeys, msg.pressedKeys.begin()));
    keyboardPub.publish(msg);
}


KeyboardNodelet::~KeyboardNodelet()
{
}
