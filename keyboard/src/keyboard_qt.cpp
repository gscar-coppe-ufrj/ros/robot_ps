// keyboard_qt.cpp



#include <boost/assign/list_of.hpp>

#include <keyboard/keyboard_qt.h>



using std::deque;
using namespace keyboard;



KeyboardQt::KeyboardQt()
{
	// Qt only let you use installEventFilter for classes on the same thread.
	this->moveToThread(QApplication::instance()->thread());
	
	// Register this class to receive Qt events.
	instance = static_cast<QApplication *>(QApplication::instance());
	instance->installEventFilter(this);
	
	// Register the keys we use in deque usedKeyButtons.
	usedKeyButtons = {
		(Qt::Key_Escape),
		(Qt::Key_Tab),
		(Qt::Key_Backspace),
		(Qt::Key_Insert),
		(Qt::Key_Delete),
		(Qt::Key_Home),
		(Qt::Key_End),
		(Qt::Key_Left),
		(Qt::Key_Up),
		(Qt::Key_Right),
		(Qt::Key_Down),
		(Qt::Key_PageUp),
		(Qt::Key_PageDown),
		(Qt::Key_Shift),
		(Qt::Key_Control),
		(Qt::Key_Alt),
		(Qt::Key_AltGr),
		(Qt::Key_CapsLock),
		(Qt::Key_F1),
		(Qt::Key_F2),
		(Qt::Key_F3),
		(Qt::Key_F4),
		(Qt::Key_F5),
		(Qt::Key_F6),
		(Qt::Key_F7),
		(Qt::Key_F8),
		(Qt::Key_F9),
		(Qt::Key_F10),
		(Qt::Key_F11),
		(Qt::Key_F12),
		(Qt::Key_Space),
		(Qt::Key_Apostrophe),
		(Qt::Key_Comma),
		(Qt::Key_Minus),
		(Qt::Key_Slash),
		(Qt::Key_0),
		(Qt::Key_1),
		(Qt::Key_2),
		(Qt::Key_3),
		(Qt::Key_4),
		(Qt::Key_5),
		(Qt::Key_6),
		(Qt::Key_7),
		(Qt::Key_8),
		(Qt::Key_9),
		(Qt::Key_Semicolon),
		(Qt::Key_Equal),
		(Qt::Key_A),
		(Qt::Key_B),
		(Qt::Key_C),
		(Qt::Key_D),
		(Qt::Key_E),
		(Qt::Key_F),
		(Qt::Key_G),
		(Qt::Key_H),
		(Qt::Key_I),
		(Qt::Key_J),
		(Qt::Key_K),
		(Qt::Key_L),
		(Qt::Key_M),
		(Qt::Key_N),
		(Qt::Key_O),
		(Qt::Key_P),
		(Qt::Key_Q),
		(Qt::Key_R),
		(Qt::Key_S),
		(Qt::Key_T),
		(Qt::Key_U),
		(Qt::Key_V),
		(Qt::Key_W),
		(Qt::Key_X),
		(Qt::Key_Y),
		(Qt::Key_Z),
		(Qt::Key_BracketLeft),
		(Qt::Key_Backslash),
		(Qt::Key_BracketRight),
		(Qt::Key_Bar),
		(Qt::Key_AsciiTilde),
		(Qt::Key_division),
		(Qt::Key_Dead_Acute),
		(Qt::Key_Return),
		(Qt::Key_Ccedilla),
		(Qt::Key_Dead_Tilde),
		(Qt::Key_Period),
		(Qt::Key_Asterisk),
		(Qt::Key_NumLock),
		(Qt::Key_Slash),
		(Qt::Key_Asterisk),
		(Qt::Key_Minus),
		(Qt::Key_7),
		(Qt::Key_Home),
		(Qt::Key_8),
		(Qt::Key_Up),
		(Qt::Key_9),
		(Qt::Key_PageUp),
		(Qt::Key_Plus),
		(Qt::Key_4),
		(Qt::Key_Left),
		(Qt::Key_5),
		(Qt::Key_Clear),
		(Qt::Key_6),
		(Qt::Key_Right),
		(Qt::Key_Period),
		(Qt::Key_1),
		(Qt::Key_End),
		(Qt::Key_2),
		(Qt::Key_Down),
		(Qt::Key_3),
		(Qt::Key_PageDown),
		(Qt::Key_0),
		(Qt::Key_Insert),
		(Qt::Key_Delete),
		(Qt::Key_Comma),
		(Qt::Key_Enter)
	}; // usedKeyButtons
}


void KeyboardQt::TreatKeyEvent(int button, bool pressed, int modifier)
{
	deque<int>::iterator buttonit;

	int NUMPAD_FIRSTKEY = 86;
	
	// Try to find this key in the list of used keys.
	if (modifier == Qt::KeypadModifier)
	{
		// The button is in the numpad region.		
		buttonit = std::find(usedKeyButtons.begin() + NUMPAD_FIRSTKEY, 
				usedKeyButtons.end(), button);
	}
	else
	{
		buttonit = std::find(usedKeyButtons.begin(), 
				usedKeyButtons.begin() + NUMPAD_FIRSTKEY, button);
	}

	// We are not tracking this key.
	if (buttonit == usedKeyButtons.end())
		return;

	int buttonid = std::distance(usedKeyButtons.begin(), buttonit);

	// Check if the key is already in the pressedButtons list.
	deque<int>::iterator pressedbuttonit = std::find(pressedButtons.begin(), 
			pressedButtons.end(), buttonid);

	if (pressed == true && pressedbuttonit == pressedButtons.end())
	{
	
	
	    pressedButtons.push_back(buttonid);
		PublishPressedKeys(); // New key pressed, publish the new list.
	}
	else if (pressed == false && pressedbuttonit != pressedButtons.end())
	{
		pressedButtons.erase(pressedbuttonit);
		PublishPressedKeys(); // A key has been released, publish the new list.
	}
}


bool KeyboardQt::eventFilter(QObject* object, QEvent* event)
{
	QEvent::Type type = event->type();
	switch (type)
	{
	case QEvent::KeyPress:
	case QEvent::KeyRelease:
	{
		QKeyEvent* keyEvent = static_cast<QKeyEvent*>(event);
		int button = keyEvent->key();

		// If a key is pressed for some time, it'll fire multiple keypress
		// events. Every event after the first, will have isAutoRepeat() set to
		// true.		
		if (keyEvent->isAutoRepeat() == false)
		{
			bool pressed = (type == QEvent::KeyPress);
	
		    TreatKeyEvent(button, pressed, keyEvent->modifiers());
		}
		break;
	}
	default:
		break;
	}

	// Let Qt handle the event.
    return instance->eventFilter(object, event);
}


KeyboardQt::~KeyboardQt()
{
	// Unregister this class from event filters, to play nice with Qt.
	instance->removeEventFilter(this);
}
