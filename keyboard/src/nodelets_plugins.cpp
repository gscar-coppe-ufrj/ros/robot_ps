// nodelets_plugins.cpp



#include <pluginlib/class_list_macros.h>
#include <keyboard/keyboard_nodelet.h>



// From ROS documentation :
// PLUGINLIB_DECLARE_CLASS(pkg, class_name, class_type, base_class_type)
PLUGINLIB_EXPORT_CLASS(keyboard::KeyboardNodelet, nodelet::Nodelet)

