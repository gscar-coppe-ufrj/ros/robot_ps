/*!
 * \file keyboard_qt.h
 */



#ifndef KEYBOARD_KEYBOARD_QT_H
#define KEYBOARD_KEYBOARD_QT_H



#include <QObject>
#include <QApplication>
#include <QKeyEvent>

#include <keyboard/keyboard.h>



namespace keyboard {



/*!
 * \class KeyboardQt
 * \brief The KeyboardQt class is an implementation of keyboard::Keyboard
 * 		using Qt events. 
 */
class KeyboardQt : public QObject, public Keyboard
{	
	/// Instance for the running QApplication we are.
	QApplication* instance;

public:
	/*!
	 * \brief Initialize default values and register this class to receive
	 * 		Qt events.
	 */
	KeyboardQt();
	
    /*!
     * \brief TreatKeyEvent will treat key events from Qt.
     * \param button 	Button pressed id.
     * \param pressed	True when the button is pressed, false otherwise.
     * \param modifier	Modifier of Qt event.
     */
	virtual void TreatKeyEvent(int button, bool pressed, int modifier);

	/*!
	 * \brief This function is registered to receive Qt events.
	 * \param object 	The object which received the event.
	 * \param event 	The event received.
	 */	 
	bool eventFilter(QObject* object, QEvent* event);

	/*!
	 * \brief ~KeyboardQt unregister this event filter.
	 */	
	virtual ~KeyboardQt();
};



}



#endif 

