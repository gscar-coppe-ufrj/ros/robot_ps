/*!
 * \file keyboard.h
 */



#ifndef KEYBOARD_KEYBOARD_H
#define KEYBOARD_KEYBOARD_H



#include <deque>

#include <robot_ps_core/component.h>



namespace keyboard {



/*!
 * \class Keyboard
 * \brief The keyboard::Keyboard class has as an objective to treat the
 *      inputs from a keyboard.
 */
class Keyboard 
{
protected:
	/// Deque containing the pressed buttons.
	std::deque<int> pressedButtons;
	/// Deque containing just the buttons that are used.
	std::deque<int> usedKeyButtons;

public:
	/*!
     * \brief Keyboard does nothing.
	 */
    Keyboard() {};

    /*!
     * \brief TreatKeyEvent treats an event of a pressed key.
     * \param button Key pressed.
     * \param state States if the key was pressed or not.
     * \param modifier Checks if the key was pressed together with a
     *      modifier, like shift.
     */
	virtual void TreatKeyEvent(int button, bool state, int modifier) = 0;

	/*!
	 * \brief PublishPressedKeys should be called every time 
	 * 		Keyboard::pressedButtons changes.
	 */
	virtual void PublishPressedKeys() = 0;
	
    /*!
     * \brief ~Keyboard does nothing.
     */	
    virtual ~Keyboard() {};
};



}



#endif
