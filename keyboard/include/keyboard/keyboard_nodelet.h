/*!
 * \file keyboard_nodelet.h
 */



#ifndef KEYBOARD_KEYBOARD_NODELET_H
#define KEYBOARD_KEYBOARD_NODELET_H



#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <keyboard/keyboard_qt.h>
#include <keyboard/KeyboardKeys.h>



namespace keyboard {



/*!
 * \class KeyboardNodelet
 * \brief The KeyboardNodelet class makes the connection
 *      between ROS and the keyboard.
 */
class KeyboardNodelet : public KeyboardQt, public nodelet::Nodelet
{
protected:
    /// ROS publisher.
    ros::Publisher keyboardPub;

public:
    /// ROS node handle.
    ros::NodeHandle nodeHandle;

    /*!
     * \brief KeyboardNodelet does nothing.
     */
    KeyboardNodelet();

    /*!
     * \brief onInit sets the node handle, topic name and
     *      ROS advertise.
     */
    void onInit();

    /*!
     * \brief PublishPressedKeys sends a ROS message with the
     *      pressed keyboard keys.
     */
    void PublishPressedKeys();

    /*!
     * \brief ~KeyboardNodelet does nothing.
     */
    ~KeyboardNodelet();
};



}



#endif
