/*!
 * \file namespace.h
 */



#ifndef KEYBOARD_NAMESPACE_H
#define KEYBOARD_NAMESPACE_H



/*!
 * \namespace keyboard
 * \brief This namespace contains classes to provide control of the robots
 *		using the keyboard.
 */
namespace keyboard {
}



#endif 
